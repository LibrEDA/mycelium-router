// Copyright (c) 2020-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Sparse two-dimensional array.

/// 2-dimensional array which is organized in a 2-level hierarchy. The actual tiles which
/// contain data are only if they are written with a value different from the default.
#[derive(Clone)]
pub struct SparseArray2D<T> {
    num_tiles_x: usize,
    num_tiles_y: usize,
    tile_width: usize,
    tile_height: usize,
    /// Default value of the array elements.
    default: T,
    /// Two-level storage for array elements.
    content: Vec<Vec<T>>,
}

impl<T: Clone> SparseArray2D<T> {
    /// Allocate a new 2-dimensional array.
    pub fn new(
        width: usize,
        height: usize,
        tile_width: usize,
        tile_height: usize,
        default: T,
    ) -> Self {
        assert!(tile_width > 0, "Tile cannot have zero size.");
        assert!(tile_height > 0, "Tile cannot have zero size.");

        let l = width * height;

        // Create empty tiles.
        let content = (0..l).into_iter().map(|_| vec![]).collect();

        Self {
            num_tiles_x: width,
            num_tiles_y: height,
            tile_width,
            tile_height,
            default,
            content,
        }
    }

    /// Get the default value of the array elements.
    pub fn default(&self) -> &T {
        &self.default
    }

    /// Compute the index of i,j in a flat array.
    fn flat_index(w: usize, h: usize, i: usize, j: usize) -> usize {
        debug_assert!(i < w, "Index out of bounds.");
        debug_assert!(j < h, "Index out of bounds.");
        i + j * w
    }

    /// Find the index of the tile that holds element i,j.
    fn find_tile_index(&self, i: usize, j: usize) -> (usize, usize) {
        (i / self.tile_width, j / self.tile_height)
    }

    /// Get a reference to the element `(i, j)`.
    pub fn get(&self, i: usize, j: usize) -> &T {
        let (tile_i, tile_j) = self.find_tile_index(i, j);
        let tile_idx = Self::flat_index(self.num_tiles_x, self.num_tiles_y, tile_i, tile_j);
        let tile = &self.content[tile_idx];

        if tile.is_empty() {
            &self.default
        } else {
            let (leaf_i, leaf_j) = (i % self.tile_width, j % self.tile_height);
            let leaf_idx = Self::flat_index(self.tile_width, self.tile_height, leaf_i, leaf_j);
            &tile[leaf_idx]
        }
    }

    /// Get a mutable reference to the element i,j.
    pub fn get_mut(&mut self, i: usize, j: usize) -> &mut T {
        let (tile_i, tile_j) = self.find_tile_index(i, j);
        let tile_idx = Self::flat_index(self.num_tiles_x, self.num_tiles_y, tile_i, tile_j);
        let tile = &mut self.content[tile_idx];

        if tile.is_empty() {
            // Allocate the tile and fill it with default values.
            let l = self.tile_width * self.tile_height;
            tile.reserve(l);
            let default = self.default.clone();
            tile.extend((0..l).map(|_| default.clone()))
        }

        let (leaf_i, leaf_j) = (i % self.tile_width, j % self.tile_height);
        let leaf_idx = Self::flat_index(self.tile_width, self.tile_height, leaf_i, leaf_j);
        &mut tile[leaf_idx]
    }

    /// Set a value at location i,j.
    pub fn set(&mut self, i: usize, j: usize, value: T) {
        *self.get_mut(i, j) = value;
    }
}

#[test]
fn test_array2d() {
    let mut a = SparseArray2D::new(16, 16, 16, 16, false);
    assert_eq!(*a.get(0, 0), false);
    assert_eq!(*a.get(255, 255), false);

    a.set(0, 0, true);
    assert_eq!(*a.get(0, 0), true);
}
