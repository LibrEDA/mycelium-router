// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Point expansion by beam search in four directions.

use iron_shapes::point::Point;
use iron_shapes::rect::Rect;
use iron_shapes::redge::{REdge, REdgeOrientation};
use iron_shapes::simple_rpolygon::SimpleRPolygon;
use iron_shapes::traits::{MapPointwise, RotateOrtho};
use iron_shapes::types::Angle;
use iron_shapes::vector::Vector;
use iron_shapes::CoordinateType;
use itertools::Itertools;
use num_traits::{Num, PrimInt, Signed};
use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::fmt::Formatter;

use super::line_search;
use super::line_search::LineSearchRegion;

/// Holds expansion direction and delta offsets for an expansion point.
/// Directions are required to remember in which directions the point
/// should be expanded (it will be expanded possibly long after it was created).
/// The delta offset is a two-dimensional vector `(dx, dy)` where each coordinate can
/// have a value in `{-1, 0, 1}`. It is used to remember on which side of a line the expansion
/// point lies. This should help to remember if the expansion point is inside or outside a polygon.
/// TODO: Not sure if the delta offset is really needed.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub(crate) struct ExpPointBitmap {
    /// Bitmap for expansion direction and delta offset.
    bitmap: u8,
}

impl ExpPointBitmap {
    pub fn new() -> Self {
        Self { bitmap: 0 }
            .set_delta((0, 0))
            .set_expansion_directions((false, false, false, false))
    }

    /// Enable all expansion directions.
    pub fn new_all_directions() -> Self {
        Self { bitmap: 0 }
            .set_delta((0, 0))
            .set_expansion_directions((true, true, true, true))
    }

    /// Return a integer with bit `n` set to `value`.
    fn bit(n: u8, value: bool) -> u8 {
        debug_assert!(n < 8);
        (value as u8) << n
    }

    /// Set bit `n` to `value`.
    fn set_bit(&self, n: u8, value: bool) -> Self {
        debug_assert!(n < 8);
        Self {
            bitmap: (self.bitmap & !ExpPointBitmap::bit(n, true)) | ExpPointBitmap::bit(n, value),
        }
    }

    /// Get bit `n` from the bitmap.
    fn get_bit(&self, n: u8) -> bool {
        debug_assert!(n < 8);
        self.bitmap & ExpPointBitmap::bit(n, true) != 0
    }

    /// Get enabled directions as a four-tuple `(right, up, left, down)`.
    pub fn get_expansion_directions(&self) -> (bool, bool, bool, bool) {
        (
            self.get_bit(0),
            self.get_bit(1),
            self.get_bit(2),
            self.get_bit(3),
        )
    }

    /// Set enabled directions as a four-tuple `(right, up, left, down)`.
    pub fn set_expansion_directions(&self, directions: (bool, bool, bool, bool)) -> Self {
        let dirs = ExpPointBitmap::bit(3, directions.3)
            | ExpPointBitmap::bit(2, directions.2)
            | ExpPointBitmap::bit(1, directions.1)
            | ExpPointBitmap::bit(0, directions.0);
        debug_assert!(dirs < 16);
        let new = Self {
            bitmap: (self.bitmap & 0b11110000) | dirs,
        };
        debug_assert_eq!(directions, new.get_expansion_directions());
        new
    }

    /// Get `(dx, dy)`, where `dx` and `dy` are one of `{-1, 0, 1}`.
    pub fn get_delta<T: PrimInt>(&self) -> (T, T) {
        let dx = (self.bitmap >> 4) & 0b11;
        let dy = (self.bitmap >> 6) & 0b11;
        (
            T::from(dx).unwrap() - T::one(),
            T::from(dy).unwrap() - T::one(),
        )
    }

    /// Set `(dx, dy)`, where `dx` and `dy` are one of `{-1, 0, 1}`.
    pub fn set_delta<T: PrimInt + Signed>(&self, delta: (T, T)) -> Self {
        let _1 = T::one();
        debug_assert!(delta.0 <= _1 && delta.0 >= T::zero() - _1);
        debug_assert!(delta.1 <= _1 && delta.1 >= T::zero() - _1);
        let dx = (delta.0 + _1).to_u8().unwrap();
        let dy = (delta.1 + _1).to_u8().unwrap();
        debug_assert!(dx <= 2);
        debug_assert!(dy <= 2);

        Self {
            bitmap: (self.bitmap & 0b1111) | (dx << 4) | (dy << 6),
        }
    }

    /// Rotate by a multiple of 90 degrees around `(0, 0)`.
    pub fn rotate_ortho(&self, angle: Angle) -> Self {
        let (a, b, c, d) = self.get_expansion_directions();
        let (dx, dy) = self.get_delta::<i8>();

        debug_assert!(dx >= -1 && dx <= 1);
        debug_assert!(dy >= -1 && dy <= 1);
        let (dir, delta) = match angle {
            Angle::R0 => ((a, b, c, d), (dx, dy)),
            Angle::R90 => ((d, a, b, c), (-dy, dx)),
            Angle::R180 => ((c, d, a, b), (-dx, -dy)),
            Angle::R270 => ((b, c, d, a), (dy, -dx)),
        };

        let new = Self::new().set_delta(delta).set_expansion_directions(dir);

        new
    }

    /// Visualize the expansion directions as Unicode arrow chars.
    pub fn visualize_expansion_directions(&self) -> String {
        let (r, u, l, d) = self.get_expansion_directions();
        let horizontal = match (r, l) {
            (false, false) => "",
            (true, false) => "→",
            (false, true) => "←",
            (true, true) => "↔",
        };
        let vertical = match (u, d) {
            (false, false) => "",
            (true, false) => "↑",
            (false, true) => "↓",
            (true, true) => "↕",
        };
        format!("{}{}", horizontal, vertical)
    }
}

#[test]
fn test_expansion_point_bitmap() {
    assert_eq!(ExpPointBitmap::new().set_delta((0, 0)).bitmap, 0b01010000);
    assert_eq!(ExpPointBitmap::new().set_delta((1, 1)).bitmap, 0b10100000);
    assert_eq!(ExpPointBitmap::new().set_delta((-1, -1)).bitmap, 0b00000000);

    assert_eq!(ExpPointBitmap::new().set_delta((1, 0)).get_delta(), (1, 0));
    assert_eq!(ExpPointBitmap::new().set_delta((0, 1)).get_delta(), (0, 1));
    assert_eq!(
        ExpPointBitmap::new().set_delta((-1, 1)).get_delta(),
        (-1, 1)
    );
    assert_eq!(
        ExpPointBitmap::new().set_delta((1, -1)).get_delta(),
        (1, -1)
    );
    assert_eq!(
        ExpPointBitmap::new()
            .set_expansion_directions((false, false, false, false))
            .get_expansion_directions(),
        (false, false, false, false)
    );
    assert_eq!(
        ExpPointBitmap::new()
            .set_expansion_directions((true, true, false, false))
            .get_expansion_directions(),
        (true, true, false, false)
    );
    assert_eq!(
        ExpPointBitmap::new()
            .set_expansion_directions((false, true, true, false))
            .get_expansion_directions(),
        (false, true, true, false)
    );
    assert_eq!(
        ExpPointBitmap::new()
            .set_expansion_directions((true, false, false, true))
            .get_expansion_directions(),
        (true, false, false, true)
    );
}

#[test]
fn test_expansion_point_rotate() {
    let exp = ExpPointBitmap::new().set_expansion_directions((false, true, false, false));
    assert_eq!(
        exp.rotate_ortho(Angle::R180),
        ExpPointBitmap::new().set_expansion_directions((false, false, false, true))
    );
    assert_eq!(
        exp.rotate_ortho(Angle::R90),
        ExpPointBitmap::new().set_expansion_directions((false, false, true, false))
    );
    assert_eq!(
        exp.rotate_ortho(Angle::R270),
        ExpPointBitmap::new().set_expansion_directions((true, false, false, false))
    );
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub(crate) struct ExpansionPoint<T: PrimInt> {
    /// A 3D point as `((x, y), layer)`.
    pub(crate) point: (Point<T>, u8),
    /// Predecessor of this expansion point.
    /// A 3D point as `((x, y), layer)`.
    pub(crate) origin: (Point<T>, u8),
    /// Index of the terminal that belongs to this expansion.
    pub(crate) terminal_id: usize,
    /// Accumulated cost from the source terminal to this expansion point.
    pub(crate) cost: T,
    /// A lower bound for the remaining cost to the closest target.
    /// This is used to compute the priority of the expansion point
    /// following the A-Star routing algorithm.
    pub(crate) cost_to_target_lower_bound: T,
    /// Expansion directions and delta offset.
    pub(crate) details: ExpPointBitmap,
    /// If the expansion of a target is hit this field
    /// is set to (target ID, target edge).
    /// The target ID is just the index of the target and must be translated according to the context.
    /// The target edge is the edge which was hit.
    pub(crate) target_hit: Option<(usize, REdge<T>)>,
}

impl<T: PrimInt + std::fmt::Display> std::fmt::Display for ExpansionPoint<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} -> {}, {}, delta = {:?}",
            self.origin.0,
            self.point.0,
            self.details.visualize_expansion_directions(),
            self.details.get_delta::<i8>()
        )
    }
}

impl<T: PrimInt> ExpansionPoint<T> {
    fn expansion_point(&self) -> (Point<T>, u8) {
        self.point
    }

    fn origin(&self) -> (Point<T>, u8) {
        self.origin
    }

    fn rotate_ortho(&self, angle: Angle) -> Self {
        ExpansionPoint {
            point: (self.point.0.rotate_ortho(angle), self.point.1),
            origin: (self.origin.0.rotate_ortho(angle), self.origin.1),
            details: self.details.rotate_ortho(angle),
            ..*self
        }
    }
}

impl<T: PrimInt> Ord for ExpansionPoint<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        // Change order, closest points should come first in the max heap.
        (other.cost + other.cost_to_target_lower_bound)
            .cmp(&(self.cost + self.cost_to_target_lower_bound))
    }
}

impl<T: PrimInt> PartialOrd for ExpansionPoint<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// Expand a expansion point in a region.
///
/// Search in all enabled directions until an obstacle is hit. There
/// a new expansion point is created with its directions set according to the obstacle.
///
/// # Parameters
///
/// * `obstacles`: Obstacles that should not be crossed.
/// * `targets`: Path skeletons of the routing targets.
/// * `guides`: A guide triggers an expansion but is neither obstacle nor target.
/// Guides server the purpose of detecting objects on other layers.
/// Guides can also be used to find a tunnel (hole) in a metal shape on an adjacent layer.
/// The tunnel must be marked with guides that expand from all polygon corners.
/// * `exp`: The start of the expansion.
/// * `half_beam_width`: The half width of the beam used for the search.
pub(crate) fn expand<T: PrimInt + Signed>(
    obstacles: &LineSearchRegion<T>,
    targets: &[&LineSearchRegion<T>],
    guides: &[&LineSearchRegion<T>],
    exp: &ExpansionPoint<T>,
    half_beam_width: T,
    wire_weight_horizontal: T,
    wire_weight_vertical: T,
) -> Vec<ExpansionPoint<T>> {
    // Find location of expansion point.
    let (xy, _) = exp.point;
    // Find expansion directions.
    let (go_east, go_north, go_west, go_south) = exp.details.get_expansion_directions();

    // Container for new expansion points.
    let mut result = Vec::new();

    // Compute angles and their inverses for all enabled expansion directions.
    let angles: Vec<Angle> = [go_east, go_north, go_west, go_south]
        .iter()
        .zip(0..4)
        .filter(|&(enable, _angle)| *enable)
        .map(|(_enable, angle)| Angle::from_u32(angle as u32))
        .collect_vec();

    for angle in angles {
        // Normalize such that expansion direction points to north.
        let rot_to_north = Angle::R90 - angle;

        // Vector pointing into the direction of expansion.
        let direction_vector = Vector::new(half_beam_width, T::zero()).rotate_ortho(angle);

        // Orientation of the expansion direction.
        let beam_orientation = match angle {
            Angle::R0 | Angle::R180 => REdgeOrientation::Horizontal,
            _ => REdgeOrientation::Vertical,
        };

        // Get cost of this wire per unit distance.
        let wire_weight = match beam_orientation {
            REdgeOrientation::Horizontal => wire_weight_horizontal,
            REdgeOrientation::Vertical => wire_weight_vertical,
        };

        let back = xy - direction_vector;
        let front = xy + direction_vector;

        let num_targets = targets.len();
        let search_regions = [&[obstacles], targets, guides].concat();

        // Expand in y direction from the front.
        let (from_front_prev, from_front_hit, from_front_next) =
            line_search::multi_closest_beam_intersection(
                &search_regions,
                front,
                beam_orientation,
                half_beam_width - T::one(),
            );

        // Select the right expansion direction.
        let from_front = match angle {
            Angle::R0 | Angle::R90 => from_front_next,
            _ => from_front_prev,
        };

        let from_front = if from_front_hit.is_empty() {
            from_front
        } else {
            from_front_hit
        };

        // There should always be a hit. At least at the core boundary.
        debug_assert!(!from_front.is_empty());

        // Check if we hit an obstacle.
        let obstacle_hit = from_front
            .iter()
            .find(|(_e, region_id)| *region_id == 0)
            .is_some();

        // Check if we hit an other target.
        // Targets have higher priority than obstacles (a target overwrites an obstacle).
        // This makes it possible to use the same data structure of routed nets as obstacles
        // while allowing to re-define some of the shapes as targets when they belong
        // to the net that is currently routed.
        let target_hit = from_front
            .iter()
            .find(|(_e, region_id)| 1 <= *region_id && *region_id < num_targets + 1);

        // Check if we hit a guide.
        let guide_hit = from_front
            .iter()
            .find(|(_e, region_id)| *region_id >= num_targets + 1)
            .is_some();

        debug_assert!(obstacle_hit || target_hit.is_some() || guide_hit); // Must be one of the types.

        if let Some(&(target_edge, target_id)) = target_hit {
            // Process target hits.

            // Obstacle region was prepended. Make sure first target has ID 0.
            let target_id = target_id - 1;

            debug_assert_eq!(target_edge.orientation, beam_orientation.other());

            // Find the point where the target was hit.
            // I.e. the intersection with the target edge.
            let hit_point = match target_edge.orientation {
                REdgeOrientation::Horizontal => Point::new(exp.point.0.x, target_edge.offset),
                REdgeOrientation::Vertical => Point::new(target_edge.offset, exp.point.0.y),
            };

            // Find the distance to the target.
            let dist = (hit_point - exp.point.0).norm1();
            let delta_cost = dist * wire_weight;

            // Create an expansion point which indicates a hit.
            let layer = exp.point.1;
            result.push(ExpansionPoint {
                point: (hit_point, layer),
                origin: exp.point,
                cost: exp.cost + delta_cost,
                cost_to_target_lower_bound: T::zero(),
                details: ExpPointBitmap::new()
                    // Continue the expansion straight ahead.
                    .set_expansion_directions((false, true, false, false))
                    .rotate_ortho(-rot_to_north),
                terminal_id: exp.terminal_id,
                target_hit: Some((target_id, target_edge)),
            });
        } else {
            // No target was hit, just obstacles or guides.

            // Shift intersecting edges towards the center point of the beam end.
            // This simulates bloating of the obstacles in order to ensure correct minimal distance to them.
            let from_front_normalized = from_front
                .iter()
                .map(|&(e, region_id)| {
                    let mut e = e.rotate_ortho(rot_to_north);
                    e.offset = e.offset - half_beam_width;
                    e
                })
                .collect_vec();

            // Expand in y direction from the back.
            let (from_back_prev, _, from_back_next) =
                obstacles.closest_beam_intersection(back, beam_orientation, half_beam_width);

            // Select the right expansion direction.
            let from_back = match angle {
                Angle::R0 | Angle::R90 => from_back_next,
                _ => from_back_prev,
            };

            // Shift intersecting edges towards the center point of the beam end.
            // This simulates bloating of the obstacles in order to ensure correct minimal distance to them.
            let from_back_normalized = from_back
                .iter()
                .map(|&e| {
                    let mut e = e.rotate_ortho(rot_to_north);
                    e.offset = e.offset + half_beam_width;
                    e
                })
                .collect_vec();

            // Select the hits that where found first.
            // If there is a tie, the hits from the front have priority.
            let (hits_normalized, is_front_hit) = match (
                from_back_normalized.is_empty(),
                from_front_normalized.is_empty(),
            ) {
                (true, true) => (vec![], false),
                (true, false) => (from_front_normalized, true),
                (false, true) => (from_back_normalized, false),
                (false, false) => {
                    // Take the hit which is closer.
                    if from_back_normalized[0].offset < from_front_normalized[0].offset {
                        // Can do this comparison because the expansion direction is normalized towards north.
                        (from_back_normalized, false)
                    } else {
                        (from_front_normalized, true)
                    }
                }
            };

            let exp_normalized = exp.rotate_ortho(rot_to_north);

            let expand_north = !is_front_hit;
            let next_normalized = next_expansion_point_north(
                &exp_normalized,
                &hits_normalized,
                expand_north,
                wire_weight,
            );

            let next_normalized = if !obstacle_hit {
                // No obstacle was hit, only a guide.
                debug_assert!(guide_hit);
                // println!("Guide hit.");

                // A guide triggers an expansion in all directions except the incident direction.
                next_normalized.map(|mut n| {
                    n.details = n
                        .details
                        .set_expansion_directions((true, true, true, false));
                    // Advance a tiny step towards north to jump over the guide.
                    let (dx, _) = n.details.get_delta();
                    n.details = n.details.set_delta((dx, 1));
                    let (p, layer) = n.point;
                    n.point = (Point::new(p.x, p.y + half_beam_width), layer);
                    n
                })
            } else {
                next_normalized
            };

            // Rotate back to original expansion direction.
            let next = next_normalized
                .iter()
                .map(|n| n.rotate_ortho(-rot_to_north));

            result.extend(next);
        }
    }

    result
}

/// Find next expansion point when going north from `prev`. The closest edge hits must be given in `hits`.
/// * `prev`: Previous expansion point.
/// * `wire_weight`: Factor for computing the cost of a wire ( `cost = length * wire_weight` ).
fn next_expansion_point_north<T: PrimInt + Signed>(
    prev: &ExpansionPoint<T>,
    hits: &Vec<REdge<T>>,
    expand_north: bool, // TODO: Still needed?
    wire_weight: T,
) -> Option<ExpansionPoint<T>> {
    let (_, go_north, _, _) = prev.details.get_expansion_directions();
    debug_assert!(go_north);

    if hits.is_empty() {
        None
    } else {
        let (xy, _) = prev.point;

        let (dx, _): (i8, i8) = prev.details.get_delta();

        // Test if the edge was hit at start or end or somewhere in the middle.
        let is_tangent_left = hits
            .iter()
            .all(|e| xy.x <= e.start().x && xy.x <= e.end().x)
            && dx != 1;
        let is_tangent_right = hits
            .iter()
            .all(|e| xy.x >= e.start().x && xy.x >= e.end().x)
            && dx != -1;
        let is_tangent = is_tangent_right || is_tangent_left;

        // Compute the next expansion directions.
        // Can only expand towards north when the edge was touched on the start or on the end.
        // let expand_north = is_tangent;
        let expand_west = !(dx == -1 && is_tangent_left);
        let expand_east = !(dx == 1 && is_tangent_right);
        // Don't bounce back where we came from.
        let expand_south = false;

        // Compute new `dy`.
        // When we hit a horizontal line and cannot expand towards north
        // we have to stay on this southern side of the line, therefore `dy = -1`.
        let dy_new = if is_tangent { 1 } else { -1 };

        let details = ExpPointBitmap::new()
            .set_expansion_directions((expand_east, expand_north, expand_west, expand_south))
            .set_delta((dx, dy_new));

        let p: Point<T> = Point::new(xy.x, hits[0].offset);

        // Compute distance to previous expansion point.
        let dist = (xy.y - p.y).abs();
        let delta_cost = dist * wire_weight;

        Some(ExpansionPoint {
            point: (p, prev.point.1),
            origin: prev.point,
            cost: prev.cost + delta_cost,
            cost_to_target_lower_bound: T::zero(),
            details,
            terminal_id: prev.terminal_id,
            target_hit: None,
        })
    }
}
