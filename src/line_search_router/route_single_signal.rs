// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Basic algorithm for connecting terminals of a single signal.

use itertools::Itertools;
use num_traits::{PrimInt, Signed};
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::hash::Hash;

use iron_shapes::prelude::*;

use log;

use crate::visualize::TermPlot;

use super::expansion;
use super::expansion::*;
use super::line_search::LineSearchRegion;

const MAX_PARTIAL_EDGES_PER_BUCKET: usize = 256;

/// Do a line search in multiple line search regions.
///
pub struct MultiLineSearchRegion<'a, T: PrimInt> {
    same_signal: &'a LineSearchRegion<T>,
    targets: Vec<&'a LineSearchRegion<T>>,
    obstacles: Vec<&'a LineSearchRegion<T>>,
}

/// Multi layers of line-search regions.
pub struct LineSearchLayerStack<T: PrimInt> {
    /// Layers of search regions.
    pub layers: Vec<LineSearchRegion<T>>,
}

impl<T: PrimInt> LineSearchLayerStack<T> {
    /// Create an empty layer stack with a given number of layers.
    pub fn new(num_layers: usize) -> Self {
        LineSearchLayerStack {
            layers: (0..num_layers)
                .map(|_| LineSearchRegion::new(MAX_PARTIAL_EDGES_PER_BUCKET))
                .collect(),
        }
    }
}

/// Find a route between the two closest terminals of a single signal.
///
/// If a path is found returns
/// (terminal_id 1, terminal_id 2, path) where `terminal_id` are the indices
/// of the both terminals that were connected.
/// otherwise returns `None`.
///
/// # Parameters
///
/// * `terminals`: Shapes of the routing terminals in a form like: `[[pin1-shape1, pin1-shape2, ...], [pin2-shape1, ...], ...]`
/// All shapes of a pin are assumed to be connected already. Pins are assumed to be not connected.
/// * `obstacle_layers`: Routing blockages.
/// * `wire_weights`: horizontal and vertical wire weight for each layer.
pub fn route_single_signal_closest_terminals<T>(
    terminals: &Vec<Vec<(SimpleRPolygon<T>, u8)>>,
    obstacle_layers: &LineSearchLayerStack<T>,
    num_layers: u8,
    wire_half_width: T,
    min_spacing: T,
    wire_weights: &Vec<(T, T)>,
    debug: bool,
) -> Option<(usize, usize, Vec<(Point<T>, u8)>)>
where
    T: CoordinateType + PrimInt + Signed + std::fmt::Display + std::fmt::Debug + Hash,
{
    assert_eq!(
        wire_weights.len(),
        num_layers as usize,
        "Need to declare wire weights for each layer."
    );

    // Define shortcuts for constants.
    let _0 = T::zero();
    let _1 = T::one();
    let _2 = _1 + _1;

    assert!(min_spacing >= _0, "Minimal spacing cannot be negative.");
    assert!(wire_half_width >= _0, "Wire width cannot be negative.");

    let half_beam_width = wire_half_width + min_spacing;

    // TODO: Take from parameters.
    // Cost of a via.
    let via_cost = T::from(100).unwrap();

    // Convert terminal shapes into locations of expansion points.
    // (This can be corner points, or edge centers for example.)
    let terminal_points =
        create_initial_expansion_points(obstacle_layers, terminals, half_beam_width);

    let visualize = debug;
    // Visualize the obstacles
    let mut plt = TermPlot::new();
    if visualize {
        for layer in &obstacle_layers.layers {
            layer.edges().for_each(|e| plt.draw_edge(0, e))
        }
        println!("Obstacles:");
        plt.print();
    }

    // Create containers for expansions of each terminal.
    let mut terminal_expansions = (0..terminal_points.len())
        .map(|_| LineSearchLayerStack::new(num_layers as usize))
        .collect_vec();

    // Set of visited points together with their predecessors.
    let mut trace: HashMap<_, _> = HashMap::new();

    // A-star heuristic.
    // Compute a lower bound of the cost from the point `p` to the closest of the
    // targets. Targets are all terminals with another ID.
    // TODO: Targets can be only points now. Extend this to support edges as targets.
    // TODO: Take wire weights into account.
    let cost_lower_bound_to_target = |terminal_id: usize, p: Point<T>, layer: u8| -> T {
        terminal_points
            .iter()
            .enumerate()
            .filter(|(i, ts)| *i != terminal_id)
            .flat_map(|(_, ts)| ts.iter())
            // Compute the lower bound as the rectilinear distance
            // to the terminals.
            .map(|&(point2, layer2, _dirs)| {
                (point2 - p).norm1()
                    + (T::from(layer as i32 - layer2 as i32).unwrap().abs()) * via_cost
            })
            // Find the lowest bound.
            .min()
            // Return 0 if no bound could be found.
            .unwrap_or(_0)
    };

    // The moment a path is found the expansions of the two terminals touch
    // roughly in the middle.
    // This HashMap stores for each target hit the corresponding expansion point
    // of the other terminal. This allows find the shortest path by tracing back
    // along the two halves.
    let mut back_tracing_centers = HashMap::new();

    // Visited expansion points together with their cost.
    let mut visited = HashMap::new();

    // Create initial expansion points.
    log::debug!("Create initial expansion points."); // TODO remove
    let mut front = BinaryHeap::new();
    for (i, ts) in terminal_points.iter().enumerate() {
        for (p, layer, exp_dirs) in ts {
            let layer = *layer;
            if obstacle_layers.layers[layer as usize]
                .contains_point(p.x, p.y)
                .is_no()
            {
                // Skip expansion points that lie within an obstacle.

                front.push(ExpansionPoint {
                    point: (*p, layer),
                    origin: (*p, layer),
                    terminal_id: i,
                    cost: _0,
                    cost_to_target_lower_bound: _0,
                    details: ExpPointBitmap::new()
                        .set_expansion_directions(*exp_dirs)
                        .set_delta((0, 0)),
                    target_hit: None,
                })
            }
        }
    }

    // Count how many expansion points have been processed.
    let mut expansion_point_counter = 0usize;

    while let Some(exp) = front.pop() {
        expansion_point_counter += 1;
        // if obstacle_layers.layers[exp.point.1 as usize].contains_point(exp.point.0.x, exp.point.0.y)
        //     .inclusive_bounds() {
        //     // Skip expansion points that are within obstacles.
        //     continue;
        // }

        // debug_assert!(
        //     !obstacle_layers.layers[exp.point.1].contains_point(exp.point.0.x, exp.point.0.y)
        //         .is_within_bounds(),
        //     "Expansion point should not be within an obstacle."
        // );

        if !obstacle_layers.layers[exp.point.1 as usize]
            .contains_point(exp.point.0.x, exp.point.0.y)
            .is_no()
        {
            log::warn!(
                "Expansion point should not be touching an obstacle: {}",
                exp.point.0
            );
            if debug {
                println!(
                    "Expansion point should not be touching an obstacle: {}",
                    exp.point.0
                );
                assert!(false);
            }
            // Skip it.
            // return None; // Break the search for debugging.
            continue;
        }

        if exp.target_hit.is_some() {
            // The two search expansions touch each other in the `hit` point.
            // A path is found.

            let result = assemble_paths(&exp, &trace, &back_tracing_centers, debug, visualize);

            log::info!("Processed expansion points: {}", expansion_point_counter);

            // Plot the path.
            if visualize {
                plt.clear();

                // Draw obstacles.
                for layer in &obstacle_layers.layers {
                    layer.edges().for_each(|e| plt.draw_edge(0, e))
                }

                let (_, _, path) = &result;

                path.windows(2).for_each(|w| {
                    let p1 = w[0].0;
                    let p2 = w[1].0;
                    let e = REdge::new(p1, p2);
                    plt.draw_edge(0, e);
                });

                println!("Resulting path:");
                plt.print();
            }

            return Some(result);
        };

        // Skip expansion points that have been already processed.
        if let Some(&cost) = visited.get(&(exp.terminal_id, exp.point)) {
            if cost <= exp.cost {
                // Skip the expansion point if it does not bring an improvement over
                // the one we already processed.
                continue;
            }
        }

        // Create a list of the other terminals

        if debug {
            println!(
                "Expand {}. Lower bound on cost: {}.",
                exp,
                exp.cost_to_target_lower_bound + exp.cost
            );
            println!("Layer {}", exp.point.1);
        }

        let (xy, layer) = exp.point;

        // Create a list of all terminals on the current layer.
        let terminals_same_layer = terminal_expansions
            .iter()
            .enumerate()
            .map(|(_, t)| &t.layers[layer as usize])
            .collect_vec();

        // Find obstacle regions on all other layers.
        let obstacles_other_layers = obstacle_layers
            .layers
            .iter()
            .enumerate()
            .filter(|(i, _)| *i != layer as usize)
            .map(|t| t.1);

        // Find terminal expansion regions on all other layers.
        let terminals_other_layers = terminal_expansions.iter().flat_map(|t| {
            t.layers
                .iter()
                .enumerate()
                .filter_map(|(i, l)| if i != layer as usize { Some(l) } else { None })
        });

        // Use obstacles and terminal expansions on other layers as guides.
        // This allows to trigger an expansion and change layers when necessary.
        // let guides = obstacles_other_layers
        //     .chain(terminals_other_layers)
        //     .collect_vec();
        let guides = terminals_other_layers.collect_vec();

        // let dirs = exp.details.get_expansion_directions();
        //
        // // Disable expansion directions that are not the preferred routing directions.
        // let dirs = if layer % 2 != 0 {
        //     // Horizontal.
        //     (dirs.0, false, dirs.2, false)
        // } else {
        //     // Vertical
        //     (false, dirs.1, false, dirs.3)
        // };
        //
        // let exp = ExpansionPoint {
        //     details: exp.details.set_expansion_directions(dirs),
        //     ..exp
        // };

        // Set wire weight in order to prefer one routing direction.
        let (wire_weight_horizontal, wire_weight_vertical) = wire_weights[layer as usize];

        // Find next expansion points.
        let next = expansion::expand(
            &obstacle_layers.layers[layer as usize],
            terminals_same_layer.as_slice(),
            guides.as_slice(),
            &exp,
            half_beam_width,
            wire_weight_horizontal,
            wire_weight_vertical,
        );

        // A-star heuristic:
        // Find proper lower bounds on the remaining cost to a target.
        let next = next.into_iter().map(|mut n| {
            n.cost_to_target_lower_bound =
                cost_lower_bound_to_target(n.terminal_id, n.point.0, n.point.1);
            n
        });

        // Set correct priority for target hits.
        // The lower bound can now be given exactly.
        let next = next
            .filter_map(|mut n| {
                let layer = n.point.1;
                // If this expansion point hits a target the lower bound of the cost
                // to the target can be given exactly.
                if let Some((target_id, edge)) = n.target_hit {
                    // Find the expansion point that created the found edge.
                    let exp_trace_back: ExpansionPoint<T> =
                        [(edge.start(), layer), (edge.end(), layer)]
                            .iter()
                            .flat_map(|p| trace.get(&(target_id, *p)))
                            .cloned()
                            // Find the first if there's more than one.
                            .min_by_key(|e: &ExpansionPoint<T>| e.cost)
                            // At least one end of the edge should have been inserted into `trace`.
                            .unwrap();

                    // Compute the cost from the hit point to the closest expansion point
                    // of the other terminal.
                    let v: Vector<T> = n.point.0 - exp_trace_back.point.0;
                    let dist_to_other_exp_point = v.norm1();
                    // Get correct wire weight.
                    let layer = n.point.1;
                    assert_eq!(
                        n.point.1, exp_trace_back.point.1,
                        "Both points must be on the same layer."
                    );
                    let wire_weight = if v.y == T::zero() {
                        // Horizontal.
                        wire_weights[layer as usize].0
                    } else {
                        // Vertical.
                        wire_weights[layer as usize].1
                    };

                    let cost_to_other_exp_point = dist_to_other_exp_point * wire_weight;

                    if target_id == exp.terminal_id {
                        // Self-hit.

                        // Do not treat this as a target hit.
                        n.target_hit = None;

                        // If a short cut has been found and modify the expansion point accordingly.

                        // Get the original cost of the hit point. (As it was found from the
                        // existing expansion that was just hit).
                        let cost_found = exp_trace_back.cost + cost_to_other_exp_point;

                        if cost_found < n.cost {
                            // Found a shortcut.
                            // Expand from the hit point into the direction we just came from.

                            if debug {
                                println!(
                                    "Shortcut found: {} {}",
                                    n.point.0, exp_trace_back.point.0
                                );
                            }

                            // Update the origin and cost such that the found shortcut is used.
                            n.origin = exp_trace_back.point;
                            n.cost = cost_found;

                            // Turn the expansion direction by 180 degrees.
                            n.details = n.details.set_expansion_directions(
                                n.details
                                    .rotate_ortho(Angle::R180)
                                    .get_expansion_directions(),
                            );
                        } else {
                            // Not a shortcut. Do not consider this as a hit.
                            // Do not process the self-hit any further.
                            return None;
                        }
                    } else {
                        debug_assert_eq!(
                            exp_trace_back.point.1, exp.point.1,
                            "Must be on the same layer."
                        );
                        // Compute total remaining cost to the other target.
                        // Add the missing piece between this two expansions.
                        let cost_to_target = exp_trace_back.cost + cost_to_other_exp_point;

                        n.cost_to_target_lower_bound = cost_to_target;

                        if debug {
                            println!(
                                "Potential result hit at: {} {}",
                                exp_trace_back.point.0, n.point.0
                            );
                            println!("Cost: {}", cost_to_target + n.cost);
                        }

                        // Remember the other expansion point for later back tracing.
                        let _prev =
                            back_tracing_centers.insert((n.terminal_id, n.point), exp_trace_back);
                        // debug_assert!(prev.is_none(), "Should not overwrite existing back-tracing centers.");
                    }
                }
                Some(n)
            })
            .collect_vec();

        // Draw extension traces.
        for n in &next {
            let layer1 = exp.point.1;
            let layer2 = n.point.1;
            if debug {
                println!("\t-> {}", n);
            }
            let edge = REdge::new(xy, n.point.0);
            if layer1 == layer2 {
                if edge.start != edge.end {
                    terminal_expansions[n.terminal_id].layers[layer as usize]
                        .insert_edge(edge, false);
                }
            } else {
                println!("Layer crossing not implemented yet.")
            }
            if visualize {
                plt.draw_edge(0, edge);
            }
        }

        front.extend(next);

        // Expand to adjacent layers.
        let mut try_expand_to_layer = |other_layer: u8| {
            // Test if there is an obstacle on the other layer.
            let can_expand = !obstacle_layers.layers[other_layer as usize].overlaps_rect(
                exp.point.0,
                half_beam_width,
                half_beam_width,
            );
            if can_expand {
                front.push(ExpansionPoint {
                    point: (exp.point.0, other_layer),
                    origin: exp.point,
                    terminal_id: exp.terminal_id,
                    cost: exp.cost + via_cost,
                    cost_to_target_lower_bound: exp.cost_to_target_lower_bound,
                    details: exp
                        .details
                        .set_expansion_directions((true, true, true, true)),
                    target_hit: None,
                })
            }
        };

        if layer > 0 {
            // Expand to layer below.
            try_expand_to_layer(layer - 1)
        }

        if layer < num_layers - 1 {
            // Expand to layer above.
            try_expand_to_layer(layer + 1)
        }

        visited.insert((exp.terminal_id, exp.point), exp.cost);

        trace.insert((exp.terminal_id, exp.point), exp);

        if visualize {
            println!("Current state:");
            plt.set_char(1, xy, 'X'); // Mark the current point.
            plt.print();
            plt.set_char(1, xy, '+'); // Mark the old point.
        }
    }

    log::info!("Processed expansion points: {}", expansion_point_counter);

    // No path was found.
    log::debug!("No path found.");
    None
}

/// Find routes between all terminals of a single signal.
pub fn route_single_signal<T>(
    terminals: &Vec<Vec<(SimpleRPolygon<T>, u8)>>,
    obstacle_layers: &LineSearchLayerStack<T>,
    num_layers: u8,
    half_wire_width: T,
    min_spacing: T,
    wire_weights: &Vec<(T, T)>,
    debug: bool,
) -> Option<(Vec<(u8, PointString<T>)>, Vec<(u8, Rect<T>)>)>
where
    T: CoordinateType + PrimInt + Signed + std::fmt::Display + std::fmt::Debug + Hash,
{
    assert_eq!(wire_weights.len() as u8, num_layers);

    // Create mutable copies.
    let mut terminals = terminals.clone();

    // Resulting routes.
    let mut result_paths = Vec::new();
    let mut result_shapes = Vec::new();

    // Subsequently connect the two closest terminals until all are connected.
    while terminals.len() > 1 {
        // No routing is necessary if there's one or less terminals.

        let result = route_single_signal_closest_terminals(
            &terminals,
            obstacle_layers,
            num_layers,
            half_wire_width,
            min_spacing,
            wire_weights,
            debug,
        );

        if let Some((term1, term2, path)) = result {
            // `term1` and `term2` get connected by `path`.

            // Create geometric shapes of the new path.
            // Split the new path onto layers.
            let new_paths: Vec<(u8, PointString<_>)> = path
                .iter()
                // Create path segments that are on a single layer only.
                .group_by(|(point, layer)| *layer)
                .into_iter()
                // Create (layer, path) pairs.
                .map(|(layer, path_segment)| {
                    let points = path_segment.map(|(point, _layer)| point).dedup();
                    let path_segment = PointString::new(points);
                    (layer, path_segment)
                })
                .collect();

            // Remember the path between this terminals.
            result_paths.extend(new_paths.iter().cloned());

            // Create rectangles from the path.
            let mut new_terminal = Vec::new();
            for (layer, path) in new_paths {
                for e in path.edges() {
                    debug_assert!(
                        !e.is_degenerate(),
                        "Routing edge must have non-zero length."
                    );
                    debug_assert!(
                        e.is_rectilinear(),
                        "Routing edge must be horizontal or vertical."
                    );

                    // Create rectangle shape from this edge.
                    let (lower_left, upper_right) = if e.is_horizontal() {
                        let w = Vector::new(half_wire_width, half_wire_width);
                        if e.start.x < e.end.x {
                            (e.start - w, e.end + w)
                        } else {
                            (e.end - w, e.start + w)
                        }
                    } else {
                        debug_assert!(e.is_vertical());
                        let w = Vector::new(half_wire_width, half_wire_width);
                        if e.start.y < e.end.y {
                            (e.start - w, e.end + w)
                        } else {
                            (e.end - w, e.start + w)
                        }
                    };
                    let rect = Rect::new(lower_left, upper_right);
                    debug_assert!(
                        rect.width() == half_wire_width + half_wire_width
                            || rect.height() == half_wire_width + half_wire_width
                    );

                    // obstacles.push((rect.into(), layer)); // TODO: Was this necessary?
                    new_terminal.push((rect.into(), layer));
                    result_shapes.push((layer, rect));
                }
            }

            // Merge existing terminal shapes.
            new_terminal.extend(terminals[term1].iter().cloned());
            new_terminal.extend(terminals[term2].iter().cloned());

            let new_terminals = terminals
                .into_iter()
                .enumerate()
                .filter_map(|(i, t)| {
                    if i == term1 {
                        Some(new_terminal.clone()) // Replace the old terminal shapes with the new ones.
                    } else if i == term2 {
                        None
                    } else {
                        Some(t)
                    }
                })
                .collect_vec();

            terminals = new_terminals
        } else {
            println!("Failed to route.");
            log::warn!("Failed to route.");

            return None;
        }
    }

    Some((result_paths, result_shapes))
}

/// Convert shapes of the terminals of a single net into expansion points.
///
/// # Parameters
/// * `obstacle_layers`: Used to check if an expansion point does not conflict with obstacles.
/// * `half_beam_width`: `(with of routing path) / 2 + minimum spacing
fn create_initial_expansion_points<T>(
    obstacle_layers: &LineSearchLayerStack<T>,
    terminals: &Vec<Vec<(SimpleRPolygon<T>, u8)>>,
    half_beam_width: T,
) -> Vec<Vec<(Point<T>, u8, (bool, bool, bool, bool))>>
where
    T: CoordinateType + PrimInt + Signed,
{
    // Convert terminal shapes into locations of expansion points.
    // (This can be corner points, or edge centers for example.)
    terminals
        .iter()
        .map(|ts| {
            let mut expansion_points =
                create_expansion_points_from_terminal_shape(ts, half_beam_width);

            // Check if we can connect to the terminal shape here
            // and there will be no collision with an obstacle.
            expansion_points.retain(|(p, layer_num, _)| {
                let is_legal = !obstacle_layers.layers[*layer_num as usize].overlaps_rect(
                    *p,
                    half_beam_width,
                    half_beam_width,
                );
                is_legal
            });
            expansion_points
        })
        .collect()
}

fn create_expansion_points_from_terminal_shape<T>(
    terminal_shapes: &Vec<(SimpleRPolygon<T>, u8)>,
    half_beam_width: T,
) -> Vec<(Point<T>, u8, (bool, bool, bool, bool))>
where
    T: CoordinateType + PrimInt + Signed,
{
    // Define shortcuts for constants.
    let _0 = T::zero();
    let _1 = T::one();
    let _2 = _1 + _1;

    terminal_shapes
        .iter()
        .flat_map(|(t, layer_num)| {
            // t.edges().map(move |e| ((e.start() + e.end()) / _2, *layer_num))
            t.edges()
                .filter(|e| !e.is_degenerate()) // Skip zero-length edges.
                .map(move |e| {
                    // Find normal vector of the polygon edge.
                    let normal = e.direction().unwrap().rotate_ortho(-Angle::R90);

                    // Expand only in the direction of the normal vector.
                    let expansion_directions = (
                        normal.x == _1,
                        normal.y == _1,
                        normal.x == _0 - _1,
                        normal.y == _0 - _1,
                    );

                    let margin = normal * half_beam_width;

                    // Create an expansion point at the center of the edge.
                    // Move the expansion point away from the edge in the direction of the normal.
                    let p = (e.start() + e.end()) / _2 + margin;

                    (p, *layer_num, expansion_directions)
                })
        })
        .collect()
}

/// Assemble the path by backtracing in both directions. Starts at `hit`.
fn assemble_paths<T>(
    exp: &ExpansionPoint<T>,
    trace: &HashMap<(usize, (Point<T>, u8)), ExpansionPoint<T>>,
    back_tracing_centers: &HashMap<(usize, (Point<T>, u8)), ExpansionPoint<T>>,
    debug: bool,
    visualize: bool,
) -> (usize, usize, Vec<(Point<T>, u8)>)
where
    T: CoordinateType + PrimInt + Signed + std::fmt::Display + std::fmt::Debug + Hash,
{
    // Extract where the target is hit.
    let hit: (usize, REdge<T>) = exp.target_hit.expect("`exp` must hit a target.");

    if debug {
        println!(
            "Hit target {}. Total cost: {}",
            hit.0,
            exp.cost + exp.cost_to_target_lower_bound
        );
        println!("Cost: {}", exp.cost);
        println!("Cost lower bound: {}", exp.cost_to_target_lower_bound);
    }

    let other_exp = &back_tracing_centers[&(exp.terminal_id, exp.point)];

    // Trace back to the origin of the expansion.
    let path1 = trace_back(&trace, &exp);
    let path2 = trace_back(&trace, other_exp);

    if debug {
        println!(
            "Expansions meet at: {} and {}",
            exp.point.0, other_exp.point.0
        );
    }

    // Assemble the two half paths into the full path.
    let path = {
        let path = path1
            .into_iter()
            .rev() // Make sure both parts are oriented the same way.
            .chain(path2)
            .dedup() // Remove zero-length edges.
            .collect_vec();

        // Simplify by removing redundant vertices along straight lines.
        let corners = (1..path.len() - 1).filter_map(|i| {
            let a = path[i - 1];
            let b = path[i];
            let c = path[i + 1];
            let same_layer = a.1 == b.1 && a.1 == c.1;
            let same_x = a.0.x == b.0.x && a.0.x == c.0.x;
            let same_y = a.0.y == b.0.y && a.0.y == c.0.y;
            if same_layer && (same_x || same_y) {
                // Not a corner.
                None
            } else {
                Some(path[i])
            }
        });

        let mut path_simplified = vec![path[0]];
        path_simplified.extend(corners);
        path_simplified.push(path[path.len() - 1]); // Push the last.

        path_simplified
    };

    (exp.terminal_id, other_exp.terminal_id, path)
}

/// Find the path to the origin of the expansion.
fn trace_back<T: CoordinateType + PrimInt + Hash>(
    trace: &HashMap<(usize, (Point<T>, u8)), ExpansionPoint<T>>,
    start: &ExpansionPoint<T>,
) -> Vec<(Point<T>, u8)> {
    let mut path = Vec::new();

    let terminal_id = start.terminal_id;
    let mut current = start;
    path.push(current.point);

    // Trace back along the expansion points.
    // Loop as long as the origin has not been reached yet.
    while current.origin != current.point {
        let previous = &trace[&(terminal_id, current.origin)];
        current = previous;
        // Remember the path we traverse.
        path.push(current.point);
    }

    path
}
