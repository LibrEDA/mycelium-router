// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Grid-less router implementation based on a line-search or "line-probing" algorithm.

mod expansion;
mod line_search;
mod route_multi_signal;
mod route_single_signal;

pub use route_multi_signal::SimpleLineSearchRouter;

use itertools::Itertools;
use num_traits::{PrimInt, Signed};
use std::hash::Hash;

use crate::visualize::TermPlot;
use iron_shapes::point_string::PointString;
use iron_shapes::prelude::*;
use log;
use std::collections::{BinaryHeap, HashMap, HashSet};

use line_search::LineSearchRegion;
use route_single_signal::{route_single_signal, LineSearchLayerStack};

use expansion::*;

//
// #[test]
// fn test_beam_expansion_single_terminal() {
//     let terminals = vec![
//         (Point::new(15, 1), 0),
//     ];
//
//     // Create initial expansion points.
//     let mut front = BinaryHeap::new();
//     for (i, t) in terminals.iter().enumerate() {
//         front.push(
//             ExpansionPoint {
//                 point: *t,
//                 origin: *t,
//                 terminal_id: i,
//                 cost: 0,
//                 cost_to_target_lower_bound: 0,
//                 details: ExpPointBitmap::new_all_directions().set_delta((0, 0)),
//                 target_hit: None,
//             }
//         )
//     }
//
//     let num_layers = 2;
//
//     let line_search_bucket_size = 64;
//
//
//     let core_size = 30;
//     // (obstacle, layer) pairs.
//     let obstacles = vec![
//         (SimpleRPolygon::try_new(&vec![(10i32, 10), (20, 10), (20, 20), (10, 20)]).unwrap(), 0),
//         (SimpleRPolygon::try_new(&vec![(0i32, 10), (8, 10), (8, 20), (0, 20)]).unwrap(), 0),
//         (SimpleRPolygon::try_new(&vec![(10i32, 10), (20, 10), (20, 11), (10, 11)]).unwrap(), 0),
//         (SimpleRPolygon::try_new(&vec![(15i32, 15), (25, 15), (25, 25), (15, 25)]).unwrap(), 0),
//         // (SimpleRPolygon::try_new(&vec![(10i32, 10), (20, 10), (20, 30), (10, 30)]).unwrap(), 0),
//         // (SimpleRPolygon::try_new(&vec![(10i32, 10), (20, 10), (20, 20), (10, 20)]).unwrap(), 0),
//         // (SimpleRPolygon::try_new(&vec![(11i32, 11), (21, 11), (21, 31), (11, 31)]).unwrap(), 0),
//         // Containing box.
//         // Hole (oriented clock wise).
//         (SimpleRPolygon::try_new(&vec![(0, 0), (0, core_size), (core_size, core_size), (core_size, 0)]).unwrap(), 0),
//         // Outer border (oriented counter-clock wise).
//         (SimpleRPolygon::try_new(&vec![(-1, -1), (core_size + 1, -1), (core_size + 1, core_size + 1), (-1, core_size + 1)]).unwrap(), 0),
//     ];
//
//     // Visualize the obstacles
//     let mut plt = TermPlot::new();
//     for (obs, _) in &obstacles {
//         obs.edges().for_each(|e| plt.draw_edge(e))
//     }
//     println!("Obstacles");
//     plt.print();
//
//     // plt.clear();
//
//     let mut obstacle_layers = LineSearchLayerStack::new(num_layers);
//
//     let mut terminal_expansions = terminals.iter()
//         .map(|_| LineSearchLayerStack::new(num_layers))
//         .collect_vec();
//
//     // Insert obstacles and remember which IDs are associated with which obstacle.
//     let obstacle_edge_ids: HashMap<_, _> = obstacles.iter()
//         .flat_map(|(obs, layer)| {
//             let ids = obstacle_layers.layers[*layer].insert_polygon(&obs);
//             ids.into_iter().map(move |id| (id, (obs, *layer)))
//         })
//         .collect();
//
//     let half_beam_width = 1;
//
//
//     // Set of visited expansion points.
//     let mut visited = HashSet::new();
//
//     while let Some(exp) = front.pop() {
//         if visited.contains(&exp.point.0) {
//             continue;
//         }
//
//         println!("Expand {}", exp);
//         let (xy, layer) = exp.point;
//
//         let next = expansion::expand(
//             &obstacle_layers.layers[layer],
//             &[],
//             &[],
//             &exp,
//             half_beam_width,
//         );
//
//         for n in next.iter()
//         // Skip already visited points.
//         // .filter(|e| !visited.contains(&e.point.0))
//         {
//             let layer1 = exp.point.1;
//             let layer2 = n.point.1;
//             println!("\t-> {}", n);
//             let edge = REdge::new(xy, n.point.0);
//             if layer1 == layer2 {
//                 if edge.start != edge.end {
//                     terminal_expansions[n.terminal_id].layers[layer]
//                         .insert_edge(edge, false);
//                 }
//             } else {
//                 unimplemented!("Layer crossing not implemented yet.")
//             }
//             plt.draw_edge(edge);
//         }
//
//         front.extend(
//             next.into_iter()
//                 // Skip already visited points.
//                 .filter(|e| !visited.contains(&e.point.0))
//         );
//
//         visited.insert(xy);
//
//         println!("Current state:");
//         plt.print();
//     }
// }

#[test]
fn test_beam_search_multi_terminal() {
    let num_layers = 2;

    let mut terminals: Vec<Vec<(SimpleRPolygon<_>, u8)>> = vec![
        vec![(Rect::new((15, 3), (17, 5)).into(), 0)],
        vec![(Rect::new((4, 2), (6, 4)).into(), 0)],
        vec![(Rect::new((11, 21), (14, 24)).into(), 0)],
        // vec![(Rect::new((15, 25), (16, 26)).into(), 1)],
    ];

    let core_size = 30;
    // (obstacle, layer) pairs.
    let mut obstacles = vec![
        // (SimpleRPolygon::try_new(vec![(3i32, 10), (17, 10), (17, 12), (3, 12)]).unwrap(), 0),
        //(SimpleRPolygon::try_new(vec![(0i32, 10), (19, 10), (19, 12), (0, 12)]).unwrap(), 0),
        // (SimpleRPolygon::try_new(vec![(20, 10), (24, 10), (24, 14), (20, 14)]).unwrap(), 0),
        // (SimpleRPolygon::try_new(vec![(4, 16), (30, 16), (30, 17), (4, 17)]).unwrap(), 0),

        //(SimpleRPolygon::try_new(vec![(5, 5), (10, 5), (10, 10), (15, 10), (15, 20), (10, 20),
        //                              (10, 15), (5, 15)]).unwrap(), 0),
        // (SimpleRPolygon::try_new(vec![(4, 16), (30, 16), (30, 17), (4, 17)]).unwrap(), 1),
    ];

    for layer in 0..num_layers {
        // Containing box.
        // Hole (oriented clock wise).
        let inner = (
            SimpleRPolygon::try_new(&vec![
                (0, 0),
                (0, core_size),
                (core_size, core_size),
                (core_size, 0),
            ])
            .unwrap(),
            layer,
        );
        // Outer border (oriented counter-clock wise).
        let outer = (
            SimpleRPolygon::try_new(&vec![
                (-1, -1),
                (core_size + 1, -1),
                (core_size + 1, core_size + 1),
                (-1, core_size + 1),
            ])
            .unwrap(),
            layer,
        );
        obstacles.push(inner);
        obstacles.push(outer);
    }

    // Treat terminals as obstacles.
    for t in terminals.iter().flatten().cloned() {
        obstacles.push(t);
    }

    let wire_width = 2;
    assert_eq!(wire_width % 2, 0, "Wire width must be an even integer.");
    let min_spacing = 0;

    let wire_weights = vec![(1, 1), (1, 1)];

    // Create a line search region of obstacles for each layer.
    let mut obstacle_layers = LineSearchLayerStack::new(num_layers as usize);

    // Insert obstacles and remember which IDs are associated with which obstacle.
    let _obstacle_edge_ids: HashMap<_, _> = obstacles
        .iter()
        .flat_map(|(obs, layer)| {
            let ids = obstacle_layers.layers[*layer as usize].insert_polygon(&obs);
            ids.into_iter().map(move |id| (id, (obs, *layer)))
        })
        .collect();

    let result = route_single_signal(
        &terminals,
        &obstacle_layers,
        num_layers as u8,
        wire_width / 2,
        min_spacing,
        &wire_weights,
        true,
    );

    dbg!(&result);
    assert!(result.is_some());
}
