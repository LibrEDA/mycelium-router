// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Fast search for closest intersecting lines.
//! All obstacle line segments are parallel to each other and arranged in a pre-computed data
//! structure. Given a line `l` orthogonal to the obstacle lines through a point `p` the intersection of
//! `l` with the obstacles closest to `p` can be found efficiently.

mod bucket;
mod edge;
mod line_search;
mod line_search_region;

pub use line_search_region::multi_closest_beam_intersection;
pub use line_search_region::LineSearchRegion;
