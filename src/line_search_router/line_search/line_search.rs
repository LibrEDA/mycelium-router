// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Collection of line segments (edges) of same orientation
//! prepared for fast lookup of closest intersection with a ray.
//! Used for [`super::LineSearchRegion`].

use iron_shapes::simple_rpolygon::SimpleRPolygon;
use iron_shapes::types::ContainsResult;
use itertools::Itertools;
use num_traits::PrimInt;
use std::cmp::Ordering;

use super::bucket::Bucket;
use super::edge::Edge;

/// Collection of line segments (edges) of same orientation
/// prepared for fast lookup of closest intersection with a ray.
///
/// For simplicity and without loss of generality the orientation of the edges shall be along the `x`-axis.
/// (To handle vertical edges, the coordinate system is just rotated by 90 degrees.)
/// The plane containing the edges is subdivided into disjoint `Bucket`s or 'beams' along the `y`-axis.
/// Each bucket keeps track of all edges that intersect it.
///
/// To find an intersection of a horizontal edge with a vertical line first the right bucket is found.
/// Then inside this bucket the possible intersection candidates can be found efficiently due to
/// sorting along the `y`-axis.
///
#[derive(Clone)]
pub(crate) struct LineSearch<T, U>
where
    U: Ord + Copy,
{
    buckets: Vec<Bucket<T, U>>,
    max_partial_edges_per_bucket: usize,
}

impl<T: PrimInt, U: Ord + Copy> LineSearch<T, U> {
    /// Create an empty collection with one bucket spanning the full coordinate system.
    pub fn new(max_partial_edges_per_bucket: usize) -> Self {
        Self::new_finite(T::min_value(), T::max_value(), max_partial_edges_per_bucket)
    }

    /// Create an empty collection with one bucket spawning a certain range.
    fn new_finite(initial_start: T, initial_end: T, max_partial_edges_per_bucket: usize) -> Self {
        LineSearch {
            buckets: vec![Bucket::new(initial_start, initial_end)],
            max_partial_edges_per_bucket,
        }
    }

    /// Return the width of the covered region.
    pub fn width(&self) -> T {
        self.end() - self.start()
    }

    /// Get the x-coordinate of the left end covered by this region.
    fn start(&self) -> T {
        self.buckets.first().unwrap().start()
    }

    /// Get the x-coordinate of the right end covered by this region.
    fn end(&self) -> T {
        self.buckets.last().unwrap().end()
    }

    /// Expand by adding buckets such that coordinate `x` is contained in the structure.
    fn expand(&mut self, x: T) {
        let current_start = self.start();
        let current_end = self.end();
        if x < current_start {
            let min_width = (current_start - x).max(self.width());
            let new_width = (0..)
                .scan(min_width, |acc, _| {
                    let before = *acc;
                    *acc = before + before;
                    Some(before)
                })
                .find(|w| *w >= min_width)
                .unwrap();
            // Create a bucket of double the necessary width.
            let bucket = Bucket::new(current_start - new_width, current_start - T::one());

            self.buckets.insert(0, bucket);
        }

        if current_end < x {
            let min_width = (x + T::one() - current_end).max(self.width());
            // Double the current width as long until it is bigger than the minimal width.
            let new_width = (0..)
                .scan(min_width, |acc, _| {
                    let before = *acc;
                    *acc = before + before;
                    Some(before)
                })
                .find(|w| *w >= min_width)
                .unwrap();

            // Create a bucket of double the necessary width.
            let bucket = Bucket::new(current_end + T::one(), current_end + new_width);

            self.buckets.push(bucket);
        }
    }

    /// Recursively split buckets in the middle until each has less
    /// than a fixed number of partial edges.
    fn split_buckets(&self, buckets: Vec<Bucket<T, U>>) -> Vec<Bucket<T, U>> {
        buckets.into_iter().map(|b| self.split_bucket(b)).concat()
    }

    /// Recursively split bucket in the middle until each has less
    /// than a fixed number of partial edges.
    fn split_bucket(&self, bucket: Bucket<T, U>) -> Vec<Bucket<T, U>> {
        if bucket.num_partial_edges() <= self.max_partial_edges_per_bucket {
            vec![bucket]
        } else {
            let partial_starts = bucket.partial_edges().iter().map(|(e, _)| e.start);
            let partial_ends = bucket.partial_edges().iter().map(|(e, _)| e.end + T::one()); // Increment: Ends should be excluded from the left bucket.

            // Create a list of start and end points sorted by their x-coordinate.
            let merged = partial_starts.merge(partial_ends);
            // Find the position that occurs most often.
            let best = merged
                .filter(|&x| bucket.start() < x && x <= bucket.end())
                // Count occurrences.
                .scan((0, None), |(count, value), x| {
                    if Some(x) == *value {
                        *count = *count + 1
                    } else {
                        *count = 1;
                    }
                    *value = Some(x);

                    Some((*count, x))
                })
                // Take position which occurs most often, break ties by x-coordinate.
                .max()
                .map(|(_count, pos)| pos)
                .unwrap();

            let (a, b) = bucket.split_horizontal_at(best);

            [self.split_bucket(a), self.split_bucket(b)].concat()
        }
    }

    /// Find the index of the bucket that is responsible for the coordinate (x, y).
    fn find_bucket_id_by_coordinate(&self, x: T, _y: T) -> usize {
        // Find intersecting buckets.
        self.buckets
            .binary_search_by(|b| {
                if x < b.start() {
                    Ordering::Greater
                } else if x > b.end() {
                    Ordering::Less
                } else {
                    Ordering::Equal
                }
            })
            .unwrap()
    }

    /// Insert a new edge and return its ID.
    pub(super) fn insert(&mut self, edge: Edge<T>, data: U) -> () {
        debug_assert!(edge.start <= edge.end);

        assert!(!self.buckets.is_empty());
        // if self.buckets.is_empty() {
        //     // Create first bucket.
        //     let width = edge.end - edge.start;
        //     debug_assert!(width >= T::zero());
        //     // Create a bucket of double the width of the edge.
        //     // TODO: Choose width = smallest power of two bigger than the width of the edge.
        //     let bucket = Bucket::new(edge.start - width, edge.end + width);
        //     self.buckets.push(bucket)
        // }

        // Expand such that the edge can be surely contained.
        if edge.start < self.start() {
            self.expand(edge.start);
        }
        if self.end() < edge.end {
            self.expand(edge.end)
        }

        // Find intersecting buckets.
        let first_bucket = self.find_bucket_id_by_coordinate(edge.start, edge.offset);
        let last_bucket = self.find_bucket_id_by_coordinate(edge.end, edge.offset);

        debug_assert!(first_bucket <= last_bucket);

        // Insert the edge in all intersecting buckets.
        let mut need_resizing = false;
        for i in first_bucket..last_bucket + 1 {
            let bucket = &mut self.buckets[i];
            bucket.insert(&edge, data);
            if bucket.num_partial_edges() > self.max_partial_edges_per_bucket {
                need_resizing = true;
            }
        }

        // Resize the buckets if needed.
        if need_resizing {
            let mut buckets = Vec::new();
            std::mem::swap(&mut self.buckets, &mut buckets);
            self.buckets = self.split_buckets(buckets);
        }
    }

    /// Find the closest intersections of a vertical line through (x, y).
    pub fn line_intersection(&self, x: T, y: T) -> (Vec<U>, Vec<U>, Vec<U>) {
        // Find the right bucket.
        let bucket_id = self.find_bucket_id_by_coordinate(x, y);

        // Search for an intersection in the right bucket.
        self.buckets[bucket_id].find_closest_line_intersections(y, x)
    }

    /// Find the closest intersections of a vertical beam through (x, y).
    /// Returns a tuple of `(closest intersections in negative y direction, intersections at y, intersections in positive y direction)`.
    pub fn beam_intersection(
        &self,
        x: T,
        y: T,
        half_beam_width: T,
    ) -> (
        impl Iterator<Item = (T, U)> + '_,
        Vec<U>,
        impl Iterator<Item = (T, U)> + '_,
    ) {
        // Find the right buckets.
        let bucket_id_first = self.find_bucket_id_by_coordinate(x - half_beam_width, y);
        let bucket_id_last = self.find_bucket_id_by_coordinate(x + half_beam_width, y);
        let num_spanned_buckets = 1 + bucket_id_last - bucket_id_first;

        let mut prevs = Vec::new();
        let mut here = Vec::new();
        let mut nexts = Vec::new();

        // Reserve space since it is already known.
        prevs.reserve(num_spanned_buckets);
        here.reserve(num_spanned_buckets);
        nexts.reserve(num_spanned_buckets);

        // Search for an intersection in the buckets of interest.
        // Then take the closest results.
        for bucket_id in bucket_id_first..bucket_id_last + 1 {
            let (prev_current, here_current, next_current) =
                self.buckets[bucket_id].find_beam_intersections(y, x, half_beam_width);

            // Intersections directly at (x, y) always have the same distance (0).
            // Hence we take all of them.
            here.extend(here_current);

            prevs.push(prev_current);
            nexts.push(next_current);
        }

        // Merge the results while keeping the ordering.
        let prevs = prevs.into_iter().kmerge_by(|a, b| a.0 > b.0);
        let nexts = nexts.into_iter().kmerge_by(|a, b| a.0 < b.0);

        (prevs, here, nexts)
    }

    /// Test if the point lies in one or more polygons.
    ///
    /// This takes into account edges that are marked as lower or upper boundary.
    ///
    /// This is **not correct yet when the point lies on a vertical boundary**.
    ///
    /// For correct point inclusion testing two line search queries need to be made. One for
    /// the vertical edges of the polygons and one for the horizontal edges.
    pub fn contains_point(&self, x: T, y: T) -> ContainsResult {
        // Find the right bucket.
        let bucket_id = self.find_bucket_id_by_coordinate(x, y);

        // Query the correct bucket.
        self.buckets[bucket_id].contains_point(x, y)
    }
}

#[test]
fn test_empty_line_intersection() {
    let ls: LineSearch<_, ()> = LineSearch::new(1);
    let (prev, hit, next) = ls.line_intersection(0, 0);
    assert!(prev.is_empty());
    assert!(hit.is_empty());
    assert!(next.is_empty());
}

#[test]
fn test_line_intersection_single_edge() {
    let mut ls = LineSearch::new(1);
    ls.insert(Edge::new(0, 10, 7), 1);
    let (prev, hit, next) = ls.line_intersection(0, 0);
    assert!(prev.is_empty());
    assert!(hit.is_empty());
    assert_eq!(next.len(), 1);
}

#[test]
fn test_empty_beam_intersection() {
    let ls: LineSearch<_, ()> = LineSearch::new(1);
    let (prev, hit, next) = ls.beam_intersection(0, 0, 1);
    assert_eq!(prev.count(), 0);
    assert!(hit.is_empty());
    assert_eq!(next.count(), 0);
}

#[test]
fn test_line_search_insert_without_expand() {
    let mut ls = LineSearch::new_finite(0, 100, 256);
    // Insert an edge which lies in the bounds. No expansion necessary.
    ls.insert(Edge::new(1, 99, 0), 1);
    assert_eq!(ls.buckets.len(), 1);
}

#[test]
fn test_line_search_insert_with_expand() {
    let mut ls = LineSearch::new_finite(0, 100, 256);
    // Insert an edge which does not lie in the bounds. Expansion is necessary.
    ls.insert(Edge::new(1000, 1999, 0), 2);
    assert_eq!(ls.buckets.len(), 2);
}

#[test]
fn test_line_search_insert_and_expand() {
    let mut ls = LineSearch::new_finite(0, 100, 256);
    // Expansion necessary.
    ls.insert(Edge::new(-10, -5, 0), 3);
    assert_eq!(ls.buckets.len(), 2);
    ls.insert(Edge::new(10, 15, 0), 4);
    assert_eq!(ls.buckets.len(), 2);
    ls.insert(Edge::new(99, 100, 0), 5);
    assert_eq!(ls.buckets.len(), 2);
    ls.insert(Edge::new(99, 101, 0), 6);
    assert_eq!(ls.buckets.len(), 3);
}

#[test]
fn test_line_search_insert_with_minimal_bucket_size_single_edge() {
    let mut ls = LineSearch::new(1);
    ls.insert(Edge::new(0, 10, 0), 1);
    assert_eq!(
        ls.buckets.len(),
        1,
        "There must be a single partial edge in the bucket."
    );
}

#[test]
fn test_line_search_insert_with_minimal_bucket_size_duplicated_edge() {
    let mut ls = LineSearch::new(1);
    ls.insert(Edge::new(0, 10, 0), 1);
    ls.insert(Edge::new(0, 10, 0), 2);
    // Three buckets are necessary to hold identical edges such that there are at most one partial
    // edge per bucket.
    assert_eq!(ls.buckets.len(), 3);
    // Adding a third identical edge should not change the number of buckets.
    ls.insert(Edge::new(0, 10, 0), 3);
    assert_eq!(ls.buckets.len(), 3);
}

#[test]
fn test_line_search_insert_with_minimal_bucket_size_duplicated_edge_2() {
    let mut ls = LineSearch::new(1);
    ls.insert(Edge::new(0, 10, 0), 1);
    ls.insert(Edge::new(0, 10, 0), 2);
    // Three buckets are necessary to hold identical edges such that there are at most one partial
    // edge per bucket.
    assert_eq!(ls.buckets.len(), 3);

    // Add two other duplicated edges.
    ls.insert(Edge::new(20, 30, 0), 4);
    assert_eq!(ls.buckets.len(), 3);
    ls.insert(Edge::new(20, 30, 0), 5);
    assert_eq!(ls.buckets.len(), 5);
}
