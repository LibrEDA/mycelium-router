// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Fast search for closest intersecting lines.
//! All obstacle line segments are parallel to each other and arranged in a pre-computed data
//! structure. Given a line `l` orthogonal to the obstacle lines through a point `p` the intersection of
//! `l` with the obstacles closest to `p` can be found efficiently.

use super::edge::*;
use iron_shapes::types::ContainsResult;
use itertools::Itertools;
use num_traits::PrimInt;

/// Structure holding edges sorted by offset.
///
/// A bucket can be thought of a beam orthogonal to the edge direction which contains all edge segments
/// that intersect with the beam.
///
/// All edges inside a bucket are either fully or partially crossing the beam. A fully crossing edge covers
/// the full range of the bucket and possibly beyond. A partially intersecting edge does not intersect
/// the full width of the bucket.
///
/// Fully crossing edges are stored in a `Vec` sorted by their offset. This allows to find
/// neighbouring edges very efficiently by binary search. Because they are known to cross
/// the bucket fully it is also known that they always intersect a line which is in the range
/// of the bucket.
/// Edges that cross the bucket only partially are stored in a separate `Vec` sorted by their offset.
/// Potential intersecting edges can be found by a binary search, however potentially all of the candidates
/// must be separately checked for intersection until an intersection is found.
#[derive(Debug, Clone)]
pub(super) struct Bucket<T, U>
where
    U: Ord + Copy,
{
    /// Start coordinate of the bucket.
    start: T,
    /// End (inclusive) coordinate of the bucket.
    end: T,
    /// Edges crossing the full width of the bucket.
    /// Stored as (offset, associated data) tuples.
    full_edges: Vec<FullEdge<T, U>>,
    /// Edges crossing the bucket only partially.
    partial_edges: Vec<(Edge<T>, U)>,
}

/// An edge spans the full width of the bucket.
/// Stored as (offset, associated data) tuples.
#[derive(Debug, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub(super) struct FullEdge<T, U>
where
    U: Ord + Copy,
{
    /// Position of the line, stored as the distance to the origin.
    offset: T,
    /// Type of the polygon boundary.
    boundary_type: BoundaryType,
    /// User defined data associated with this edge.
    user_data: U,
    /// Counts how many polygons have been opened before and with this edge
    /// and are not closed yet.
    /// This edge itself IS ALSO taken into account.
    /// 'Number of lower bounds before and with this edge' - 'Number of upper bounds before and with this edge'
    inside_polygon_count: i32,
}

impl<T, U> FullEdge<T, U>
where
    U: Ord + Copy,
{
    /// Create a new `FullEdge`.
    fn new(offset: T, boundary_type: BoundaryType, user_data: U) -> Self {
        Self {
            offset,
            boundary_type,
            user_data,
            inside_polygon_count: 0,
        }
    }
}

impl<T: PrimInt, U: Ord + Copy> Bucket<T, U> {
    /// Create a new empty bucket with a start and end coordinate.
    pub(super) fn new(start: T, end: T) -> Self {
        assert!(start <= end, "Bucket width must be non-negative.");
        Self {
            start,
            end,
            full_edges: Default::default(),
            partial_edges: Default::default(),
        }
    }

    /// Get the start boundary of the bucket.
    pub(super) fn start(&self) -> T {
        self.start
    }

    /// Get the end boundary of the bucket.
    pub(super) fn end(&self) -> T {
        self.end
    }

    /// Access the sorted list of partial edges.
    pub(super) fn partial_edges(&self) -> &Vec<(Edge<T>, U)> {
        &self.partial_edges
    }

    /// Get the number of partial edges.
    pub(super) fn num_partial_edges(&self) -> usize {
        self.partial_edges.len()
    }

    /// Insert a line segment into this bucket.
    /// The line segment must be touching this bucket.
    pub(super) fn insert(&mut self, edge: &Edge<T>, id: U) {
        debug_assert!(edge.start < edge.end);
        let no_intersection = edge.end < self.start || self.end < edge.start;
        debug_assert!(!no_intersection, "Edge must intersect with the bucket.");

        let is_full_intersection = edge.start <= self.start && edge.end >= self.end;

        if is_full_intersection {
            self.insert_full(edge.offset, edge.boundary_type, id);
        } else {
            self.insert_partial(edge, id);
        };
    }

    /// Insert a line segment into this bucket.
    /// The line segment must fully intersect this bucket.
    /// End-points are outside of the bucket or on the bucket boundaries.
    pub(super) fn insert_full(&mut self, offset: T, boundary_type: BoundaryType, id: U) {
        // Store only the offset and the ID of the edge.
        let mut new_entry = FullEdge::new(offset, boundary_type, id);

        match self.full_edges.binary_search(&new_entry) {
            Ok(_found_index) => {
                // Such an entry already exists. This should not happen.
                // Offsets might be the same, but 'IDs' should be unique.
                panic!("Bucket entry (full) already exists. IDs should be unique.");
            }
            Err(insert_index) => {
                // Entry does not yet exist, insert it.

                // Find number of open polygons by looking at the previous edge.
                let num_open_polygons = match insert_index {
                    0 => 0,
                    _ => self.full_edges[insert_index - 1].inside_polygon_count,
                };

                new_entry.inside_polygon_count = num_open_polygons;

                self.full_edges.insert(insert_index, new_entry);

                // Update the lower bound counter in all following edges.
                match boundary_type {
                    BoundaryType::None => {} // No action required.
                    BoundaryType::Lower => {
                        // Increment lower bounds. Also of the newly inserted edge.
                        self.full_edges[insert_index..]
                            .iter_mut()
                            .for_each(|fe| fe.inside_polygon_count += 1)
                    }
                    BoundaryType::Upper => {
                        // Decrement lower bounds.
                        self.full_edges[insert_index..]
                            .iter_mut()
                            .for_each(|fe| fe.inside_polygon_count -= 1)
                    }
                }
            }
        }
    }

    /// Insert a line segment into this bucket.
    /// The line segment must be touching this bucket.
    pub(super) fn insert_partial(&mut self, edge: &Edge<T>, id: U) {
        debug_assert!(edge.start < edge.end);
        debug_assert!(self.start < self.end);
        let no_intersection = edge.end < self.start || self.end < edge.start;
        debug_assert!(!no_intersection, "Edge must intersect with the bucket.");
        let is_full_intersection = edge.start <= self.start && edge.end >= self.end;
        debug_assert!(
            !is_full_intersection,
            "Edge must not fully intersect the bucket."
        );

        let new_entry = (edge.clone(), id);
        match self.partial_edges.binary_search(&new_entry) {
            Ok(insert_index) => {
                // Such an entry already exists.
                self.partial_edges.insert(insert_index, new_entry)
            }
            Err(insert_index) => {
                // Entry does not yet exist, insert it.
                self.partial_edges.insert(insert_index, new_entry)
            }
        }
    }

    /// Find the IDs of the closest intersecting edges with the vertical line with distance `offset_orth`
    /// to the origin.
    ///
    /// Returns a three-tuple (IDs of intersecting edges before the point,
    /// IDs of edges intersecting with the beam, IDs of intersecting edges after the point).
    /// Where "before/after" means "under/over" or "left of/right of" depending on the orientation of the
    /// obstacle edges.
    pub(super) fn find_closest_line_intersections(
        &self,
        offset: T,
        offset_orth: T,
    ) -> (Vec<U>, Vec<U>, Vec<U>) {
        // Search for beam intersections with a zero-width beam.
        // Strip away the offset information.
        let (prev, here, next) =
            self.find_closest_beam_intersections(offset, offset_orth, T::zero(), 1);

        (
            // Take only the closest intersections.
            prev.into_iter().map(|f| f.1).next().unwrap_or(vec![]),
            here,
            // Take only the closest intersections.
            next.into_iter().map(|f| f.1).next().unwrap_or(vec![]),
        )
    }

    /// Find the IDs of the `n` closest intersecting edges with the orthogonal 'beam'.
    /// The beam center has distance `offset_orth` to `(0, 0)`.
    /// The width of the beam is `2*half_beam_width`. When set to zero, the beam is equal
    /// to a line.
    ///
    /// Returns a tuple like:
    /// (
    /// Vec<(Offset, and IDs of intersecting edges before the point)>
    /// IDs of edges intersecting with the beam,
    /// Vec<(Offset, IDs of intersecting edges after the point)>
    /// )
    ///
    /// Where "before/after" means "under/over" or "left of/right of" depending on the orientation of the
    /// obstacle edges.
    pub(super) fn find_closest_beam_intersections(
        &self,
        offset: T,
        offset_orth: T,
        half_beam_width: T,
        n: usize,
    ) -> (Vec<(T, Vec<U>)>, Vec<U>, Vec<(T, Vec<U>)>) {
        assert!(
            half_beam_width >= T::zero(),
            "`half_beam_width` is not allowed to be negative."
        );
        assert!(
            self.start <= offset_orth + half_beam_width
                && offset_orth - half_beam_width <= self.end,
            "Position out of bucket."
        );

        let (backward_iter, intersections_on_point, forward_iter) =
            self.find_beam_intersections(offset, offset_orth, half_beam_width);

        // Find all closest intersections in forward direction.
        let intersection_next = forward_iter
            .group_by(|(off, _)| *off) // Take as long as they have the same offset.
            .into_iter()
            .take(n)
            .map(|(key, group)| (key, group.map(|(_, id)| id).collect()))
            .collect();

        // Find all closest intersections in backward direction.
        let intersection_prev = backward_iter
            .group_by(|(off, _)| *off) // Take as long as they have the same offset.
            .into_iter()
            .take(n)
            .map(|(key, group)| (key, group.map(|(_, id)| id).collect()))
            .collect();

        (intersection_prev, intersections_on_point, intersection_next)
    }

    /// Find edges that touch the beam originating from (x, y).
    /// The hits are sorted by ascending distance to (x, y).
    ///
    /// Returns a tuple (iterator over previous intersections (y, user_data),
    /// Vec of perfect hits,
    /// iterator over next intersections (y, user_data) )
    pub(super) fn find_beam_intersections(
        &self,
        y: T,
        x: T,
        half_beam_width: T,
    ) -> (
        impl Iterator<Item = (T, U)> + '_,
        Vec<U>,
        impl Iterator<Item = (T, U)> + '_,
    ) {
        assert!(
            half_beam_width >= T::zero(),
            "`half_beam_width` is not allowed to be negative."
        );
        assert!(
            self.start <= x + half_beam_width && x - half_beam_width <= self.end,
            "Position out of bucket."
        );

        // Test if the edge touches the beam.
        // The beam center has distance `offset_orth` to the origin, the beam has
        // width `beam_width`.
        let edge_touches_beam = move |e: &Edge<T>| -> bool {
            e.start <= x + half_beam_width && x - half_beam_width <= e.end
        };

        let edge_hits_beam = move |e: &Edge<T>| -> bool {
            e.start < x + half_beam_width && x - half_beam_width < e.end
        };

        let (found_index_full_edges, is_perfect_match_full_edges) =
            match self.full_edges.binary_search_by_key(&y, |fe| fe.offset) {
                Ok(found_index) => {
                    // The index does not point necessarily to the first match.
                    // Find and return the index of the first match.
                    let num_below = self.full_edges[0..found_index]
                        .iter()
                        .rev()
                        .take_while(|fe| fe.offset == y)
                        .count();

                    (found_index - num_below, true)
                }
                Err(between_index) => (between_index, false),
            };

        let (found_index_partial_edges, is_perfect_match_partial_edges) =
            match self.partial_edges.binary_search_by_key(&y, |e| e.0.offset) {
                Ok(found_index) => {
                    // The index does not point necessarily to the first match.
                    // Find and return the index of the first match.
                    let num_below = self.partial_edges[0..found_index]
                        .iter()
                        .rev()
                        .take_while(|(e, _)| e.offset == y)
                        .count();
                    (found_index - num_below, true)
                }
                Err(between_index) => (between_index, false),
            };

        let forward_iter_full = self.full_edges[found_index_full_edges..]
            .iter()
            .map(|e| (e.offset, e.user_data));
        let backward_iter_full = self.full_edges[..found_index_full_edges]
            .iter()
            .rev()
            .map(|e| (e.offset, e.user_data));

        // Create an iterator over all partial edges in forward direction that interact with the beam.
        let forward_iter_partial = self.partial_edges[found_index_partial_edges..]
            .iter()
            // Check for intersection.
            .filter(move |(e, _)| edge_touches_beam(e))
            .map(|(e, id)| (e.offset, *id));

        // Create an iterator over all partial edges in backward direction that interact with the beam.
        let backward_iter_partial = self.partial_edges[..found_index_partial_edges]
            .iter()
            .rev()
            // Check for intersection.
            .filter(move |(e, _)| edge_touches_beam(e))
            .map(|(e, id)| (e.offset, *id));

        // Merge partial and full edges.
        // This can be done efficiently because they are sorted.
        let mut forward_iter = forward_iter_full.merge(forward_iter_partial).peekable();
        let backward_iter = backward_iter_full
            .merge_by(backward_iter_partial, |a, b| a.0 >= b.0)
            .peekable();

        // Find intersections that are perfectly on the probe point.
        let intersection_on_point = forward_iter
            .by_ref()
            .peeking_take_while(|(off, _id)| *off == y)
            .map(|(_, id)| id)
            .collect();

        (backward_iter, intersection_on_point, forward_iter)
    }

    /// Test if the point lies in one or more polygons.
    ///
    /// This is **not correct yet when the point lies on a vertical boundary**.
    ///
    /// For correct point inclusion testing two buckets need to be queried. One holding
    /// the vertical edges of the polygons and one holding the horizontal edges.
    pub fn contains_point(&self, x: T, y: T) -> ContainsResult {
        assert!(self.start <= x && x <= self.end, "Position out of bucket.");

        let offset = y;
        let offset_orth = x;

        // Query the full edges.
        // Get the inside-polygon-count just before the point and just after
        // while only looking at the full edges.
        let (inside_count_full_before, inside_count_full_after) = match self
            .full_edges
            .binary_search_by_key(&offset, |fe| fe.offset)
        {
            Ok(found_index) => {
                // The index does not point necessarily to the first match.
                // Find and return the index of the first match.
                let num_below = self.full_edges[0..found_index]
                    .iter()
                    .rev()
                    .take_while(|fe| fe.offset == offset)
                    .count();

                // Index of first full edge which intersects the probe point.
                let first_index = found_index - num_below;

                let first = &self.full_edges[first_index];
                let inside_count_before = first.inside_polygon_count
                    + match first.boundary_type {
                        // Compensate: The edge counts itself already, need to subtract it.
                        BoundaryType::None => 0,
                        BoundaryType::Lower => -1,
                        BoundaryType::Upper => 1,
                    };

                // There are possibly many boundaries at the same location.
                // Find out if a point just after the lines would be inside or outside a polygon.
                let inside_count_after = self.full_edges[first_index..]
                    .iter()
                    .take_while(|fe| fe.offset == offset)
                    .last()
                    .unwrap() // There must be a match because the binary search returned a hit.
                    .inside_polygon_count;

                (inside_count_before, inside_count_after)
            }
            Err(between_index) => {
                if between_index == 0 {
                    // No full edge is below.
                    (0, 0)
                } else {
                    // Get counter from previous edge.
                    let inside_count = self.full_edges[between_index - 1].inside_polygon_count;
                    (inside_count, inside_count)
                }
            }
        };

        // Get the inside-polygon-count just before the point and just after.
        let (inside_count_partial_before, inside_count_partial_after) = {
            // Query the partial edges.
            // Each edge before the point must be looked at and tested for intersection with the vertical line
            // through the point.
            let mut partial_edges_iter = self
                .partial_edges
                .iter()
                // Check for intersection.
                // Start points should be included, end points should be excluded.
                .filter(|(e, _)| e.start <= offset_orth && offset_orth < e.end)
                .peekable();
            let inside_count_before: i32 = partial_edges_iter
                .by_ref()
                .peeking_take_while(|(e, _)| e.offset < offset)
                .map(|(e, _)| match e.boundary_type {
                    BoundaryType::None => 0,
                    BoundaryType::Lower => 1,
                    BoundaryType::Upper => -1,
                })
                .sum();

            // Iterate over edges that intersect with the probe point.
            let inside_count_after = inside_count_before
                + partial_edges_iter
                    .take_while(|(e, _)| e.offset == offset)
                    .map(|(e, _)| match e.boundary_type {
                        BoundaryType::None => 0,
                        BoundaryType::Lower => 1,
                        BoundaryType::Upper => -1,
                    })
                    .sum::<i32>();

            (inside_count_before, inside_count_after)
        };

        let inside_count_before = inside_count_full_before + inside_count_partial_before;
        let inside_count_after = inside_count_full_after + inside_count_partial_after;

        let is_inside_before = inside_count_before != 0;
        let is_inside_after = inside_count_after != 0;

        // Determine whether the point is inside one or more polygons, on the boundary or outside.
        if is_inside_before == is_inside_after {
            if is_inside_before {
                ContainsResult::WithinBounds
            } else {
                ContainsResult::No
            }
        } else {
            // If the containment of a point just before the lines is different
            // from the one of a point just after, then this must be a bound.
            ContainsResult::OnBounds
        }
    }

    /// Split the bucket into two buckets with smaller width and disjoint range.
    pub(super) fn split_horizontal_at(&self, offset_orth: T) -> (Bucket<T, U>, Bucket<T, U>) {
        assert!(
            self.start < offset_orth && offset_orth <= self.end,
            "Must split somewhere in the middle of the bucket."
        );

        // log::debug!("Split bucket horizontally.");

        let mut a = Bucket::new(self.start, offset_orth - T::one());
        let mut b = Bucket::new(offset_orth, self.end);

        a.full_edges = self.full_edges.clone();
        b.full_edges = self.full_edges.clone();

        for (e, id) in &self.partial_edges {
            let id = *id;
            if e.start < offset_orth {
                a.insert(e, id);
            }
            if e.end >= offset_orth {
                b.insert(e, id);
            }
        }

        (a, b)
    }

    /// Split the bucket into two buckets with same range (`start` and `end`).
    pub(super) fn _split_vertical_at(&self, offset: T) -> (Bucket<T, U>, Bucket<T, U>) {
        let mut a = Bucket::new(self.start, self.end);
        let mut b = Bucket::new(self.start, self.end);

        // Find smallest index such that all elements below this index belong in the lower bucket.
        let split_index_full = match self
            .full_edges
            .binary_search_by_key(&offset, |fe| fe.offset)
        {
            Ok(found_index) => {
                // The index does not point necessarily to the first match.
                // Find and return the index of the first match.
                let num_below = self.full_edges[0..found_index]
                    .iter()
                    .rev()
                    .take_while(|fe| fe.offset == offset)
                    .count();

                found_index - num_below
            }
            Err(between_index) => between_index,
        };

        // Split the full edges into two partitions.
        let (lower, upper) = self.full_edges.split_at(split_index_full);
        a.full_edges = lower.to_vec();
        b.full_edges = upper.to_vec();

        // Find smallest index such that all elements below this index belong in the lower bucket.
        let split_index_partial = match self
            .partial_edges
            .binary_search_by_key(&offset, |(e, _)| e.offset)
        {
            Ok(found_index) => {
                // The index does not point necessarily to the first match.
                // Find and return the index of the first match.
                let num_below = self.partial_edges[0..found_index]
                    .iter()
                    .rev()
                    .take_while(|(e, _)| e.offset == offset)
                    .count();

                found_index - num_below
            }
            Err(between_index) => between_index,
        };

        // Split the partial edges into two partitions.
        let (lower, upper) = self.partial_edges.split_at(split_index_partial);
        a.partial_edges = lower.to_vec();
        b.partial_edges = upper.to_vec();

        (a, b)
    }
}

#[test]
fn test_bucket_insert_full_span_edges() {
    let mut bucket = Bucket::new(0, 10);

    let b = BoundaryType::None;

    // Exactly full span.
    let e1 = Edge::new(0, 10, 1);
    let id1 = 1;

    bucket.insert(&e1, id1);
    assert_eq!(bucket.full_edges, vec![FullEdge::new(e1.offset, b, id1)]);

    // More than full span.
    let e3 = Edge::new(-1, 11, 3);
    let id3 = 3;

    bucket.insert(&e3, id3);
    assert_eq!(
        bucket.full_edges,
        vec![
            FullEdge::new(e1.offset, b, id1),
            FullEdge::new(e3.offset, b, id3)
        ]
    );

    // More than full span.
    let e2 = Edge::new(0, 11, 2);
    let id2 = 2;

    bucket.insert(&e2, id2);
    assert_eq!(
        bucket.full_edges,
        vec![
            FullEdge::new(e1.offset, b, id1),
            FullEdge::new(e2.offset, b, id2),
            FullEdge::new(e3.offset, b, id3)
        ]
    );
}

#[test]
fn test_bucket_insert_partial_span_edges() {
    let mut bucket = Bucket::new(0, 10);

    let e1 = Edge::new(1, 10, 1);
    let id1 = 1;

    bucket.insert(&e1, id1);
    assert_eq!(bucket.partial_edges, vec![(e1.clone(), id1)]);

    let e3 = Edge::new(0, 9, 3);
    let id3 = 3;

    bucket.insert(&e3, id3);
    assert_eq!(
        bucket.partial_edges,
        vec![(e1.clone(), id1), (e3.clone(), id3)]
    );

    let e2 = Edge::new(4, 5, 2);
    let id2 = 2;

    bucket.insert(&e2, id2);
    assert_eq!(
        bucket.partial_edges,
        vec![(e1.clone(), id1), (e2.clone(), id2), (e3.clone(), id3)]
    );
}

#[test]
fn test_bucket_closest_intersection_full_edges() {
    let mut bucket = Bucket::new(0, 10);

    let e1 = Edge::new(0, 10, 1);
    let id1 = 1;
    bucket.insert(&e1, id1);

    let e3 = Edge::new(-1, 11, 3);
    let id3 = 3;
    bucket.insert(&e3, id3);

    let e2 = Edge::new(0, 11, 2);
    let id2 = 2;
    bucket.insert(&e2, id2);

    let bucket = bucket; // Make immutable.

    let (below, exact, above) = bucket.find_closest_line_intersections(0, 5);
    assert_eq!(below, vec![]);
    assert_eq!(exact, vec![]);
    assert_eq!(above, vec![id1]);

    let (below, exact, above) = bucket.find_closest_line_intersections(1, 5);
    assert_eq!(below, vec![]);
    assert_eq!(exact, vec![id1]);
    assert_eq!(above, vec![id2]);

    let (below, exact, above) = bucket.find_closest_line_intersections(2, 5);
    assert_eq!(below, vec![id1]);
    assert_eq!(exact, vec![id2]);
    assert_eq!(above, vec![id3]);

    let (below, exact, above) = bucket.find_closest_line_intersections(3, 5);
    assert_eq!(below, vec![id2]);
    assert_eq!(exact, vec![id3]);
    assert_eq!(above, vec![]);

    let (below, exact, above) = bucket.find_closest_line_intersections(4, 5);
    assert_eq!(below, vec![id3]);
    assert_eq!(exact, vec![]);
    assert_eq!(above, vec![]);
}

#[test]
fn test_bucket_beam_intersection_full_edges() {
    let mut bucket = Bucket::new(0, 10);

    let e1 = Edge::new(0, 10, 1);
    let id1 = 1;
    bucket.insert(&e1, id1);

    let e3 = Edge::new(-1, 11, 3);
    let id3 = 3;
    bucket.insert(&e3, id3);

    let e2 = Edge::new(0, 11, 2);
    let id2 = 2;
    bucket.insert(&e2, id2);

    let bucket = bucket; // Make immutable.

    let (below, exact, above) = bucket.find_beam_intersections(2, 5, 0);
    assert_eq!(below.collect_vec(), vec![(e1.offset, id1)]);
    assert_eq!(exact, vec![id2]);
    assert_eq!(above.collect_vec(), vec![(e3.offset, id3)]);
}

#[test]
fn test_bucket_beam_intersection_partial_edges() {
    let mut bucket = Bucket::new(0, 10);

    let e1 = Edge::new(0, 1, 1);
    let id1 = 1;
    bucket.insert(&e1, id1);

    let e2 = Edge::new(3, 4, 2);
    let id2 = 2;
    bucket.insert(&e2, id2);

    let e3 = Edge::new(1, 3, 3);
    let id3 = 3;
    bucket.insert(&e3, id3);

    let bucket = bucket; // Make immutable.

    let (below, exact, above) = bucket.find_beam_intersections(2, 2, 1);
    assert_eq!(below.collect_vec(), vec![(e1.offset, id1)]);
    assert_eq!(exact, vec![id2]);
    assert_eq!(above.collect_vec(), vec![(e3.offset, id3)]);
}

#[test]
fn test_bucket_closest_intersection_partial_edges() {
    let mut bucket = Bucket::new(0, 10);

    let e1 = Edge::new(1, 9, 1);
    let id1 = 1;
    bucket.insert(&e1, id1);

    let e2 = Edge::new(2, 9, 2);
    let id2 = 2;
    bucket.insert(&e2, id2);

    let e3 = Edge::new(3, 9, 3);
    let id3 = 3;
    bucket.insert(&e3, id3);

    // Add some edges that will be not intersecting with x = 5.
    let e4 = Edge::new(8, 9, 0);
    let id4 = 4;
    bucket.insert(&e4, id4);

    let e5 = Edge::new(8, 9, 1);
    let id5 = 5;
    bucket.insert(&e5, id5);

    let bucket = bucket; // Make immutable.

    let (below, exact, above) = bucket.find_closest_line_intersections(0, 5);
    assert_eq!(below, vec![]);
    assert_eq!(exact, vec![]);
    assert_eq!(above, vec![id1]);

    let (below, exact, above) = bucket.find_closest_line_intersections(1, 5);
    assert_eq!(below, vec![]);
    assert_eq!(exact, vec![id1]);
    assert_eq!(above, vec![id2]);

    let (below, exact, above) = bucket.find_closest_line_intersections(2, 5);
    assert_eq!(below, vec![id1]);
    assert_eq!(exact, vec![id2]);
    assert_eq!(above, vec![id3]);

    let (below, exact, above) = bucket.find_closest_line_intersections(3, 5);
    assert_eq!(below, vec![id2]);
    assert_eq!(exact, vec![id3]);
    assert_eq!(above, vec![]);

    let (below, exact, above) = bucket.find_closest_line_intersections(4, 5);
    assert_eq!(below, vec![id3]);
    assert_eq!(exact, vec![]);
    assert_eq!(above, vec![]);
}

#[test]
fn test_bucket_closest_intersection_mixed_edges() {
    let mut bucket = Bucket::new(0, 10);

    let e1 = Edge::new(0, 10, 1);
    let id1 = 1;
    bucket.insert(&e1, id1);

    let e2 = Edge::new(2, 9, 2);
    let id2 = 2;
    bucket.insert(&e2, id2);

    let e3 = Edge::new(-1, 11, 3);
    let id3 = 3;
    bucket.insert(&e3, id3);

    // Add some edges that will be not intersecting with x = 5.
    let e4 = Edge::new(8, 9, 0);
    let id4 = 4;
    bucket.insert(&e4, id4);

    let e5 = Edge::new(8, 9, 1);
    let id5 = 5;
    bucket.insert(&e5, id5);

    let bucket = bucket; // Make immutable.

    let (below, exact, above) = bucket.find_closest_line_intersections(0, 5);
    assert_eq!(below, vec![]);
    assert_eq!(exact, vec![]);
    assert_eq!(above, vec![id1]);

    let (below, exact, above) = bucket.find_closest_line_intersections(1, 5);
    assert_eq!(below, vec![]);
    assert_eq!(exact, vec![id1]);
    assert_eq!(above, vec![id2]);

    let (below, exact, above) = bucket.find_closest_line_intersections(2, 5);
    assert_eq!(below, vec![id1]);
    assert_eq!(exact, vec![id2]);
    assert_eq!(above, vec![id3]);

    let (below, exact, above) = bucket.find_closest_line_intersections(3, 5);
    assert_eq!(below, vec![id2]);
    assert_eq!(exact, vec![id3]);
    assert_eq!(above, vec![]);

    let (below, exact, above) = bucket.find_closest_line_intersections(4, 5);
    assert_eq!(below, vec![id3]);
    assert_eq!(exact, vec![]);
    assert_eq!(above, vec![]);
}

#[test]
fn test_bucket_closest_intersection_mixed_edges_multi() {
    let mut bucket = Bucket::new(0, 10);

    let e1 = Edge::new(0, 10, 1);
    let id1 = 1;
    bucket.insert(&e1, id1);

    let e2 = Edge::new(2, 9, 2);
    let id2 = 2;
    bucket.insert(&e2, id2);

    let e3 = Edge::new(-1, 11, 3);
    let id3 = 3;
    bucket.insert(&e3, id3);

    let e4 = Edge::new(2, 11, 1);
    let id4 = 4;
    bucket.insert(&e4, id4);

    let e5 = Edge::new(1, 9, 3);
    let id5 = 5;
    bucket.insert(&e5, id5);

    let bucket = bucket; // Make immutable.

    let (below, exact, above) = bucket.find_closest_line_intersections(0, 5);
    assert_eq!(below, vec![]);
    assert_eq!(exact, vec![]);
    assert_eq!(above, vec![id1, id4]);

    let (below, exact, above) = bucket.find_closest_line_intersections(1, 5);
    assert_eq!(below, vec![]);
    assert_eq!(exact, vec![id1, id4]);
    assert_eq!(above, vec![id2]);

    let (below, exact, above) = bucket.find_closest_line_intersections(2, 5);
    assert_eq!(below, vec![id1, id4]);
    assert_eq!(exact, vec![id2]);
    assert_eq!(above, vec![id3, id5]);

    let (below, exact, above) = bucket.find_closest_line_intersections(3, 5);
    assert_eq!(below, vec![id2]);
    assert_eq!(exact, vec![id3, id5]);
    assert_eq!(above, vec![]);

    let (below, exact, above) = bucket.find_closest_line_intersections(4, 5);
    assert_eq!(below, vec![id3, id5]);
    assert_eq!(exact, vec![]);
    assert_eq!(above, vec![]);
}

#[test]
fn test_bucket_contains_point_full_edges() {
    let mut bucket = Bucket::new(0, 10);

    assert_eq!(bucket.contains_point(5, 2), ContainsResult::No);

    let e = Edge::new_boundary(0, 10, 1, BoundaryType::Lower);
    bucket.insert(&e, 1);

    let e = Edge::new_boundary(0, 10, 3, BoundaryType::Upper);
    bucket.insert(&e, 2);

    let e = Edge::new_boundary(0, 10, 10, BoundaryType::Lower);
    bucket.insert(&e, 3);

    let e = Edge::new_boundary(0, 10, 12, BoundaryType::Upper);
    bucket.insert(&e, 4);

    let e = Edge::new_boundary(0, 10, 12, BoundaryType::Lower);
    bucket.insert(&e, 5);

    let e = Edge::new_boundary(0, 10, 13, BoundaryType::Upper);
    bucket.insert(&e, 6);

    assert_eq!(bucket.contains_point(5, 0), ContainsResult::No);
    assert_eq!(bucket.contains_point(5, 1), ContainsResult::OnBounds);
    assert_eq!(bucket.contains_point(5, 2), ContainsResult::WithinBounds);
    assert_eq!(bucket.contains_point(5, 3), ContainsResult::OnBounds);
    assert_eq!(bucket.contains_point(5, 4), ContainsResult::No);

    assert_eq!(bucket.contains_point(5, 11), ContainsResult::WithinBounds);
    assert_eq!(bucket.contains_point(5, 12), ContainsResult::WithinBounds);
}

#[test]
fn test_bucket_contains_point_partial_edges() {
    let mut bucket = Bucket::new(0, 10);

    assert_eq!(bucket.contains_point(5, 2), ContainsResult::No);

    let e = Edge::new_boundary(4, 6, 1, BoundaryType::Lower);
    bucket.insert(&e, 1);

    let e = Edge::new_boundary(4, 6, 3, BoundaryType::Upper);
    bucket.insert(&e, 2);

    let e = Edge::new_boundary(0, 2, -100, BoundaryType::Lower);
    bucket.insert(&e, 1);

    let e = Edge::new_boundary(0, 2, -50, BoundaryType::Upper);
    bucket.insert(&e, 2);

    assert_eq!(bucket.contains_point(5, 0), ContainsResult::No);
    assert_eq!(bucket.contains_point(5, 1), ContainsResult::OnBounds);
    assert_eq!(bucket.contains_point(5, 2), ContainsResult::WithinBounds);
    assert_eq!(bucket.contains_point(5, 3), ContainsResult::OnBounds);
    assert_eq!(bucket.contains_point(5, 4), ContainsResult::No);

    assert_eq!(bucket.contains_point(2, 2), ContainsResult::No);
}

#[test]
fn test_bucket_contains_point_partial_edges_over_endpoints() {
    // Test if the start points and end points of edges are properly counted.
    // A start point should be counted, but not an end point.

    let mut bucket = Bucket::new(0, 10);

    assert_eq!(bucket.contains_point(5, 2), ContainsResult::No);

    let e = Edge::new_boundary(1, 5, 1, BoundaryType::Lower);
    bucket.insert(&e, 1);

    let e = Edge::new_boundary(5, 9, 3, BoundaryType::Upper);
    bucket.insert(&e, 2);

    let e = Edge::new_boundary(5, 9, 13, BoundaryType::Lower);
    bucket.insert(&e, 1);

    let e = Edge::new_boundary(1, 5, 1, BoundaryType::Upper);
    bucket.insert(&e, 2);

    assert_eq!(bucket.contains_point(5, 0), ContainsResult::No);
    assert_eq!(bucket.contains_point(5, 1), ContainsResult::No);
    assert_eq!(bucket.contains_point(5, 3), ContainsResult::OnBounds);
    assert_eq!(bucket.contains_point(5, 4), ContainsResult::WithinBounds);
    assert_eq!(bucket.contains_point(5, 11), ContainsResult::WithinBounds);
    assert_eq!(bucket.contains_point(5, 12), ContainsResult::WithinBounds);
    assert_eq!(bucket.contains_point(5, 13), ContainsResult::OnBounds);
    assert_eq!(bucket.contains_point(5, 14), ContainsResult::No);
}
