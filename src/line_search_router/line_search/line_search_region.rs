// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Fast search for closest intersecting lines.
//! All obstacle line segments are parallel to each other and arranged in a pre-computed data
//! structure. Given a line `l` orthogonal to the obstacle lines through a point `p` the intersection of
//! `l` with the obstacles closest to `p` can be found efficiently.
//!

use iron_shapes::point::Point;
use iron_shapes::redge::{REdge, REdgeOrientation};
use iron_shapes::simple_rpolygon::SimpleRPolygon;
use iron_shapes::types::ContainsResult;
use itertools::Itertools;
use num_traits::PrimInt;
use std::collections::HashMap;

use super::edge::{BoundaryType, Edge};
use super::line_search::LineSearch;

/// Identifier for edges.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct EdgeId(pub(super) usize);

/// Provides look-up of close intersections among rectilinear edges.
#[derive(Clone)]
pub struct LineSearchRegion<T: PrimInt> {
    /// Lookup-structure for horizontal lines.
    horizontal: LineSearch<T, EdgeId>,
    /// Lookup-structure for vertical lines.
    vertical: LineSearch<T, EdgeId>,
    /// Counter for generating unique IDs for inserted edges.
    edge_id_counter: usize,
    /// Lookup table for finding the actual edges based on the ID.
    edges: HashMap<EdgeId, REdge<T>>,
}

impl<T: PrimInt> LineSearchRegion<T> {
    /// Create a new empty line search region.
    ///
    /// # Parameters
    ///
    /// * `max_partial_edges_per_bucket`: Number of partial edges a bucket is allowed to hold before being split.
    ///
    /// The intersection lookup is in the worst case proportional to `max_partial_edges_per_bucket`.
    /// Small values increase the memory usage and insertion time but lead to a faster lookup.
    pub fn new(max_partial_edges_per_bucket: usize) -> Self {
        Self {
            horizontal: LineSearch::new(max_partial_edges_per_bucket),
            vertical: LineSearch::new(max_partial_edges_per_bucket),
            edge_id_counter: 0,
            edges: Default::default(),
        }
    }

    /// Insert a horizontal or vertical edge into the line search region.
    ///
    /// Edges that are oriented from left to right (start < end) are treated
    /// as lower boundaries of a polygon, edges from right to left (end < start)
    /// are treated as upper boundaries of a polygon.
    ///
    /// TODO: Insert in bulk. Inserting edges in bulk allows them to be sorted before insertion.
    /// This makes the insertion more efficient.
    ///
    /// Returns an ID associated with this edges.
    ///
    /// # Parameters
    ///
    /// * `is_polygon_boundary`: If set to `true`, the edge will be treated as a polygon boundary.
    pub fn insert_edge(&mut self, edge: REdge<T>, is_polygon_boundary: bool) -> EdgeId {
        // Create a unique ID for the edge.
        let id = EdgeId(self.edge_id_counter);
        self.edge_id_counter += 1;

        // Sort start and end point of the edge.
        // Assign the polygon boundary type based on the orientation of the edge.
        let e = if edge.start < edge.end {
            let boundary = if is_polygon_boundary {
                match edge.orientation {
                    REdgeOrientation::Horizontal => BoundaryType::Lower,
                    REdgeOrientation::Vertical => BoundaryType::Upper,
                }
            } else {
                BoundaryType::None
            };
            Edge::new_boundary(edge.start, edge.end, edge.offset, boundary)
        } else {
            let boundary = if is_polygon_boundary {
                match edge.orientation {
                    REdgeOrientation::Horizontal => BoundaryType::Upper,
                    REdgeOrientation::Vertical => BoundaryType::Lower,
                }
            } else {
                BoundaryType::None
            };
            Edge::new_boundary(edge.end, edge.start, edge.offset, boundary)
        };

        match edge.orientation {
            REdgeOrientation::Horizontal => self.horizontal.insert(e, id),
            REdgeOrientation::Vertical => self.vertical.insert(e, id),
        };

        // Remember the edge to be able to find it later based on the ID.
        self.edges.insert(id, edge);

        id
    }

    /// Insert all edges of a rectilinear polygon into the line search region.
    /// Returns a `Vec` with all edge IDs associated with the edges of this polygon.
    /// The order of the edge IDs corresponds to the order `polygon.edges()`.
    pub fn insert_polygon(&mut self, polygon: &SimpleRPolygon<T>) -> Vec<EdgeId> {
        // Insert all edges into a line search structure.
        polygon.edges().map(|e| self.insert_edge(e, true)).collect()
    }

    /// Do a line search from the point `start` into two opposing directions. The directions
    /// are specified by `line_orientation` and can be either up/down (vertical) or left/right (horizontal).
    ///
    /// Returns the closest intersecting objects.
    pub fn closest_line_intersection(
        &self,
        start: Point<T>,
        line_orientation: REdgeOrientation,
    ) -> (Vec<REdge<T>>, Vec<REdge<T>>, Vec<REdge<T>>) {
        self.closest_beam_intersection(start, line_orientation, T::zero())
    }

    /// Do a beam search from the point `start` into two opposing directions. The directions
    /// are specified by `line_orientation` and can be either up/down (vertical) or left/right (horizontal).
    ///
    /// Returns the closest intersecting objects.
    pub fn closest_beam_intersection(
        &self,
        start: Point<T>,
        line_orientation: REdgeOrientation,
        beam_half_width: T,
    ) -> (Vec<REdge<T>>, Vec<REdge<T>>, Vec<REdge<T>>) {
        let (prev, hit, next) = self.beam_intersection(start, line_orientation, beam_half_width);

        let id_to_edge = |id: &EdgeId| -> REdge<T> {
            let offset = self.edges[id].offset;
            let intersection_point = match line_orientation {
                REdgeOrientation::Horizontal => Point::new(offset, start.y),
                REdgeOrientation::Vertical => Point::new(start.x, offset),
            };
            // Return intersection point and the edge that was hit.
            self.edges[id]
        };

        // Edges with zero distance.
        let hit = hit.iter().map(id_to_edge).collect();

        // Find the closest intersections by grouping the sorted intersection by their
        // distance to the extension point. Then taking the first group.
        let closest_next = next
            .group_by(|(off, _id)| *off)
            .into_iter()
            .map(|(key, group)| group.map(|(_y, id)| id_to_edge(&id)).collect())
            .next()
            .unwrap_or(vec![]);

        // Find the closest intersections by grouping the sorted intersection by their
        // distance to the extension point. Then taking the first group.
        let closest_prev = prev
            .group_by(|(off, _id)| *off)
            .into_iter()
            .map(|(key, group)| group.map(|(_y, id)| id_to_edge(&id)).collect())
            .next()
            .unwrap_or(vec![]);

        // Convert the found IDs into points.
        (closest_prev, hit, closest_next)
    }

    /// Find the intersections of a beam through `start` and orientation `line_orientation`.
    ///
    /// Returns a tuple `(previous, hit, next)`.
    /// Where
    /// * `previous` is an iterator over all the intersections encountered when travelling towards negative coordinates.
    /// * `hit` is a list of all intersections at the `start` point.
    /// * `next` is an iterator over all the intersections encountered when travelling towards positive coordinates.
    ///
    pub fn beam_intersection(
        &self,
        start: Point<T>,
        line_orientation: REdgeOrientation,
        beam_half_width: T,
    ) -> (
        impl Iterator<Item = (T, EdgeId)> + '_,
        Vec<EdgeId>,
        impl Iterator<Item = (T, EdgeId)> + '_,
    ) {
        match line_orientation {
            REdgeOrientation::Vertical => {
                self.horizontal
                    .beam_intersection(start.x, start.y, beam_half_width)
            }
            REdgeOrientation::Horizontal => {
                self.vertical
                    .beam_intersection(start.y, start.x, beam_half_width)
            }
        }
    }

    /// Test if the rectangle overlaps with the polygons in this line search region.
    /// Zero-area overlaps are not considered to be an overlap,
    /// e.g. touching at boundaries is not considered an overlap.
    /// The rectangle is represented by its `center`, the half width and the half height.
    pub fn overlaps_rect(&self, center: Point<T>, half_width: T, half_height: T) -> bool {
        // Algorithm: check if one point of the rectangle lies inside the polygons,
        // for instance the lower left corner. If it does, the rectangle touches the
        // polygon. If it does not, then use a beam search to find any polygon boundaries
        // withing the rectangle. If there are any, the rectangle touches the polygons,
        // if there are none, the rectangle does not touch the polygons.
        // TODO: Need to be able to ignore non-boundary edges in the beam search.
        assert!(half_width > T::zero(), "Width must be positive.");
        assert!(half_height > T::zero(), "Height must be positive.");

        let touches_point = self.contains_point(center.x, center.y);

        if !touches_point.is_no() {
            true
        } else {
            // Do a vertical beam search.
            let (_, _, next_vertical) = self.beam_intersection(
                Point::new(center.x, center.y - half_height),
                REdgeOrientation::Vertical,
                half_width - T::one(),
            );

            // Test if there's any horizontal line crossing the rectangle. (Using a vertical beam search).
            let v_intersection = next_vertical
                .take_while(|(off, _)| *off < center.y + half_height)
                .next()
                .is_some();

            if v_intersection {
                true
            } else {
                // Do a horizontal beam search.
                let (_, _, next_horizontal) = self.beam_intersection(
                    Point::new(center.x - half_width, center.y),
                    REdgeOrientation::Horizontal,
                    half_height - T::one(),
                );

                // Test if there's any vertical line crossing the rectangle.
                let h_intersection = next_horizontal
                    .take_while(|(off, _)| *off < center.x + half_width)
                    .next()
                    .is_some();

                h_intersection
            }
        }
    }

    /// Check if the point is contained in one or more polygons.
    pub fn contains_point(&self, x: T, y: T) -> ContainsResult {
        let result = self.horizontal.contains_point(x, y);
        // debug_assert_eq!(result.is_no(), self.vertical.contains_point(y, x).is_no());

        // Vertical edges are in the horizontal buckets not necessarily recognized as 'OnBound'.
        // Hence it is necessary to also query the vertical edges and combine the result.
        match result {
            ContainsResult::No => ContainsResult::No,
            ContainsResult::OnBounds => ContainsResult::OnBounds,
            ContainsResult::WithinBounds => {
                // Could still be 'OnBounds'.
                // Query other line orientations to be sure.
                let other = self.vertical.contains_point(y, x);
                debug_assert!(other.inclusive_bounds());
                result.min(other)
            }
        }
    }

    /// Iterate over all edges that that are currently in this region.
    pub fn edges(&self) -> impl Iterator<Item = REdge<T>> + '_ {
        self.edges.values().copied()
    }
}

#[test]
fn test_find_simple_intersection() {
    let mut line_search = LineSearchRegion::new(64);

    let square1 = SimpleRPolygon::try_new(&vec![(0, 0), (0, 10), (10, 10), (10, 0)]).unwrap();
    let square2 = SimpleRPolygon::try_new(&vec![(5, 11), (5, 21), (15, 21), (15, 11)]).unwrap();

    line_search.insert_polygon(&square1);
    line_search.insert_polygon(&square2);

    line_search.closest_line_intersection(Point::new(1, -1), REdgeOrientation::Vertical);
    line_search.closest_line_intersection(Point::new(11, -1), REdgeOrientation::Vertical);
}

#[test]
fn test_polygon_contains_point() {
    let mut line_search = LineSearchRegion::new(64);

    let square1 = SimpleRPolygon::try_new(&vec![(0, 0), (10, 0), (10, 10), (0, 10)]).unwrap();
    let square2 = SimpleRPolygon::try_new(&vec![(5, 11), (15, 11), (15, 21), (5, 21)]).unwrap();

    line_search.insert_polygon(&square1);
    line_search.insert_polygon(&square2);

    // dbg!(&line_search.horizontal.buckets);
    // dbg!(&line_search.vertical.buckets);

    assert_eq!(line_search.contains_point(-1, 0), ContainsResult::No);
    assert_eq!(line_search.contains_point(0, 0), ContainsResult::OnBounds);

    assert_eq!(line_search.contains_point(1, 0), ContainsResult::OnBounds);
    assert_eq!(line_search.contains_point(0, 1), ContainsResult::OnBounds);

    assert_eq!(
        line_search.contains_point(1, 1),
        ContainsResult::WithinBounds
    );

    assert_eq!(line_search.contains_point(4, 11), ContainsResult::No);

    assert_eq!(line_search.contains_point(4, 12), ContainsResult::No);

    assert_eq!(
        line_search.contains_point(6, 12),
        ContainsResult::WithinBounds
    );
}

#[test]
fn test_overlaps_rect() {
    let mut line_search = LineSearchRegion::new(64);

    let square1 = SimpleRPolygon::try_new(&vec![(0, 0), (10, 0), (10, 10), (0, 10)]).unwrap();

    line_search.insert_polygon(&square1);

    // Test somewhere far off the polygons.
    assert!(!line_search.overlaps_rect(Point::new(-10, 5), 1, 1));

    // Test just outside boundaries.
    assert!(!line_search.overlaps_rect(Point::new(-2, -2), 2, 2));
    assert!(!line_search.overlaps_rect(Point::new(5, -2), 2, 2));
    assert!(!line_search.overlaps_rect(Point::new(-2, 5), 2, 2));
    assert!(!line_search.overlaps_rect(Point::new(12, 12), 2, 2));
    assert!(!line_search.overlaps_rect(Point::new(12, 5), 2, 2));
    assert!(!line_search.overlaps_rect(Point::new(5, 12), 2, 2));

    // Test intersecting with boundaries.
    assert!(line_search.overlaps_rect(Point::new(0, 0), 1, 1));
    assert!(line_search.overlaps_rect(Point::new(5, -1), 1, 2));
    assert!(line_search.overlaps_rect(Point::new(-1, 5), 2, 1));
    assert!(line_search.overlaps_rect(Point::new(5, 11), 1, 2));
    assert!(line_search.overlaps_rect(Point::new(11, 5), 2, 1));

    // Test fully inside.
    assert!(line_search.overlaps_rect(Point::new(5, 5), 1, 1));
}

/// Find the closest beam intersections among many line search regions.
/// Return the intersecting edges together with the ID of the search region.
pub fn multi_beam_intersection<'a, T: PrimInt>(
    regions: &'a [&'a LineSearchRegion<T>],
    start: Point<T>,
    line_orientation: REdgeOrientation,
    beam_half_width: T,
) -> (
    impl Iterator<Item = (REdge<T>, usize)> + 'a,
    Vec<(REdge<T>, usize)>,
    impl Iterator<Item = (REdge<T>, usize)> + 'a,
) {
    let mut prevs = Vec::new();
    let mut here = Vec::new();
    let mut nexts = Vec::new();

    // Search for an intersection in the buckets of interest.
    // Then take the closest results.
    for (i, region) in regions.iter().enumerate() {
        let (prev, hit, next) = region.beam_intersection(start, line_orientation, beam_half_width);

        // Intersections directly at (x, y) always have the same distance (0).
        // Hence we take all of them.
        let hit = hit.iter().map(|id| (region.edges[id], i));
        here.extend(hit);

        // Map to edges instead of IDs.
        let prev = prev.map(move |(off, id)| (region.edges[&id], i));
        let next = next.map(move |(off, id)| (region.edges[&id], i));

        prevs.push(prev);
        nexts.push(next);
    }

    // Merge the results while keeping the ordering.
    let prevs = prevs.into_iter().kmerge_by(|a, b| a.0.offset > b.0.offset);
    let nexts = nexts.into_iter().kmerge_by(|a, b| a.0.offset < b.0.offset);

    (prevs, here, nexts)
}

/// Do a beam search in multiple regions
/// from the point `start` into two opposing directions. The directions
/// are specified by `line_orientation` and can be either up/down (vertical) or left/right (horizontal).
///
/// Returns the closest intersecting objects together with the index of their region.
pub fn multi_closest_beam_intersection<'a, T: PrimInt>(
    regions: &'a [&'a LineSearchRegion<T>],
    start: Point<T>,
    line_orientation: REdgeOrientation,
    beam_half_width: T,
) -> (
    Vec<(REdge<T>, usize)>,
    Vec<(REdge<T>, usize)>,
    Vec<(REdge<T>, usize)>,
) {
    let (prev, hit, next) =
        multi_beam_intersection(regions, start, line_orientation, beam_half_width);

    // Find the closest intersections by grouping the sorted intersection by their
    // distance to the extension point. Then taking the first group.
    let closest_next = next
        .group_by(|(e, _id)| e.offset)
        .into_iter()
        .map(|(key, group)| group.collect())
        .next()
        .unwrap_or(vec![]);

    // Find the closest intersections by grouping the sorted intersection by their
    // distance to the extension point. Then taking the first group.
    let closest_prev = prev
        .group_by(|(e, _id)| e.offset)
        .into_iter()
        .map(|(key, group)| group.collect())
        .next()
        .unwrap_or(vec![]);

    // Convert the found IDs into points.
    (closest_prev, hit, closest_next)
}
