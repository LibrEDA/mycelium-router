// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Representation of either horizontal or vertical line segments (edges).

use std::cmp::Ordering;

/// Specifies whether an edge is a upper or lower boundary of a polygon
/// (as seen in the direction of the scan beam). This is used to keep track
/// of what is the outside or inside of polygons.
/// If the edge is not part of a polygon, the boundary type can also be `None`.
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub(crate) enum BoundaryType {
    None,
    Lower,
    Upper,
}

impl PartialOrd for BoundaryType {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for BoundaryType {
    /// Boundaries are ordered as follows: `Lower` < `None` < `Upper`.
    fn cmp(&self, other: &Self) -> Ordering {
        if self == other {
            Ordering::Equal
        } else {
            match (self, other) {
                (BoundaryType::Upper, _) => Ordering::Greater,
                (_, BoundaryType::Upper) => Ordering::Less,
                (BoundaryType::Lower, _) => Ordering::Less,
                (_, BoundaryType::Lower) => Ordering::Greater,
                _ => Ordering::Equal,
            }
        }
    }
}

/// Represents a segment of the line which has `offset` distance to the origin.
/// `start` and `end` are the `x` or `y` coordinates of the start and end-point.
/// The orientation of the line is given by the context. In practice the orientation is likely
/// either horizontal or vertical.
#[derive(Debug, Clone, Eq, PartialEq)]
pub(crate) struct Edge<T> {
    /// Start coordinate of the line segment.
    pub(crate) start: T,
    /// End coordinate of the line segment.
    pub(crate) end: T,
    /// Distance of the line to the origin.
    pub(crate) offset: T,
    /// Polygon boundary type of this edge.
    pub(crate) boundary_type: BoundaryType,
}

impl<T: PartialOrd> Edge<T> {
    /// Create a new edge which is not marked as a boundary.
    pub(super) fn new(start: T, end: T, offset: T) -> Self {
        Self::new_boundary(start, end, offset, BoundaryType::None)
    }

    /// Create a new edge which is marked as `boundary`.
    pub(super) fn new_boundary(start: T, end: T, offset: T, boundary_type: BoundaryType) -> Self {
        debug_assert!(start < end);
        Self {
            start,
            end,
            offset,
            boundary_type,
        }
    }
}

impl<T: PartialOrd> PartialOrd for Edge<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.offset.partial_cmp(&other.offset)
    }
}

impl<T: Ord + PartialOrd> Ord for Edge<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.offset.cmp(&other.offset)
    }
}
