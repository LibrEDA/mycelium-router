// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Find routes of many signals.

use log;
use num_traits::{One, PrimInt, Signed};
use std::collections::HashMap;
use std::hash::Hash;

use libreda_pnr::db::SInt;
use libreda_pnr::libreda_db::iron_shapes::prelude::*;
use libreda_pnr::route::simple_router::{SimpleRoutedNet, SimpleRouter};

use super::route_single_signal::{route_single_signal, LineSearchLayerStack};

/// Example implementation of a simple detail router.
#[derive(Clone, Debug)]
pub struct SimpleLineSearchRouter {
    /// Half of the wire width.
    /// The same wire width is used on all layers.
    half_wire_width: SInt,
    /// Minimum spacing between metal shapes.
    /// The same spacing rule is used on all routing layers.
    min_spacing: SInt,
    /// Horizontal and vertical wire weights for each layer.
    /// Starting with the lowest routing layer (closest to the transistors).
    /// The cost of a wire segment is computed by the product of the length and the wire weight.
    wire_weights: Vec<(SInt, SInt)>,
}

impl SimpleLineSearchRouter {
    /// Create a new router instance.
    pub fn new(half_wire_width: SInt, min_spacing: SInt, wire_weights: Vec<(SInt, SInt)>) -> Self {
        Self {
            half_wire_width,
            min_spacing,
            wire_weights,
        }
    }
}

impl SimpleRouter for SimpleLineSearchRouter {
    fn name(&self) -> &str {
        "SimpleLineSearchRouter"
    }

    fn compute_routes_impl(
        &self,
        boundary: Rect<SInt>,
        net_terminals: &HashMap<usize, Vec<Vec<(SimpleRPolygon<SInt>, u8)>>>,
        obstacles: &Vec<(SimpleRPolygon<SInt>, u8)>,
    ) -> HashMap<usize, SimpleRoutedNet> {
        // Add boundary as an obstacle.
        let inner_boundary = boundary;
        let outer_boundary = boundary.sized(SInt::one(), SInt::one());

        let inner_boundary = SimpleRPolygon::from(inner_boundary).reversed();
        let outer_boundary = SimpleRPolygon::from(outer_boundary);

        // Create mutable copy of the obstacles.
        let mut obstacles = obstacles.clone();
        // Create core boundary as an obstacle on each layer.
        assert!(self.wire_weights.len() < 256);
        for layer in 0..self.wire_weights.len() {
            obstacles.push((outer_boundary.clone(), layer as u8));
            obstacles.push((inner_boundary.clone(), layer as u8));
        }

        let results = route_multi_signal(
            net_terminals,
            &obstacles,
            self.wire_weights.len() as u8,
            self.half_wire_width,
            self.min_spacing,
            &self.wire_weights,
        );

        // Convert the rectangles into `Geometry`s.

        let geometries = results
            .into_iter()
            .map(|(net, shapes)| {
                let geometries = shapes
                    .into_iter()
                    .map(|(layer, s)| (layer, s.into()))
                    .collect();

                let routed_net = SimpleRoutedNet {
                    routes: geometries,
                    vias: Default::default(), // TODO: Add via locations
                };

                (net, routed_net)
            })
            .collect();

        geometries
    }
}

/// Connect terminals of multiple signals.
fn route_multi_signal<T>(
    net_terminals: &HashMap<usize, Vec<Vec<(SimpleRPolygon<T>, u8)>>>,
    obstacles: &Vec<(SimpleRPolygon<T>, u8)>,
    num_layers: u8,
    half_wire_width: T,
    min_spacing: T,
    wire_weights: &Vec<(T, T)>,
) -> HashMap<usize, Vec<(u8, Rect<T>)>>
where
    T: CoordinateType + PrimInt + Signed + std::fmt::Display + std::fmt::Debug + Hash,
{
    // Create a line search region of obstacles for each layer.
    let mut obstacle_layers = LineSearchLayerStack::new(num_layers as usize);

    // Insert obstacles and remember which IDs are associated with which obstacle.
    let _obstacle_edge_ids: HashMap<_, _> = obstacles
        .iter()
        .flat_map(|(obs, layer)| {
            let ids = obstacle_layers.layers[*layer as usize].insert_polygon(&obs);
            ids.into_iter().map(move |id| (id, (obs, *layer)))
        })
        .collect();

    let num_nets = net_terminals.len();

    // List of nets that could not be routed.
    let mut failed_nets = Vec::new();

    // For each net: Geometries on layers that describe the new wires.
    let mut routing_shapes: HashMap<usize, Vec<_>> = HashMap::new();

    for (i, (net, terminals)) in net_terminals.into_iter().enumerate().take(1000)
    // For debugging.
    {
        log::info!(
            "Route single net {}/{}. ({} terminals)",
            i + 1,
            num_nets,
            terminals.len()
        );

        // dbg!(&terminals);

        let result = route_single_signal(
            terminals,
            &obstacle_layers,
            num_layers,
            half_wire_width,
            min_spacing,
            &wire_weights,
            false,
        );

        // dbg!(&paths);

        // Process result of single signal routing.
        if let Some((paths, shapes)) = result {
            // Treat the newly routed signal as an obstacle.
            for &(layer, rect) in &shapes {
                // obstacles.push((rect.into(), layer));

                let poly: SimpleRPolygon<_> = rect.into();
                obstacle_layers.layers[layer as usize].insert_polygon(&poly);
            }

            routing_shapes.insert(*net, shapes);
        } else {
            failed_nets.push(*net)
        }
    }

    log::warn!("Number of un-routed nets: {}", failed_nets.len());

    routing_shapes
}
