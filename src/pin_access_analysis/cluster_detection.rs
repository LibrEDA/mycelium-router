// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Create clusters of standard cells based on their rows (or columns).
//! The clustering is then needed for generating the cluster access patterns.

use fnv::{FnvHashMap, FnvHashSet};
use itertools::Itertools;

use db::MapPointwise;
use libreda_pnr::db;

use std::cmp::Ord;
use std::collections::{HashMap, HashSet};

/// Configuration struct for the cluster detection algorithm.
pub(crate) struct ClusterDetector<'a, L: db::LayoutIds> {
    /// Find the abutment box of a cell.
    pub cell_outline_fn: Box<dyn Fn(&L::CellId) -> Option<db::Rect<L::Coord>> + 'a>,
}

impl<'a, L: db::LayoutBase> ClusterDetector<'a, L>
where
    L::Coord: Ord,
{
    /// Detect standard-cell rows (or columns) from a bunch of cell instances.
    /// Return the instances grouped by rows (or columns) and sorted by their position in the row.
    ///
    /// Algorithm:
    /// 1) Get the lower-left corners of the cell instances.
    /// 2) Create a set X with the x coordinates of the lower-left coordinates and a set Y with the Y coordinates.
    /// 3) If Y is smaller than X then it is assumed that the cells are on rows, where the values in Y
    /// are the row offsets.
    pub fn detect_clusters(
        &self,
        chip: &L,
        instances: &[L::CellInstId],
    ) -> Vec<Vec<L::CellInstId>> {
        // Sanity check: all cell instances must live in the same parent cell.
        {
            let parent_cells: FnvHashSet<_> = instances
                .iter()
                .map(|inst| chip.parent_cell(inst))
                .collect();

            if parent_cells.len() > 1 {
                let parents = parent_cells
                    .iter()
                    .map(|c| format!("'{}'", chip.cell_name(c)))
                    .join(", ");
                log::error!(
                    "Cell instances must all belong to the same parent cell.\
                 But multiple parents are present: {}",
                    parents
                );
                panic!("cell instances must live in the same parent cell");
            }
        }

        let mut ignored_cells = HashSet::new(); // Keep track of cells without outline.

        let instances_with_outline: Vec<(L::CellInstId, db::Rect<_>)> = instances
            .iter()
            .flat_map(|inst| {
                let cell = chip.template_cell(&inst);
                let outline = (self.cell_outline_fn)(&cell);

                if outline.is_none() {
                    // Keep track of cell without outline.
                    ignored_cells.insert(cell);
                }

                // Transform to the position of the instance.
                let outline_tf = outline.map(|r| {
                    let tf = chip.get_transform(&inst);
                    r.transform(|p| tf.transform_point(p))
                });

                outline_tf.map(|o| (inst.clone(), o))
            })
            .collect();

        // Warn about cells without outline.
        for no_outline in &ignored_cells {
            log::warn!(
                "Cell without outline will not be assigned to clusters: {}",
                chip.cell_name(no_outline)
            );
        }

        // Guess row or column offsets.
        let (is_rows, offsets) = {
            let mut potential_rows: FnvHashSet<_> = Default::default();
            let mut potential_columns: FnvHashSet<_> = Default::default();

            for (_inst, outline) in &instances_with_outline {
                let ll = outline.lower_left();
                potential_rows.insert(ll.y);
                potential_columns.insert(ll.x);
            }

            let is_rows = potential_rows.len() < potential_columns.len();

            let mut offsets: Vec<_> = if is_rows {
                log::info!("Detected {} rows", potential_rows.len());
                potential_rows
            } else {
                log::info!("Detected {} columns", potential_columns.len());
                potential_columns
            }
            .into_iter()
            .collect();

            offsets.sort();

            (is_rows, offsets)
        };

        // Check standard-cell row heights.
        {
            let pitches: FnvHashSet<_> = offsets.windows(2).map(|w| w[1] - w[0]).collect();

            if pitches.len() > 1 {
                log::warn!("Found a non-uniform standard cell pitch.")
            } else {
                log::debug!("Standard-cell row pitch is uniform.");
            }
        }

        // Build clusters.
        let clusters = {
            let mut clusters: FnvHashMap<L::Coord, Vec<(L::CellInstId, _)>> = Default::default();

            for (inst, outline) in instances_with_outline {
                let ll = outline.lower_left();
                let (offset, position) = if is_rows { (ll.y, ll.x) } else { (ll.x, ll.y) };

                clusters
                    .entry(offset)
                    .or_insert(vec![])
                    .push((inst, position));
            }

            // Sort instances in a row from left to right.
            clusters
                .values_mut()
                .for_each(|cluster| cluster.sort_by_key(|(_inst, pos)| *pos));

            // Remove the 'position' attribute. Leave only the instance ID.
            let mut clusters: FnvHashMap<_, _> = clusters
                .into_iter()
                .map(|(k, cluster)| {
                    let only_ids: Vec<L::CellInstId> =
                        cluster.into_iter().map(|(id, _pos)| id).collect();
                    (k, only_ids)
                })
                .collect();

            // Get the row indices and sort them.
            let mut cluster_indices: Vec<_> = clusters.keys().copied().collect();
            cluster_indices.sort();

            // Sort the cluster by their row indices.
            let sorted_clusters: Vec<_> = cluster_indices
                .iter()
                .map(|k| clusters.remove(k).unwrap())
                .collect();

            sorted_clusters
        };

        clusters
    }
}
