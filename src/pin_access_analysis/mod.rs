// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#![deny(missing_docs)]

//! Find non-conflicting locations and directions where a detail router can access pins of cells.
//!
//! # Literature
//! * "The Tao of PAO: Anatomy of a Pin Access Oracle for Detailed Routing", A. Kahng, L. Wang, B. Xu,
//! [PDF](https://vlsicad.ucsd.edu/Publications/Conferences/377/c377.pdf),
//! [archived](https://web.archive.org/web/20220507200541/https://vlsicad.ucsd.edu/Publications/Conferences/377/c377.pdf)

use fnv::FnvHashSet;
use itertools::Itertools;
use log;

mod access_direction;
pub(crate) mod access_pattern;
mod access_point_candidates;
pub(crate) mod cluster_access_pattern;
pub(crate) mod cluster_detection;
pub(crate) mod pin_access_oracle;
mod test_data;
mod tracks;
pub(crate) mod unique_instance;
pub(crate) mod via_extraction;
mod viadef;

// Public exports
pub(crate) use access_pattern::AccessPatternGenerator;
pub(crate) use access_point_candidates::SimplePinAccessPointGenerator;
pub use pin_access_oracle::*;
pub use tracks::Tracks;
pub use viadef::ViaDef;

use access_direction::*;
use unique_instance::*;

use db::traits::*;
use libreda_pnr::db;
use num_traits::{Bounded, Num};

use libreda_drc::*;

use std::collections::HashMap;
use std::sync::Arc;

/// Access location to a pin.
#[derive(Debug)]
pub(super) struct PinAccessPoint<LN>
where
    LN: db::LayoutIds,
{
    /// Layer of the pin.
    pub layer: LN::LayerId,
    /// Location where a via would need to be placed relative to a [`UniqueInstance`].
    pub rel_location: db::Point<LN::Coord>,
    /// Allowed access directions.
    pub access_directions: PinAccessDirections,
    /// List of vias that can be used for access from 'up' direction.
    /// The first via in the list is the preferred via.
    pub possible_vias: Vec<Arc<ViaDef<LN::Coord, LN::LayerId>>>,
    /// Cost of the access point. Access points with lower costs should be prioritized in access patterns.
    pub cost: u32,
}

impl<LN: db::L2NIds> Clone for PinAccessPoint<LN> {
    fn clone(&self) -> Self {
        Self {
            layer: self.layer.clone(),
            rel_location: self.rel_location.clone(),
            access_directions: self.access_directions.clone(),
            possible_vias: self.possible_vias.clone(),
            cost: self.cost.clone(),
        }
    }
}

impl<LN: db::L2NBase> PinAccessPoint<LN>
where
    LN::Coord: db::CoordinateType,
{
    /// Compute the absolute location of the pin access point given a cell instance.
    pub fn absolute_location<C>(
        &self,
        unique_inst: &UniqueInstance<C, LN::Coord>,
    ) -> db::Point<LN::Coord> {
        self.rel_location + unique_inst.transform.transform_point(db::Point::zero()).v()
    }
}

/// Generate pin access point candidates.
pub(super) trait PinAccessPointCandidateGenerator<LN: db::L2NBase> {
    /// Generate candidate access points for the given shape of a pin.
    /// The returned access points are not necessarily DRC clean.
    fn generate_pin_access_point_candidates(
        &self,
        pin_shape: &db::SimpleRPolygon<LN::Coord>,
        layer: &LN::LayerId,
    ) -> Vec<PinAccessPoint<LN>>;
}

/// Possible access points for all pins of a cell.
#[derive(Clone, Debug)]
pub struct PinAccessPoints<LN: db::L2NIds> {
    pub(crate) cell: UniqueInstance<LN::CellId, LN::Coord>,
    pub(crate) access_points: HashMap<LN::PinId, Vec<PinAccessPoint<LN>>>,
}

/// Find valid access points for all pins of the given cell.
///
/// An access point is considered valid if a via can be put there without design rule violations.
///
/// However, the access points can be conflicting. To resolve conflicts, access 'patterns' need to
/// be generated in a further step.
pub(super) fn find_valid_pin_access_points<LN, PGen, Drc>(
    layout: &LN,
    cell: &UniqueInstance<LN::CellId, LN::Coord>,
    viadefs: &[Arc<ViaDef<LN::Coord, LN::LayerId>>],
    candidate_generator: &PGen,
    drc_engine: &Drc,
) -> PinAccessPoints<LN>
where
    LN: db::L2NBase,
    PGen: PinAccessPointCandidateGenerator<LN>,
    Drc: DrcEngine<LN>,
{
    log::debug!(
        "find pin access points for cell '{}'",
        layout.cell_name(&cell.cell_id)
    );

    let max_accesspoints = 10;

    if viadefs.is_empty() {
        log::warn!("No vias are defined. Will not find any valid access points.");
    }

    let mut all_candidates = Vec::new();

    // Find access points for all pin shapes.
    for pin in layout.each_pin(&cell.cell_id) {
        let mut num_candidates_for_pin = 0; // Count access point candidates. Used to emit a warning if no access points are found.

        for pin_shape_id in layout.shapes_of_pin(&pin) {
            layout.with_shape( // Access the pin geometry.
                               &pin_shape_id,
                               |layer, geometry: &db::Geometry<_>| {
                                   let geometry = geometry.transformed(&cell.transform);

                                   // Convert to rectilinear polygon.
                                   let polygon: Option<db::SimpleRPolygon<_>> = match geometry {
                                       db::Geometry::SimpleRPolygon(p) => Some(p.clone()),
                                       db::Geometry::Rect(p) => Some(p.clone().into()),
                                       db::Geometry::SimplePolygon(p) => {
                                           // Try to convert into rectilinear polygon.
                                           db::SimpleRPolygon::try_new(p.points())
                                       }
                                       _ => {
                                           log::error!("only rectilinear polygons without holes are supported for pin access analysis");
                                           None
                                       }
                                   };

                                   // If a valid pin geometry was found: use it to find pin access points.
                                   if let Some(polygon) = polygon {
                                       // Transform to position of the instance.
                                       // let polygon = polygon.transformed(&cell.transform);

                                       // Find access points for this shape.
                                       let mut candidates = candidate_generator.generate_pin_access_point_candidates(
                                           &polygon,
                                           layer,
                                       );

                                       num_candidates_for_pin += candidates.len(); // Keep track of the number of candidates.

                                       candidates.sort_by_key(|ap| ap.cost); // Prioritize access points by cost.

                                       all_candidates.extend(
                                           candidates.into_iter()
                                               // Take a limited number of candidates only.
                                               .take(max_accesspoints)
                                               .map(|p| (pin.clone(), p))
                                       );
                                   }
                               });

            // Print a warning if a pin has no access point candidates.
            if num_candidates_for_pin == 0 {
                log::warn!(
                    "pin has no access point candidates: '{}/{}'",
                    layout.cell_name(&cell.cell_id),
                    layout.pin_name(&pin)
                );
            }
        }
    }

    // Check if the cell itself is DRC clean (without any access vias).
    let do_differential_drc = {
        // Find layers used by access points.
        let layers_of_interest: Vec<_> = all_candidates
            .iter()
            .map(|(_, access_point)| &access_point.layer)
            .unique()
            .cloned()
            .collect();

        let num_drc_violations = is_cell_drc_clean(layout, cell, &layers_of_interest, drc_engine);

        if num_drc_violations > 0 {
            log::warn!(
                "Cell '{}' has {} DRC violations. This will impact the selection of access points.",
                layout.cell_name(&cell.cell_id),
                num_drc_violations
            );
        }

        num_drc_violations > 0
    };

    // Use only access points which allow the DRC-clean placement of a via.
    let valid_candidates = all_candidates
        .into_iter()
        // Find valid vias for access points.
        .map(|(pin_id, mut access_point)| {
            let valid_vias_with_rating = find_valid_vias_for_access_point(
                layout,
                cell,
                &pin_id,
                &viadefs,
                &access_point,
                drc_engine,
                do_differential_drc,
            );

            let best_rating = valid_vias_with_rating
                .iter()
                .map(|(_, r)| *r)
                .min()
                .unwrap_or(u32::max_value());

            // Strip away the rating.
            let valid_vias = valid_vias_with_rating.into_iter().map(|(v, _)| v).collect();
            access_point.possible_vias = valid_vias;
            access_point.cost += best_rating;

            (pin_id, access_point)
        })
        .filter(|(_pin_id, ap)| !ap.possible_vias.is_empty());

    // Group the results by the pin ID.
    let access_points_per_pin: HashMap<_, _> = {
        let mut access_points_per_pin = HashMap::new();
        for (pin_id, access_point) in valid_candidates {
            access_points_per_pin
                .entry(pin_id)
                .or_insert(vec![])
                .push((access_point));
        }
        access_points_per_pin
    };

    for pin_id in layout.each_pin(&cell.cell_id) {
        let num_access_points = access_points_per_pin
            .get(&pin_id)
            .map(|aps| aps.len())
            .unwrap_or(0);

        if num_access_points == 0 {
            log::warn!(
                "pin has no valid access points: '{}/{}'",
                layout.cell_name(&cell.cell_id),
                layout.pin_name(&pin_id)
            );
        }
    }

    PinAccessPoints {
        cell: cell.clone(),
        access_points: access_points_per_pin,
    }
}

/// Find all vias that can be placed at the access point without causing design rule violations.
/// Returned vias are sorted by their rating. The first is the best.
///
/// # Parameters
/// * `do_differential_drc`: Allow the cell to have DRC violations itself.
/// Find access points which do not add more DRC violations on top.
fn find_valid_vias_for_access_point<LN, Drc>(
    layout: &LN,
    cell: &UniqueInstance<LN::CellId, LN::Coord>,
    pin_id: &LN::PinId,
    vias: &[Arc<ViaDef<LN::Coord, LN::LayerId>>],
    access_point: &PinAccessPoint<LN>,
    drc_engine: &Drc,
    do_differential_drc: bool,
) -> Vec<(Arc<ViaDef<LN::Coord, LN::LayerId>>, u32)>
where
    LN: db::L2NBase,
    Drc: DrcEngine<LN>,
{
    let rated_vias = vias
        .iter()
        // Look only at vias which start at the right layer.
        .filter(|via| via.low_layer == access_point.layer)
        // Take only vias which pass the DRC check.
        .filter_map(|via| {
            let rating = is_valid_access_point(
                layout,
                cell,
                pin_id,
                via,
                access_point,
                drc_engine,
                do_differential_drc,
            );
            // Drop invalid vias (with rating = None).
            rating.map(|r| (via.clone(), r))
        });

    rated_vias.sorted_by_key(|(_, rating)| *rating).collect()
}

/// Check if the cell is DRC clean (without any access vias).
/// Used as a sanity check to see beforehand if it is impossible to find DRC-clean access points.
///
/// Return the number of DRC violations.
fn is_cell_drc_clean<LN, Drc>(
    layout: &LN,
    cell: &UniqueInstance<LN::CellId, LN::Coord>,
    layers_of_interest: &[LN::LayerId],
    drc_engine: &Drc,
) -> usize
where
    LN: db::L2NBase,
    Drc: DrcEngine<LN>,
{
    let flat_shapes =
        extract_shapes_from_cell(layout, &cell.cell_id, layers_of_interest, cell.transform);

    // Use the DRC engine to check if a via can be put at this access point.
    let drc_result = drc_engine
        .flat_check(&flat_shapes)
        .map_err(|_| log::error!("DRC engine failed"))
        .ok() // convert to Option
        .expect("DRC engine failed"); // TODO: Pass the error if the DRC engine fails.

    log::debug!(
        "Num DRC violations in '{}': {}",
        layout.cell_name(&cell.cell_id),
        drc_result.num_violations()
    );

    drc_result.num_violations()
}

/// Check if a via can be placed on this access point without violating design rules.
/// Give a cost rating to valid vias.
/// Return `None` for invalid vias.
///
/// # Parameters
/// * `do_differential_drc`: Allow the cell to have DRC violations itself.
/// Find access points which do not add more DRC violations on top.
fn is_valid_access_point<LN, Drc>(
    layout: &LN,
    cell: &UniqueInstance<LN::CellId, LN::Coord>,
    pin_id: &LN::PinId,
    via: &ViaDef<LN::Coord, LN::LayerId>,
    access_point: &PinAccessPoint<LN>,
    drc_engine: &Drc,
    do_differential_drc: bool,
) -> Option<u32>
where
    LN: db::L2NBase,
    Drc: DrcEngine<LN>,
{
    // Check that layer is matching.
    let is_correct_layers = via.low_layer == access_point.layer;

    if !is_correct_layers {
        return None;
    };

    // Create layout of the cell including the via at the given access point.
    let layers_of_interest: Vec<_> = via
        .shapes
        .iter()
        .map(|(layer, _)| layer)
        .unique()
        .cloned()
        .collect();

    let cell_position = cell.transform;

    let mut flat_shapes =
        extract_shapes_from_cell(layout, &cell.cell_id, &layers_of_interest, cell_position);

    let via_location = access_point.rel_location.v();

    // Sanity check: via location must touch some shape of the cell.
    debug_assert! {
        flat_shapes.get(&access_point.layer).into_iter()
            .flatten()
            .any(|(rpoly, _net)| rpoly.contains_point(via_location.into())),
        "via location must touch some shape of the cell"
    };

    // Check that the via enclosure is contained in the pin shape.
    let fits_into_enclosure = {
        let via_enclosure = via
            .shapes
            .iter()
            .filter(|(layer, _)| layer == &via.low_layer)
            .next()
            .map(|(_layer, rect)| rect)
            .expect("via has no metal shape");

        let via_enclosure = via_enclosure.translate(via_location);
        let shapes = &flat_shapes[&via.low_layer];
        let corners = [
            via_enclosure.lower_left(),
            via_enclosure.upper_right(),
            via_enclosure.upper_left(),
            via_enclosure.lower_right(),
        ];

        let via_enclosure_in_pin = shapes
            .iter()
            .any(|(shape, _net)| corners.iter().all(|point| shape.contains_point(*point)));

        via_enclosure_in_pin
    };

    let out_of_original_pin_penalty = if fits_into_enclosure { 0 } else { 1 };

    let drc_baseline = if do_differential_drc {
        // Do a DRC check without the via.
        drc_engine
            .flat_check(&flat_shapes)
            .map_err(|_| log::error!("DRC engine failed"))
            .ok() // convert to Option
            .expect("DRC engine failed")
            .num_violations()
    } else {
        0
    };

    // Add shapes of the via.
    for (layer, rect) in &via.shapes {
        let rect = rect.translate(via_location);
        let via_net = layout.net_of_pin(pin_id);
        flat_shapes
            .entry(layer.clone())
            .or_default()
            .push((rect.into(), via_net));
    }

    // Use the DRC engine to check if a via can be put at this access point.
    let drc_result = drc_engine
        .flat_check(&flat_shapes)
        .map_err(|_| log::error!("DRC engine failed"))
        .ok() // convert to Option
        .expect("DRC engine failed"); // TODO: Pass the error if the DRC engine fails.

    let num_additional_violations = drc_result.num_violations().saturating_sub(drc_baseline);
    let via_cost = out_of_original_pin_penalty + 4 * (num_additional_violations as u32);

    if drc_result.num_violations() > 0 {
        // TODO: Remove
        dbg!(drc_result.num_violations());
    }

    if drc_result.num_violations() <= drc_baseline {
        Some(via_cost)
    } else {
        // Invalid via.
        None
    }
}

/// Abstract engine which checks if two access points can be used without design-rule violation.
pub(super) trait AccessPointPairChecker<LN>
where
    LN: db::L2NBase,
{
    fn is_valid_access_point_pair(
        &self,
        cell: &UniqueInstance<LN::CellId, LN::Coord>,
        via1: &ViaDef<LN::Coord, LN::LayerId>,
        access_point1: &PinAccessPoint<LN>,
        pin_id1: &LN::PinId,
        via2: &ViaDef<LN::Coord, LN::LayerId>,
        access_point2: &PinAccessPoint<LN>,
        pin_id2: &LN::PinId,
    ) -> bool;
}

/// Check if two vias can be placed together without causing a design rule violation.
fn is_valid_access_point_pair<LN, Drc>(
    layout: &LN,
    cell: &UniqueInstance<LN::CellId, LN::Coord>,
    via1: &ViaDef<LN::Coord, LN::LayerId>,
    access_point1: &PinAccessPoint<LN>,
    pin_id1: &LN::PinId,
    via2: &ViaDef<LN::Coord, LN::LayerId>,
    access_point2: &PinAccessPoint<LN>,
    pin_id2: &LN::PinId,
    drc_engine: &Drc,
) -> bool
where
    LN: db::L2NBase,
    Drc: DrcEngine<LN>,
{
    // Create mutable copy of the layout.

    // TODO Use the DRC engine to check if a via can be put at this access point.
    true
}

/// Extract flattened shapes from the cell on the given layers.
fn extract_shapes_from_cell<LN>(
    layout: &LN,
    cell_id: &LN::CellId,
    layers_of_interest: &[LN::LayerId],
    cell_position: db::SimpleTransform<LN::Coord>,
) -> HashMap<LN::LayerId, Vec<(db::SimpleRPolygon<LN::Coord>, Option<LN::NetId>)>>
where
    LN: db::L2NBase,
{
    let mut flat_shapes: HashMap<LN::LayerId, Vec<_>> = Default::default();

    // Collect all relevant shapes of the cell.
    for layer in layers_of_interest {
        layout.for_each_shape_recursive(cell_id, layer, |tf, shape_id, geometry| {
            let actual_tf = cell_position.then(&tf);
            let geometry = geometry.transformed(&actual_tf);

            // TODO: Handle non-rectilinear shapes.
            let simple_rpolygon: Option<db::SimpleRPolygon<_>> = match geometry {
                db::Geometry::Rect(r) => Some(r.into()),
                db::Geometry::SimpleRPolygon(r) => Some(r),
                db::Geometry::SimplePolygon(p) => db::SimpleRPolygon::try_new(p.points()),
                _ => None,
            };

            if let Some(simple_rpolygon) = simple_rpolygon {
                let pin = layout.get_pin_of_shape(shape_id);
                let pin_net = pin.as_ref().and_then(|pin| layout.net_of_pin(pin));

                // Store the shape.
                flat_shapes
                    .entry(layer.clone())
                    .or_default()
                    .push((simple_rpolygon, pin_net));
            } else {
                // Ignore other geometries.
            }
        })
    }

    flat_shapes
}

#[test]
fn test_generate_valid_pin_access_points() {
    use db::technology::prelude::*;
    use db::traits::*;
    use libreda_lefdef as lefdef;

    let (lef, chip) = test_data::create_test_layout_from_lef();
    let cell = chip.cell_by_name("INVX1").expect("cell not found");

    let unique_instance = UniqueInstance {
        cell_id: cell.clone(),
        transform: db::SimpleTransform::identity(),
        // Dummy value for `id`:
        id: UniqueInstanceId {
            cell_id: cell.clone(),
            rotation: db::Angle::R0,
            mirror: false,
            track_offsets: Default::default(),
        },
    };

    let design_rules = lefdef::LEFDesignRuleAdapter::new(&lef, &chip);
    assert!(design_rules.layer_stack().len() >= 3);
    assert!(design_rules.routing_layer_stack().len() >= 2);
    assert!(design_rules.via_layer_stack().len() >= 1);

    println!("Layer stack:");
    for l in design_rules.layer_stack() {
        println!("  - {:?}", chip.layer_info(l.as_id()).name);
    }

    let drc_engine = DummyDrcEngine;

    let routing_region = db::Rect::new((0, 0), (1000, 1000));

    let mut routing_tracks = HashMap::new();
    routing_tracks.insert(
        chip.layer_by_name("metal1").unwrap(),
        Tracks {
            region: routing_region,
            pitch: 100,
            offset: 0,
            orientation: db::Orientation2D::Horizontal,
        },
    );
    routing_tracks.insert(
        chip.layer_by_name("metal2").unwrap(),
        Tracks {
            region: routing_region,
            pitch: 100,
            offset: 0,
            orientation: db::Orientation2D::Vertical,
        },
    );

    let candidate_generator = access_point_candidates::SimplePinAccessPointGenerator {
        design_rules: &design_rules,
        tracks: routing_tracks,
        allow_planar_access: true,
        allow_via_access: true,
        max_num_candidates: 10,
    };

    // Extract via definitions from the layout and the LEF file.
    let viadefs = crate::extract_via_defs_from_lef(&chip, &lef, &design_rules)
        .expect("failed to extract via definitions");

    let access_points = find_valid_pin_access_points(
        &chip,
        &unique_instance,
        &viadefs,
        &candidate_generator,
        &drc_engine,
    );

    dbg!(&access_points);

    for pin in chip.each_pin(&cell) {
        assert!(
            access_points.access_points.get(&pin).is_some(),
            "no access points found for pin {}",
            chip.pin_name(&pin)
        );
    }
}
