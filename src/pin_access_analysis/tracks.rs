// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Definition of routing tracks.

use crate::graph::GridGraph;
use iron_shapes::concepts::*;
use libreda_pnr::db;
use num_traits::{FromPrimitive, PrimInt};

/// A rectangular region with vertical or horizontal equidistant routing tracks.
#[derive(Copy, Clone, Debug)]
pub struct Tracks<C> {
    /// Border of the rectangular region containing the routing tracks.
    pub region: db::Rect<C>,
    /// Routing pitch: spacing between centerlines of tracks.
    pub pitch: C,
    /// Shift of the tracks relative to the point `(0, 0)`.
    pub offset: C,
    /// Orientation of the tracks.
    pub orientation: db::Orientation2D,
}

impl<C> Tracks<C>
where
    C: db::CoordinateType + FromPrimitive, // TODO: Add constraint `PrimInt`
{
    fn grid_floor(x: C, pitch: C) -> C {
        x - (x % pitch)
    }

    fn grid_ceil(x: C, pitch: C) -> C {
        Self::grid_floor(x + pitch - C::one(), pitch)
    }

    /// Get the offset (distance to the x or y axis) of the first routing track.
    pub fn first_track_offset(&self) -> C {
        let r = self.region;
        let start = r.lower_left().get(self.orientation.other());

        // Grid ceil.
        let first_track = {
            let without_offset = (start - self.offset);
            Self::grid_ceil(without_offset, self.pitch) + self.offset
        };

        first_track
    }

    /// Get the offset (distance to the x or y axis) of the last routing track.
    pub fn last_track_offset(&self) -> C {
        let r = self.region;
        let end = r.upper_right().get(self.orientation.other());

        // Grid ceil.
        let last_track = {
            let without_offset = (end - self.offset);
            Self::grid_floor(without_offset, self.pitch) + self.offset
        };

        last_track
    }

    /// Get all track offsets (distance to the axis) which are inside the rectangle `r`.
    pub fn all_offsets_in_region(&self, r: &db::Rect<C>) -> impl Iterator<Item = C> + '_ {
        let (start, end) = match self.orientation {
            db::Orientation2D::Horizontal => (r.lower_left().y, r.upper_right().y),
            db::Orientation2D::Vertical => (r.lower_left().x, r.upper_right().x),
        };

        // Grid ceil.
        let first_track = {
            let without_offset = (start - self.offset);
            Self::grid_ceil(without_offset, self.pitch) + self.offset
        };

        (0..)
            .map(move |i| first_track + C::from_usize(i).unwrap() * self.pitch)
            .take_while(move |offset| offset < &end)
    }

    /// Get tracks shifted by half a pitch.
    pub fn half_tracks(&self) -> Self {
        let _1 = C::one();
        let _2 = _1 + _1;
        Self {
            offset: self.offset + self.pitch / _2,
            ..*self
        }
    }
}

impl<C> Tracks<C>
where
    C: db::CoordinateType + FromPrimitive + PrimInt,
{
    /// Check if a point lies on a track.
    pub fn is_on_track(&self, p: db::Point<C>) -> bool {
        let p_offset = match self.orientation {
            db::Orientation2D::Horizontal => p.y,
            db::Orientation2D::Vertical => p.x,
        };

        self.region.contains_point(p) && ((p_offset - self.offset) % self.pitch).is_zero()
    }

    /// Find points which are on both track patterns.
    /// The both track patterns must be perpendicular, otherwise this function returns `None`.
    pub fn crossings(&self, other: &Self) -> Option<GridGraph<C>> {
        if self.orientation == other.orientation {
            // No crossings if orientations are the same.
            return None;
        }

        let region = self.region.intersection(&other.region);

        let (grid_pitch, grid_offset) = if self.orientation.is_vertical() {
            (
                db::Vector::new(self.pitch, other.pitch),
                db::Vector::new(self.offset, other.offset),
            )
        } else {
            (
                db::Vector::new(other.pitch, self.pitch),
                db::Vector::new(other.offset, self.offset),
            )
        };

        region.map(|r| GridGraph::new(r, 1, grid_pitch, grid_offset))
    }
}

#[test]
fn test_first_and_last_track() {
    // Check that track offsets are computed correctly.

    let tracks = Tracks {
        region: db::Rect::new((0, 0), (100, 200)),
        pitch: 20,
        offset: 10,
        orientation: db::Orientation2D::Horizontal,
    };

    assert_eq!(tracks.first_track_offset(), 10);
    assert_eq!(tracks.last_track_offset(), 190);

    assert_eq!(
        tracks.all_offsets_in_region(&tracks.region).next(),
        Some(10)
    );
    assert_eq!(
        tracks.all_offsets_in_region(&tracks.region).last(),
        Some(190)
    );
}
