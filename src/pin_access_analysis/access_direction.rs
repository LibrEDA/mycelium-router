// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Bitmap for encoding possible access directions.

use bitmaps::{BitOps, Bitmap, Bits};

/// Set of access directions (east, north, west, south, up, down).
#[derive(Copy, Clone, Hash, Eq, PartialEq)]
pub struct PinAccessDirections {
    direction_bits: Bitmap<6>,
}

impl PinAccessDirections {
    /// Create a new set of directions with no direction enabled.
    pub fn none() -> Self {
        Self {
            direction_bits: Default::default(),
        }
    }

    pub fn all() -> Self {
        Self::none().neg()
    }

    fn set_bit(mut self, idx: usize) -> Self {
        self.direction_bits.set(idx, true);
        self
    }

    pub fn east() -> Self {
        Self::none().set_bit(0)
    }

    pub fn north() -> Self {
        Self::none().set_bit(1)
    }

    pub fn west() -> Self {
        Self::none().set_bit(2)
    }

    pub fn south() -> Self {
        Self::none().set_bit(3)
    }

    /// Up in z direction.
    pub fn up() -> Self {
        Self::none().set_bit(4)
    }

    /// Down in z direction.
    pub fn down() -> Self {
        Self::none().set_bit(5)
    }

    pub fn is_east(&self) -> bool {
        self.direction_bits.get(0)
    }
    pub fn is_north(&self) -> bool {
        self.direction_bits.get(1)
    }
    pub fn is_west(&self) -> bool {
        self.direction_bits.get(2)
    }
    pub fn is_south(&self) -> bool {
        self.direction_bits.get(3)
    }
    pub fn is_up(&self) -> bool {
        self.direction_bits.get(4)
    }
    pub fn is_down(&self) -> bool {
        self.direction_bits.get(5)
    }

    /// Bit-wise AND.
    pub fn and(self, other: Self) -> Self {
        Self {
            direction_bits: self.direction_bits & other.direction_bits,
        }
    }

    /// Bit-wise OR.
    pub fn add(self, other: Self) -> Self {
        Self {
            direction_bits: self.direction_bits | other.direction_bits,
        }
    }

    /// Bit-wise OR.
    pub fn neg(mut self) -> Self {
        self.direction_bits.invert();
        self
    }

    /// Remove the directions in `other` from `self`.
    pub fn sub(self, other: Self) -> Self {
        self.and(other.neg())
    }
}

impl std::fmt::Display for PinAccessDirections {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "AccessDirection(")?;
        let dir_names = ["E", "N", "W", "S", "U", "D"];
        for (i, dir_name) in dir_names.iter().enumerate() {
            if self.direction_bits.get(i) {
                write!(f, "{}", dir_name)?;
            }
        }
        write!(f, ")")
    }
}

impl std::fmt::Debug for PinAccessDirections {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}
