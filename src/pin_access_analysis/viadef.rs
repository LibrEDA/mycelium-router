// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Simple definition of via geometries.

use libreda_pnr::db;
use num_traits::Num;

/// Geometry of a via stack.
#[derive(Clone, Debug)]
pub struct ViaDef<Coord, LayerId> {
    /// Name of the via.
    pub via_name: String,
    /// The lowest metal layer touched by the via.
    pub low_layer: LayerId,
    /// The highest metal layer touched by the via.
    pub high_layer: LayerId,
    /// Via shapes stored as rectangles associated with a layer.
    pub shapes: Vec<(LayerId, db::Rect<Coord>)>,
}

impl<Coord, LayerId> ViaDef<Coord, LayerId>
where
    Coord: PartialOrd + Num + Copy,
    LayerId: PartialEq,
{
    /// Find the orientation of the rectangle shape on the given layer.
    /// The layer must be either the low or high metal layer of the via.
    fn preferred_routing_direction(&self, layer: &LayerId) -> Option<db::Orientation2D> {
        debug_assert!(layer == &self.low_layer || layer == &self.high_layer);
        let mut shapes = self.shapes.iter().filter(|(l, _)| layer == l);

        if let Some((_, shape)) = shapes.next() {
            if shapes.next().is_some() {
                // More than two metal shapes on this layer: no well defined preferred access direction.
                None
            } else {
                let w = shape.width();
                let h = shape.height();

                if w == h {
                    None
                } else if w < h {
                    Some(db::Orientation2D::Vertical)
                } else {
                    debug_assert!(w > h);
                    Some(db::Orientation2D::Horizontal)
                }
            }
        } else {
            // No metal shapes found.
            None
        }
    }

    /// Get the routing direction on the lower metal layer.
    /// Return `None` if the routing direction is not clearly seen by the metal shape of the via.
    pub fn low_routing_direction(&self) -> Option<db::Orientation2D> {
        self.preferred_routing_direction(&self.low_layer)
    }

    /// Get the routing direction on the lower metal layer.
    /// Return `None` if the routing direction is not clearly seen by the metal shape of the via.
    pub fn high_routing_direction(&self) -> Option<db::Orientation2D> {
        self.preferred_routing_direction(&self.high_layer)
    }
}
