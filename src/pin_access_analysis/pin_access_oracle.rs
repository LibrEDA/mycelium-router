// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Top-level API for the pin access analysis.

use fnv::{FnvHashMap, FnvHashSet};
use num_traits::{FromPrimitive, NumCast, PrimInt};
use rayon::prelude::*;

use libreda_pnr::db;

use super::*;

use super::access_pattern::{CellAccessPattern, CellAccessPointReference};

/// Bundles all the steps of pin access analysis.
pub struct PinAccessOracleBuilder<'a, LN, Rules, Drc>
where
    LN: db::L2NBase,
{
    /// Layout to operate on.
    chip: &'a LN,
    /// Top cell to operate on. The child cell instances will be analyzed.
    top_cell: LN::CellId,
    /// Design-rules to respect for selecting pin access locations.
    design_rules: &'a Rules,
    /// Design-rule checker
    drc_engine: &'a Drc,
    /// Find the abutment box of a cell.
    cell_outline_fn: Box<dyn Fn(&LN::CellId) -> Option<db::Rect<LN::Coord>> + 'a>,
    /// Vias which can be used.
    via_definitions: Vec<Arc<ViaDef<LN::Coord, LN::LayerId>>>,
    routing_tracks: HashMap<LN::LayerId, Tracks<LN::Coord>>,
    /// Maximum number of access point candidates which should be generated for a single pin.
    max_num_access_point_candidates_per_pin: usize,
}

impl<'a, LN, Rules, Drc> PinAccessOracleBuilder<'a, LN, Rules, Drc>
where
    LN: db::L2NBase,
{
    /// Create a new builder for a pin access oracle.
    pub fn new(
        chip: &'a LN,
        top_cell: LN::CellId,
        design_rules: &'a Rules,
        drc_engine: &'a Drc,
        cell_outline_fn: Box<dyn Fn(&LN::CellId) -> Option<db::Rect<LN::Coord>> + 'a>,
        via_definitions: Vec<Arc<ViaDef<LN::Coord, LN::LayerId>>>,
        routing_tracks: HashMap<LN::LayerId, Tracks<LN::Coord>>,
    ) -> Self {
        Self {
            chip,
            top_cell,
            design_rules,
            drc_engine,
            cell_outline_fn,
            via_definitions,
            routing_tracks,
            max_num_access_point_candidates_per_pin: 256,
        }
    }
}

impl<'a, LN, Rules, Drc> PinAccessOracleBuilder<'a, LN, Rules, Drc>
where
    LN: db::L2NBaseMT,
    LN::Coord: PrimInt + FromPrimitive,
    Rules: db::RuleBase<LayerId = LN::LayerId> + db::RoutingRules + db::RoutingLayerStack + Sync,
    Drc: DrcEngine<LN> + Sync,
{
    /// Do the pin access analysis and return the precomputed pin-access points.
    pub fn build(&self) -> PinAccessOracle<LN> {
        log::info!("Run pin access analysis.");
        let chip = self.chip;
        let top_cell = self.top_cell.clone();

        let all_cell_instances = chip.each_cell_instance_vec(&top_cell);
        log::info!("Number of cell instances: {}", all_cell_instances.len());

        // Get the set of used standard-cell types.
        let all_cells: FnvHashSet<LN::CellId> = all_cell_instances
            .iter()
            .map(|inst| chip.template_cell(inst))
            .collect();
        log::info!(
            "Number of different standard cell types: {}",
            all_cells.len()
        );

        log::info!("Find unique cell instances.");
        let unique_cell_instances = unique_instance::create_unique_instances(
            chip,
            &all_cell_instances,
            self.design_rules,
            &self.routing_tracks,
        );

        // log::info!("Number of unique instances: {}", unique_cell_instances.len());

        log::info!("Generate pin access points.");

        let candidate_generator = SimplePinAccessPointGenerator {
            design_rules: self.design_rules,
            tracks: self.routing_tracks.clone(),
            allow_planar_access: true,
            allow_via_access: true,
            max_num_candidates: self.max_num_access_point_candidates_per_pin,
        };

        log::info!(
            "Generate valid pin access points for {} unique instances.",
            unique_cell_instances.each_unique_instance().count()
        );

        let access_points: Vec<(_, _)> = unique_cell_instances
            .each_unique_instance()
            .map(|unique_instance| {
                let access_points = find_valid_pin_access_points(
                    chip,
                    &unique_instance,
                    &self.via_definitions,
                    &candidate_generator,
                    self.drc_engine,
                );

                (unique_instance.id, access_points)
            })
            .collect();

        // // Print statistics on pin access points.
        // {
        //     for (unique_instance_id, pin_access_points) in &access_points {
        //         log::info!(
        //             "Number of pin access points for '{}': {}",
        //             chip.cell_name(&unique_instance_id.cell_id),
        //             pin_access_points.access_points.len()
        //         );
        //     }
        // }

        // Find pin access patterns.
        let cell_access_patterns = {
            log::info!("Generate pin access patterns.");

            let pattern_generator = AccessPatternGenerator::<LN> {
                is_valid_access_point_pair: Box::new(|_cell, _a1, _ap2| true), // TODO: Use real DRC check.
                penalty_cost: 1, // TODO: Find reasonable penalty cost.
                drc_cost: 2,     // TODO: Find reasonable DRC cost.
            };

            let max_num_patterns = 10; // Number of access patterns to generate per unique instance.

            // Generate access patterns for all unique instances.
            let access_patterns: HashMap<UniqueInstanceId<_, _>, _> = access_points
                .par_iter() //  Run in parallel.
                // .iter() // Run sequentially.
                .map(|(unique_instance_id, aps)| {
                    let unique_instance =
                        unique_cell_instances.get_unique_instance(unique_instance_id);

                    let access_patterns: Vec<CellAccessPattern<_>> = pattern_generator
                        .generate_access_patterns(
                            &unique_instance,
                            &aps.access_points,
                            max_num_patterns,
                        );

                    // Convert to resource counted patterns.
                    let access_patterns: Vec<_> = access_patterns
                        .into_iter()
                        .map(|pattern| Arc::new(pattern))
                        .collect();

                    (unique_instance_id.clone(), access_patterns)
                })
                .collect();

            access_patterns
        };

        // Cluster cell instances by the standard-cell rows.
        // Used to generate cluster access patterns.
        let clusters: Vec<Vec<LN::CellInstId>> = {
            log::info!("Detect standard-cell clusters (rows or columns).");

            let cluster_detector = cluster_detection::ClusterDetector::<LN> {
                cell_outline_fn: Box::new(|cell| (self.cell_outline_fn)(cell)),
            };

            let mut clusters = cluster_detector.detect_clusters(chip, &all_cell_instances);

            log::info!("Number of clusters: {}", clusters.len());

            // Make sure all instances are assigned to a cluster.
            {
                // Find cell instance which have not been assigned to a cluster.
                let remaining_instances: Vec<_> = {
                    // Get set of instances already assigned to a cluster.
                    let all_cell_insts_in_cluster: FnvHashSet<_> =
                        clusters.iter().flatten().collect();

                    // Get set of instances NOT assigned to a cluster.
                    all_cell_instances
                        .iter()
                        .filter(move |inst| !all_cell_insts_in_cluster.contains(inst))
                        .cloned()
                        .collect()
                };

                if !remaining_instances.is_empty() {
                    log::info!(
                        "{} of {} instances have not been assigned to a cluster.",
                        remaining_instances.len(),
                        all_cell_instances.len()
                    );
                    log::info!(
                        "Create single-instance clusters for {} instances.",
                        remaining_instances.len()
                    );
                }

                // Create clusters of size 1 for the remaining instances.
                clusters.extend(remaining_instances.into_iter().map(|inst| vec![inst]));
            }

            clusters
        };

        // Sanity check: all cell instances must be assigned to a cluster.
        {
            let num_insts = all_cell_instances.len();
            let num_insts_in_cluster = clusters.iter().flatten().unique().count();
            assert_eq!(
                num_insts, num_insts_in_cluster,
                "all cell instances must be assigned to a cluster"
            );
        }

        // Generate cluster access patterns (minimum-cost selection of pin access locations in a standard-cell row).
        let final_access_patterns: FnvHashMap<LN::CellInstId, Arc<CellAccessPattern<_>>> = {
            log::info!("Generate cluster access patterns.");

            let cluster_access_pattern_generator =
                cluster_access_pattern::ClusterAccessPatternGenerator::new(
                    1, // TODO: Find good penalty and DRC costs.
                    2,
                );

            // Associate the access patterns with concrete instances.
            let cell_access_patterns_by_instance: HashMap<_, _> = all_cell_instances
                .iter()
                .map(|inst| {
                    let unique_inst_id = unique_cell_instances.get_unique_instance_id(inst);
                    let patterns = &cell_access_patterns[unique_inst_id];

                    (inst.clone(), patterns)
                })
                .collect();

            // Generate access pattern for each cluster.
            let cluster_access_patterns = clusters.iter().map(|sorted_cell_instances| {
                cluster_access_pattern_generator.generate_cluster_access_pattern(
                    sorted_cell_instances,
                    &cell_access_patterns_by_instance,
                )
            });

            // Collect the access patterns of all cell instances in one hash map.
            let final_access_patterns = cluster_access_patterns
                .flat_map(|cluster_pattern| cluster_pattern.access_pattern.into_iter())
                .collect();

            final_access_patterns
        };

        // Copy the locations of the cell instances.
        let cell_instance_locations = all_cell_instances
            .iter()
            .map(|inst| {
                let location = chip.get_transform(inst);
                (inst.clone(), location)
            })
            .collect();

        // Store final access patterns.
        PinAccessOracle {
            final_access_patterns,
            cell_instance_locations,
        }
    }
}

/// Precomputed pin access points.
/// Use [`PinAccessOracleBuilder`] to create this struct.
#[derive(Debug, Clone)]
pub struct PinAccessOracle<LN>
where
    LN: db::L2NBase,
{
    /// Selected access patterns of cell pins.
    pub(crate) final_access_patterns: FnvHashMap<LN::CellInstId, Arc<CellAccessPattern<LN>>>,
    /// Used to compute the absolute positions of the access points.
    cell_instance_locations: FnvHashMap<LN::CellInstId, db::SimpleTransform<LN::Coord>>,
}

impl<LN: db::L2NBase> Default for PinAccessOracle<LN> {
    fn default() -> Self {
        Self {
            final_access_patterns: Default::default(),
            cell_instance_locations: Default::default(),
        }
    }
}

impl<LN: db::L2NBase> libreda_pnr::prelude::PinAccessOracle<LN> for PinAccessOracle<LN> {
    fn get_best_pin_access_location(
        &self,
        chip: &LN,
        pin_inst: &LN::PinInstId,
    ) -> (db::Point<LN::Coord>, LN::LayerId) {
        self.get_pin_access_location(chip, pin_inst)
    }

    fn get_best_pin_access_location_with_via(
        &self,
        chip: &LN,
        pin_inst: &LN::PinInstId,
    ) -> (db::Point<LN::Coord>, LN::LayerId, Option<LN::CellId>) {
        let (location, layer) = self.get_pin_access_location(chip, pin_inst);
        (location, layer, None)
    }
}

impl<LN: db::L2NBase> PinAccessOracle<LN> {
    /// Get the absolute coordinates of the access point for the given pin instance.
    pub fn get_pin_access_location(
        &self,
        chip: &LN,
        pin_inst: &LN::PinInstId,
    ) -> (db::Point<LN::Coord>, LN::LayerId) {
        let cell_inst = chip.parent_of_pin_instance(pin_inst);
        let pin = chip.template_pin(pin_inst);
        let pattern = self
            .final_access_patterns
            .get(&cell_inst)
            .expect("No access pattern found for this cell instance.");
        let inst_location = self
            .cell_instance_locations
            .get(&cell_inst)
            .expect("No location found for this cell instance.");

        let access_point = pattern
            .access_points
            .iter()
            .find(|(pin_id, _ap)| pin_id == &pin)
            .map(|(_pin, ap)| ap)
            .expect("No access point found for this pin.");

        let rel_location = access_point.rel_location; // Relative location of access point.
        let location = rel_location + inst_location.transform_point(db::Point::zero()); // Absolute location of access point.

        (location, access_point.layer.clone())
    }
}
