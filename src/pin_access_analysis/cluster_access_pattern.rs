// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Find access points for cells in a contiguous row.
//! The cluster access pattern is formed by selecting one of the cell access patterns
//! for each cell in the cluster such that the number of conflicts between access points is minimized.

use fnv::{FnvHashMap, FnvHashSet};
use libreda_pnr::db;

use std::cell::Cell;
use std::collections::{HashMap, HashSet};
use std::marker::PhantomData;
use std::sync::Arc;

use super::access_pattern::CellAccessPattern;

/// Selected access patterns for a 'cluster' of cells.
/// The cluster is typically a row or partial row of cells.
#[derive(Clone)]
pub(crate) struct ClusterAccessPattern<LN: db::L2NIds> {
    /// One selected access pattern per cell instance in the cluster.
    pub access_pattern: HashMap<LN::CellInstId, Arc<CellAccessPattern<LN>>>,
    /// Total cost of the selected access points.
    pub cost: u32,
}

pub(crate) struct ClusterAccessPatternGenerator<LN> {
    /// Cost for an access point at the cell boundary (left or right) which has been already used.
    pub penalty_cost: u32,
    /// Edge cost between two access point which cause a DRC violation.
    pub drc_cost: u32,
    _ln: PhantomData<LN>,
}

/// Node in the directed graph used for finding access patterns.
/// A node represents an access pattern of a cell instance. It remembers the path cost from the left-most node
/// until the current node and remembers the previous node in the path.
struct GraphNode<LN: db::L2NIds> {
    /// The access pattern which is represented by this node.
    pattern: Arc<CellAccessPattern<LN>>,
    /// Path cost to this node.
    cost: Cell<u32>,
    /// Index of the previous node on the minimum-cost path.
    /// Has the form `(cell_index, pattern_index)`.
    prev: Cell<Option<(usize, usize)>>,
}

impl<LN: db::L2NBase> ClusterAccessPatternGenerator<LN>
where
    LN::Coord: Ord,
{
    /// Create a new cluster access pattern generator.
    pub fn new(penalty_cost: u32, drc_cost: u32) -> Self {
        Self {
            penalty_cost,
            drc_cost,
            _ln: Default::default(),
        }
    }

    pub fn generate_cluster_access_pattern<'a>(
        &self,
        sorted_cell_instances: &[LN::CellInstId],
        cell_access_patterns: &HashMap<LN::CellInstId, &'a Vec<Arc<CellAccessPattern<LN>>>>,
    ) -> ClusterAccessPattern<LN> {
        // Warn once about incomplete implementation.
        {
            use std::sync::Once;
            static LOG: Once = Once::new();
            LOG.call_once(|| {
                log::warn!("TODO: The edge cost for cluster access patterns is not yet implemented and uses a constant value.");

            });
        }
        // Create graph nodes.
        // Each cell in the cluster has a set of nodes which correspond to the possible cell access patterns.
        let sorted_nodes: Vec<Vec<_>> = sorted_cell_instances
            .iter()
            .enumerate()
            .map(|(pin_index, cell_inst_id)| {
                let cell_access_patterns = &cell_access_patterns[cell_inst_id];

                let mut sorted_access_patterns: Vec<_> = cell_access_patterns
                    .iter()
                    .enumerate()
                    .map(|(pattern_index, pattern)| GraphNode {
                        pattern: pattern.clone(),
                        cost: Cell::new(u32::MAX),
                        prev: Cell::new(None),
                    })
                    .collect();

                sorted_access_patterns.sort_by_key(|node| node.cost.get());
                sorted_access_patterns
            })
            .collect();

        // Algorithm 2 of paper "The Tao of PAO".
        for (current_cell_index, current_patterns) in sorted_nodes.iter().enumerate() {
            if current_cell_index == 0 {
                // Initialize node costs of access points in left-most pin.
                for curr_node in current_patterns {
                    curr_node.cost.set(curr_node.pattern.cost);
                }
            } else {
                // Set of nodes of the previous pin.
                let prev_cell_index = current_cell_index - 1;
                let prev_patterns = &sorted_nodes[prev_cell_index];

                // Iterate over all edges between the current and previous pin.
                for (curr_pattern_index, curr_node) in current_patterns.iter().enumerate() {
                    for (prev_pattern_index, prev_node) in prev_patterns.iter().enumerate() {
                        // Find the cost of using the combination of this two access points.
                        let edge_cost =
                            self.compute_edge_cost(&prev_node.pattern, &curr_node.pattern);

                        let path_cost = prev_node.cost.get() + edge_cost;

                        if path_cost < curr_node.cost.get() {
                            // Found a better path.
                            curr_node.cost.set(path_cost);
                            curr_node
                                .prev
                                .set(Some((prev_cell_index, prev_pattern_index)));
                        }
                    }
                }
            }
        }

        // Find the minimum-cost path by backtracing.
        let (path, path_cost) = {
            let last_node = sorted_nodes[sorted_nodes.len() - 1]
                .iter()
                .min_by_key(|node| node.cost.get())
                .expect("no access point found on last pin");

            let mut path = vec![last_node];

            // Trace back until no previous node exists.
            while let Some((prev_cell_index, prev_pattern_index)) = path[path.len() - 1].prev.get()
            {
                let prev_node = &sorted_nodes[prev_cell_index][prev_pattern_index];
                path.push(prev_node);
            }

            path.reverse();

            (path, last_node.cost.get())
        };

        // Clone the access points.
        let access_pattern = path
            .into_iter()
            .enumerate()
            .map(|(i, node)| (sorted_cell_instances[i].clone(), node.pattern.clone()))
            .collect();

        ClusterAccessPattern {
            access_pattern,
            cost: path_cost,
        }
    }

    /// Compute the cost of choosing the both access patterns for neighbouring cells.
    fn compute_edge_cost(&self, prev: &CellAccessPattern<LN>, curr: &CellAccessPattern<LN>) -> u32 {
        // TODO: Implement the edge cost based on the both access patterns.
        1
    }
}
