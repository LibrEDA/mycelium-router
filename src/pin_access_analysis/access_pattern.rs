// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Select access points which can be used together without creating design rule
//! violations.

use crate::pin_access_analysis::unique_instance::UniqueInstance;
use crate::pin_access_analysis::{is_valid_access_point_pair, PinAccessPoint};
use fnv::{FnvHashMap, FnvHashSet};
use libreda_pnr::db;
use std::cell::Cell;
use std::collections::HashMap;
use std::sync::Arc;

/// A selection of good access points to connect to all pins of a cell.
#[derive(Clone, Debug)]
pub(crate) struct CellAccessPattern<LN: db::L2NIds> {
    /// One access point per pin.
    pub access_points: Vec<(LN::PinId, PinAccessPoint<LN>)>,
    /// Cost of the access pattern.
    pub cost: u32,
}

/// Light-weight reference to an access point within a cell.
#[derive(Clone, Debug)]
pub(crate) struct CellAccessPointReference<LN: db::L2NIds> {
    /// Reference to the cell access pattern.
    pub cell_access_pattern: Arc<CellAccessPattern<LN>>,
    /// Index into the `access_points` array of the access pattern.
    pub access_point_index: usize,
}

impl<LN: db::L2NBase> CellAccessPointReference<LN> {
    pub fn get_access_point(&self) -> &PinAccessPoint<LN> {
        &self.cell_access_pattern.access_points[self.access_point_index].1
    }
}

pub(crate) struct AccessPatternGenerator<'a, LN: db::L2NIds> {
    /// Check compatibility of two access points.
    pub is_valid_access_point_pair: Box<
        dyn Fn(
                &UniqueInstance<LN::CellId, LN::Coord>,
                &PinAccessPoint<LN>,
                &PinAccessPoint<LN>,
            ) -> bool
            + Sync
            + 'a,
    >,
    /// Cost for an access point at the cell boundary (left or right) which has been already used.
    pub penalty_cost: u32,
    /// Edge cost between two access point which cause a DRC violation.
    pub drc_cost: u32,
}

#[test]
fn test_generate_access_pattern() {
    // Generate pin access patterns for an inverter.

    use super::test_data;
    use super::{Tracks, UniqueInstance, UniqueInstanceId};
    use db::technology::prelude::*;
    use db::traits::*;
    use libreda_drc::DummyDrcEngine;
    use libreda_lefdef as lefdef;

    let (lef, chip) = test_data::create_test_layout_from_lef();
    let cell = chip.cell_by_name("INVX1").expect("cell not found");

    let unique_instance = UniqueInstance {
        cell_id: cell.clone(),
        transform: db::SimpleTransform::identity(),
        // Dummy value for `id`:
        id: UniqueInstanceId {
            cell_id: cell.clone(),
            rotation: db::Angle::R0,
            mirror: false,
            track_offsets: Default::default(),
        },
    };

    // Generate pin access points first.
    let access_points = {
        let design_rules = lefdef::LEFDesignRuleAdapter::new(&lef, &chip);
        assert!(design_rules.layer_stack().len() >= 3);
        assert!(design_rules.routing_layer_stack().len() >= 2);
        assert!(design_rules.via_layer_stack().len() >= 1);

        println!("Layer stack:");
        for l in design_rules.layer_stack() {
            println!("  - {:?}", chip.layer_info(l.as_id()).name);
        }

        let drc_engine = DummyDrcEngine;

        let routing_region = db::Rect::new((0, 0), (1000, 1000));

        let mut routing_tracks = HashMap::new();
        routing_tracks.insert(
            chip.layer_by_name("metal1").unwrap(),
            Tracks {
                region: routing_region,
                pitch: 100,
                offset: 0,
                orientation: db::Orientation2D::Horizontal,
            },
        );
        routing_tracks.insert(
            chip.layer_by_name("metal2").unwrap(),
            Tracks {
                region: routing_region,
                pitch: 100,
                offset: 0,
                orientation: db::Orientation2D::Vertical,
            },
        );

        let candidate_generator = super::access_point_candidates::SimplePinAccessPointGenerator {
            design_rules: &design_rules,
            tracks: routing_tracks,
            allow_planar_access: true,
            allow_via_access: true,
            max_num_candidates: 10,
        };

        // Extract via definitions from the layout and the LEF file.
        let viadefs = crate::extract_via_defs_from_lef(&chip, &lef, &design_rules)
            .expect("failed to extract via definitions");

        let access_points = super::find_valid_pin_access_points(
            &chip,
            &unique_instance,
            &viadefs,
            &candidate_generator,
            &drc_engine,
        );

        dbg!(&access_points);

        for pin in chip.each_pin(&cell) {
            assert!(
                access_points.access_points.get(&pin).is_some(),
                "no access points found for pin {}",
                chip.pin_name(&pin)
            );
        }
        access_points
    };

    let pattern_generator = AccessPatternGenerator::<db::Chip> {
        is_valid_access_point_pair: Box::new(|_cell, _ap1, _ap2| true), // Dummy DRC check.
        penalty_cost: 1,
        drc_cost: 2,
    };

    let max_num_patterns = 10;
    let access_patterns = pattern_generator.generate_access_patterns(
        &unique_instance,
        &access_points.access_points,
        max_num_patterns,
    );

    assert!(!access_patterns.is_empty(), "no access patterns found");
    assert!(
        access_patterns.len() <= max_num_patterns,
        "too many patterns found"
    );
}

/// Node in the directed graph used for finding access patterns.
/// A node represents an access point, remembers the path cost from the left-most node
/// until the current node and remembers the previous node in the path.
struct GraphNode<'a, LN>
where
    LN: db::L2NIds,
{
    ap: &'a PinAccessPoint<LN>,
    /// Path cost to this node.
    cost: Cell<u32>,
    /// Index of the previous node on the minimum-cost path.
    prev: Cell<Option<(usize, usize)>>,
}

impl<'a, LN: db::L2NBase> AccessPatternGenerator<'a, LN>
where
    LN::Coord: Ord,
{
    /// Find multiple different access patterns.
    pub fn generate_access_patterns(
        &self,
        unique_instance: &UniqueInstance<LN::CellId, LN::Coord>,
        access_points: &HashMap<LN::PinId, Vec<PinAccessPoint<LN>>>,
        max_num_patterns: usize,
    ) -> Vec<CellAccessPattern<LN>> {
        // Keep track of boundary access points (left-most and right-most point in a pattern).
        // Used to create a more diverse selection of boundary access-points. This gives more space for selection of cluster access patterns.
        let mut used_boundary_nodes: FnvHashSet<_> = Default::default();

        // Storage for found access patterns.
        let mut patterns = vec![];

        for i in 0..max_num_patterns {
            let pattern =
                self.generate_access_pattern(unique_instance, access_points, &used_boundary_nodes);

            let (_, first) = &pattern.access_points[0];
            let (_, last) = &pattern.access_points[pattern.access_points.len() - 1];
            used_boundary_nodes.insert(first.rel_location);
            used_boundary_nodes.insert(last.rel_location);

            patterns.push(pattern);
        }

        // Normalize the access points locations.
        for pattern in &mut patterns {
            for (_pin_id, ap) in &mut pattern.access_points {
                ap.rel_location = ap.rel_location
                    - unique_instance
                        .transform
                        .transform_point(db::Point::zero())
                        .v();
            }
        }

        patterns
    }

    /// Find a cost-optimal access pattern.
    ///
    /// * Sort pins from left to right.
    /// * Construct a graph: All access points are vertices. Access points of neighbouring pins are fully connected
    /// with edges.
    fn generate_access_pattern(
        &self,
        cell: &UniqueInstance<LN::CellId, LN::Coord>,
        access_points: &HashMap<LN::PinId, Vec<PinAccessPoint<LN>>>,
        used_boundary_nodes: &FnvHashSet<db::Point<LN::Coord>>,
    ) -> CellAccessPattern<LN> {
        let sorted_pins = self.sort_pins_left_to_right(access_points);

        // Get access points grouped by pin. Groups are sorted from left to right.
        // Access points inside the groups are sorted by (x, y) coordinate.
        let sorted_nodes: Vec<Vec<_>> = sorted_pins
            .iter()
            .enumerate()
            .map(|(pin_index, pin_id)| {
                let access_points = &access_points[pin_id];
                let mut sorted_access_points: Vec<_> = access_points
                    .iter()
                    .enumerate()
                    .map(|(ap_index, ap)| GraphNode {
                        ap,
                        cost: Cell::new(u32::MAX),
                        prev: Cell::new(None),
                    })
                    .collect();
                // Sort access points of a specific pin.
                sorted_access_points.sort_by_key(|node| node.ap.rel_location);
                sorted_access_points
            })
            .collect();

        // Algorithm 2 of paper "The Tao of PAO".
        for (current_pin_index, current_aps) in sorted_nodes.iter().enumerate() {
            if current_pin_index == 0 {
                // Initialize node costs of access points in left-most pin.
                for curr_node in current_aps {
                    curr_node.cost.set(curr_node.ap.cost);
                }
            } else {
                // Set of nodes of the previous pin.
                let prev_pin_index = current_pin_index - 1;
                let prev_aps = &sorted_nodes[prev_pin_index];

                // Iterate over all edges between the current and previous pin.
                for (curr_ap_index, curr_node) in current_aps.iter().enumerate() {
                    for (prev_ap_index, prev_node) in prev_aps.iter().enumerate() {
                        // Check compatibility with the second-previous access point.
                        let prev_prev = prev_node
                            .prev
                            .get()
                            .map(|(pin_index, ap_index)| sorted_nodes[pin_index][ap_index].ap);

                        // Find the cost of using the combination of this two access points.
                        let edge_cost = self.compute_edge_cost(
                            cell,
                            prev_prev,
                            &prev_node.ap,
                            &curr_node.ap,
                            prev_ap_index == 0, // Is at cell boundary?
                            curr_ap_index + 1 == current_aps.len(), // Is at cell boundary?
                            used_boundary_nodes,
                        );

                        let path_cost = prev_node.cost.get() + edge_cost;

                        if path_cost < curr_node.cost.get() {
                            // Found a better path.
                            curr_node.cost.set(path_cost);
                            curr_node.prev.set(Some((prev_pin_index, prev_ap_index)));
                        }
                    }
                }
            }
        }

        // Find the minimum-cost path by backtracing.
        let (path, path_cost) = {
            let last_node = sorted_nodes
                .last()
                .into_iter()
                .flatten()
                .min_by_key(|node| node.cost.get())
                .expect("no access point found on last pin");
            let mut path = vec![last_node];

            while let Some((prev_pin_index, prev_ap_index)) = path[path.len() - 1].prev.get() {
                let prev_node = &sorted_nodes[prev_pin_index][prev_ap_index];
                path.push(prev_node);
            }

            path.reverse();

            (path, last_node.cost.get())
        };

        // Clone the access points.
        let access_points = path
            .into_iter()
            .enumerate()
            .map(|(i, node)| (sorted_pins[i].clone(), node.ap.clone()))
            .collect();

        CellAccessPattern {
            access_points,
            cost: path_cost,
        }
    }

    /// Compute cost of the edge between two access points of neighbour pins.
    /// See: Algorithm 3 in paper "The Tao of PAO"
    fn compute_edge_cost(
        &self,
        cell: &UniqueInstance<LN::CellId, LN::Coord>,
        prev_prev_access_point: Option<&PinAccessPoint<LN>>,
        prev_access_point: &PinAccessPoint<LN>,
        curr_access_point: &PinAccessPoint<LN>,
        prev_is_at_cell_boundary: bool,
        curr_is_at_cell_boundary: bool,
        used_boundary_points: &FnvHashSet<db::Point<LN::Coord>>,
    ) -> u32 {
        let penalty_cost = self.penalty_cost;
        let base_cost = prev_access_point.cost + curr_access_point.cost;

        if used_boundary_points.contains(&prev_access_point.rel_location)
            || used_boundary_points.contains(&curr_access_point.rel_location)
        {
            self.penalty_cost
        } else if !self.is_valid_access_point_pair(cell, prev_access_point, curr_access_point) {
            self.drc_cost
        } else {
            if let Some(prev_prev) = prev_prev_access_point {
                if !self.is_valid_access_point_pair(cell, prev_prev, prev_access_point) {
                    self.drc_cost
                } else {
                    base_cost
                }
            } else {
                base_cost
            }
        }
    }

    fn is_valid_access_point_pair(
        &self,
        cell: &UniqueInstance<LN::CellId, LN::Coord>,
        ap1: &PinAccessPoint<LN>,
        ap2: &PinAccessPoint<LN>,
    ) -> bool {
        (self.is_valid_access_point_pair)(cell, ap1, ap2)
    }

    /// Sort pins by the center of the bounding box around their access points.
    fn sort_pins_left_to_right(
        &self,
        access_points: &HashMap<LN::PinId, Vec<PinAccessPoint<LN>>>,
    ) -> Vec<LN::PinId> {
        // Find bounding boxes of access points for each pin.
        let mut bbox_centers: Vec<_> = access_points
            .iter()
            // Skip pins without access points.
            .filter(|(_, aps)| !aps.is_empty())
            // Create bounding boxes.
            .map(|(pin_id, aps)| {
                let mut points = aps.iter().map(|ap| ap.rel_location);

                let p = points.next().unwrap();
                let mut bbox = db::Rect::new(p, p);
                points.for_each(|p| {
                    bbox = bbox.add_point(p);
                });

                (pin_id.clone(), bbox.center())
            })
            .collect();

        bbox_centers.sort_by_key(|(pin_id, center)| *center);

        // Return sorted pin IDs.
        bbox_centers.into_iter().map(|(pin_id, _)| pin_id).collect()
    }
}

// // Generalized algorithm 2.
// fn find_minimum_cost_path<Node, EdgeCostFn, NodeCostFn>(
//     mut sorted_nodes: Vec<Vec<Node>>,
//     edge_cost: &EdgeCostFn,
//     node_cost: &NodeCostFn,
// ) -> Vec<usize>
//     where EdgeCostFn: Fn(&Node, &Node) -> u32,
//           NodeCostFn: Fn(&Node) -> u32 {
//
//     let mut node_cost: FnvHashMap<_, _> = Default::default();
//     let mut prev: FnvHashMap<_, _> = Default::default();
//
//     // Algorithm 2 of paper "The Tao of PAO".
//     for (current_pin_index, current_aps) in sorted_nodes.iter().enumerate() {
//         if current_pin_index == 0 {
//
//             // Initialize node costs of access points in left-most pin.
//             for (current_node_index, curr_node) in current_aps.iter().enumerate() {
//                 node_cost.insert((current_pin_index, current_node_index), node_cost(curr_node))
//             }
//         } else {
//
//             // Set of nodes of the previous pin.
//             let prev_pin_index = current_pin_index - 1;
//             let prev_aps = &sorted_nodes[prev_pin_index];
//
//
//             // Iterate over all edges between the current and previous pin.
//             for (curr_ap_index, curr_node) in current_aps.iter().enumerate() {
//                 for (prev_ap_index, prev_node) in prev_aps.iter().enumerate() {
//
//                     // Find the cost of using the combination of this two access points.
//                     let edge_cost = edge_cost(prev_node, curr_node);
//
//                     let path_cost = prev_node.cost.get() + edge_cost;
//
//                     if path_cost < curr_node.cost.get() {
//                         // Found a better path.
//                         curr_node.cost.set(path_cost);
//                         curr_node.prev.set(Some((prev_pin_index, prev_ap_index)));
//                     }
//                 }
//             }
//         }
//     }
//
//     // Find the minimum-cost path by backtracing.
//     let (path, path_cost) = {
//         let last_node = sorted_nodes[sorted_nodes.len() - 1].iter()
//             .min_by_key(|node| {
//                 node.cost.get()
//             })
//             .expect("no access point found on last pin");
//         let mut path = vec![last_node];
//
//         while let Some((prev_pin_index, prev_ap_index)) = path[path.len() - 1].prev.get() {
//             let prev_node = &sorted_nodes[prev_pin_index][prev_ap_index];
//             path.push(prev_node);
//         }
//
//         path.reverse();
//
//         (path, last_node.cost.get())
//     };
//
//     // Clone the access points.
//     let access_points = path.into_iter()
//         .enumerate()
//         .map(|(i, node)| (sorted_pins[i].clone(), node.ap.clone()))
//         .collect();
//
//     CellAccessPattern {
//         access_points,
//         cost: path_cost,
//     }
// }
