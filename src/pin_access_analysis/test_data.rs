// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Simple test data such as standard-cells with pins and vias.

use libreda_pnr::db;
use libreda_pnr::db::L2NEdit;

use libreda_lefdef as lefdef;

fn test_lef() -> &'static str {
    r##"
VERSION 5.8 ;

BUSBITCHARS "[]" ;
DIVIDERCHAR "/" ;

UNITS
  DATABASE MICRONS 1000 ;
END UNITS

MANUFACTURINGGRID 0.005 ;

LAYER metal1
    TYPE ROUTING ;
    DIRECTION HORIZONTAL ;
    PITCH 0.4 ;
    WIDTH 0.1 ;
END metal1

LAYER via1
    TYPE CUT ;
    WIDTH 0.2 ;
    SPACING 0.2 ;
    ENCLOSURE BELOW 0.05 0.06 ;
    ENCLOSURE ABOVE 0.05 0.06 ;
END via1

LAYER metal2
    TYPE ROUTING ;
    DIRECTION VERTICAL ;
    PITCH 0.4 ;
    WIDTH 0.1 ;
END metal2

# VIAS
VIA M1_M2 DEFAULT
  LAYER metal1 ;
  RECT -0.15 -0.1 0.15 0.1 ;
  LAYER via1 ;
  RECT -0.08 -0.08 0.08 0.08 ;
  LAYER metal2 ;
  RECT -0.1 -0.15 0.1 0.15 ;
END M1_M2


MACRO INVX1
    CLASS CORE ;
    ORIGIN 0 0 ;
    SIZE 1.50 BY 2.00 ;
    SYMMETRY X Y ;

    PIN A
        DIRECTION INPUT ;
        USE SIGNAL ;
        PORT
            LAYER metal1 ;
                RECT 0.1 0.1 0.4 1.9 ;
        END
    END A

    PIN Y
        DIRECTION OUTPUT ;
        USE SIGNAL ;
        PORT
            LAYER metal1 ;
                RECT 0.5 0.1 1.2 1.9 ;
        END
    END Y

END INVX1

END LIBRARY
    "##
}

fn create_via_m1_m2<L2N>(chip: &mut L2N)
where
    L2N: L2NEdit<Coord = i32>,
{
    let via_cell = chip.create_cell("VIA_M1_M2".to_string().into());

    let metal1 = chip
        .layer_by_name("metal1")
        .expect("metal1 layer not found");
    let via1 = chip.layer_by_name("via1").expect("via1 layer not found");
    let metal2 = chip
        .layer_by_name("metal1")
        .expect("metal2 layer not found");

    let metal1_shape = db::Rect::new((-100, -100), (100, 100));
    let via1_shape = db::Rect::new((-50, -50), (50, 50));
    let metal2_shape = db::Rect::new((-100, -100), (100, 100));

    chip.insert_shape(&via_cell, &metal1, metal1_shape.into());
    chip.insert_shape(&via_cell, &via1, via1_shape.into());
    chip.insert_shape(&via_cell, &metal2, metal2_shape.into());
}

fn create_layers<L2N>(chip: &mut L2N)
where
    L2N: L2NEdit<Coord = i32>,
{
    let layers = vec![((1, 0), "metal1"), ((2, 0), "via1"), ((3, 0), "metal2")];

    for ((idx, dtype), name) in layers {
        let layer_id = chip.create_layer(idx, dtype);
        chip.set_layer_name(&layer_id, Some(name.to_string().into()));
    }
}

fn create_simple_cell<L2N>(chip: &mut L2N)
where
    L2N: L2NEdit<Coord = i32>,
{
    let cell = chip.create_cell("SIMPLE_CELL".to_string().into());

    let metal1 = chip
        .layer_by_name("metal1")
        .expect("metal1 layer not found");

    let pin1_shape = db::Rect::new((100, 100), (400, 600));
    let pin2_shape = db::Rect::new((1000, 100), (1400, 600));

    let pin1 = chip.create_pin(&cell, "A".to_string().into(), db::Direction::Input);
    let pin2 = chip.create_pin(&cell, "B".to_string().into(), db::Direction::Output);

    let pin1_shape_id = chip.insert_shape(&cell, &metal1, pin1_shape.into());
    let pin2_shape_id = chip.insert_shape(&cell, &metal1, pin2_shape.into());

    // Associate logical pin with shape.
    chip.set_pin_of_shape(&pin1_shape_id, Some(pin1));
    chip.set_pin_of_shape(&pin2_shape_id, Some(pin2));
}

/// Create a simple layout with one cell and one via for testing the pin access analysis.
pub(crate) fn create_test_layout() -> db::Chip<i32> {
    let mut chip = Default::default();

    create_layers(&mut chip);
    create_via_m1_m2(&mut chip);
    create_simple_cell(&mut chip);

    chip
}

/// Create a simple layout with one cell and one via for testing the pin access analysis.
pub(crate) fn create_test_layout_from_lef() -> (lefdef::LEF, db::Chip<i32>) {
    let lef = lefdef::LEF::load(&mut test_lef().as_bytes()).expect("failed to parse LEF");

    let mut chip = Default::default();

    let mut import_options = lefdef::import::LEFImportOptions::default();
    import_options.create_missing_layers = true;
    import_options.pin_suffix = "".into();

    lefdef::import::import_lef_into_db(&import_options, &lef, &mut chip)
        .expect("failed to import LEF");

    (lef, chip)
}

#[test]
fn test_is_valid_lef() {
    create_test_layout_from_lef();
}
