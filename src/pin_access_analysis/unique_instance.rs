// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Representation of 'unique' cell instances.
//!
//! Cell instances can be grouped into equivalence-classes regarding their placement relative
//! to the periodic routing tracks. The equivalence-class is called 'UniqueInstance'.
//! Two cell instances are considered equivalent if they have the same rotation/mirroring and the same 'phase' offset relative to the routing
//! tracks.

use fnv::{FnvHashMap, FnvHashSet};
use num_traits::Zero;

use super::tracks::Tracks;
use crate::db::L2NBase;
use itertools::Itertools;
use libreda_pnr::db;
use libreda_pnr::db::RoutingLayerStack;
use smallvec::SmallVec;
use std::collections::{HashMap, HashSet};
use std::hash::Hash;

/// A cell with a placement.
/// Cells which have an equivalent placement relative to the track patterns are considered equal.
///
// TODO: Merge `UniqueInstance` and `UniqueInstanceId`
#[derive(Clone, Debug)]
pub(crate) struct UniqueInstance<CellId, Coord> {
    /// ID of the cell template.
    pub(crate) cell_id: CellId,
    /// Placement of the cell instance.
    pub(crate) transform: db::SimpleTransform<Coord>,
    /// Identifier of this class of instances.
    pub id: UniqueInstanceId<CellId, Coord>,
}

/// Identifier for equivalent cell instances.
///
/// Two cell instances which have the same placement relative to the periodic track pattern
/// are equivalent when their `UniqueInstanceId`s are equal, and vice versa.
#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub(crate) struct UniqueInstanceId<CellId, Coord> {
    /// ID of the cell template.
    pub cell_id: CellId,
    /// Rotation of the instance.
    pub(super) rotation: db::Angle,
    /// Instance is mirrored.
    pub(super) mirror: bool,
    /// Phase-offsets relative to the tracks.
    /// The offsets are computed for the set of layers used by the pins of the cell plus the layers
    /// above the pins.
    ///
    /// In practice the number of offsets are expected to be `number_of_pin_layers + 1`.
    /// A `smallvec` of lengths 4 seems reasonable therefore.
    pub(super) track_offsets: SmallVec<[Coord; 4]>,
}

#[derive(Debug, Clone)]
pub(crate) struct UniqueInstances<LN: db::LayoutIds> {
    /// Map cell instances to unique instance IDs.
    pub cell_instances_to_unique_ids:
        HashMap<LN::CellInstId, UniqueInstanceId<LN::CellId, LN::Coord>>,
    /// Store one representative cell position for each unique instance ID.
    pub unique_ids_to_transforms:
        HashMap<UniqueInstanceId<LN::CellId, LN::Coord>, db::SimpleTransform<LN::Coord>>,
}

impl<LN: L2NBase> UniqueInstances<LN>
where
    LN::Coord: Eq + Hash,
{
    /// Iterate over all unique instances.
    pub fn each_unique_instance(
        &self,
    ) -> impl Iterator<Item = UniqueInstance<LN::CellId, LN::Coord>> + '_ {
        self.unique_ids_to_transforms
            .iter()
            .map(move |(unique_id, transform)| UniqueInstance {
                cell_id: unique_id.cell_id.clone(),
                transform: *transform,
                id: unique_id.clone(),
            })
    }

    pub fn get_unique_instance(
        &self,
        unique_id: &UniqueInstanceId<LN::CellId, LN::Coord>,
    ) -> UniqueInstance<LN::CellId, LN::Coord> {
        UniqueInstance {
            cell_id: unique_id.cell_id.clone(),
            transform: self.unique_ids_to_transforms[unique_id],
            id: unique_id.clone(),
        }
    }

    /// Get the `UniqueInstanceId` which represents the class of the given cell instance.
    pub fn get_unique_instance_id(
        &self,
        cell_instance: &LN::CellInstId,
    ) -> &UniqueInstanceId<LN::CellId, LN::Coord> {
        &self.cell_instances_to_unique_ids[cell_instance]
    }
}

pub(crate) fn create_unique_instances<LN: db::L2NBase, LayerStack>(
    chip: &LN,
    cell_instances: &[LN::CellInstId],
    layer_stack: &LayerStack,
    routing_tracks: &HashMap<LN::LayerId, Tracks<LN::Coord>>,
) -> UniqueInstances<LN>
where
    LayerStack: RoutingLayerStack<LayerId = LN::LayerId>,
    LN::Coord: Eq + Hash,
{
    // Find all used templates.
    let used_cells = cell_instances
        .iter()
        .map(|inst| chip.template_cell(inst))
        .unique()
        .collect_vec();

    // Find layers used for pins.
    let used_pin_layers: HashMap<LN::CellId, Vec<LN::LayerId>> = used_cells
        .iter()
        .map(|cell_id| {
            let all_pin_layers = chip
                .each_pin(cell_id)
                // .filter(|pin_id| !chip.pin_direction(pin_id).is_power()) // Ignore power pins. They are routed by other means.
                .flat_map(|pin_id| chip.shapes_of_pin(&pin_id))
                .map(|shape_id| chip.shape_layer(&shape_id))
                .unique();

            // Include also metal layers up to two layers above the pins.
            let all_relevant_layers: HashSet<_> = all_pin_layers
                .into_iter()
                .flat_map(|l| {
                    // Get two metal layers above.
                    let upper1 = layer_stack.get_upper_metal_layer(&l);
                    let upper2 = upper1
                        .as_ref()
                        .and_then(|l| layer_stack.get_upper_metal_layer(l));

                    std::iter::once(l)
                        .chain(upper1.into_iter())
                        .chain(upper2.into_iter())
                })
                .unique()
                .collect();

            // Sort the layers.
            let all_relevant_layers_sorted = {
                let mut s = layer_stack.routing_layer_stack();
                s.retain(|l| all_relevant_layers.contains(l));
                s
            };

            (cell_id.clone(), all_relevant_layers_sorted)
        })
        .collect();

    // // Print relevant pin layers.
    // for (cell_id, layer_ids) in &used_pin_layers {
    //     let layer_names = layer_ids.iter()
    //         .map(|l| format!("{:?}", chip.layer_info(l).name))
    //         .join(", ");
    //     log::info!("Used pin layers in cell '{}': {}", chip.cell_name(cell_id), layer_names);
    // }

    // Compute the equivalence class of a cell instance.
    let instance_class_id = |inst: &LN::CellInstId| -> UniqueInstanceId<LN::CellId, LN::Coord> {
        let tf = chip.get_transform(inst);

        let cell_id = chip.template_cell(inst);
        let relevant_layers = &used_pin_layers[&cell_id];

        // Compute for all layers the 'phase'-offset of the instance towards the tracks.
        let track_offsets = relevant_layers
            .iter()
            .map(|layer| {
                if let Some(tracks) = routing_tracks.get(layer) {
                    let cell_offset = match tracks.orientation {
                        db::Orientation2D::Vertical => tf.displacement.x,
                        db::Orientation2D::Horizontal => tf.displacement.y,
                    };

                    // Compute 'phase shift'
                    (cell_offset - tracks.offset) % tracks.pitch
                } else {
                    // No offset, if no routing tracks exist on that layer.
                    LN::Coord::zero()
                }
            })
            .collect();

        UniqueInstanceId {
            cell_id,
            rotation: tf.rotation,
            mirror: tf.mirror,
            track_offsets,
        }
    };

    let cell_instance_classes: Vec<_> = cell_instances
        .iter()
        .map(|inst| {
            let class = instance_class_id(inst);
            let cell_id = chip.template_cell(inst);

            (inst.clone(), class)
        })
        .collect();

    // Print statistics on unique instances.
    {
        // Count number of different unique instances per cell template.
        let mut counts: FnvHashMap<LN::CellId, FnvHashSet<_>> = Default::default();
        cell_instance_classes.iter().for_each(|(inst, class)| {
            counts
                .entry(class.cell_id.clone())
                .or_insert(Default::default())
                .insert(class);
        });

        // Sort by descending number of unique instances.
        let mut counts: Vec<_> = counts.into_iter().collect();
        counts.sort_by_key(|(_, u)| u.len());
        counts.reverse();

        let total_counts = cell_instances
            .iter()
            .map(|inst| chip.template_cell(inst))
            .counts();

        for (template, unique_instances) in &counts {
            log::info!(
                "(number of unique instances)/(total number of instances) of '{}': {}/{}",
                chip.cell_name(template),
                unique_instances.len(),
                total_counts[template]
            );
        }
    }

    // Associate one representative cell position with each unique instance ID.
    let transforms = {
        let mut transforms = HashMap::new();

        for (inst, unique_id) in &cell_instance_classes {
            transforms.insert(unique_id.clone(), chip.get_transform(inst));
        }

        transforms
    };

    UniqueInstances {
        cell_instances_to_unique_ids: cell_instance_classes.into_iter().collect(),
        unique_ids_to_transforms: transforms,
    }
}
