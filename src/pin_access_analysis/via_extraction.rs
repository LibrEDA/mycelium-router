// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Extract via definitions from layouts, LEF or DEF (TODO).

use super::viadef::ViaDef;
use iron_shapes_algorithms::prelude::decompose_rectangles;
use libreda_pnr::db;
use log;
use num_traits::Bounded;

/// Extract the via definition from a cell which holds the layout of a via.
///
/// # Parameters
/// * `layer_stack`: Layer IDs of the routing layers ordered from the lowest metal layer to the highest metal layer.
pub fn extract_via_from_layout<L>(
    layer_stack: &Vec<db::RoutingLayer<L::LayerId>>,
    layout: &L,
    via_cell: &L::CellId,
) -> Result<ViaDef<L::Coord, L::LayerId>, ()>
where
    L: db::LayoutBase,
    L::Coord: Bounded + Ord,
{
    let mut low_layer = None;
    let mut high_layer = None;
    let mut via_shapes = Vec::new();

    log::debug!("extract via definition: {}", layout.cell_name(via_cell));

    // Inspect the layer stack and detect shapes of the via.
    for layer in layer_stack {
        // log::debug!("extract via definition, layer: {:?}", layout.layer_info(layer.as_id()).name);

        for shape in layout.each_shape_id(via_cell, layer.as_id()) {
            // Keep track of lowest and highest used layer.
            if low_layer.is_none() {
                low_layer = Some(layer);
            }
            high_layer = Some(layer);

            // Extract the geometry.
            let geometry = layout.shape_geometry(&shape);

            match geometry {
                db::Geometry::Rect(r) => via_shapes.push((layer.as_id().clone(), r)),
                db::Geometry::SimpleRPolygon(p) => {
                    for r in decompose_rectangles(p.edges()) {
                        via_shapes.push((layer.as_id().clone(), r))
                    }
                }
                _ => {
                    log::error!("only rectangle shapes supported");
                    return Err(());
                }
            };
        }
    }

    if let (Some(low_layer), Some(high_layer)) = (low_layer, high_layer) {
        // Assemble via definition structure.
        let via_def = ViaDef {
            via_name: layout.cell_name(via_cell).into(),
            low_layer: low_layer.as_id().clone(),
            high_layer: high_layer.as_id().clone(),
            shapes: via_shapes,
        };
        Ok(via_def)
    } else {
        log::error!("no via shapes found");
        Err(())
    }
}

#[test]
fn test_via_extraction_from_layout() {
    use db::technology::prelude::*;
    use db::traits::*;
    use libreda_lefdef as lefdef;

    let (lef, chip) = super::test_data::create_test_layout_from_lef();
    let design_rules = lefdef::LEFDesignRuleAdapter::new(&lef, &chip);

    let via_cell = chip.cell_by_name("M1_M2").expect("cell not found");

    let maybe_via_def = extract_via_from_layout(&design_rules.layer_stack(), &chip, &via_cell);

    assert!(maybe_via_def.is_ok());
    let via_def = maybe_via_def.unwrap();

    assert_eq!(Some(via_def.low_layer), chip.layer_by_name("metal1"));
    assert_eq!(Some(via_def.high_layer), chip.layer_by_name("metal2"));
    assert_eq!(via_def.shapes.len(), 3);
}
