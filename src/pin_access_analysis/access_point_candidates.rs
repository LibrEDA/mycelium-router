// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Generate pin access point candidates (not necessarily DRC clean).

use db::technology::prelude::*;
use libreda_pnr::db;

use num_traits::{Bounded, FromPrimitive, Num};
use std::marker::PhantomData;

use iron_shapes_algorithms::prelude::decompose_maximal_rectangles;

use super::*;
use crate::pin_access_analysis::tracks::Tracks;
use iron_shapes::prelude::Orientation2D;
use itertools::{assert_equal, Itertools};
use libreda_pnr::libreda_db::chip::LayerId;
use log::debug;
use std::collections::HashMap;
use std::hash::Hash;

/// Generate pin access point candidates.
///
/// Generated pins consist of:
///
/// 1) Pins on routing tracks.
/// 2) Pins on half routing tracks.
/// 3) Pins at centers of shapes (centers of maximum-area rectangles which fit in the shape).
pub struct SimplePinAccessPointGenerator<'a, LN, Rules>
where
    LN: db::L2NBase,
{
    /// Design rules.
    pub(crate) design_rules: &'a Rules,
    /// Track definitions.
    pub(crate) tracks: HashMap<LN::LayerId, Tracks<LN::Coord>>,
    /// Allow access to pin shapes with metal routing from the same layer.
    pub(crate) allow_planar_access: bool,
    /// Allow access to pin shapes from upper layers with a via.
    pub(crate) allow_via_access: bool,
    /// Maximum number of access point candidates to generate.
    pub(crate) max_num_candidates: usize,
}

/// Type of access coordinates.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum AccessCoordinateType {
    /// On a preferred track.
    OnTrack,
    /// In the middle of two preferred tracks.
    HalfTrack,
    /// Center-line of a maximal rectangle.
    ShapeCenter,
    /// Close to the boundary of the metal shape such that the via enclosure rule is just met.
    BoundaryEnclosure,
}

/// Order of access coordinate types. Most desired comes first.
const ACCESS_COORD_TYPE_PRIORITY: [AccessCoordinateType; 4] = [
    AccessCoordinateType::OnTrack,
    AccessCoordinateType::HalfTrack,
    AccessCoordinateType::ShapeCenter,
    AccessCoordinateType::BoundaryEnclosure,
];
// TODO: Make parametrizable in API
// const ACCESS_COORD_TYPE_PRIORITY: [AccessCoordinateType; 1] = [
//     AccessCoordinateType::ShapeCenter,
// ];

impl<'a, LN, Rules> SimplePinAccessPointGenerator<'a, LN, Rules>
where
    LN: db::L2NBase,
    LN::Coord: Bounded + Ord + FromPrimitive + std::fmt::Debug,
    Rules: RoutingLayerStack<LayerId = LN::LayerId> + PreferredRoutingDirection,
{
    /// Find pin candidates for access points within a rectangular shape.
    ///
    /// # Parameters
    /// * `pin_shape`: Find access point candidates within this shape.
    /// * `layer`: Use the design rules and track definitions of this layer.
    fn gen_pins(
        &self,
        pin_shape: &db::SimpleRPolygon<LN::Coord>,
        layer: &LN::LayerId,
    ) -> Vec<PinAccessPoint<LN>> {
        /// Find the metal layer on top of `layer`.
        /// TODO: This could be slow.
        let upper_layer = self
            .design_rules
            .get_upper_metal_layer(layer)
            .expect("no upper metal layer found");

        // Find access points for each maximal rectangle in the pin shape.
        let mut points = Vec::new();
        {
            let max_rectangles = decompose_maximal_rectangles(pin_shape.edges());

            let tracks_lower = self
                .tracks
                .get(layer)
                .expect("no routing tracks found for layer");

            let tracks_upper = self
                .tracks
                .get(&upper_layer)
                .expect("no routing tracks found for layer");

            for pin_shape_max_rect in max_rectangles {
                self.gen_points(
                    &mut |p| points.push(p), // Collect points.
                    tracks_lower,
                    tracks_upper,
                    layer,
                    &pin_shape_max_rect,
                );
            }

            points.sort_unstable();
            points.dedup();
        }

        let access_directions = self.default_access_directions(layer);

        // Convert to access points.
        points
            .into_iter()
            .map(|p| PinAccessPoint {
                layer: layer.clone(),
                rel_location: p,
                access_directions,
                possible_vias: vec![],
                cost: 0,
            })
            .collect()
    }

    /// Default access directions of a pin access point.
    fn default_access_directions(&self, _layer: &LN::LayerId) -> PinAccessDirections {
        let access_directions = PinAccessDirections::none();

        let access_directions = if self.allow_planar_access {
            access_directions
                .add(PinAccessDirections::east())
                .add(PinAccessDirections::north())
                .add(PinAccessDirections::west())
                .add(PinAccessDirections::south())
        } else {
            access_directions
        };

        if self.allow_via_access {
            access_directions.add(PinAccessDirections::up())
        } else {
            access_directions
        }
    }

    /// Generate access coordinates (x or y) within a rectangular shape.
    /// The output is written to the `output` function.
    fn gen_coordinates<OutFn>(
        &self,
        coord_orientation: Orientation2D,
        tracks: &Tracks<LN::Coord>,
        tracks_upper: &Tracks<LN::Coord>,
        pin_shape_max_rect: &db::Rect<LN::Coord>,
        coord_type: AccessCoordinateType,
        output: &mut OutFn, // Output functor.
    ) where
        OutFn: FnMut(LN::Coord),
    {
        assert_ne!(
            tracks.orientation, tracks_upper.orientation,
            "tracks on upper and lower metal layer must have perpendicular orientation"
        );

        match coord_type {
            AccessCoordinateType::OnTrack => {
                [&tracks, &tracks_upper]
                    .iter()
                    .filter(|tracks| tracks.orientation == coord_orientation.other())
                    .flat_map(|tracks| tracks.all_offsets_in_region(pin_shape_max_rect))
                    .for_each(output);
            }
            AccessCoordinateType::HalfTrack => {
                [tracks.half_tracks(), tracks_upper.half_tracks()]
                    .iter()
                    .filter(|tracks| tracks.orientation == coord_orientation.other())
                    .flat_map(|tracks| tracks.all_offsets_in_region(pin_shape_max_rect))
                    .for_each(output);
            }
            AccessCoordinateType::ShapeCenter => {
                let c = pin_shape_max_rect.center();
                match coord_orientation {
                    Orientation2D::Horizontal => output(c.x),
                    Orientation2D::Vertical => output(c.y),
                }
            }
            AccessCoordinateType::BoundaryEnclosure => {
                // TODO
            }
        }
    }

    /// Construct points from combinations of on-track/half-track/shape-center coordinates.
    fn gen_points<OutFn>(
        &self,
        output: &mut OutFn, // Output functor.
        tracks_lower: &Tracks<LN::Coord>,
        tracks_upper: &Tracks<LN::Coord>,
        layer: &LN::LayerId,
        pin_shape_max_rect: &db::Rect<LN::Coord>,
    ) where
        OutFn: FnMut(db::Point<LN::Coord>),
    {
        let preferred_direction = self
            .design_rules
            .preferred_routing_direction(layer)
            .expect("no preferred routing direction known for layer");

        let mut count = 0;

        for non_pref in ACCESS_COORD_TYPE_PRIORITY.iter().take(3).copied() {
            for pref in ACCESS_COORD_TYPE_PRIORITY.iter().copied() {
                // Stop when enough candidates have been found.
                if count > self.max_num_candidates {
                    return;
                }

                self.gen_coordinates(
                    preferred_direction.other(),
                    tracks_lower,
                    tracks_upper,
                    pin_shape_max_rect,
                    non_pref,
                    &mut |coord1| {
                        self.gen_coordinates(
                            preferred_direction,
                            tracks_lower,
                            tracks_upper,
                            pin_shape_max_rect,
                            non_pref,
                            &mut |coord2| {
                                let p = if preferred_direction.is_vertical() {
                                    db::Point::new(coord1, coord2)
                                } else {
                                    db::Point::new(coord2, coord1)
                                };

                                debug_assert!(pin_shape_max_rect.contains_point(p));

                                output(p);
                                count += 1;
                            },
                        );
                    },
                );
            }
        }
    }
}

impl<'a, LN, Rules> PinAccessPointCandidateGenerator<LN>
    for SimplePinAccessPointGenerator<'a, LN, Rules>
where
    LN: db::L2NBase,
    LN::Coord: Bounded + Ord + FromPrimitive + std::fmt::Debug,
    Rules: RoutingLayerStack<LayerId = LN::LayerId> + PreferredRoutingDirection,
{
    fn generate_pin_access_point_candidates(
        &self,
        pin_shape: &db::SimpleRPolygon<LN::Coord>,
        layer: &LN::LayerId,
    ) -> Vec<PinAccessPoint<LN>> {
        self.gen_pins(pin_shape, layer)
    }
}

#[test]
fn test_generate_coordinates() {
    let mut chip = db::Chip::new();

    // Create layers.
    let metal1 = chip.create_layer(1, 0);
    let via1 = chip.create_layer(2, 0);
    let metal2 = chip.create_layer(3, 0);

    let mut tracks = HashMap::new();

    let tracks0 = Tracks {
        region: db::Rect::new((0i32, 0), (100, 200)),
        pitch: 10,
        offset: 1,
        orientation: Orientation2D::Horizontal,
    };

    let tracks2 = Tracks {
        region: db::Rect::new((0, 0), (100, 200)),
        pitch: 10,
        offset: 2,
        orientation: Orientation2D::Vertical,
    };

    tracks.insert(metal1, tracks0);
    tracks.insert(metal2, tracks2);

    struct DummyRules<LayerId> {
        layer_stack: Vec<RoutingLayer<LayerId>>,
        routing_directions: HashMap<LayerId, db::Orientation2D>,
    }

    impl RuleBase for DummyRules<LayerId> {
        type LayerId = LayerId;
    }

    impl RoutingLayerStack for DummyRules<LayerId> {
        fn layer_stack(&self) -> Vec<RoutingLayer<Self::LayerId>> {
            self.layer_stack.clone()
        }
    }

    impl PreferredRoutingDirection for DummyRules<LayerId> {
        fn preferred_routing_direction(&self, layer: &Self::LayerId) -> Option<Orientation2D> {
            self.routing_directions.get(layer).cloned()
        }
    }

    let rules = DummyRules {
        layer_stack: vec![
            RoutingLayer::new(metal1, RoutingLayerType::Routing),
            RoutingLayer::new(via1, RoutingLayerType::Cut),
            RoutingLayer::new(metal2, RoutingLayerType::Routing),
        ],
        routing_directions: vec![
            (metal1, db::Orientation2D::Horizontal),
            (metal2, db::Orientation2D::Vertical),
        ]
        .into_iter()
        .collect(),
    };

    let coord_gen: SimplePinAccessPointGenerator<db::Chip, _> = SimplePinAccessPointGenerator {
        design_rules: &rules,
        tracks,
        allow_planar_access: false,
        allow_via_access: true,
        max_num_candidates: 10,
    };

    let pin_layer = 0;
    let pin_shape = db::Rect::new((10, 10), (30, 50));

    let test_vectors = vec![
        // (coordinate axis, coordinate type, expected coordinates)
        (
            Orientation2D::Horizontal,
            AccessCoordinateType::OnTrack,
            vec![12, 22],
        ), // on-track x
        (
            Orientation2D::Horizontal,
            AccessCoordinateType::HalfTrack,
            vec![17, 27],
        ), // half-track x
        (
            Orientation2D::Vertical,
            AccessCoordinateType::OnTrack,
            vec![11, 21, 31, 41],
        ), // on-track y
        (
            Orientation2D::Vertical,
            AccessCoordinateType::HalfTrack,
            vec![16, 26, 36, 46],
        ), // half-track y
        (
            Orientation2D::Horizontal,
            AccessCoordinateType::ShapeCenter,
            vec![pin_shape.center().x],
        ), // center lines
        (
            Orientation2D::Vertical,
            AccessCoordinateType::ShapeCenter,
            vec![pin_shape.center().y],
        ), // center lines
    ];

    // Run and check test vectors.
    for (coord_axis, coord_type, expected_coords) in test_vectors {
        let mut coords = vec![];
        coord_gen.gen_coordinates(
            coord_axis,
            &coord_gen.tracks[&metal1],
            &coord_gen.tracks[&metal2],
            &pin_shape,
            coord_type,
            &mut |coord| coords.push(coord),
        );

        assert_eq!(coords, expected_coords);
    }
}
