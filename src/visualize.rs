// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Plot rectilinear shapes on the terminal using Unicode box characters.

use iron_shapes::prelude::{Point, REdge, REdgeOrientation, TryCastCoord};
use num_traits::PrimInt;
use std::collections::{HashMap, HashSet};

/// Plot rectilinear shapes on the terminal using Unicode box characters.
#[derive(Clone)]
pub struct TermPlot {
    char_layers: HashMap<i32, HashMap<(i32, i32), Char>>,
    /// Print layers in different colors.
    use_layer_colors: bool,
}

#[derive(Copy, Clone)]
enum Char {
    Char(char),
    Box(BoxChar),
}

/// Bitmap representation for box characters.
/// Each bit of the lower nibble enables a box line in one quarter of a character.
#[derive(Copy, Clone)]
struct BoxChar(u8);

impl BoxChar {
    /// Convert the box bitmap into the printable Unicode character.
    fn to_char(&self) -> char {
        match self.0 {
            0b0000 => ' ',
            0b0001 => '╶',
            0b0010 => '╵',
            0b0100 => '╴',
            0b1000 => '╷',
            0b0011 => '└',
            0b0110 => '┘',
            0b1100 => '┐',
            0b0101 => '─',
            0b1010 => '│',
            0b1001 => '┌',
            0b0111 => '┴',
            0b1110 => '┤',
            0b1101 => '┬',
            0b1011 => '├',
            0b1111 => '┼',
            _ => '?',
        }
    }

    /// Enable a box line in the specified quarter.
    fn set_dir(&self, dir: u32, enable: bool) -> Self {
        assert!(dir < 4);
        let mask = 1 << dir;
        let value = if enable { mask } else { 0 };
        BoxChar(self.0 & (!mask) | value)
    }
}

impl Char {
    /// Convert to a printable character.
    fn to_char(&self) -> char {
        match self {
            Char::Char(c) => *c,
            Char::Box(bx) => bx.to_char(),
        }
    }
}

impl TermPlot {
    /// Create an empty plot.
    pub fn new() -> Self {
        TermPlot {
            char_layers: Default::default(),
            use_layer_colors: false,
        }
    }

    /// Print layers in different colors.
    pub fn enable_colors(&mut self, enable: bool) {
        self.use_layer_colors = enable;
    }

    /// Clear the plot.
    pub fn clear(&mut self) {
        self.char_layers.clear()
    }

    /// Clear the plot.
    pub fn clear_layer(&mut self, layer: i32) {
        self.char_layers.remove(&layer);
    }

    /// Print the buffer to the terminal.
    /// Empty space is cropped away.
    pub fn print(&self) {
        {
            // Avoid interleaving of stderr and stdout.
            use std::io::Write;
            std::io::stderr().flush();
        }

        let bbox = self
            .char_layers
            .values()
            .flat_map(|chars| chars.keys())
            .copied()
            .fold(None, |acc: Option<((i32, i32), (i32, i32))>, (x, y)| {
                if let Some(((xmin, ymin), (xmax, ymax))) = acc {
                    Some(((xmin.min(x), ymin.min(y)), (xmax.max(x), ymax.max(y))))
                } else {
                    Some(((x, y), (x, y)))
                }
            });

        // Check which coordinates are actually used.
        // All others can be skipped for compressing the plot.
        // Can destroy aspect ratio though.
        let enable_compressed_plot = false;
        let used_x: HashSet<_> = self
            .char_layers
            .values()
            .flat_map(|chars| chars.keys())
            .map(|(x, _)| *x)
            .collect();
        let used_y: HashSet<_> = self
            .char_layers
            .values()
            .flat_map(|chars| chars.keys())
            .map(|(_, y)| *y)
            .collect();

        if let Some(((xmin, ymin), (xmax, ymax))) = bbox {
            for y in (ymin..=ymax).rev() {
                if used_y.contains(&y) || !enable_compressed_plot {
                    for x in xmin..=xmax {
                        if used_x.contains(&x) || !enable_compressed_plot {
                            // Top most char.

                            let (layer, top_char) = self
                                .char_layers
                                .iter()
                                .flat_map(|(layer, chars)| {
                                    let c = chars.get(&(x, y)).map(|c| c.to_char());
                                    c.map(|c| (layer, c))
                                })
                                .max_by_key(|(layer, _)| **layer)
                                .map(|(layer, c)| (*layer, c))
                                .unwrap_or((0, ' '));

                            if self.use_layer_colors {
                                let color = layer + 1;
                                print!("\x1b[0;3{}m", color);
                            }
                            print!("{}", top_char);
                        }
                    }
                    println!();
                    if self.use_layer_colors {
                        print!("\x1b[0;0m");
                    }
                }
            }
        } else {
            println!("[empty]");
        }

        {
            // Avoid interleaving of stderr and stdout.
            use std::io::Write;
            std::io::stdout().flush();
        }
    }

    /// Enable a half-line in one quadrant.
    /// 0 = 3 o'clock
    /// 1 = 12 o'clock
    /// 2 = 9 o'clock
    /// 3 = 6 o'clock
    pub fn set_box_dir<T: PrimInt>(&mut self, layer: i32, point: Point<T>, dir: u32, enable: bool) {
        let p: (i32, i32) = point.cast().into();

        let chars = self.char_layers.entry(layer).or_insert(HashMap::new());

        let old = chars.get(&p).copied().unwrap_or(Char::Box(BoxChar(0)));

        let new = match old {
            Char::Char(_) => BoxChar(0).set_dir(dir, enable),
            Char::Box(bx) => bx.set_dir(dir, enable),
        };

        chars.insert(p, Char::Box(new));
    }

    /// Draw a character at a given location.
    pub fn set_char<T: PrimInt>(&mut self, layer: i32, point: Point<T>, c: char) {
        let p: (i32, i32) = point.cast().into();

        let chars = self.char_layers.entry(layer).or_insert(HashMap::new());

        chars.insert(p, Char::Char(c));
    }

    /// Draw a rectilinear edge.
    pub fn draw_edge<T: PrimInt>(&mut self, layer: i32, edge: REdge<T>) {
        let start = edge.start.to_i32().unwrap();
        let end = edge.end.to_i32().unwrap();
        let offset = edge.offset.to_i32().unwrap();

        // Normalize edge direction.
        let (start, end) = if start < end {
            (start, end)
        } else {
            (end, start)
        };

        if end - start > 0 {
            // Skip edges of zero length.
            // Pixelize the edge.
            match edge.orientation {
                REdgeOrientation::Horizontal => {
                    self.set_box_dir(layer, Point::new(start, offset), 0, true);
                    for x in start + 1..end {
                        self.set_box_dir(layer, Point::new(x, offset), 0, true);
                        self.set_box_dir(layer, Point::new(x, offset), 2, true);
                    }
                    self.set_box_dir(layer, Point::new(end, offset), 2, true);
                }
                REdgeOrientation::Vertical => {
                    self.set_box_dir(layer, Point::new(offset, start), 1, true);
                    for y in start + 1..end {
                        self.set_box_dir(layer, Point::new(offset, y), 1, true);
                        self.set_box_dir(layer, Point::new(offset, y), 3, true);
                    }
                    self.set_box_dir(layer, Point::new(offset, end), 3, true);
                }
            }
        }
    }
}
