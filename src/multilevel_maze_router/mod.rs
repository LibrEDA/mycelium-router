// Copyright (c) 2020-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Multi-level maze routing.
//!
//! # References
//! * MGR: Multi-Level Global Router <https://home.engineering.iastate.edu/~cnchu/pubs/c66.pdf>
use fnv::{FnvHashMap, FnvHashSet};
use itertools::Itertools;
use num_traits::{FromPrimitive, Num, PrimInt, ToPrimitive, Zero};

use libreda_pnr::db::Point;

use std::borrow::Cow;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::hash::Hash;
use std::io::{BufWriter, Error};
use std::marker::PhantomData;

use crate::global_router::*;
use crate::graph::*;
use crate::maze_router::router_traits::{MultiSignalRouter, RoutedSignal};
use crate::multilevel_maze_router::coarsen::GraphCoarsener;
use slice_refinement::SliceRefinementRouter;

mod coarsen;
mod slice_refinement;

/// Creates a multi-level router from a multi-signal routing algorithm.
/// The multi-level routing algorithm finds routing solutions on a coarsened routing graph
/// and then refines them on the original routing graph. This reduces the search space for
/// the underlying routing algorithm with loss of optimality.
pub struct MultilevelMazeRouter<'a, R, Coord> {
    /// Underlying multi-signal router.
    router: &'a R,
    /// Minimum dimensions of the grid in such that graph coarsening is applied.
    /// The coarsening ends, if the grid width and heights are both below the given values.
    min_level_size_for_coarsening: (usize, usize),
    /// Write global congestion information to files in this directory.
    pub dump_global_congestion_directory: Option<std::path::PathBuf>,
    _coordinate_type: PhantomData<Coord>,
}

impl<'a, R, Coord> MultilevelMazeRouter<'a, R, Coord> {
    /// Create a new multi-level router based on a normal multi-signal router.
    /// The muli-level router does the coarsening and refinement of the routes but uses the underlying `multi_signal_router` for the
    /// actual path-search and collision avoidance.
    pub fn new(multi_signal_router: &'a R) -> Self {
        Self {
            router: multi_signal_router,
            min_level_size_for_coarsening: (128, 128),
            dump_global_congestion_directory: None,
            _coordinate_type: Default::default(),
        }
    }

    /// Specify a directory where global congestion information should be written to.
    /// This is meant for inspecting the algorithm.
    pub fn set_global_congestion_output_directory(&mut self, dir: std::path::PathBuf) -> &mut Self {
        log::info!("Set output directory for congestion information: {:?}", dir);
        self.dump_global_congestion_directory = Some(dir);
        self
    }
}

impl<'a, R, Coord> MultilevelMazeRouter<'a, R, Coord>
where
    Coord: PrimInt,
{
    /// Write edge capacities to files.
    fn dump_congestion_information(
        &self,
        graph: &GlobalRoutingGraph<Coord>,
    ) -> std::io::Result<()> {
        if let Some(dir) = self.dump_global_congestion_directory.as_ref() {
            log::info!("Write congestion information to: {:?}", dir);

            let grid = graph.grid_graph();

            let size = format!("{}x{}", grid.num_x(), grid.num_y());

            /// Dump all layers, each x/y/z direction each.
            for axis in [Axis3D::X, Axis3D::Y, Axis3D::Z] {
                let num_layers = if axis == Axis3D::Z {
                    // There is one less via layers than metal layers.
                    grid.num_layers - 1
                } else {
                    grid.num_layers
                };
                for layer in 0..num_layers {
                    let axis_name = match axis {
                        Axis3D::X => "x",
                        Axis3D::Y => "y",
                        Axis3D::Z => "z",
                    };

                    // Edge capacity.
                    {
                        let edge_file_name =
                            format!("{}_edge_capacity_{}_layer_{}.mat", size, axis_name, layer);
                        let mut edge_output_path = dir.clone();
                        edge_output_path.push(edge_file_name);

                        log::info!("Write edge capacity information to: {:?}", edge_output_path);
                        let mut writer = BufWriter::new(std::fs::File::create(edge_output_path)?);
                        graph.dump_edge_data(
                            &mut writer,
                            &|edge_data| edge_data.capacity,
                            layer as usize,
                            axis,
                        )?;
                    }

                    // Edge usage.
                    {
                        let edge_file_name =
                            format!("{}_edge_usage_{}_layer_{}.mat", size, axis_name, layer);
                        let mut edge_output_path = dir.clone();
                        edge_output_path.push(edge_file_name);

                        log::info!("Write edge usage information to: {:?}", edge_output_path);
                        let mut writer = BufWriter::new(std::fs::File::create(edge_output_path)?);
                        graph.dump_edge_data(
                            &mut writer,
                            &|edge_data| edge_data.usage,
                            layer as usize,
                            axis,
                        )?;
                    }
                }
            }
        }
        Ok(())
    }
}

impl<'a, R, Coord> MultiSignalRouter for MultilevelMazeRouter<'a, R, Coord>
where
    R: MultiSignalRouter<Graph = GridGraph<Coord>, GraphNode = Node3D<Coord>>,
    R: Sync,
    R::Cost: Num + Ord + FromPrimitive + ToPrimitive + Copy + Send + Sync,
    R::RoutingResult: Sync + Send,
    R::NetId: std::fmt::Debug + Clone + Hash + Eq + Sync + Send + 'static,
    Coord: std::fmt::Debug + Copy + Hash + Sync + Send + PrimInt + FromPrimitive + 'static,
{
    type Graph = GlobalRoutingGraph<Coord>;
    type GraphNode = Node3D<Coord>;
    type NetId = R::NetId;
    type RoutingResult = R::RoutingResult;
    type Cost = R::Cost;

    /// Multi-level routing.
    fn route_multisignal<NodeCostFn, EdgeCostFn>(
        &self,
        fine_graph: &Self::Graph,
        node_cost_fn: &NodeCostFn,
        edge_cost_fn: &EdgeCostFn,
        node_capacity_fn: &(dyn Fn(&Self::GraphNode) -> u32 + Sync),
        edge_capacity_fn: &(dyn Fn(&Self::GraphNode, &Self::GraphNode) -> u32 + Sync),
        can_use_node_fn: &(dyn Fn(&Self::NetId, &Self::GraphNode) -> bool + Sync),
        net_terminals: &Vec<(Self::NetId, Vec<Vec<Self::GraphNode>>)>,
        net_weights: &HashMap<Self::NetId, f64>,
    ) -> Result<Vec<(Self::NetId, R::RoutingResult)>, ()>
    where
        NodeCostFn: Fn(&Self::GraphNode) -> Self::Cost + Sync,
        EdgeCostFn: Fn(&Self::GraphNode, &Self::GraphNode) -> Self::Cost + Sync,
    {
        let original_fine_graph = fine_graph;
        let fine_grid = *fine_graph.grid_graph();

        log::info!("Grid size: {} * {}", fine_grid.num_x(), fine_grid.num_y());

        // Sanity check. All routing terminals must be contained in the graph.
        {
            let mut all_terminals = net_terminals
                .iter()
                .flat_map(|(_, pins)| pins.iter().flat_map(|terms| terms.iter()));
            let all_terminals_in_graph = all_terminals.all(|t| fine_grid.contains_node(t));
            if !all_terminals_in_graph {
                log::error!("Supplied routing terminals are not all contained in the graph.");
                return Err(());
            }
        }

        let reached_coarsest_level = fine_grid.num_x() <= self.min_level_size_for_coarsening.0
            && fine_grid.num_y() <= self.min_level_size_for_coarsening.1;

        if reached_coarsest_level {
            // Coarsest level. Route directly on the current graph without any more coarsening steps.
            log::info!("Reached coarsest level.");

            // // Compute routing hints with FLUTE.
            // // The 2D routes computed by FLUTE will be used to bias the node and edge weights.
            // {
            //     log::info!("Generate lookup-table for steiner-trees.")
            //     let lut = steiner_tree::gen_lut::gen_full_lut(8); //  TODO: Pre-compute LUT and store it somewhere else.
            //
            //     log::info!("Generate steiner-trees.");
            //
            //     // An accuracy value must be provided for the heuristic.
            //     // Larger values lead to better results but also to longer computations.
            //     let accuracy = 3;
            //
            //     net_terminals.iter()
            //         .map(|(net, terminals)| {
            //
            //         })
            //
            //     {
            //         let points = vec![(1, 2).into(), (3, 4).into(), (5, 6).into(), (7, 8).into()];
            //         let (medium_tree, medium_tree_weight) = lut.rsmt_medium_degree(points, accuracy);
            //     }
            // }

            // Anchor of recursion.
            // Pass to underlying router.
            let routes = self.router.route_multisignal(
                fine_graph.grid_graph(),
                &node_cost_fn,
                &edge_cost_fn,
                &|node| fine_graph.get_node_data(node).capacity,
                &|a, b| {
                    let edge = (*a, *b).into();

                    fine_graph.get_edge_data(&edge).capacity
                },
                can_use_node_fn,
                net_terminals,
                net_weights,
            );
            if routes.is_err() {
                log::error!("Failed to find routes on coarsest level.");
            }
            routes
        } else {
            // Coarsening.
            log::info!("Coarsening the grid graph.");

            // Find coarsened terminals of the nets.
            let graph_coarsener = GraphCoarsener::new2x2(fine_grid).with_capacity_margin(20);

            let coarse_grid = graph_coarsener.get_coarsened_graph();
            let coarse_terminals = graph_coarsener.coarsen_nets(net_terminals);

            // Sanity check.
            assert!(
                {
                    let mut all_terminals = coarse_terminals
                        .iter()
                        .flat_map(|(_, pins)| pins.iter().flat_map(|terms| terms.iter()));
                    all_terminals.all(|t| coarse_grid.contains_node(t))
                },
                "Coarsened terminals must be in the coarse graph."
            );

            // Create a mutable copy of the graph to be able to adjust capacities.
            // Also adjust the dimension such that it is divisible by the coarsening factor.
            let enlarged_fine_graph = {
                let coarsening_factor = 2;
                let grow_x = coarse_grid.num_x() * coarsening_factor - fine_grid.num_x();
                let grow_y = coarse_grid.num_y() * coarsening_factor - fine_grid.num_y();
                fine_graph.enlarged(grow_x, grow_y)
            };
            let mut adjusted_fine_graph = enlarged_fine_graph.clone();

            // Find nets which occupy only one node in the coarsened graph.
            // Store the coarse location of this nets.
            let disappeared_nets: FnvHashMap<_, _> = coarse_terminals
                .iter()
                .map(|(net, pins)| {
                    let pin_locations = pins
                        .iter()
                        .flatten()
                        .map(|n| n.point())
                        .unique()
                        .collect_vec();
                    (net.clone(), pin_locations)
                })
                .filter(|(_net, pins)| pins.len() <= 1)
                .collect();
            log::info!(
                "Number of nets which will disappear with the next coarsening step: {}",
                disappeared_nets.len()
            );

            // Strip disappearing nets. They need not be considered in the coarse routing.
            let coarse_terminals: Vec<_> = coarse_terminals
                .into_iter()
                .filter(|(net, _pins)| !disappeared_nets.contains_key(&net))
                .collect();

            // Route nets which will disappear in this coarsening step.
            // This helps estimating the available capacity on the coarser level.
            let current_level_routes = {
                log::info!(
                    "Route nets which will disappear in this coarsening step (local-level nets)."
                );

                // Find all nets that need to be routed before coarsening.
                let nets_to_be_routed: Vec<_> = net_terminals
                    .iter()
                    .filter(|(net_id, _)| disappeared_nets.contains_key(net_id))
                    .cloned()
                    .collect();
                log::info!("Number of local-level nets: {}", nets_to_be_routed.len());

                // Start underlying router algorithm.
                let routing_result = self.router.route_multisignal(
                    adjusted_fine_graph.grid_graph(),
                    &node_cost_fn,
                    &edge_cost_fn,
                    &|node| adjusted_fine_graph.get_node_data(node).capacity,
                    &|a, b| {
                        let edge = (*a, *b).into();
                        adjusted_fine_graph.get_edge_data(&edge).capacity
                    },
                    can_use_node_fn,
                    &nets_to_be_routed,
                    net_weights,
                ); // TODO: How to handle errors here?

                if routing_result.is_err() {
                    log::error!("Failed to route nets on local coarsening level.");
                }

                routing_result?
            };

            // Update the usages of the fine graph with the routes of the current level.
            {
                // Increment node usages.
                current_level_routes
                    .iter()
                    .flat_map(|(_, route)| route.nodes())
                    .for_each(|node| {
                        // adjusted_fine_graph.get_node_data_mut(&node).usage += 1;
                        let cap = &mut adjusted_fine_graph.get_node_data_mut(&node).capacity;
                        *cap = cap.saturating_sub(1);
                    });
                // Increment edge usages.
                current_level_routes
                    .iter()
                    .flat_map(|(_, route)| route.edges())
                    .for_each(|edge| {
                        // adjusted_fine_graph.get_edge_data_mut(&edge.into()).usage += 1;
                        let cap = &mut adjusted_fine_graph.get_edge_data_mut(&edge.into()).capacity;
                        *cap = cap.saturating_sub(1);
                    });
            }

            /// Dump congestion information if desired.
            match self.dump_congestion_information(&adjusted_fine_graph) {
                Ok(_) => {}
                Err(e) => {
                    log::warn!("Failed to write congestion information: {}", e)
                }
            };

            // Compute node and edge capacities of the coarsened graph.
            log::info!("Compute node and edge capacities of the coarsened graph.");

            let coarse_graph = graph_coarsener.coarsen_capacities(&adjusted_fine_graph);

            let coarse_routes = {
                // Recursive call to this routing function
                // with the coarsened graph.
                let coarse_routes = self.route_multisignal(
                    &coarse_graph,
                    node_cost_fn, // TODO
                    edge_cost_fn, // TODO
                    &|node| coarse_graph.get_node_data(node).capacity,
                    &|a, b| {
                        let edge = (*a, *b).into();
                        coarse_graph.get_edge_data(&edge).capacity
                    },
                    &|_, _| true, // Every net can use every node.
                    &coarse_terminals,
                    net_weights,
                )?;
                assert_eq!(
                    coarse_terminals.len(),
                    coarse_routes.len(),
                    "Number of nets and computed routes must be equal."
                );

                let mut coarse_routes: FnvHashMap<_, _> = coarse_routes.into_iter().collect();

                // Insert coarse routes of nets which disappear in the next coarsening step.
                // Their coarse routes consist of a single coarse routing location.
                for (net, coarse_location) in &disappeared_nets {
                    assert!(!coarse_routes.contains_key(net));
                    let trivial_route = if let Some(node) = coarse_location.iter().next() {
                        R::RoutingResult::from_node(Node3D(*node, 0))
                    } else {
                        panic!("Disappearing net has no coarse route.")
                    };
                    coarse_routes.insert(net.clone(), trivial_route);
                }

                // Restore ordering of the nets.
                let coarse_routes = net_terminals
                    .iter()
                    .map(|(net, _)| {
                        let route = coarse_routes.remove(net).expect("Net has no route.");
                        (net.clone(), route)
                    })
                    .collect();

                coarse_routes
            };

            {
                // Refine the coarse routes.
                // Use the coarse routes as guides. Allow the refined routes to use other layers
                // than their coarse routes.
                log::info!(
                    "Refine coarse routes from {}x{} grid to {}x{} grid.",
                    coarse_grid.num_x(),
                    coarse_grid.num_x(),
                    enlarged_fine_graph.grid_graph().num_x(),
                    enlarged_fine_graph.grid_graph().num_x(),
                );

                let refinement_router = SliceRefinementRouter::new(self.router);

                let original_grid = original_fine_graph.grid_graph();
                let can_use_node_for_refinement_fn =
                    |net: &R::NetId, node: &R::GraphNode| -> bool {
                        can_use_node_fn(net, node) && original_grid.contains_node(node)
                    };

                refinement_router.refine_routes(
                    &graph_coarsener,
                    &coarse_graph,
                    &enlarged_fine_graph,
                    &coarse_routes,
                    node_cost_fn,
                    edge_cost_fn,
                    &can_use_node_for_refinement_fn,
                    net_terminals,
                    net_weights,
                )

                // self.refine_routes(
                //     &coarse_graph,
                //     original_fine_graph,
                //     &adjusted_fine_graph,
                //     &coarse_routes,
                //     node_cost_fn,
                //     edge_cost_fn,
                //     can_use_node_fn,
                //     net_terminals,
                //     net_weights,
                // )
            }
        }
    }

    fn enable_pin_feed_through(&mut self, enable_feed_through: bool) {
        todo!();
        // self.router.enable_pin_feed_through(enable_feed_through)
    }

    fn is_pin_feed_through_enabled(&self) -> bool {
        self.router.is_pin_feed_through_enabled()
    }
}
//
// impl<'a, R, Coord> MultilevelMazeRouter<'a, R, Coord>
//     where R: MultiSignalRouter<Graph=GridGraph<Coord>, GraphNode=Node3D<Coord>>,
//           R: Sync,
//     // R::Graph: TraverseGraph<R::GraphNode> + Sync,
//           R::GraphNode: Eq + Hash + Copy + Sync,
//           R::Cost: Num + Ord + FromPrimitive + ToPrimitive + Copy + Send + Sync,
//           R::RoutingResult: Sync + Send,
//           R::NetId: std::fmt::Debug + Clone + Hash + Eq + Sync,
//           Coord: std::fmt::Debug + Copy + Hash + Sync + PrimInt + FromPrimitive
// {
//     /// Propagate coarse routes to the fine grid.
//     fn refine_routes<NodeCostFn, EdgeCostFn>(
//         &self,
//         graph_coarsener: &GraphCoarsener<Coord>,
//         original_fine_graph: &GlobalRoutingGraph<Coord>,
//         adjusted_fine_graph: &GlobalRoutingGraph<Coord>,
//         coarse_routes: &Vec<(R::NetId, R::RoutingResult)>,
//         node_cost_fn: &NodeCostFn,
//         edge_cost_fn: &EdgeCostFn,
//         can_use_node_fn: &(dyn Fn(&R::NetId, &R::GraphNode) -> bool + Sync),
//         net_terminals: &Vec<(R::NetId, Vec<Vec<R::GraphNode>>)>,
//         net_weights: &HashMap<R::NetId, f64>,
//     )
//         -> Result<Vec<(R::NetId, R::RoutingResult)>, ()>
//         where
//             NodeCostFn: Fn(&R::GraphNode) -> R::Cost + Sync,
//             EdgeCostFn: Fn(&R::GraphNode, &R::GraphNode) -> R::Cost + Sync,
//     {
//         let coarse_graph = graph_coarsener.get_coarsened_graph();
//         let num_layers = coarse_graph.num_layers;
//
//         let guides: FnvHashMap<R::NetId, FnvHashSet<R::GraphNode>> = coarse_routes.iter()
//             .map(|(net, route)| {
//                 let mut guide: FnvHashSet<_> = Default::default();
//                 for (p, layer) in route.nodes() {
//                     // The fine route can use the layer used by the coarse route
//                     // and also layers just below or above.
//                     guide.insert((p, layer));
//                     if layer + 1 < num_layers {
//                         guide.insert((p, layer + 1));
//                     }
//                     if layer > 0 {
//                         guide.insert((p, layer - 1));
//                     }
//                 }
//                 (*net, guide)
//             })
//             .collect();
//
//         // Check if the `fine_node` is contained in the coarse guide of the `net`.
//         let can_use_fine_node_fn = |net: &R::NetId, fine_node: &R::GraphNode| -> bool {
//             let coarse_node = graph_coarsener.find_coarse_node(fine_node);
//             let (p, layer) = coarse_node;
//             // let coarse_node = (coarse_node.0, 0); // Set to layer 0 to ignore layers.
//
//             let allowed_by_guide = guides.get(net)
//                 .map(|guide| {
//                     // Allow to use the layer used by the guide plus the ones below or above.
//                     guide.contains(&(p, layer))
//                         || guide.contains(&(p, layer + 1))
//                         || (layer != 0 && guide.contains(&(p, layer - 1)))
//                 })
//                 .unwrap_or(false);
//             allowed_by_guide && can_use_node_fn(net, fine_node) // Also respect the original net&node assignment.
//         };
//
//         // Call the router on the fine graph. The coarse routes
//         // narrow down the search space.
//
//         let refined_routes = self.router.route_multisignal(
//             original_fine_graph.grid_graph(), // Use the grid nodes of the original graph.
//             &node_cost_fn,
//             &edge_cost_fn,
//             &|node| {
//                 adjusted_fine_graph.get_node_data(node).capacity
//             },
//             &|a, b| {
//                 let edge = (*a, *b).into();
//                 adjusted_fine_graph.get_edge_data(&edge).capacity
//             },
//             &can_use_fine_node_fn,
//             net_terminals,
//             net_weights,
//         );
//
//         refined_routes
//     }
// }

#[test]
fn test_multilevel_maze_router() {
    // Find route for three signals where the independent solutions conflict with eachother.

    use crate::maze_router::pathfinder_sequential::PathFinderSequential;
    use crate::maze_router::simple_signal_router::SimpleSignalRouter;
    use iron_shapes::prelude::{Rect, Vector};

    let grid = GridGraph::new(
        Rect::new((0, 0), (32, 32)),
        2,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    let mut graph = GlobalRoutingGraph::new(grid);

    // Set node capacities.
    graph.grid_graph().clone().all_nodes().for_each(|n| {
        graph.get_node_data_mut(&n).capacity = 1;
    });

    // Set edge capacities.
    graph.grid_graph().clone().all_edges().for_each(|e| {
        graph.get_edge_data_mut(&e).capacity = 1;
    });

    let net1 = vec![vec![Node3D::new(1, 10, 0)], vec![Node3D::new(30, 10, 0)]];

    let net2 = vec![vec![Node3D::new(5, 10, 0)], vec![Node3D::new(25, 10, 0)]];

    let net3 = vec![vec![Node3D::new(15, 10, 0)], vec![Node3D::new(20, 10, 0)]];

    let signal_pins = vec![(1, net1), (2, net2), (3, net3)];

    type T = i32;

    // A-star heuristic.
    let heuristic_fn = |n1: &Node3D<_>, n2: &Node3D<T>| -> T { (n1.0 - n2.0).norm1() };

    let node_cost_fn = |n1: &Node3D<T>| -> T { 0 };

    let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
        if n1.1 != n2.1 {
            // Different layers.
            1 as T // Via cost
        } else {
            let (h_cost, v_cost) = if n1.1 % 2 == 0 {
                // Vertical layer. Lower cost for verticals.
                (40, 10)
            } else {
                (10, 40)
            };
            let mut diff = (n1.0 - n2.0);
            diff.x *= h_cost;
            diff.y *= v_cost;
            diff.norm1()
        }
    };

    let node_capacity_fn = |n1: &Node3D<T>| -> u32 { 1 };

    let edge_capacity_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> u32 { 1 };

    // Signal router which will be called from the Pathfinder algorithm.
    let signal_router = {
        let mut router = SimpleSignalRouter::new();
        router.min_spacing = 1;
        router.wire_half_width = 1;
        router
    };

    let path_finder = PathFinderSequential::new(&signal_router);

    let mut multilevel_router = MultilevelMazeRouter::new(&path_finder);
    multilevel_router.min_level_size_for_coarsening = (32, 32);

    // Find routes with the multilevel router.
    let result = multilevel_router
        .route_multisignal(
            &graph,
            &node_cost_fn,
            &edge_cost_fn,
            &node_capacity_fn,
            &edge_capacity_fn,
            &|_, _| true,
            &signal_pins,
            &HashMap::new(),
        )
        .expect("Routing failed.");

    // Convert to Vec for plotting.
    let routes: Vec<_> = result
        .iter()
        .map(|(net_id, route)| route.routes.clone())
        .collect();

    // assert_eq!(path[0], Node3D::new(8, 0, 0));
    // assert_eq!(path[path.len() - 1], Node3D::new(8, 8, 0));

    // Plot the result.
    use crate::visualize::TermPlot;
    use iron_shapes::prelude::REdge;
    let mut plt = TermPlot::new();

    for paths in routes {
        for path in paths {
            path.iter().zip(path.iter().skip(1)).for_each(|(a, b)| {
                let Node3D(p1, l1) = a;
                let Node3D(p2, l2) = b;
                if l1 == l2 {
                    let edge = REdge::new(p1, p2);
                    plt.draw_edge(*l1 as i32, edge);
                }
            });
        }
    }

    // Plot pin locations.
    for (_, pins) in &signal_pins {
        for pin in pins {
            for &Node3D(p, layer) in pin {
                plt.set_char(3, p, 'x');
            }
        }
    }

    println!("Resulting routes:");
    plt.enable_colors(true);
    plt.print()
}
