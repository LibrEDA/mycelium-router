// Copyright (c) 2020-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Create a coarsened routing graph.

use crate::global_router::GlobalRoutingGraph;
use crate::graph::*;
use itertools::Itertools;
use libreda_pnr::libreda_db::iron_shapes::prelude::*;
use num_traits::{FromPrimitive, PrimInt};
use std::hash::Hash;

/// Coarsening algorithm which packs together 2x2 nodes of the fine grid into a node of the coarse grid.
pub struct GraphCoarsener<Coord> {
    fine_grid: grid_graph::GridGraph<Coord>,
    coarse_grid: grid_graph::GridGraph<Coord>,
    /// Only a coarsening factor of 2 is supported now.
    coarsening_factor: u32,
    /// Remove a fraction of the edge capacity during coarsening.
    capacity_margin_percent: u32,
}

impl<Coord> GraphCoarsener<Coord>
where
    Coord: CoordinateType + PrimInt + Hash,
{
    /// Coarsen the fine grid by grouping 2x2 nodes together.
    pub fn new2x2(fine_grid: grid_graph::GridGraph<Coord>) -> Self {
        Self::new(fine_grid, 2)
    }

    /// Coarsen the grid by grouping `coarsening_factor`x`coarsening_factor` nodes together.
    pub fn new(fine_grid: grid_graph::GridGraph<Coord>, coarsening_factor: u32) -> Self {
        Self {
            fine_grid,
            coarse_grid: Self::coarsen_grid(&fine_grid, coarsening_factor),
            coarsening_factor,
            capacity_margin_percent: 0,
        }
    }
    /// Set a margin for the coarsened capacities. The coarsened capacities
    /// will be reduced by this fraction to ensure routability during the refinement.
    pub fn with_capacity_margin(mut self, capacity_margin_percent: u32) -> Self {
        assert!(capacity_margin_percent < 100);
        self.capacity_margin_percent = capacity_margin_percent;
        self
    }

    /// Get the coarsened grid.
    pub fn get_coarsened_graph(&self) -> grid_graph::GridGraph<Coord> {
        self.coarse_grid
    }

    /// Get the coarsened grid.
    fn coarsen_grid(
        fine_grid: &grid_graph::GridGraph<Coord>,
        coarsening_factor: u32,
    ) -> grid_graph::GridGraph<Coord> {
        // Create global routing tiles that have a width of `coarsening_factor` routing tracks.

        // Eventually enlarge the boundary such that the number of
        // nodes in x and y directions is divisible by the coarsening factor.
        let boundary = {
            // Compute by which amount the boundary needs to be enlarged.
            let missing_num_x = (coarsening_factor
                - (fine_grid.num_x() as u32) % coarsening_factor)
                % coarsening_factor;
            let missing_num_y = (coarsening_factor
                - (fine_grid.num_y() as u32) % coarsening_factor)
                % coarsening_factor;

            // Enlarge the boundary.
            let mut boundary = fine_grid.boundary;
            boundary.upper_right.x = boundary.upper_right.x
                + Coord::from(missing_num_x).unwrap() * fine_grid.grid_pitch.x;
            boundary.upper_right.y = boundary.upper_right.y
                + Coord::from(missing_num_y).unwrap() * fine_grid.grid_pitch.y;

            boundary
        };

        let coarsening_factor = Coord::from(coarsening_factor).unwrap();

        // Find routing guides on a coarser grid.
        // Create a coarser grid.
        let coarse_grid = grid_graph::GridGraph {
            grid_pitch: fine_grid.grid_pitch * coarsening_factor,
            boundary,
            ..*fine_grid
        };

        // Sanity check
        {
            let adapted_fine_grid = grid_graph::GridGraph {
                boundary,
                ..*fine_grid
            };
            assert_eq!(coarse_grid.num_x() * 2, adapted_fine_grid.num_x());
            assert_eq!(coarse_grid.num_y() * 2, adapted_fine_grid.num_y());
        }

        log::info!(
            "Coarse grid: {}x{}",
            coarse_grid.num_x(),
            coarse_grid.num_y()
        );

        coarse_grid
    }

    /// Returns: Tuple of `(coarse graph, coarse net terminals, list of disappeared nets)`.
    pub fn coarsen_nets<NetId>(
        &self,
        pin_nodes: &Vec<(NetId, Vec<Vec<Node3D<Coord>>>)>,
    ) -> Vec<(NetId, Vec<Vec<Node3D<Coord>>>)>
    where
        NetId: Clone,
    {
        let fine_grid = &self.fine_grid;

        // Sanity check.
        debug_assert!(
            {
                let mut all_terminals = pin_nodes
                    .iter()
                    .flat_map(|(_, pins)| pins.iter().flat_map(|terms| terms.iter()));
                all_terminals.all(|t| fine_grid.contains_node(t))
            },
            "Supplied terminals must be nodes of the fine graph."
        );

        // Create global routing tiles that have a width of `coarsening_factor` routing tracks.
        let coarsening_factor = Coord::from(self.coarsening_factor).unwrap();

        let coarse_grid = self.get_coarsened_graph();

        // Find coarse routing terminals first.
        let coarse_terminals: Vec<_> = pin_nodes
            .iter()
            .map(|(net, pins)| {
                let coarse_pins: Vec<_> = pins
                    .iter()
                    .map(|terminals| {
                        // Map the terminal nodes to the closest nodes on the coarse grid.
                        terminals
                            .iter()
                            .map(|n| self.find_coarse_node(n))
                            .unique()
                            .collect_vec()
                    })
                    .collect();

                (net.clone(), coarse_pins)
            })
            .collect();

        // Sanity check.
        debug_assert!(
            {
                let mut all_terminals = coarse_terminals
                    .iter()
                    .flat_map(|(_, pins)| pins.iter().flat_map(|terms| terms.iter()));
                all_terminals.all(|t| coarse_grid.contains_node(t))
            },
            "Coarsened terminals must be nodes of the coarse graph."
        );

        coarse_terminals
    }

    /// Find the node which represents the `fine_node` in the coarsened graph.
    pub fn find_coarse_node(&self, fine_node: &Node3D<Coord>) -> Node3D<Coord> {
        let Node3D(p, layer) = *fine_node;
        let coarse_grid = &self.coarse_grid;

        let coarse_p = coarse_grid.grid_floor(p);
        let ll = coarse_grid.lower_left_node_coord();
        // Limit to the boundaries.
        let coarse_p = Point::new(coarse_p.x.max(ll.x), coarse_p.y.max(ll.y));

        let coarse_node = Node3D(coarse_p, layer);
        // assert!(coarse_node.0.x <= p.x);
        // assert!(coarse_node.0.y <= p.y);
        // let coarse_node = Node3D(coarse_grid.closest_grid_point(p), layer); // Alternative

        assert!(
            coarse_grid.contains_node(&coarse_node),
            "Coarsened node must be contained in the coarse graph."
        );

        coarse_node
    }
}

impl<Coord> GraphCoarsener<Coord>
where
    Coord: CoordinateType + PrimInt + Hash + FromPrimitive,
{
    /// Produce a coarse graph which summarizes edge capacities and node capacities of the `fine_graph`.
    /// The coarse graph summarizes groups of 4 nodes of the fine graph into one node.
    pub fn coarsen_capacities(
        &self,
        fine_graph: &GlobalRoutingGraph<Coord>,
    ) -> GlobalRoutingGraph<Coord> {
        //assert!(&self.fine_grid == fine_graph.grid_graph(), "Grid sizes don't match.");

        let coarsening_factor = self.coarsening_factor;

        dbg!(fine_graph.grid_graph().num_x());
        dbg!(fine_graph.grid_graph().num_y());

        assert_eq!(
            fine_graph.grid_graph().num_x() % coarsening_factor as usize,
            0,
            "Number of nodes in x direction ({}) must be divisible by the coarsening factor ({}).",
            fine_graph.grid_graph().num_x(),
            coarsening_factor
        );
        assert_eq!(
            fine_graph.grid_graph().num_y() % coarsening_factor as usize,
            0,
            "Number of nodes in y direction ({}) must be divisible by the coarsening factor ({}).",
            fine_graph.grid_graph().num_y(),
            coarsening_factor
        );

        let fine_grid = fine_graph.grid_graph();
        let coarse_grid = self.coarse_grid;

        assert_eq!(coarse_grid.num_x() * 2, fine_graph.grid_graph().num_x());
        assert_eq!(coarse_grid.num_y() * 2, fine_graph.grid_graph().num_y());

        let mut coarse_graph = GlobalRoutingGraph::new(coarse_grid);
        let coarse_grid = *coarse_graph.grid_graph();

        for coarse_node in coarse_grid.all_nodes() {
            debug_assert!(coarse_grid.contains_node(&coarse_node));

            let Node3D(coarse_point, layer) = coarse_node;

            let fine_nodes: Vec<_> =
                find_fine_nodes(&coarse_grid, fine_grid, coarse_node).collect();
            debug_assert!(fine_nodes.iter().all(|node| fine_grid.contains_node(&node)));

            // The sum of the capacities of the fine nodes is an upper bound for the capacity of the coarse node.
            let node_capacity_sum: u32 = fine_nodes
                .iter()
                .map(|n| fine_graph.get_node_data(n).capacity)
                .sum();
            // Store coarse node capacity.
            coarse_graph.get_node_data_mut(&coarse_node).capacity = node_capacity_sum;

            // Summarize edges into increasing coordinate directions.
            for axis in [Axis3D::X, Axis3D::Y, Axis3D::Z] {
                // Find neighbour of the coarse node.
                if let Some(coarse_neighbour) = coarse_grid.monotonic_neighbour(axis, coarse_node) {
                    debug_assert!(coarse_grid.contains_node(&coarse_neighbour));

                    let coarse_edge =
                        GridGraphEdgeId::new_undirected(coarse_node, coarse_neighbour);

                    let summarized_edge_capacity: u32 = fine_nodes
                        .iter()
                        // Find all edges into increasing edge direction.
                        .filter_map(|&fine_node|
                            // Create the edge.
                            fine_grid.monotonic_neighbour(axis, fine_node)
                                .map(|neighbour| (fine_node, neighbour)))
                        // Take only edges which go to the neighbouring coarse node.
                        .filter(|(a, neighbour)| {
                            self.find_coarse_node(neighbour) == coarse_neighbour
                        })
                        // Sum edge capacities. Take the node capacities into account too.
                        .map(|(a, b)| {
                            let edge = GridGraphEdgeId::new_undirected(a, b);
                            let edge_capacity = fine_graph.get_edge_data(&edge).capacity;
                            let node1_capacity = fine_graph.get_node_data(&a).capacity;
                            let node2_capacity = fine_graph.get_node_data(&b).capacity;

                            edge_capacity.min(node1_capacity).min(node2_capacity)
                                * (100 - self.capacity_margin_percent)
                                / 100 // include margin
                        })
                        .sum();

                    // Store capacity of coarse edge.
                    coarse_graph.get_edge_data_mut(&coarse_edge).capacity =
                        summarized_edge_capacity;
                }
            }
        }

        coarse_graph
    }
}

/// Create two sets of nets: Nets that have still more than one terminal on the coarse graph
/// and nets which have one or less terminals, i.e. which disappeared with the coarsening step.
pub fn partition_disappearing_nets<NetId, Coord>(
    coarse_terminals: Vec<(NetId, Vec<Vec<Node3D<Coord>>>)>,
) -> (
    Vec<(NetId, Vec<Vec<Node3D<Coord>>>)>,
    Vec<(NetId, Vec<Vec<Node3D<Coord>>>)>,
)
where
    Coord: CoordinateType + PrimInt + Hash,
    NetId: Copy,
{
    let (not_disappeared_net_terminals, disappeared_nets_terminals): (Vec<_>, Vec<_>) =
        coarse_terminals
            .into_iter()
            .partition(|(_net, pins)| pins.iter().flatten().unique().count() > 1);

    (not_disappeared_net_terminals, disappeared_nets_terminals)
}

#[test]
fn test_coarsen_capacities() {
    let mut fine_graph = GlobalRoutingGraph::new(GridGraph::new(
        Rect::new((0, 0), (4, 4)),
        4,
        Vector::new(1, 1),
        Vector::zero(),
    ));
    assert_eq!(fine_graph.grid_graph().num_x(), 4);
    assert_eq!(fine_graph.grid_graph().num_y(), 4);

    // Set node capacities.
    fine_graph.grid_graph().clone().all_nodes().for_each(|n| {
        fine_graph.get_node_data_mut(&n).capacity = 1;
    });

    // Set edge capacities.
    fine_graph.grid_graph().clone().all_edges().for_each(|e| {
        fine_graph.get_edge_data_mut(&e).capacity = 1;
    });

    // Create coarse graph.
    let coarsener = GraphCoarsener::new2x2(*fine_graph.grid_graph());
    let coarse_graph = coarsener.coarsen_capacities(&fine_graph);
    assert_eq!(coarse_graph.grid_graph().num_x(), 2);
    assert_eq!(coarse_graph.grid_graph().num_y(), 2);

    // Check node capacities.
    coarse_graph.grid_graph().all_nodes().for_each(|n| {
        assert_eq!(coarse_graph.get_node_data(&n).capacity, 4);
    });

    // Check edge capacities.
    coarse_graph.grid_graph().all_edges().for_each(|e| {
        let (a, b) = e.into();
        let expected_capacity = if a.1 != b.1 {
            // Via node. A coarse via summarizes 2x2 vias, thus the capacity should be 4.
            4
        } else {
            // Non-via node.
            2
        };
        assert_eq!(coarse_graph.get_edge_data(&e).capacity, expected_capacity);
    });
}

/// Find all fine nodes which are summarized by the `coarse_node`.
pub fn find_fine_nodes<'a, T>(
    coarse_grid: &'a grid_graph::GridGraph<T>,
    fine_grid: &'a grid_graph::GridGraph<T>,
    coarse_node: Node3D<T>,
) -> impl Iterator<Item = Node3D<T>> + 'a
where
    T: CoordinateType + PrimInt + Hash,
{
    debug_assert!(coarse_grid.contains_node(&coarse_node));
    debug_assert!(coarse_grid.boundary.contains_rectangle(&fine_grid.boundary));

    let Node3D(p, layer) = coarse_node;

    debug_assert!(p == coarse_grid.grid_floor(p));
    // Find rectangle which is represented by the coarse node.
    // All fine nodes in this rectangle should map to the coarse node.
    let upper_right = p + coarse_grid.grid_pitch;
    let coarse_tile = Rect::new(p, upper_right);

    let fine_node_xy_coordinates = fine_grid.all_nodes_xy(&coarse_tile);
    fine_node_xy_coordinates
        .map(move |xy| Node3D(xy, layer))
        // Sanity checks.
        .inspect(move |n| {
            debug_assert!(
                fine_grid.contains_node(n),
                "Node must be contained in the fine grid."
            );
            // debug_assert!(&find_coarse_node(coarse_grid, n) == coarse_node, "Coarsening and refining must be consistent.");
        })
}

#[test]
fn test_find_fine_nodes() {
    let fine_graph = grid_graph::GridGraph::new(
        Rect::new((0, 0), (11, 11)),
        4,
        Vector::new(1, 1),
        Vector::zero(),
    );

    let coarsener = GraphCoarsener::new2x2(fine_graph);

    let coarse_graph = coarsener.get_coarsened_graph();
    let coarse_node = Node3D(Point::new(2, 2), 0);
    let fine_nodes: Vec<_> = find_fine_nodes(&coarse_graph, &fine_graph, coarse_node).collect();
    dbg!(&fine_nodes);
    assert_eq!(fine_nodes.len(), 4);
}

#[test]
fn test_coarsen_grid_graph() {
    let fine_graph = grid_graph::GridGraph::new(
        Rect::new((-1i32, -1), (11, 11)),
        4,
        Vector::new(1, 1),
        Vector::zero(),
    );

    let coarsener = GraphCoarsener::new2x2(fine_graph);
    let coarse_graph = coarsener.get_coarsened_graph();

    let fine_nets = vec![
        (1, vec![]),
        (
            2,
            vec![vec![Node3D::new(1, 1, 0)], vec![Node3D::new(9, 9, 0)]],
        ),
    ];

    let coarse_nets = coarsener.coarsen_nets(&fine_nets);

    let (coarse_nets, disappeared_nets) = partition_disappearing_nets(coarse_nets);

    assert_eq!(coarse_nets.len(), 1);
    assert_eq!(disappeared_nets.len(), 1);
    assert_eq!(disappeared_nets[0].0, 1);
    assert_eq!(
        coarse_nets[0],
        (
            2,
            vec![vec![Node3D::new(0, 0, 0)], vec![Node3D::new(8, 8, 0)]]
        )
    );
}

#[test]
fn test_coarsen_refine_consistency() {
    // Check that the mapping from fine to coarse nodes and the mapping from coarse to fine nodes
    // are consistent.

    let fine_graph = grid_graph::GridGraph::new(
        Rect::new((-10i32, -10), (10, 10)),
        1,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    let coarsener = GraphCoarsener::new2x2(fine_graph);

    let coarse_graph = coarsener.get_coarsened_graph();

    for coarse_node in coarse_graph.all_nodes() {
        for refined_node in find_fine_nodes(&coarse_graph, &fine_graph, coarse_node) {
            assert_eq!(coarsener.find_coarse_node(&refined_node), coarse_node);
        }
    }

    for fine_node in fine_graph.all_nodes() {
        let coarse_node = coarsener.find_coarse_node(&fine_node);
        let fine_nodes: Vec<_> = find_fine_nodes(&coarse_graph, &fine_graph, coarse_node).collect();

        assert_eq!(
            find_fine_nodes(&coarse_graph, &fine_graph, coarse_node)
                .filter(|n| n == &fine_node)
                .count(),
            1
        );
    }
}
