// Copyright (c) 2020-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Propagate a coarse routing solution to the finer routing graph.
//! The propagation happens slice by slice. A slice is defined as nodes on all metal layers
//! in a horizontal or vertical row of the coarse grid.

use itertools::Itertools;
use petgraph::data::Build;
use rayon::prelude::*;

use fnv::{FnvHashMap, FnvHashSet};
use num_traits::{FromPrimitive, Num, NumCast, PrimInt, ToPrimitive};

use libreda_pnr::db;

use std::collections::HashMap;
use std::hash::Hash;
use std::marker::PhantomData;

use crate::global_router::GlobalRoutingGraph;
use crate::graph::{GridGraph, GridGraphEdgeId, Node3D};
use crate::maze_router::router_traits::{MultiSignalRouter, RoutedSignal};
use crate::multilevel_maze_router::coarsen::GraphCoarsener;
use crate::task_scheduler::TaskScheduler;

/// Routing engine which refines coarse routes.
/// Partitions the coarse routing solution into regions
/// and calls the underlying routing algorithm for each of this regions.
pub struct SliceRefinementRouter<'a, R, Coord> {
    /// Underlying routing algorithm.
    multi_signal_router: &'a R,
    /// Divide the routing problem into this number of clusters for the refinement of the routes.
    num_refinement_regions: usize,
    _coord_type: PhantomData<Coord>,
}

impl<'a, R, Coord> SliceRefinementRouter<'a, R, Coord> {
    pub fn new(router: &'a R) -> Self {
        Self {
            multi_signal_router: router,
            num_refinement_regions: 16,
            _coord_type: Default::default(),
        }
    }
}

impl<'a, R, Coord> SliceRefinementRouter<'a, R, Coord>
where
    R: MultiSignalRouter<Graph = GridGraph<Coord>, GraphNode = Node3D<Coord>> + Sync,
    R::Cost: Num + Ord + FromPrimitive + ToPrimitive + Copy + Send + Sync,
    R::RoutingResult: Sync + Send,
    R::NetId: std::fmt::Debug + Clone + Hash + Eq + Sync + Send + 'static,
    Coord: std::fmt::Debug + Copy + Hash + Send + Sync + PrimInt + FromPrimitive + 'static,
{
    /// Propagate coarse routes to the fine grid.
    pub fn refine_routes<NodeCostFn, EdgeCostFn>(
        &self,
        graph_coarsener: &GraphCoarsener<Coord>,
        coarse_graph: &GlobalRoutingGraph<Coord>,
        fine_graph: &GlobalRoutingGraph<Coord>,
        coarse_routes: &Vec<(R::NetId, R::RoutingResult)>,
        node_cost_fn: &NodeCostFn,
        edge_cost_fn: &EdgeCostFn,
        can_use_fine_node_fn: &(dyn Fn(&R::NetId, &R::GraphNode) -> bool + Sync),
        fine_net_terminals: &Vec<(R::NetId, Vec<Vec<R::GraphNode>>)>,
        net_weights: &HashMap<R::NetId, f64>,
    ) -> Result<Vec<(R::NetId, R::RoutingResult)>, ()>
    where
        NodeCostFn: Fn(&R::GraphNode) -> R::Cost + Sync,
        EdgeCostFn: Fn(&R::GraphNode, &R::GraphNode) -> R::Cost + Sync,
    {
        assert!(
            !self.multi_signal_router.is_pin_feed_through_enabled(),
            "pin-feedthrough must be disabled"
        );

        let coarse_grid = *coarse_graph.grid_graph();
        let fine_grid = *fine_graph.grid_graph();

        {
            // Validate graph sizes.
            assert_eq!(
                coarse_grid.num_x() + coarse_grid.num_x(),
                fine_grid.num_x(),
                "Fine graph must have double the amount of nodes in x direction."
            );
            assert_eq!(
                coarse_grid.num_y() + coarse_grid.num_y(),
                fine_grid.num_y(),
                "Fine graph must have double the amount of nodes in y direction."
            );
        }

        {
            // Sanity check.
            assert_eq!(
                fine_net_terminals.len(),
                coarse_routes.len(),
                "Number of coarse routes must equal to the number of fine nets."
            );
        }

        // Create lookup-table to find terminals by the net ID.
        let fine_terminals_by_net: FnvHashMap<R::NetId, _> = fine_net_terminals
            .iter()
            .map(|(net, route)| (net.clone(), route))
            .collect();

        // Create lookup-table to find coarse routes by their net ID.
        let coarse_route_by_net: FnvHashMap<R::NetId, &R::RoutingResult> = coarse_routes
            .iter()
            .map(|(net, route)| (net.clone(), route))
            .collect();

        // For each net: Find the coarse grid points that can be used by the net.
        let coarse_locations_by_net: FnvHashMap<R::NetId, FnvHashSet<db::Point<Coord>>> = {
            coarse_routes
                .iter()
                .map(|(net, route)| {
                    let coarse_locations = route.nodes().map(|n| n.point()).collect();
                    (net.clone(), coarse_locations)
                })
                .collect()
        };

        #[cfg(debug_assertions)]
        {
            // Sanity check: coarse routes must include all the fine pins.
            for (net, fine_pins) in fine_terminals_by_net.iter() {
                let coarse_route = coarse_route_by_net
                    .get(net)
                    .expect("No coarse route found for net.");
                let all_coarse_nodes: FnvHashSet<_> = coarse_route.nodes().collect();

                debug_assert!(
                    fine_pins.iter().all(|terminals| terminals.iter().any(|t| {
                        let coarse = graph_coarsener.find_coarse_node(t);
                        all_coarse_nodes.contains(&coarse)
                    })),
                    "Net must be fully covered by the coarse route."
                )
            }
        }

        // For each coarse coordinate find the nets which use it.
        let nets_of_coarse_coordinate = Self::create_coarse_net_search_table(coarse_routes);

        let fine_terminal_lookup_table =
            Self::create_fine_terminal_search_table(graph_coarsener, fine_net_terminals);

        // Split the coarse graph into regions.
        // Each region will be routed on its own.
        let routing_regions = create_refinement_regions(coarse_graph, self.num_refinement_regions);

        debug_assert!(
            {
                // Sanity check.
                let all_locations = routing_regions.iter().flatten();
                let counted_locations = all_locations.counts();

                coarse_graph
                    .grid_graph()
                    .all_nodes_xy(&coarse_graph.grid_graph().boundary)
                    .all(|loc| counted_locations.get(&loc) == Some(&1))
            },
            "Each coarse grid location must appear exactly once in the routing regions."
        );

        log::debug!("Number of routing region: {}", routing_regions.len());

        // Create a lookup table to find routing region indices given a grid location.
        let routing_regions_by_location: FnvHashMap<db::Point<Coord>, usize> = routing_regions
            .iter()
            .enumerate()
            .flat_map(|(index, nodes)| nodes.iter().map(move |p| (*p, index)))
            .collect();

        // Create a lookup-table to find routing regions which are adjacent.
        // Used to find dependencies between routing regions.
        let neighbour_regions: FnvHashMap<usize, FnvHashSet<usize>> = routing_regions
            .par_iter()
            .enumerate()
            .map(|(index, region_nodes)| {
                // Find neighbouring regions.
                let neighbour_regions: FnvHashSet<usize> = region_nodes
                    .iter()
                    // Expand the nodes within this region by one step, then they also include
                    // some nodes of neighbour regions.
                    .flat_map(|point| coarse_grid.neighbours_iter(Node3D(*point, 0)))
                    // Convert grid locations to region indices.
                    .map(|n| routing_regions_by_location[&n.0])
                    // Exclude the current region.
                    .filter(|region_idx| region_idx != &index)
                    .collect();
                (index, neighbour_regions)
            })
            .collect();

        // Container for the routes in the routed regions.
        let mut region_routing_results: FnvHashMap<usize, FnvHashMap<R::NetId, R::RoutingResult>> =
            Default::default();

        // // Keep track of processed regions to detect bugs. For example when one is processed twice or never.
        // let mut processed_coarse_grid_locations: FnvHashSet<db::Point<Coord>> = Default::default();

        let num_routing_regions = routing_regions.len();
        let mut pending_regions: Vec<_> = routing_regions.into_iter().enumerate().collect();
        pending_regions.reverse();

        // Route regions in parallel.
        while !pending_regions.is_empty() {
            // Find regions which can be routed in parallel.
            let selected_independent_regions = {
                let mut selected_independent_regions = vec![];
                let mut selected_region_indices: FnvHashSet<_> = Default::default();
                let mut selected_neighbours: FnvHashSet<usize> = Default::default();

                // Take routing regions as long as there's no dependency conflict.
                while let Some((region_idx, region)) = pending_regions.pop() {
                    // The current region must not have a neighbour which is currently selected for routing.
                    let no_conflict = !selected_neighbours.contains(&region_idx);

                    if no_conflict {
                        selected_region_indices.insert(region_idx);
                        selected_independent_regions.push((region_idx, region));

                        let neighbours = &neighbour_regions[&region_idx];
                        selected_neighbours.extend(neighbours);
                    } else {
                        pending_regions.push((region_idx, region));
                        break;
                    }
                }

                selected_independent_regions
            };
            log::info!(
                "route {} regions in parallel: ",
                selected_independent_regions.len()
            );

            assert!(!selected_independent_regions.is_empty());

            // Route the selected regions in parallel.
            let routes: Vec<_> = selected_independent_regions
                .into_par_iter()
                .map(|(routing_region_index, routing_region)| {
                    log::info!(
                        "Routing in region {}/{}",
                        routing_region_index + 1,
                        num_routing_regions
                    );

                    // Find the set of coarse nodes which belong to this region.
                    let coarse_region_grid_locations: FnvHashSet<db::Point<Coord>> =
                        routing_region.iter().copied().collect();
                    log::debug!(
                        "Number of coarse nodes in region: {}",
                        coarse_region_grid_locations.len()
                    );

                    // // Sanity check: Each grid location must be processed exactly once.
                    // for l in &coarse_region_grid_locations {
                    //     debug_assert!(!processed_coarse_grid_locations.contains(l), "Grid location is already processed.");
                    //     processed_coarse_grid_locations.insert(*l);
                    // }

                    // Find routing terminals in this region.
                    let routing_terminals = self.create_region_routing_terminals(
                        &graph_coarsener,
                        &coarse_grid,
                        &fine_grid,
                        &coarse_region_grid_locations,
                        &fine_terminal_lookup_table,
                        &fine_terminals_by_net,
                        &nets_of_coarse_coordinate,
                        &routing_regions_by_location,
                        &region_routing_results,
                    );
                    log::debug!("Number of nets in region: {}", routing_terminals.len());

                    // Create lookup table to quickly find if a node is a routing terminal.
                    // Nodes outside of the routing region can be used if they are terminals, i.e. the
                    // routing destinations just at the boundary of the other routing region.
                    let routing_terminals_lookup: FnvHashMap<R::NetId, FnvHashSet<R::GraphNode>> =
                        routing_terminals
                            .iter()
                            .map(|(net, terminals)| {
                                let terminal_set = terminals.iter().flatten().copied().collect();
                                (net.clone(), terminal_set)
                            })
                            .collect();

                    // Check if the fine node is available for use by the net.
                    // This makes sure the partial route is confined within the routing region.
                    let can_use_node_fn = |net: &R::NetId, node: &R::GraphNode| -> bool {
                        let coarsened = graph_coarsener.find_coarse_node(node);

                        let is_terminal = routing_terminals_lookup
                            .get(net)
                            .map(|terminals| terminals.contains(node))
                            .unwrap_or(false);

                        let coarse_locations = coarse_locations_by_net
                            .get(net)
                            .expect("No coarse locations found for net.");

                        coarse_locations.contains(&coarsened.0)
                            && (coarse_region_grid_locations.contains(&coarsened.0) || is_terminal)
                            && can_use_fine_node_fn(net, node)
                    };

                    let node_capacity_fn =
                        |node: &R::GraphNode| -> u32 { fine_graph.get_node_data(node).capacity };

                    let edge_capacity_fn = |a: &R::GraphNode, b: &R::GraphNode| -> u32 {
                        fine_graph.get_edge_data(&(*a, *b).into()).capacity
                    };

                    // Pass the sub-routing problem to the underlying routing engine.
                    let routes = self.multi_signal_router.route_multisignal(
                        &fine_grid,
                        node_cost_fn,
                        edge_cost_fn,
                        &node_capacity_fn,
                        &edge_capacity_fn,
                        &can_use_node_fn,
                        &routing_terminals,
                        net_weights,
                    );

                    (routing_region_index, routes)
                })
                .collect();

            for (routing_region_index, region_routes) in routes {
                // Handle errors, store successful results.
                match region_routes {
                    Ok(routes) => {
                        // Convert to hash map.
                        let routes = routes.into_iter().collect();
                        // Store result.
                        region_routing_results.insert(routing_region_index, routes);
                    }
                    Err(err) => {
                        log::error!("Failed to solve sub-routing problem.");
                        return Err(err);
                    }
                };
            }
        }

        {
            // Assemble the partial routes into the final result.
            let mut routing_results: HashMap<R::NetId, R::RoutingResult> = HashMap::new();

            for (_region_id, routes) in region_routing_results {
                for (net, partial_result) in routes {
                    if let Some(assembled_result) = routing_results.remove(&net) {
                        let assembled_result = assembled_result.merge(partial_result);
                        routing_results.insert(net, assembled_result);
                    } else {
                        routing_results.insert(net, partial_result);
                    }
                }
            }

            // Sanity check: All pin nodes must be included in the found route.
            #[cfg(debug_assertions)]
            {
                for (net_id, pins) in fine_net_terminals {
                    let route = routing_results.get(net_id).expect("No route for net.");
                    let all_route_nodes: FnvHashSet<_> = route.nodes().collect();

                    debug_assert!(
                        pins.iter().all(|sub_pins| {
                            sub_pins.iter().any(|pin| all_route_nodes.contains(pin))
                        }),
                        "Route does not contain all pins of the net."
                    );
                }
            }

            // Sanity check: Routes must be connected.
            #[cfg(debug_assertions)]
            {
                let mut unconnected_nets = vec![]; // Keep track of nets with unconnected route.

                for (net, route) in &routing_results {
                    let mut connectivity_graph: petgraph::graphmap::UnGraphMap<_, ()> =
                        Default::default();

                    for node in route.nodes() {
                        connectivity_graph.add_node(node);
                    }

                    for edge in route.edges() {
                        connectivity_graph.add_edge(edge.0, edge.1, ());
                    }

                    // Find connected components.
                    let num_components = petgraph::algo::tarjan_scc(&connectivity_graph).len();

                    if num_components > 1 {
                        unconnected_nets.push((net, num_components));
                    }
                }

                for (_net, num_components) in unconnected_nets {
                    log::warn!(
                        "Route is not connected. Number of components: {}",
                        num_components
                    );
                }
            }

            // Convert to Vec and restore the original ordering.
            let routing_results_vec: Vec<_> = fine_net_terminals
                .iter()
                .map(|(net, _)| {
                    let route = routing_results
                        .remove(net)
                        .expect("No routing result present for this net.");
                    (net.clone(), route)
                })
                .collect();
            // Sanity check: All routed nets get returned.
            assert!(
                routing_results.is_empty(),
                "Some routing results are not returned."
            );

            Ok(routing_results_vec)
        }
    }

    /// Find fine routing terminals in a given routing region.
    /// This includes
    /// 1) terminals at boundaries of neighboring routing regions. They connect intra-region nets.
    /// 2) Pins/terminals inside the routing region
    fn create_region_routing_terminals(
        &self,
        graph_coarsener: &GraphCoarsener<Coord>,
        coarse_grid: &GridGraph<Coord>,
        fine_grid: &GridGraph<Coord>,
        region_grid_locations: &FnvHashSet<db::Point<Coord>>,
        fine_terminal_lookup_table: &FnvHashMap<db::Point<Coord>, FnvHashSet<R::NetId>>,
        fine_terminals_by_net: &FnvHashMap<R::NetId, &Vec<Vec<R::GraphNode>>>,
        nets_of_coarse_coordinate: &FnvHashMap<db::Point<Coord>, FnvHashSet<R::NetId>>,
        routing_regions_by_location: &FnvHashMap<db::Point<Coord>, usize>,
        region_routing_results: &FnvHashMap<usize, FnvHashMap<R::NetId, R::RoutingResult>>,
    ) -> Vec<(R::NetId, Vec<Vec<R::GraphNode>>)> {
        let mut region_grid_points: Vec<_> = region_grid_locations.iter().copied().collect();
        region_grid_points.sort();

        // 1.1) Find terminals of fine nets inside this region.
        let fine_terminals_within_region: FnvHashMap<R::NetId, _> = {
            // Find all fine nets which have terminals in this region.
            let fine_nets_in_this_region: FnvHashSet<R::NetId> = region_grid_locations
                .iter()
                .filter_map(|location| fine_terminal_lookup_table.get(location))
                .flatten()
                .cloned()
                .collect();

            // Check if the fine node is located in the current routing region.
            let is_fine_node_in_region = |fine_node: &R::GraphNode| -> bool {
                let coarsened = graph_coarsener.find_coarse_node(fine_node);
                region_grid_locations.contains(&coarsened.0)
            };

            // Find terminals of the fine nets which are in this region.
            let fine_terminals_within_region: FnvHashMap<_, _> = fine_nets_in_this_region
                .iter()
                .map(|net| {
                    let pins = fine_terminals_by_net.get(net).expect("Net not found.");

                    // Create pins which have only terminals inside the current region.
                    let filtered_pins: Vec<_> = pins
                        .iter()
                        .map(|pin| {
                            let filtered_pin: Vec<_> = pin
                                .iter()
                                .filter(|node| is_fine_node_in_region(*node))
                                .copied()
                                .collect();
                            filtered_pin
                        })
                        // Remove pins without terminals
                        .filter(|filtered_pin| !filtered_pin.is_empty())
                        .collect();

                    (net.clone(), filtered_pins)
                })
                .filter(|(_, pins)| !pins.is_empty())
                .collect();

            fine_terminals_within_region
        };

        // 1.2) Create terminals on the outside of this region.
        // They are created where a coarse route crosses the boundary of the region.
        let fine_terminals_outside = {
            // Offsets for computing grid neighbours.
            let offsets = {
                let grid_pitch = coarse_grid.grid_pitch; // Used to find neighbour locations.
                let _0 = Coord::zero();
                let east = db::Vector::new(grid_pitch.x, _0);
                let north = db::Vector::new(_0, _0 - grid_pitch.y);
                let west = db::Vector::new(_0 - grid_pitch.x, _0);
                let south = db::Vector::new(_0, grid_pitch.y);
                [east, north, west, south]
            };

            // Find boundary edges, i.e. all pairs of `(node inside routing region, neighbouring node outside routing region)`.
            let boundary_edges = region_grid_locations
                .iter()
                .copied()
                // Find all neighbours.
                .flat_map(|p: db::Point<Coord>| {
                    offsets
                        .iter()
                        // Get all neighbours around `p`.
                        .map(move |off| p + *off)
                        // Take only pairs which are across the region boundary.
                        .filter(|n| !region_grid_locations.contains(n))
                        // Take only the ones which are within the grid boundaries.
                        .filter(|n| coarse_grid.contains_node(&Node3D(*n, 0)))
                        // Create `(node, neighbour)` tuples.
                        .map(move |n| (p, n))
                });

            // // Get the set of all net terminals inside the routing region.
            // let all_terminals_inside: FnvHashSet<R::GraphNode> = fine_terminals_within_region.values()
            //     .flatten()
            //     .flatten()
            //     .copied()
            //     .collect();

            let mut terminals_on_outside: FnvHashMap<R::NetId, _> = Default::default();

            // Process the boundaries.
            for (inside_coarse, outside_coarse) in boundary_edges {
                // `inside` is in the current routing region and is not yet routed.
                // `outside` is outside of the current routing region and may or may not be routed already.

                // Find the routing region which is associated with the neighboring grid location.
                let other_routing_region_index = *routing_regions_by_location
                    .get(&outside_coarse)
                    .expect("Grid location is not associated with a routing region.");

                // Find the edges of the fine grid crossing this boundary.
                let crossing_edges: Vec<_> = {
                    // Find crossing edges on all layers.
                    (0..coarse_grid.num_layers)
                        .into_iter()
                        .flat_map(|layer| {
                            let inside = Node3D(inside_coarse, layer);
                            let outside = Node3D(outside_coarse, layer);
                            let fine_nodes_inside =
                                super::coarsen::find_fine_nodes(&coarse_grid, &fine_grid, inside);

                            // Find fine edges which connect the both coarse nodes.
                            // They are oriented from inside the routing region towards the outside.
                            let crossing_edges = fine_nodes_inside
                                // Find all edges starting at the fine nodes inside the routing region.
                                .flat_map(|n| {
                                    fine_grid
                                        .neighbours_iter(n)
                                        .map(move |neighbour| (n, neighbour))
                                })
                                // Find edges which cross the boundary of the routing region
                                .filter(move |(_, other)| {
                                    graph_coarsener.find_coarse_node(other) == outside
                                });

                            crossing_edges
                        })
                        .collect()
                };

                // Get all fine nodes which are outside the routing region but directly at the boundary.
                // They will be used as default routing terminals if the other region was not routed yet.
                let default_routing_terminals_outside: Vec<R::GraphNode> = crossing_edges
                    .iter()
                    .map(|(_inside, outside)| *outside)
                    .collect();

                let default_routing_terminals_inside = {
                    let default_routing_terminals_inside: Vec<R::GraphNode> = crossing_edges
                        .iter()
                        .map(|(inside, _outside)| *inside)
                        // Remove all terminals which already exist.
                        //.filter(|node| !all_terminals_inside.contains(node))
                        .collect();
                    default_routing_terminals_inside
                };

                // Create terminals for all coarse routes which cross this boundary.
                let empty = Default::default();

                // Find nets which are on both sides of the boundary.
                let crossing_nets = {
                    let nets_inside = nets_of_coarse_coordinate
                        .get(&inside_coarse)
                        .unwrap_or(&empty);
                    let nets_outside = nets_of_coarse_coordinate
                        .get(&outside_coarse)
                        .unwrap_or(&empty);

                    nets_inside.intersection(nets_outside)
                };

                for crossing_net in crossing_nets {
                    // Create terminals for the net where it crosses the boundary of this routing region.
                    // If the net is already routed on the outside of this routing region, the terminals
                    // are constrained to connect to the existing route.

                    // Find the fine route of the crossing net coming from outside, if it already exists.
                    let existing_outside_route = region_routing_results
                        .get(&other_routing_region_index)
                        .and_then(|routes| routes.get(crossing_net));

                    let terminals: Vec<_> = if let Some(existing_route) = existing_outside_route {
                        // Constrain terminals to match existing route in the neighboring region.
                        let outside_terminals: FnvHashSet<_> = existing_route.nodes().collect();
                        // Compute boolean intersection of the incoming route and
                        // the potential routing terminals.

                        // TODO: More elegant with set operation (intersection)?
                        crossing_edges
                            .iter()
                            // Take edges which connect to a route in the neighbour region.
                            .filter(|(_, t_outside)| outside_terminals.contains(t_outside))
                            .map(|(_t_inside, t_outside)| t_outside)
                            .cloned()
                            .collect()
                    } else {
                        // The net is not routed on the other side.
                        // No constraints on terminals.
                        default_routing_terminals_inside.clone()
                    };

                    terminals_on_outside
                        .entry(crossing_net.clone())
                        .or_insert(vec![])
                        .push(terminals)
                }
            }

            terminals_on_outside
        };

        {
            // Join the boundary-crossing terminals with the internal terminals.
            let all_used_nets: FnvHashSet<_> = fine_terminals_within_region
                .keys()
                .chain(fine_terminals_outside.keys())
                .collect();

            let all_region_terminals: Vec<(_, _)> = all_used_nets
                .into_iter()
                .map(|net| {
                    let empty = vec![];
                    let net_terminals_within =
                        fine_terminals_within_region.get(net).unwrap_or(&empty);
                    let net_terminals_boundary = fine_terminals_outside.get(net).unwrap_or(&empty);

                    // Merge the two sets of terminals.
                    let joined: Vec<_> = net_terminals_within
                        .iter()
                        .chain(net_terminals_boundary)
                        .cloned()
                        .collect();

                    (net.clone(), joined)
                })
                .collect();

            all_region_terminals
        }
    }

    /// Create a mapping which allows to efficiently find all nets which use grid nodes at a certain
    /// coordinate.
    /// Might not be memory efficient.
    fn create_coarse_net_search_table(
        coarse_routes: &Vec<(R::NetId, R::RoutingResult)>,
    ) -> FnvHashMap<db::Point<Coord>, FnvHashSet<R::NetId>> {
        let mut nets_of_coarse_coordinate: FnvHashMap<_, FnvHashSet<R::NetId>> = Default::default();

        // Register each net at the coordinates it is using.
        for (net, route) in coarse_routes {
            let all_used_grid_coordinates = route.nodes().map(|n| n.point());

            for p in all_used_grid_coordinates {
                nets_of_coarse_coordinate
                    .entry(p)
                    .or_insert(Default::default())
                    .insert(net.clone());
            }
        }

        nets_of_coarse_coordinate
    }

    /// Create a look-up table from coarse grid locations to nets which have fine terminals at this location.
    fn create_fine_terminal_search_table(
        coarsener: &GraphCoarsener<Coord>,
        fine_terminals: &Vec<(R::NetId, Vec<Vec<Node3D<Coord>>>)>,
    ) -> FnvHashMap<db::Point<Coord>, FnvHashSet<R::NetId>> {
        let mut terminals_of_coarse_coordinate: FnvHashMap<_, FnvHashSet<R::NetId>> =
            Default::default();

        // Register each net at the coordinates it is using.
        for (net, terminals) in fine_terminals {
            let all_used_grid_coordinates = terminals
                .iter()
                .flatten()
                .map(|n| coarsener.find_coarse_node(n))
                .map(|n| n.point());

            for p in all_used_grid_coordinates {
                terminals_of_coarse_coordinate
                    .entry(p)
                    .or_insert(Default::default())
                    .insert(net.clone());
            }
        }

        terminals_of_coarse_coordinate
    }
}

/// Partition the xy plane of the routing graph into non-overlapping regions.
/// In each region, the nets should be routed concurrently.
/// Regions are sorted from most congested to least congested. Routing should happen
/// in the same order because most congested regions have less flexibility while less congested
/// regions can more likely be routed with the restrictions coming from already routed regions.
fn create_refinement_regions<Coord>(
    coarse_graph: &GlobalRoutingGraph<Coord>,
    num_refinement_regions: usize,
) -> Vec<Vec<db::Point<Coord>>>
where
    Coord: Copy + Hash + PrimInt + NumCast + db::CoordinateType + FromPrimitive,
{
    let num_layers = coarse_graph.grid_graph().num_layers;

    // Compute the congestion at a grid location (summary over all layers).
    let congestion_metric = |p: db::Point<Coord>| -> i32 {
        // All nodes at this grid location.
        let nodes = (0..num_layers).into_iter().map(|layer| Node3D(p, layer));

        // All edges touching this grid location.
        let edges = nodes
            .flat_map(|n| {
                coarse_graph
                    .grid_graph()
                    .neighbours_iter(n)
                    .map(move |neighbour| GridGraphEdgeId::new_undirected(n, neighbour))
            })
            .unique();

        let congestion = edges
            .map(|edge| {
                let edge_data = coarse_graph.get_edge_data(&edge);
                edge_data.usage as i32 - edge_data.capacity as i32
            })
            .sum();

        congestion
    };

    // Sort grid locations by congestion. Most congested first.
    let locations_by_descending_congestion = {
        let mut grid_locations: Vec<_> = coarse_graph
            .grid_graph()
            .all_nodes_xy(&coarse_graph.grid_graph().boundary)
            .collect();
        grid_locations.sort_by_key(|p| -congestion_metric(*p));
        grid_locations
    };

    let num_locations = locations_by_descending_congestion.len();
    let refinement_region_size = (num_locations / num_refinement_regions).max(1);

    // Create clusters.

    let clusters = {
        let grid = coarse_graph.grid_graph();
        let mut remaining_locations: FnvHashSet<_> = grid.all_nodes_xy(&grid.boundary).collect();

        let mut clusters = Vec::new();

        for location in &locations_by_descending_congestion {
            if remaining_locations.remove(location) {
                // Start new cluster from the current location.
                let mut cluster = Vec::new();

                let mut neighbours: FnvHashSet<_> = Default::default();

                let mut current_location = *location;

                loop {
                    neighbours.remove(&current_location);
                    remaining_locations.remove(&current_location);
                    cluster.push(current_location);

                    if cluster.len() >= refinement_region_size {
                        break;
                    }

                    // Spread to the neighbours of the current grid location.
                    let new_neighbours = grid
                        .neighbours_iter(Node3D(current_location, 0))
                        .map(|n| n.point())
                        .filter(|p| remaining_locations.contains(p));
                    neighbours.extend(new_neighbours);

                    if neighbours.is_empty() {
                        break;
                    }

                    // Expand to the most congested neighbouring location.
                    let most_congested_neighbour = neighbours
                        .iter()
                        .max_by_key(|p| congestion_metric(**p))
                        .copied()
                        .unwrap();

                    current_location = most_congested_neighbour;
                }

                clusters.push(cluster)
            }
        }

        assert!(
            remaining_locations.is_empty(),
            "All grid locations must be assigned to a cluster."
        );

        clusters
    };

    // Create trivial clusters.
    // let clusters: Vec<_> = locations_by_descending_congestion
    //     .into_iter()
    //     .map(|l| vec![l])
    //     .collect();

    // Cluster by rows.
    // let clusters: Vec<Vec<_>> = {
    //     let grid = coarse_graph.grid_graph();
    //     (0..grid.num_y())
    //         .into_iter()
    //         .map(|y| {
    //             (0..grid.num_x())
    //                 .into_iter()
    //                 .map(|x| grid.node_coord_by_index(x, y).unwrap())
    //                 .collect()
    //         })
    //         .collect()
    // };

    // // Trivial cluster: One big cluster.
    //let clusters = vec![locations_by_descending_congestion];

    clusters
}

mod tests {
    use crate::global_router::GlobalRoutingGraph;
    use crate::graph::grid_graph::*;
    use crate::maze_router::pathfinder_parallel::PathFinder;
    use crate::maze_router::router_traits::MultiSignalRouter;
    use crate::maze_router::simple_signal_router::{SimpleSignalRouter, SimpleSignalRouterResult};
    use crate::multilevel_maze_router::coarsen::GraphCoarsener;
    use crate::multilevel_maze_router::slice_refinement::SliceRefinementRouter;
    use iron_shapes::prelude::{Rect, Vector};

    #[test]
    fn test_slice_refinement() {
        type T = i32;

        let single_signal_router = SimpleSignalRouter::new();
        let pathfinder = PathFinder::new(&single_signal_router);
        let mut refinement_router = SliceRefinementRouter::new(&pathfinder);

        let fine_grid = GridGraph::new(
            Rect::new((0, 0), (10, 10)),
            3,
            Vector::new(1, 1),
            Vector::new(0, 0),
        );

        let mut fine_graph = GlobalRoutingGraph::new(fine_grid);
        let coarsener = GraphCoarsener::new2x2(fine_grid);
        let coarse_grid = coarsener.get_coarsened_graph();
        let coarse_graph = GlobalRoutingGraph::new(coarse_grid);

        // Set node capacities.
        fine_graph.grid_graph().clone().all_nodes().for_each(|n| {
            fine_graph.get_node_data_mut(&n).capacity = 1;
        });

        // Set edge capacities.
        fine_graph.grid_graph().clone().all_edges().for_each(|e| {
            fine_graph.get_edge_data_mut(&e).capacity = 1;
        });

        //let net1 = vec![
        //    vec![Node3D::new(1, 0, 0)],
        //    vec![Node3D::new(3, 0, 0)],
        //    vec![Node3D::new(1, 5, 1)],
        //];

        let net1 = vec![
            vec![Node3D::new(1, 0, 0)],
            vec![Node3D::new(3, 3, 0)],
            vec![Node3D::new(1, 6, 2)],
        ];

        let signal_pins = vec![(1, net1)];

        //let coarse_routes = vec![(
        //    1,
        //    SimpleSignalRouterResult {
        //        routes: vec![vec![
        //            Node3D::new(2, 0, 0),
        //            Node3D::new(0, 0, 0),
        //            Node3D::new(0, 0, 1),
        //            Node3D::new(0, 2, 1),
        //            Node3D::new(0, 4, 1),
        //        ]],
        //        total_cost: 0,
        //    },
        //)];
        let coarse_routes = vec![(
            1,
            SimpleSignalRouterResult {
                routes: vec![
                    vec![
                        Node3D::new(0, 0, 0),
                        Node3D::new(0, 0, 1),
                        Node3D::new(0, 0, 2),
                        Node3D::new(0, 2, 2),
                        Node3D::new(0, 4, 2),
                        Node3D::new(0, 6, 2),
                    ],
                    vec![Node3D::new(0, 2, 2)],
                    vec![Node3D::new(0, 2, 1)],
                    vec![Node3D::new(2, 2, 1)],
                    vec![Node3D::new(2, 2, 0)],
                ],
                total_cost: 0,
            },
        )];

        // A-star heuristic.
        let heuristic_fn = |n1: &Node3D<_>, n2: &Node3D<T>| -> T { (n1.0 - n2.0).norm1() };

        let node_cost_fn = |n1: &Node3D<T>| -> T { 0 };

        let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
            if n1.1 != n2.1 {
                // Different layers.
                1 as T // Via cost
            } else {
                let (h_cost, v_cost) = if n1.1 % 2 == 0 {
                    // Vertical layer. Lower cost for verticals.
                    (40, 10)
                } else {
                    (10, 40)
                };
                let mut diff = (n1.0 - n2.0);
                diff.x *= h_cost;
                diff.y *= v_cost;
                diff.norm1()
            }
        };

        let refined_routes = refinement_router
            .refine_routes(
                &coarsener,
                &coarse_graph,
                &fine_graph,
                &coarse_routes,
                &node_cost_fn,
                &edge_cost_fn,
                &|_, _| true,
                &signal_pins,
                &Default::default(),
            )
            .expect("Routing failed.");

        dbg!(&refined_routes);
    }
}
