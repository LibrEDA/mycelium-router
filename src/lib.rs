// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Router implementations for the LibrEDA place-and-route framework.
//!
//! This crate comes with a couple different routing approaches:
//!
//! 1) [`MyceliumDR2`]: Detail router based on maze routing. Supports parallelization by routing multiple chip regions in parallel.
//! 2) [`SimpleMazeRouter`] : A simple classical maze router that finds routes on a regular grid. This is not meant to grow to something usable for practical designs but serves
//! as an easy to understand example.
//! 3) [`LineSearchRouter`] : A router based on line-search which does not require a grid. This is highly experimental.
//! 4) [`PowerRouter`] : A simple power-grid router.
//!
//! [`MyceliumDR2`]: mycelium_detail_route_v2::MyceliumDR2
//! [`SimpleMazeRouter`]: maze_router::SimpleMazeRouter
//! [`LineSearchRouter`]: line_search_router::SimpleLineSearchRouter
//! [`PowerRouter`]: power_router::PowerRouter

#![deny(missing_docs)]
// TODO: Remove those once this crate stabilizes.
#![allow(unused)]

mod branch_free_route;
mod global_router;
mod graph;
mod graph_router;
pub mod line_search_router;
pub mod maze_router;
mod multi_pin_decomposition;
mod multilevel_maze_router;
pub mod mycelium_detail_route_v2;
mod nested_list;
pub mod pin_access_analysis;
pub mod power_router;
mod sparse_array2d;
mod task_scheduler;
mod track_assignment;
mod util;
mod visualize;

use log;
use num_traits::Bounded;

use std::sync::Arc;

use libreda_pnr::db;
use pin_access_analysis::ViaDef;

/// Extract via definitions from the layout and the LEF file.
///
/// The via definitions are meant to be used as part of the input to the detail router.
///
/// On success, returns the IDs of the imported vias.
/// On error, returns the IDs of the successfully imported vias.
// Uses [`pin_access_analysis::via_extraction::extract_via_from_layout`] but detects the via cells based on the names in the LEF file.
pub fn extract_via_defs_from_lef<L, LayerStack>(
    chip: &L,
    lef: &libreda_lefdef::LEF,
    layer_stack: &LayerStack,
) -> Result<Vec<Arc<ViaDef<L::Coord, L::LayerId>>>, Vec<Arc<ViaDef<L::Coord, L::LayerId>>>>
where
    L: db::LayoutBase,
    LayerStack: db::RoutingLayerStack<LayerId = L::LayerId>,
    L::Coord: Bounded + Ord,
{
    // Use via names as defined in the LEF file.
    let via_names = lef.vias.keys().cloned();

    let layer_stack = layer_stack.layer_stack();

    let mut viadefs: Vec<Arc<ViaDef<_, _>>> = vec![];

    // Try to extract vias by name.
    let mut success = true;
    for via_name in via_names {
        // Find layout cell of via cell.
        log::debug!("Extract via definition: '{}'", via_name);
        if let Some(via_cell) = chip.cell_by_name(via_name.as_str()) {
            // Analyze the via and extract its shapes.
            let viadef = pin_access_analysis::via_extraction::extract_via_from_layout(
                &layer_stack,
                chip,
                &via_cell,
            );

            match viadef {
                Ok(viadef) => viadefs.push(Arc::new(viadef)),
                Err(_) => {
                    log::warn!("Failed to extract via definition: '{}'", via_name);
                    success = false;
                }
            }
        } else {
            log::warn!(
                "Via cell defined in LEF but not found in current layout: '{}'",
                via_name
            );
            success = false;
        }
    }

    if success {
        Ok(viadefs)
    } else {
        Err(viadefs)
    }
}
