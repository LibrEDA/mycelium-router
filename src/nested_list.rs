// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Compact representation of a two-level nested arrays.

/// Compressed representation of two-level nested arrays.
/// Allocates only two memory blocks instead of `n` for `n` sub-lists.
#[derive(Default, Clone)]
pub struct NestedList<T> {
    /// Contains start indices of blocks of elements for all the sub-lists.
    elements: Vec<T>,
    /// Contains elements of all sub-lists. They are grouped together. `elements` contains
    /// the indices to the begin of each sub-list.
    element_limits: Vec<usize>,
}

impl<T> NestedList<T> {
    /// Get the number of sub-lists.
    pub fn len(&self) -> usize {
        self.element_limits.len().max(1) - 1
    }

    /// Check if nested list is empty.
    pub fn is_empty(&self) -> bool {
        self.element_limits.is_empty()
    }

    /// Reserve memory for a given number of sub-lists.
    pub fn reserve_sub_lists(&mut self, num_sub_lists: usize) {
        self.element_limits.reserve(num_sub_lists + 1);
    }

    /// Reserve memory for a given number of total elements.
    pub fn reserve_elements(&mut self, num_elements: usize) {
        self.elements.reserve(num_elements);
    }

    /// Add a sub-list with its elements.
    /// Return the index of the sub-list.
    pub fn add_sub_list<I: Iterator<Item = T>>(&mut self, elements: I) -> usize {
        if self.element_limits.is_empty() {
            self.element_limits.push(0)
        }
        let index = self.element_limits.len() - 1;
        self.elements.extend(elements);
        self.element_limits.push(self.elements.len());

        index
    }

    /// Get a sub-list by its index.
    pub fn get_sub_list(&self, index: usize) -> &[T] {
        let start = self.element_limits[index];
        let end = self.element_limits[index];

        &self.elements[start..end]
    }

    /// Get a sub-list by its index.
    pub fn get_sub_list_mut(&mut self, index: usize) -> &mut [T] {
        let start = self.element_limits[index];
        let end = self.element_limits[index];

        &mut self.elements[start..end]
    }
}
