// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Example power router implementation for the LibrEDA place-and-route framework.
//!
//! The power router simply draws repeating GND and VDD power rails on multiple layers.
//! Where rails on two layers overlap they get connected with vias.
//!
//! # Example
//! ```
//! use libreda_pnr::db;
//! use db::traits::*;
//! use mycelium_router::power_router::*;
//!
//! // Setup the router.
//! let power_router = PowerRouter {
//!     // Define the power stripes.
//!     // This list must start and end with a metal layer. It can contain arbitrary many layers
//!     // but between each `PowerLayer` must be a `PowerVia`.
//!     stripe_definition: vec![
//!         PowerLayer {
//!             layer: (21, 0), // Define the layer by GDS/OASIS layer number.
//!             orientation: StripeOrientation::Horizontal,
//!             offset_gnd: 0,
//!             offset_pwr: 150, // Shift of the VDD rail relative to the GND rail.
//!             stripe_width: 100,
//!             step: 2470, // Spacing between power rail pairs.
//!         }.into(),
//!         PowerVia {
//!             layer: (22, 0),
//!             width: 40,
//!             via_spacing: 40,
//!             enclosure_lower: 10,
//!             enclosure_upper: 10,
//!         }.into(),
//!         PowerLayer {
//!             layer: (23, 0),
//!             orientation: StripeOrientation::Vertical,
//!             offset_gnd: 0,
//!             offset_pwr: 250,
//!             stripe_width: 200,
//!             step: 10000,
//!         }.into()
//!     ]
//! };
//!
//! // Create a new layout with one cell.
//! let mut chip = db::Chip::new();
//! let top = chip.create_cell("TOP".into());
//! let core_area = db::Rect::new((0, 0), (10000, 10000)).into();
//!
//! // Create the power grid.
//! power_router.draw_power_grid(
//!     &mut chip, &top, &core_area,
//! );
//!
//! ```

use libreda_lefdef::lef_ast::LEF;
use libreda_pnr::db;
use libreda_pnr::db::traits::*;

use iron_shapes_booleanop::*;

/// Orientation of the power stripes.
#[derive(Copy, Clone, Debug)]
pub enum StripeOrientation {
    /// Horizontal stripes.
    Horizontal,
    /// Vertical stripes.
    Vertical,
}

/// Describe the dimensions of the power stripes on this layer.
#[derive(Clone, Debug)]
pub struct PowerLayer {
    /// Layer ID.
    pub layer: (db::UInt, db::UInt),
    /// Orientation of the power stripes.
    pub orientation: StripeOrientation,
    /// Shift of the ground power stripe in perpendicular direction of the stripes.
    pub offset_gnd: db::Coord,
    /// Shift of the supply power stripe in perpendicular direction of the stripes.
    pub offset_pwr: db::Coord,
    /// With of the stripes.
    pub stripe_width: db::Coord,
    /// Repetition step.
    pub step: db::Coord,
}

/// Describe the rules for via cuts between two adjacent power layers.
#[derive(Clone, Debug)]
pub struct PowerVia {
    /// Layer ID.
    pub layer: (db::UInt, db::UInt),
    /// Width of the via.
    pub width: db::Coord,
    /// Spacing between vias.
    pub via_spacing: db::Coord,
    /// Enclosure in the lower metal layer.
    pub enclosure_lower: db::Coord,
    /// Enclosure in the upper metal layer.
    pub enclosure_upper: db::Coord,
}

/// Definition of either a power routing layer or a cut layer between
/// two power routing layers.
#[derive(Clone, Debug)]
pub enum PowerLayerDef {
    /// A routing layer.
    Routing(PowerLayer),
    /// A via cut layer between two routing layers.
    Cut(PowerVia),
}

impl Into<PowerLayerDef> for PowerVia {
    fn into(self) -> PowerLayerDef {
        PowerLayerDef::Cut(self)
    }
}

impl Into<PowerLayerDef> for PowerLayer {
    fn into(self) -> PowerLayerDef {
        PowerLayerDef::Routing(self)
    }
}

/// Simple power router (WIP).
pub struct PowerRouter {
    // lef: LEF,
    /// Power layer definition starting with the lowest metal layer.
    /// This must be an alternating sequence of routing and cut layers,
    /// starting with a routing layer and stopping with a routing layer.
    pub stripe_definition: Vec<PowerLayerDef>,
}

impl PowerRouter {
    /// Return the name of the router.
    pub fn name(&self) -> &str {
        "SimplePowerRouter"
    }

    /// WIP - Create simple-stupid power stripes.
    pub fn draw_power_grid<C: db::LayoutEdit<Coord = db::Coord>>(
        &self,
        chip: &mut C,
        cell: &C::CellId,
        boundary: &db::SimplePolygon<db::SInt>,
    ) {
        // Check that this is an alternating sequence of routing layers and cut layers.
        for (a, b) in self
            .stripe_definition
            .iter()
            .zip(self.stripe_definition.iter().skip(1))
        {
            match (a, b) {
                (PowerLayerDef::Cut(_), PowerLayerDef::Routing(_)) => {}
                (PowerLayerDef::Routing(_), PowerLayerDef::Cut(_)) => {}
                _ => panic!("Power layer definition must be an alternating sequence of routing layers and vias.")
            }
        }

        // Extract routing layer definitions.
        let routing_defs: Vec<_> = self
            .stripe_definition
            .iter()
            .filter_map(|d| match d {
                PowerLayerDef::Routing(r) => Some(r),
                PowerLayerDef::Cut(_) => None,
            })
            .collect();

        // Extract via layer definitions.
        let via_defs: Vec<_> = self
            .stripe_definition
            .iter()
            .filter_map(|d| match d {
                PowerLayerDef::Routing(r) => None,
                PowerLayerDef::Cut(c) => Some(c),
            })
            .collect();

        // Respect only the bounding box of the boundary.
        let boundary = boundary.try_bounding_box().unwrap();

        // Create stripes for each layer.
        let mut stripes = Vec::new();

        for def in routing_defs {
            let mut all_gnd_shapes = db::MultiPolygon::new();
            let mut all_supply_shapes = db::MultiPolygon::new();

            // Find layer.
            let (index, dtype) = def.layer;
            let layer = chip.find_or_create_layer(index, dtype);

            // Get start and end point of the base stripe.
            let (p1, p2) = match def.orientation {
                StripeOrientation::Horizontal => (boundary.lower_left(), boundary.lower_right()),
                StripeOrientation::Vertical => (boundary.lower_left(), boundary.upper_left()),
            };

            let stripe_direction = p2 - p1;

            // Get repetition step vector.
            let step_dir = match def.orientation {
                StripeOrientation::Horizontal => db::Vector::new(0, 1),
                StripeOrientation::Vertical => db::Vector::new(1, 0),
            };

            let step = step_dir * def.step;

            let half_width = def.stripe_width / 2;

            let mut current_offset = boundary.lower_left();

            // Fill the area withing the boundary with stripes.
            while boundary.contains_point(current_offset) {
                let start_gnd = current_offset + step_dir * def.offset_gnd;
                let end_gnd = start_gnd + stripe_direction;
                let gnd_stripe = db::Rect::new(
                    start_gnd - step_dir * half_width,
                    end_gnd + step_dir * half_width,
                );

                let start_supply = current_offset + step_dir * def.offset_pwr;
                let end_supply = start_supply + stripe_direction;
                let supply_stripe = db::Rect::new(
                    start_supply - step_dir * half_width,
                    end_supply + step_dir * half_width,
                );

                all_gnd_shapes.insert(gnd_stripe.into());
                all_supply_shapes.insert(supply_stripe.into());

                let gnd_stripe_id = chip.insert_shape(cell, &layer, gnd_stripe.into());
                let supply_stripe_id = chip.insert_shape(cell, &layer, supply_stripe.into());

                current_offset += step;
            }

            // Store the shapes on this layer.
            stripes.push((all_gnd_shapes, all_supply_shapes));
        }

        // Create vias.
        for (((gnd_lower, supply_lower), (gnd_upper, supply_upper)), via_def) in
            stripes.iter().zip(stripes.iter().skip(1)).zip(via_defs)
        {
            let (index, dtype) = via_def.layer;
            let via_layer = chip.find_or_create_layer(index, dtype);

            // Compute overlapping regions that can be connected by vias.

            let gnd_intersection = gnd_lower.intersection(gnd_upper);
            let supply_intersection = supply_lower.intersection(supply_upper);

            let mut create_via_array = |overlap: db::Rect<_>| {
                let spacing =
                    via_def.enclosure_lower.max(via_def.enclosure_upper) + via_def.width / 2;

                if spacing * 2 >= overlap.width() || spacing * 2 >= overlap.height() {
                    // No area left to place vias.
                    return;
                }

                let area = overlap.sized(-spacing, -spacing);

                let w = area.width();
                let h = area.height();
                // Compute array step size.
                let step = via_def.via_spacing + via_def.width;

                // Create via shape.
                let vw = via_def.width;
                let via_shape = db::Rect::new((-vw / 2, -vw / 2), (vw / 2, vw / 2));
                let via = via_shape.translate(area.lower_left().v());

                // Compute number of vias in x and y direction.
                let nx = (w + step - 1) / step;
                let ny = (h + step - 1) / step;

                // Set the via array into the center of the area.
                let shift_x = (w - (nx - 1) * step) / 2;
                let shift_y = (h - (ny - 1) * step) / 2;

                for i in 0..nx {
                    for j in 0..ny {
                        let x = i * step + shift_x;
                        let y = j * step + shift_y;
                        // Move to right spot.
                        let via = via.translate((x, y).into());
                        let via_id = chip.insert_shape(cell, &via_layer, via.into());
                    }
                }
            };

            for gnd_via_area in gnd_intersection.polygons {
                debug_assert_eq!(gnd_via_area.interiors.len(), 0);
                // Convert to rectangle.
                let area = gnd_via_area.try_bounding_box().unwrap();
                create_via_array(area);
                // let via_id =
                //     chip.insert_shape(cell, &via_layer, gnd_via_area.into());
            }

            for supply_via_area in supply_intersection.polygons {
                debug_assert_eq!(supply_via_area.interiors.len(), 0);
                // Convert to rectangle.
                let area = supply_via_area.try_bounding_box().unwrap();
                create_via_array(area);
                // let via_id =
                //     chip.insert_shape(cell, &via_layer, supply_via_area.into());
            }
        }
    }
}
