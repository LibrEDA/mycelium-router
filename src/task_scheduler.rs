// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Run interdependent tasks in parallel.

use log;
use num_traits::Saturating;
use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::num::NonZeroUsize;
use std::ops::Deref;
use std::sync::mpsc;
use std::sync::Arc;
use std::thread;

/// Runs tasks which may depend on each others results. If possible, tasks are executed in parallel.
pub struct TaskScheduler<'a, TaskId, TaskResult> {
    /// For each task T: store a list of tasks whose results are needed by T.
    dependencies: HashMap<TaskId, Vec<TaskId>>,
    tasks: HashMap<
        TaskId,
        Box<dyn FnOnce(TaskId, HashMap<TaskId, &TaskResult>) -> TaskResult + Send + 'a>,
    >,
    default_task:
        Option<Arc<dyn Fn(TaskId, HashMap<TaskId, &TaskResult>) -> TaskResult + Send + Sync + 'a>>,
    /// Store results of finished tasks.
    results: HashMap<TaskId, Arc<TaskResult>>,
    /// Tasks which are not selected to be run yet. The value counts the number of unresolved dependencies.
    pending_tasks: HashMap<TaskId, usize>,
    /// Queue of tasks which are ready to run (their dependencies are resolved).
    ready_tasks: Vec<TaskId>,
    /// Tasks which are currently running.
    running_tasks: HashSet<TaskId>,
    /// Desired amount of parallelism.
    num_threads: NonZeroUsize,
}

impl<'a, TaskId, TaskResult> TaskScheduler<'a, TaskId, TaskResult> {
    /// Create a new task scheduler.
    pub fn new() -> Self {
        Self {
            dependencies: Default::default(),
            tasks: Default::default(),
            default_task: Default::default(),
            results: Default::default(),
            pending_tasks: Default::default(),
            ready_tasks: Default::default(),
            running_tasks: Default::default(),
            num_threads: std::thread::available_parallelism()
                .unwrap_or(NonZeroUsize::new(1).unwrap()),
        }
    }
}

impl<'a, TaskId, TaskResult> TaskScheduler<'a, TaskId, TaskResult>
where
    TaskId: Clone + Hash + PartialEq + Eq + Sync + Send,
    TaskResult: Send + Sync,
{
    /// Set the maximal number of threads executing the tasks.
    pub fn set_num_threads(&mut self, num_threads: NonZeroUsize) {
        self.num_threads = num_threads;
    }

    /// Define a closure to be run for this specific task.
    /// See [`TaskScheduler::set_default_task`] to define a default closure for all other tasks.
    pub fn register_task<F>(&mut self, task: TaskId, f: F)
    where
        F: FnOnce(TaskId, HashMap<TaskId, &TaskResult>) -> TaskResult + Send + 'a,
    {
        self.dependencies.entry(task.clone()).or_default();

        self.tasks.insert(task, Box::new(f));
    }

    /// Register a task which should run the default function.
    pub fn register_task_with_default_function(&mut self, task: TaskId) {
        assert!(self.default_task.is_some(), "default task needs to be set");
        self.dependencies.entry(task.clone()).or_default();
    }

    /// Define default closure which will be run for tasks which don't have a specific closure defined.
    pub fn set_default_task<F>(&mut self, f: F)
    where
        F: Fn(TaskId, HashMap<TaskId, &TaskResult>) -> TaskResult + Send + Sync + 'a,
    {
        self.default_task = Some(Arc::new(f));
    }

    /// Tell the scheduler that `task` depends on the results of `depends_on`.
    pub fn register_dependency(&mut self, task: TaskId, depends_on: TaskId) {
        let tasks_are_defined =
            self.tasks.contains_key(&task) && self.tasks.contains_key(&depends_on);
        assert!(
            tasks_are_defined || self.default_task.is_some(),
            "default task needs to be set"
        );

        self.dependencies.entry(task).or_default().push(depends_on)
    }

    /// Start some tasks which are ready and return the number of started tasks.
    fn start_ready_tasks<'scope, 'env>(
        &mut self,
        thread_scope: &'scope thread::Scope<'scope, 'env>,
        tx: mpsc::Sender<(TaskId, TaskResult)>,
    ) -> usize
    where
        'a: 'scope,
        TaskId: 'scope,
        TaskResult: 'scope,
    {
        let max_num_new_tasks = self
            .num_threads
            .get()
            .saturating_sub(self.running_tasks.len())
            .min(self.ready_tasks.len());
        let ready_tasks: Vec<_> = self.ready_tasks.drain(0..max_num_new_tasks).collect();

        let num_ready = ready_tasks.len();

        for ready_task in ready_tasks {
            let tx = tx.clone();
            let task_id = ready_task.clone();

            // Get the results of the tasks dependencies.
            let dependency_results: Vec<(_, _)> = self
                .dependencies
                .get(&ready_task)
                .into_iter()
                .flatten()
                .map(|dep_id| (dep_id.clone(), self.results[dep_id].clone()))
                .collect();

            let thread_handle = if let Some(f) = self.tasks.remove(&task_id) {
                // Start the closure of the task.
                thread_scope.spawn(move || {
                    // Borrow the task results.
                    let dependency_results_ref: HashMap<TaskId, &TaskResult> = dependency_results
                        .iter()
                        .map(|(task_id, task_result)| (task_id.clone(), task_result.deref()))
                        .collect();

                    let result = f(task_id.clone(), dependency_results_ref);
                    tx.send((task_id, result))
                        .expect("failed to send result back to main thread");
                })
            } else {
                // Fall back to default task.
                let f = self.default_task.clone().expect("no default task set");

                thread_scope.spawn(move || {
                    // Borrow the task results.
                    let dependency_results_ref: HashMap<TaskId, &TaskResult> = dependency_results
                        .iter()
                        .map(|(task_id, task_result)| (task_id.clone(), task_result.deref()))
                        .collect();

                    let result = f(task_id.clone(), dependency_results_ref);
                    tx.send((task_id, result))
                        .expect("failed to send result back to main thread");
                })
            };

            self.running_tasks.insert(ready_task);
        }

        num_ready
    }

    // Sanity checks.
    fn check(&self) {
        // TODO: Check for cyclic dependencies.
    }

    /// Execute the function `f` for all tasks IDs, also pass the results of the dependencies.
    pub fn run(mut self) -> HashMap<TaskId, TaskResult> {
        log::debug!("Maximal number of threads: {}", self.num_threads);

        // Lookup-table to quickly find dependent tasks.
        let reverse_dependencies: HashMap<TaskId, HashSet<TaskId>> = {
            let mut reverse_dependencies = HashMap::new();

            self.dependencies
                .iter()
                .flat_map(|(task, deps)| deps.iter().map(move |dep| (task, dep)))
                .for_each(|(task, dep)| {
                    reverse_dependencies
                        .entry(dep.clone())
                        .or_insert(HashSet::new())
                        .insert(task.clone());
                });

            reverse_dependencies
        };

        // Initialize.
        let all_tasks = self
            .dependencies
            .keys()
            .chain(self.dependencies.values().flatten())
            .cloned();

        self.pending_tasks = all_tasks.map(|task_id| (task_id, 0)).collect();

        let num_tasks = self.pending_tasks.len();

        // Store number of dependencies of each pending task.
        self.pending_tasks
            .iter_mut()
            .for_each(|(task_id, num_dependencies)| {
                *num_dependencies = self
                    .dependencies
                    .get(task_id)
                    .map(|deps| deps.len())
                    .unwrap_or(0)
            });

        // All tasks with zero dependencies are ready to be run.
        // TODO: Use `HashMap::drain_filter` once it is stable.
        self.ready_tasks = {
            let ready_tasks: Vec<_> = self
                .pending_tasks
                .iter()
                .filter(|(_task_id, num_dependencies)| **num_dependencies == 0)
                .map(|(task_id, _)| task_id)
                .cloned()
                .collect();

            for t in &ready_tasks {
                self.pending_tasks.remove(t).unwrap();
            }

            ready_tasks
        };

        self.check();

        thread::scope(|thread_scope| {
            // Create communication channel between main thread and task threads.
            let (tx, rx) = mpsc::channel();

            let num_started = self.start_ready_tasks(&thread_scope, tx.clone());
            assert_ne!(
                num_started, 0,
                "no ready tasks, there's a cyclic dependency"
            );

            loop {
                // Wait for a task to finish.
                let (task_id, result) = rx
                    .recv()
                    .expect("failed to receive result from task thread");
                debug_assert!(!self.results.contains_key(&task_id));

                self.running_tasks.remove(&task_id);

                // Keep track of outstanding dependencies.
                // Find tasks whose dependencies are now resolved.
                {
                    let mut new_ready_tasks = vec![];
                    // Get tasks which depend on the just finished task...
                    let dependent_tasks = reverse_dependencies.get(&task_id).into_iter().flatten();
                    for dependent_task in dependent_tasks {
                        // ... decrease their dependency counter
                        if let Some(num_outstanding_dependencies) =
                            self.pending_tasks.get_mut(dependent_task)
                        {
                            *num_outstanding_dependencies =
                                num_outstanding_dependencies.saturating_sub(1);
                            if *num_outstanding_dependencies == 0 {
                                // ... mark them as ready if they don't have any outstanding dependencies.
                                new_ready_tasks.push(dependent_task.clone())
                            }
                        }
                    }

                    // Remove ready tasks from the list of pending tasks.
                    for t in &new_ready_tasks {
                        self.pending_tasks.remove(t).unwrap();
                    }

                    self.ready_tasks.extend(new_ready_tasks);
                }

                // Store the result of the task.
                self.results.insert(task_id, Arc::new(result));

                log::info!("Completed tasks: {}/{}", self.results.len(), num_tasks);

                if self.pending_tasks.len() + self.running_tasks.len() + self.ready_tasks.len() == 0
                {
                    break;
                }

                // Start new tasks.
                let num_started = self.start_ready_tasks(&thread_scope, tx.clone());
                if self.running_tasks.is_empty() {
                    // There must be some ready tasks now.
                    assert_ne!(
                        num_started, 0,
                        "no ready tasks, there's a cyclic dependency"
                    );
                }
            }
        });

        // Take results out from `Arc`.
        self.results
            .into_iter()
            .map(|(task_id, arc_result)| {
                let value = Arc::try_unwrap(arc_result)
                    .ok()
                    .expect("some result is still used"); // This should not happen.
                (task_id, value)
            })
            .collect()
    }
}

/// Example of how to use the [`TaskScheduler`].
#[test]
fn test_simple_tasks() {
    let mut scheduler = TaskScheduler::new();

    // Create task 1.
    scheduler.register_task(1, |task_id, existing_results| {
        assert_eq!(task_id, 1);
        assert!(existing_results.is_empty());

        println!("Hello! I'm task {}. I depend on no other tasks.", task_id);

        42
    });

    // Create task 2.
    scheduler.register_task(2, |task_id, existing_results| {
        assert_eq!(task_id, 2);
        assert_eq!(existing_results.len(), 1);

        println!(
            "Here's task 2. I depend on the result of task {} which is {}.",
            1, existing_results[&1]
        );
        assert_eq!(*existing_results[&1], 42);

        // All tasks need to have the same return type.
        0
    });

    scheduler.register_task(0, |task_id, existing_results| {
        println!("I'm task 0. I have no dependencies and run possibly in parallel to task 1.");
        0
    });

    // Tell the scheduler that task 2 depends on the results of task 1.
    scheduler.register_dependency(2, 1);

    // Run the tasks.
    let results = scheduler.run();
    assert_eq!(results[&1], 42);
}

#[test]
fn test_task_scheduler() {
    let shared_data: String = "some shared data without static lifetime".to_string();

    let mut scheduler = TaskScheduler::new();

    scheduler.set_default_task(|task_id, _existing_results| {
        assert!(shared_data.len() > 0);
        task_id * 2
    });

    scheduler.register_dependency(10, 0);
    scheduler.register_dependency(10, 1);
    scheduler.register_dependency(10, 2);
    scheduler.register_dependency(10, 3);

    scheduler.register_task(11, |_, _| {
        assert!(shared_data.len() > 0);
        123
    }); // 11 is an independent task.

    // Task 10 computes the sum of the results of task 0-3.
    scheduler.register_task(10, |task_id, existing_results| {
        assert!(shared_data.len() > 0);
        existing_results.values().map(|value| **value).sum()
    });

    let results = scheduler.run();

    assert_eq!(results[&11], 123);

    assert_eq!(results[&10], 12);
    assert_eq!(results[&0], 0);
    assert_eq!(results[&1], 2);
    assert_eq!(results[&2], 4);
    assert_eq!(results[&3], 6);
}

#[test]
fn test_task_with_closures() {
    let some_string = "my task id is".to_string();

    let mut scheduler = TaskScheduler::new();

    scheduler
        .set_default_task(move |task_id, existing_results| format!("{} {}", some_string, task_id));

    scheduler.register_task_with_default_function(1);
    scheduler.register_task_with_default_function(2);

    let results = scheduler.run();

    assert_eq!(results[&1], "my task id is 1");
    assert_eq!(results[&2], "my task id is 2");
}
