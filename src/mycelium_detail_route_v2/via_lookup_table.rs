// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Fast lookup for routing vias.

use fnv::FnvHashMap;
use libreda_pnr::db;

use std::marker::PhantomData;
use std::sync::Arc;

use crate::pin_access_analysis as pa;

/// Lookup table to quickly find vias used for routing.
pub struct ViaLookupTable<L: db::LayoutIds> {
    vias: FnvHashMap<L::LayerId, Vec<Arc<pa::ViaDef<L::Coord, L::LayerId>>>>,
    _layout: PhantomData<L>,
}

impl<L: db::LayoutBase> ViaLookupTable<L> {
    /// Create via lookup-table based on a set of via definitions.
    pub fn new(
        via_definitions: impl Iterator<Item = Arc<pa::ViaDef<L::Coord, L::LayerId>>>,
    ) -> Self {
        let mut vias: FnvHashMap<_, _> = Default::default();

        // Associate the via definitions with the metal layers used by the vias.
        for via_def in via_definitions {
            vias.entry(via_def.low_layer.clone())
                .or_insert(vec![])
                .push(via_def.clone());

            vias.entry(via_def.high_layer.clone())
                .or_insert(vec![])
                .push(via_def.clone());
        }

        Self {
            vias,
            _layout: Default::default(),
        }
    }

    /// Get all vias which touch the given layer.
    pub fn get_vias(
        &self,
        start_layer: &L::LayerId,
    ) -> impl Iterator<Item = &Arc<pa::ViaDef<L::Coord, L::LayerId>>> {
        self.vias.get(start_layer).into_iter().flatten()
    }

    /// Find vias with constrained start/end layer and constrained routing directions on start and end layers.
    pub fn get_vias_with_directions<'a>(
        &'a self,
        start_layer: &'a L::LayerId,
        end_layer: &'a L::LayerId,
        start_direction: Option<db::Orientation2D>,
        end_direction: Option<db::Orientation2D>,
    ) -> impl Iterator<Item = &Arc<pa::ViaDef<L::Coord, L::LayerId>>> + 'a {
        self.get_vias(start_layer)
            // Filter by end layer.
            .filter(move |via_def| {
                &via_def.low_layer == end_layer || &via_def.high_layer == end_layer
            })
            // Filter by directions.
            .filter(move |via_def| {
                let low_dir = via_def.low_routing_direction();
                let high_dir = via_def.high_routing_direction();

                let (start_dir, end_dir) = if &via_def.low_layer == start_layer {
                    // Looking for up-via.
                    (low_dir, high_dir)
                } else {
                    // Looking for down-via.
                    (high_dir, low_dir)
                };

                // Match the directions.
                // If the via defines routing directions, they must match.
                // If the via does not define directions, accept any.
                (start_direction.is_none() || start_dir.is_none() || start_direction == start_dir)
                    && (end_direction.is_none() || end_dir.is_none() || end_direction == end_dir)
            })
    }
}

impl<L: db::LayoutBase> Default for ViaLookupTable<L> {
    fn default() -> Self {
        Self::new(std::iter::empty())
    }
}
