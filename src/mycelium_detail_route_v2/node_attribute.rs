// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Attribute data for nodes in the routing graph.

/// Bit offsets as `(start, end)` tuples. `end` is excluded.
#[derive(Copy, Clone)]
struct Bits(usize, usize);

/// Bit offset of the routing guide flag.
const BIT_IS_ROUTING_GUIDE: Bits = Bits(0, 1);
/// Bit offset of the 'assigned track' flag.
const BIT_IS_ASSIGNED_TRACK: Bits = Bits(1, 2);
const BIT_TRACE_BACK_DIRECTION: Bits = Bits(2, 5);
const BIT_IS_DESTINATION: Bits = Bits(5, 6);
const BIT_IS_ACCESSPOINT: Bits = Bits(7, 8);

/// Integer type used to store the bitmap.
type BitmapType = u8;

/// Attributes of a node in the routing graph.
/// TODO: Pack densely, maybe use bitmap.
#[derive(Default, Clone, Debug)]
pub struct NodeAttribute {
    /// Cost of using this node.
    cost: i32,
    /// This node cannot be used if this value is non-zero.
    /// Counts how many times the node is reserved.
    reserved: u8,
    /// Compactly store bit flags and small integers.
    bitmap: u8,
}

impl NodeAttribute {
    /// Check if the node is reserved. A reserved node can't be used.
    pub fn is_reserved(&self) -> bool {
        self.reserved != 0
    }

    /// Increment the reservation counter.
    pub fn reserve(&mut self) {
        self.reserved += 1
    }

    /// Decrement the reservation counter.
    /// Panics when the reservation counter is already zero.
    pub fn unreserve(&mut self) {
        self.reserved -= 1
    }

    /// Get the cost of the node.
    pub fn cost(&self) -> i32 {
        self.cost
    }

    /// Add a value to the node cost.
    pub fn add_cost(&mut self, cost: i32) {
        self.cost += cost
    }

    /// Add a value to the node cost.
    pub fn add_marker_cost(&mut self, cost: i32) {
        self.cost += cost
    }

    fn bitmask(bits: Bits) -> BitmapType {
        ((1 << (bits.1 - bits.0)) - 1) << bits.0
    }

    fn get_bits(&self, position: Bits) -> BitmapType {
        debug_assert!(position.1 >= position.0);
        (self.bitmap & Self::bitmask(position)) >> position.0
    }

    fn set_bits(&mut self, position: Bits, value: BitmapType) {
        debug_assert!(position.1 >= position.0);
        let mask = Self::bitmask(position);
        self.bitmap = (self.bitmap & !mask) | ((value << position.0) & mask);
    }

    /// Select/deselect the node to be a destination node.
    pub fn enable_destination(&mut self, enable: bool) {
        self.set_bits(BIT_IS_DESTINATION, enable as BitmapType);
    }

    /// Test if the node is marked as a destination node.
    pub fn is_destination(&self) -> bool {
        self.get_bits(BIT_IS_DESTINATION) != 0
    }

    /// Select/deselect the node to be part of the routing guide for the current net.
    pub fn enable_routing_guide(&mut self, enable: bool) {
        self.set_bits(BIT_IS_ROUTING_GUIDE, enable as BitmapType);
    }

    /// Select/deselect the node as a part of an assigned track of the current net.
    pub fn set_assigned_track(&mut self, enable: bool) {
        self.set_bits(BIT_IS_ASSIGNED_TRACK, enable as BitmapType);
    }

    /// Test if the node is part of the routing guide for the current net.
    pub fn is_routing_guide_enabled(&self) -> bool {
        self.get_bits(BIT_IS_ROUTING_GUIDE) != 0
    }

    /// Test if the node is part of a track assigned to the current net.
    pub fn is_assigned_track(&self) -> bool {
        self.get_bits(BIT_IS_ASSIGNED_TRACK) != 0
    }

    /// Test if the node is marked as an 'access point'. Access points are terminal nodes which connect to pins of cells.
    pub fn is_access_point(&self) -> bool {
        self.get_bits(BIT_IS_ACCESSPOINT) != 0
    }

    /// Mark the node as an 'access point'. Access points are terminal nodes which connect to pins of cells.
    pub fn set_access_point(&mut self, enable: bool) {
        self.set_bits(BIT_IS_ACCESSPOINT, enable as BitmapType)
    }

    /// Get the direction towards the previous node.
    ///
    /// Used to back-trace the path after running the path search.
    /// Also used to mark a node as visited.
    pub fn get_trace_back_direction(&self) -> GridDirection {
        let bits = self.get_bits(BIT_TRACE_BACK_DIRECTION);
        GridDirection::from_u8(bits as u8).unwrap()
    }

    /// Set the direction towards the previous node.
    ///
    /// Used to back-trace the path after running the path search.
    /// Also used to mark a node as visited.
    pub fn set_trace_back_direction(&mut self, value: GridDirection) {
        self.set_bits(BIT_TRACE_BACK_DIRECTION, value.to_u8() as BitmapType);
    }
}

/// Pointer into the direction of the previous node.
/// Used for tracing back the shortest path in maze routing.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum GridDirection {
    NoDirection,
    XUp,
    XDown,
    YUp,
    YDown,
    ZUp,
    ZDown,
}

impl Default for GridDirection {
    fn default() -> Self {
        Self::NoDirection
    }
}

impl GridDirection {
    /// Convert to a integer. Only the three lowest-value bits are used.
    fn to_u8(self) -> u8 {
        use GridDirection::*;
        match self {
            NoDirection => 0,
            XUp => 1,
            XDown => 2,
            YUp => 3,
            YDown => 4,
            ZUp => 5,
            ZDown => 6,
        }
    }

    fn from_u8(value: u8) -> Option<Self> {
        use GridDirection::*;
        match value {
            0 => Some(NoDirection),
            1 => Some(XUp),
            2 => Some(XDown),
            3 => Some(YUp),
            4 => Some(YDown),
            5 => Some(ZUp),
            6 => Some(ZDown),
            _ => None,
        }
    }
}

#[test]
fn test_trace_back_direction_conversion() {
    assert! {
        (0..=6).all(|i| {
            i == GridDirection::from_u8(i)
                .unwrap()
                .to_u8()
        })
    }
}

#[test]
fn test_node_attr_bitmask() {
    assert_eq!(NodeAttribute::bitmask(Bits(1, 4)), 0b1110);
}

#[test]
fn test_node_attr() {
    let mut attr = NodeAttribute::default();

    assert_eq!(attr.get_trace_back_direction(), GridDirection::NoDirection);
    assert_eq!(attr.is_routing_guide_enabled(), false);
    assert_eq!(attr.is_assigned_track(), false);

    attr.enable_routing_guide(true);
    attr.set_assigned_track(true);
    attr.set_trace_back_direction(GridDirection::ZUp);

    assert_eq!(attr.get_trace_back_direction(), GridDirection::ZUp);
    assert_eq!(attr.is_routing_guide_enabled(), true);
    assert_eq!(attr.is_assigned_track(), true);
}
