// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Routing graph used for detail routing.
//! The routing graph is defined by routing tracks of the layer stack.

use fnv::FnvHashMap;
use itertools::{Itertools, MinMaxResult};

use std::hash::Hash;

use num_traits::{PrimInt, Signed};
use iron_shapes::isotropy::Direction1D;
use libreda_pnr::db;
use crate::graph::{CreateNodeAttributes, NodeAttributes, TraverseGraph};
use crate::mycelium_detail_route_v2::node_attribute::GridDirection;
use super::irregular_tracks::IrregularTracks;
use super::LayerId;

/// 2.5D point used as node in the routing graph.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub(super) struct Node<Crd> {
    /// Layer of the node.
    layer: LayerId,
    /// Location in the plane.
    coord: db::Point<Crd>,
}

impl<Crd: Copy> Node<Crd> {
    /// Get the layer of the node.
    pub fn layer(&self) -> LayerId {
        self.layer
    }

    /// Get the xy coordinate of the node.
    pub fn coord(&self) -> db::Point<Crd> {
        self.coord
    }
}

pub(super) struct RoutingGraph<Crd> {
    preferred_routing_direction_on_first_layer: db::Orientation2D,
    tracks: Vec<IrregularTracks<Crd>>,
}


impl<Crd> RoutingGraph<Crd> {
    /// Create new routing graph based on track patterns.
    pub fn new(preferred_routing_direction_on_first_layer: db::Orientation2D,
               tracks: Vec<IrregularTracks<Crd>>) -> Self {
        Self {
            preferred_routing_direction_on_first_layer,
            tracks,
        }
    }
}

impl<Crd> RoutingGraph<Crd>
    where Crd: PrimInt + Signed {
    /// Get a graph node from a point in the plane and a layer.
    /// Returns `None` if the routing graph has no node at this location.
    pub fn node_from_coordinates(&self, p: db::Point<Crd>, layer: LayerId) -> Option<Node<Crd>> {
        let n = Node {
            coord: p,
            layer,
        };

        self.contains_node(&n)
            .then_some(n)
    }

    /// Convert the graph node into a `(point, layer)` coordinate.
    pub fn node_to_coordinates(&self, n: &Node<Crd>) -> (db::Point<Crd>, LayerId) {
        (n.coord, n.layer)
    }

    /// Get the layer of a node in the routing graph.
    pub fn node_layer(&self, n: &Node<Crd>) -> LayerId {
        self.node_to_coordinates(n).1
    }

    /// Get the xy coordinates of a node in the routing graph.
    pub fn node_coord(&self, n: &Node<Crd>) -> db::Point<Crd> {
        self.node_to_coordinates(n).0
    }

    fn num_layers(&self) -> usize {
        self.tracks.len()
    }

    /// Get the bounding box of all tracks.
    fn boundary(&self) -> Option<db::Rect<Crd>> {
        let layers = (0..self.num_layers())
            .into_iter()
            .map(|l| LayerId(l as u8));

        let tracks_horiz = self.tracks.iter()
            .zip(layers.clone())
            .filter(|(t, layer)| self.track_orientation_on_layer(layer).is_horizontal());

        let tracks_vert = self.tracks.iter()
            .zip(layers)
            .filter(|(t, layer)| self.track_orientation_on_layer(layer).is_horizontal());

        use std::iter::once;
        let y_span: MinMaxResult<_> = tracks_horiz.clone()
            .flat_map(|(t, _layer)| once(t.first()).chain(once(t.last())))
            .flatten()
            .minmax();

        let x_span: MinMaxResult<_> = tracks_vert.clone()
            .flat_map(|(t, _layer)| once(t.first()).chain(once(t.last())))
            .flatten()
            .minmax();


        match (x_span, y_span) {
            (MinMaxResult::MinMax(x0, x1), MinMaxResult::MinMax(y0, y1)) =>
                Some(db::Rect::new((x0, y0), (x1, y1))),
            _ => None
        }
    }

    fn has_layer(&self, layer: &LayerId) -> bool {
        (layer.0 as usize) < self.num_layers()
    }

    fn lower_layer(&self, layer: &LayerId) -> Option<LayerId> {
        (self.has_layer(layer) && layer.0 > 0)
            .then(|| LayerId(layer.0 - 1))
    }

    fn upper_layer(&self, layer: &LayerId) -> Option<LayerId> {
        let upper = LayerId(layer.0 + 1);
        self.has_layer(&upper)
            .then_some(upper)
    }


    fn get_neighbour_layer(&self, direction: db::Direction1D, layer: &LayerId) -> Option<LayerId> {
        match direction {
            Direction1D::Low => self.lower_layer(layer),
            Direction1D::High => self.upper_layer(layer),
        }
    }

    fn track_orientation_on_layer(&self, layer: &LayerId) -> db::Orientation2D {
        if layer.0 % 2 == 0 {
            self.preferred_routing_direction_on_first_layer
        } else {
            self.preferred_routing_direction_on_first_layer.other()
        }
    }

    /// Get a reference to the track definition of a layer.
    fn get_tracks(&self, l: &LayerId) -> &IrregularTracks<Crd> {
        &self.tracks[l.0 as usize]
    }

    /// Check if the routing graph contains the given node.
    pub fn contains_node(&self, n: &Node<Crd>) -> bool {
        let contains_layer = (n.layer.0 as usize) < self.num_layers();

        let routing_direction = self.track_orientation_on_layer(&n.layer);

        let this_layer_contains_track = self.get_tracks(&n.layer)
            .contains_track(&n.coord.get(routing_direction.other()));

        let lower_layer_contains_track = n.layer.0 > 0 &&
            self.get_tracks(&LayerId(n.layer.0 - 1))
                .contains_track(&n.coord.get(routing_direction));

        let upper_layer_contains_track = (n.layer.0 as usize) + 1 < self.num_layers() &&
            self.get_tracks(&LayerId(n.layer.0 + 1))
                .contains_track(&n.coord.get(routing_direction));

        contains_layer
            && this_layer_contains_track && (lower_layer_contains_track || upper_layer_contains_track)
    }

    /// Find the closest neighbour node of `n` in the given three-dimensional direction.
    pub fn get_neighbour(&self, direction: GridDirection, n: &Node<Crd>) -> Option<Node<Crd>> {
        use GridDirection::*;
        match direction {
            NoDirection => None,
            XUp => self.get_neighbour_xy(db::Direction2D::Right, n),
            XDown => self.get_neighbour_xy(db::Direction2D::Left, n),
            YUp => self.get_neighbour_xy(db::Direction2D::Up, n),
            YDown => self.get_neighbour_xy(db::Direction2D::Down, n),
            ZUp => self.get_neighbour_z(db::Direction1D::High, n),
            ZDown => self.get_neighbour_z(db::Direction1D::Low, n),
        }
    }

    /// Get neighbour of a node in z direction.
    pub fn get_neighbour_z(&self, direction: db::Direction1D, n: &Node<Crd>) -> Option<Node<Crd>> {
        let other_layer = self.get_neighbour_layer(direction, &n.layer)?;
        let neighbour = Node { layer: other_layer, ..*n };
        self.contains_node(&neighbour)
            .then_some(neighbour)
    }

    /// Get neighbour of a node in x or y direction.
    pub fn get_neighbour_xy(&self, direction: db::Direction2D, n: &Node<Crd>) -> Option<Node<Crd>> {
        debug_assert!(self.contains_node(n), "node is not in the graph");

        let orientation_this_layer = self.track_orientation_on_layer(&n.layer);

        let search_orient = direction.orientation();
        let node_offset = n.coord.get(search_orient);

        let neighbour_point = if search_orient != orientation_this_layer {
            // Neighbour in non-preferred routing direction.
            let tracks = self.get_tracks(&n.layer);

            // Find the track index which contains the node.
            let track_of_node = tracks.find_track_index(&node_offset)?;

            let neighbour_idx = tracks.neighbour(direction.into(), track_of_node)?;
            let neighbour_track_offset = tracks.track_offset(neighbour_idx);

            let mut p = n.coord;
            p.set(search_orient, neighbour_track_offset);
            p
        } else {
            // Search in preferred routing direction.
            // Search tracks of lower and upper layers.

            let neighbour_layers = self.lower_layer(&n.layer).into_iter()
                .chain(self.upper_layer(&n.layer));

            // Find neighbour track on the neighbour layers.
            let neighbour_tracks = neighbour_layers
                .flat_map(|l| {
                    let tracks = self.get_tracks(&l);
                    let track_of_node = tracks.find_track_index(&node_offset)?;
                    let neighbour_idx = tracks.neighbour(direction.into(), track_of_node);
                    neighbour_idx.map(|idx| tracks.track_offset(idx))
                });

            // Find closest track.
            let closest = neighbour_tracks
                .min_by_key(|off| {
                    (node_offset - *off).abs()
                })?;

            let mut p = n.coord;
            p.set(search_orient, closest);
            p
        };


        let n = Node {
            coord: neighbour_point,
            layer: n.layer,
        };

        debug_assert!(self.contains_node(&n), "node is not in the graph");

        Some(n)
    }

    /// Iterate over all nodes in the graph.
    pub fn each_node(&self) -> impl Iterator<Item=Node<Crd>> + '_ {
        self.boundary()
            .into_iter()
            .flat_map(move |boundary| {
                (0..self.num_layers()).into_iter()
                    .map(|l| LayerId(l as u8))
                    .flat_map(move |layer| self.nodes_in_rect(layer, &boundary))
            })
    }

    /// Iterate over all nodes contained in the rectangle `rect` on a specific layer.
    pub fn nodes_in_rect(&self, layer: LayerId, rect: &db::Rect<Crd>) -> impl Iterator<Item=Node<Crd>> + '_ {
        let track_orientation = self.track_orientation_on_layer(&layer);

        let (ll, ur) = (rect.lower_left(), rect.upper_right());
        let (dx, dy) = ((ll.x, ur.x), (ll.y, ur.y));

        let (interval_this_layer, interval_other_layer) = match track_orientation {
            db::Orientation2D::Horizontal => (dy, dx),
            db::Orientation2D::Vertical => (dx, dy)
        };

        let neighbour_layers = self.lower_layer(&layer).into_iter()
            .chain(self.upper_layer(&layer));

        // Merge tracks of lower and upper layers (all within the rectangle).
        let lower_and_upper_tracks = neighbour_layers
            .map(|l| {
                self.get_tracks(&l)
                    .get_tracks_in_interval(interval_other_layer)
            })
            .kmerge()
            .dedup();

        // Compute cartesian product of 'track offsets on this layer' x 'track offsets on neighbour layers'.
        let points = lower_and_upper_tracks
            .flat_map(move |other_offset| {
                self.get_tracks(&layer)
                    .get_tracks_in_interval(interval_this_layer)
                    .map(move |offset| (offset, other_offset))
            })
            // Convert to point.
            .map(move |(offset, other_offset)| {
                match track_orientation {
                    db::Orientation2D::Horizontal => db::Point::new(other_offset, offset),
                    db::Orientation2D::Vertical => db::Point::new(offset, other_offset)
                }
            });

        // Convert to nodes.
        points.map(move |p| Node { coord: p, layer })
    }
}


impl<Crd> TraverseGraph<Node<Crd>> for RoutingGraph<Crd>
    where Crd: PrimInt + Signed {
    fn neighbours(&self, buffer: &mut Vec<Node<Crd>>, n: &Node<Crd>) {
        debug_assert!(self.contains_node(n));
        debug_assert!(buffer.is_empty(), "Buffer must be cleared.");

        // Get neighbours on same layer.
        use db::Direction2D::*;
        let directions = [Right, Up, Left, Down];
        buffer.extend(
            directions.iter()
                .flat_map(|dir| self.get_neighbour_xy(*dir, n))
        );

        buffer.extend(self.get_neighbour_z(db::Direction1D::Low, n));
        buffer.extend(self.get_neighbour_z(db::Direction1D::High, n));
    }

    fn contains_node(&self, n: &Node<Crd>) -> bool {
        self.contains_node(n)
    }
}

#[test]
fn test_neighbours() {
    let tracks1 = IrregularTracks::new(
        0, 100, 10,
        vec![],
    );
    let tracks2 = IrregularTracks::new(
        0, 100, 10,
        vec![],
    );
    let tracks3 = IrregularTracks::new(
        0, 100, 20,
        vec![],
    );

    let graph = RoutingGraph {
        preferred_routing_direction_on_first_layer: db::Orientation2D::Horizontal,
        tracks: vec![tracks1, tracks2, tracks3],
    };

    let n = Node {
        layer: LayerId(0),
        coord: db::Point::new(0, 0),
    };

    assert_eq!(
        graph.get_neighbour_xy(db::Direction2D::Right, &n),
        Some(Node { coord: db::Point::new(10, 0), ..n })
    );

    assert_eq!(
        graph.get_neighbour_xy(db::Direction2D::Up, &n),
        Some(Node { coord: db::Point::new(0, 10), ..n })
    );

    assert_eq!(
        graph.get_neighbour_xy(db::Direction2D::Left, &n),
        None
    );

    assert_eq!(
        graph.get_neighbour_xy(db::Direction2D::Down, &n),
        None
    );

    let n = Node {
        layer: LayerId(0),
        coord: db::Point::new(10, 10),
    };

    assert_eq!(
        graph.get_neighbour_xy(db::Direction2D::Left, &n),
        Some(Node { coord: db::Point::new(0, 10), ..n })
    );

    assert_eq!(
        graph.get_neighbour_xy(db::Direction2D::Down, &n),
        Some(Node { coord: db::Point::new(10, 0), ..n })
    );
}

#[test]
fn test_nodes_in_rectangle() {
    let tracks1 = IrregularTracks::new(
        0, 100, 5,
        vec![],
    );
    let tracks2 = IrregularTracks::new(
        0, 100, 10,
        vec![],
    );
    let tracks3 = IrregularTracks::new(
        0, 100, 20,
        vec![],
    );

    let graph = RoutingGraph {
        preferred_routing_direction_on_first_layer: db::Orientation2D::Horizontal,
        tracks: vec![tracks1, tracks2, tracks3],
    };

    let rect = db::Rect::new((20, 20), (40, 40));

    let points: Vec<_> = graph.nodes_in_rect(LayerId(1), &rect)
        .map(|n| (n.coord.x, n.coord.y))
        .collect();

    assert_eq!(points, vec![
        (20, 20),
        (30, 20),
        (40, 20),
        (20, 25),
        (30, 25),
        (40, 25),
        (20, 30),
        (30, 30),
        (40, 30),
        (20, 35),
        (30, 35),
        (40, 35),
        (20, 40),
        (30, 40),
        (40, 40),
    ])
}


impl<Crd, Attr> CreateNodeAttributes<Node<Crd>, Attr> for RoutingGraph<Crd>
    where Crd: Copy + Hash + Eq,
          Attr: Clone {
    type NodeAttrs = RoutingGraphNodeAttributes<Crd, Attr>;

    fn create_attributes(&self, default_value: Attr) -> Self::NodeAttrs {
        Self::NodeAttrs {
            default_value,
            attributes: Default::default(),
        }
    }
}

#[derive(Debug, Default, Clone)]
pub struct RoutingGraphNodeAttributes<Crd, Attr> {
    default_value: Attr,
    // TODO: Store attributes in an array.
    attributes: FnvHashMap<Node<Crd>, Attr>,
}

impl<Crd, Attr> NodeAttributes<Node<Crd>, Attr> for RoutingGraphNodeAttributes<Crd, Attr>
    where Crd: Copy + Hash + Eq,
          Attr: Clone {
    fn get(&self, node: &Node<Crd>) -> &Attr {
        self.attributes.get(node)
            .unwrap_or(&self.default_value)
    }

    fn get_mut(&mut self, node: &Node<Crd>) -> &mut Attr {
        self.attributes.entry(*node)
            .or_insert(self.default_value.clone())
    }

    fn set(&mut self, node: &Node<Crd>, value: Attr) {
        self.attributes.insert(*node, value);
    }
}


#[test]
fn test_attributes() {
    let tracks1 = IrregularTracks::new(
        0, 100, 10,
        vec![],
    );
    let tracks2 = IrregularTracks::new(
        0, 100, 20,
        vec![],
    );

    let graph = RoutingGraph {
        preferred_routing_direction_on_first_layer: db::Orientation2D::Horizontal,
        tracks: vec![tracks1, tracks2],
    };

    let n = Node {
        layer: LayerId(0),
        coord: db::Point::new(0, 0),
    };

    let mut int_attr = graph.create_attributes(7);

    // Test default value:
    assert_eq!(int_attr.get(&n), &7);

    // Test `set()`:
    int_attr.set(&n, 42);
    assert_eq!(int_attr.get(&n), &42);

    // Test `get_mut()`:
    let attr_mut = int_attr.get_mut(&n);
    *attr_mut += 1;

    assert_eq!(int_attr.get(&n), &43);
}
