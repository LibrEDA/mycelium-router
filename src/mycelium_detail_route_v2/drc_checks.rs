// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Simple DRC check for detecting short circuits created during detail routing.

use super::clip_worker::SubnetId;
use super::geometric_objects::*;
use super::region_query::RegionQuery;
use super::LayerId;
use libreda_pnr::db;
use num_traits::{PrimInt, Signed, Zero};
use rstar::RTreeObject;

/// Detect short-circuits of a net with other nets.
/// For now only metal wires are respected, not vias. Vias are likely to be done right by the detail router.
///
/// Returns found shorts as tuples of the form `(layer, rectangle, other net)`.
pub(super) fn detect_shorts<Coord, NetId>(
    net: &SubnetId<NetId>,
    net_shapes: impl Iterator<Item = DrcCheckShape<Coord>>,
    region_query: &RegionQuery<Coord, SubnetId<NetId>>,
) -> Vec<(LayerId, db::Rect<Coord>, Option<SubnetId<NetId>>)>
where
    Coord: PrimInt + Signed + std::fmt::Debug,
    NetId: Clone + PartialEq,
    GeometricObject<Coord, SubnetId<NetId>>: RTreeObject,
{
    net_shapes
        // Find short circuits for each shape of the net.
        .flat_map(|check_shape| {
            // Use a slightly enlarged rectangle to make sure the intersection
            // with a touching rectangle will not be empty.
            let layer = check_shape.layer;
            let rect = check_shape.shape;
            let rect_enlarged = rect.sized(Coord::one(), Coord::one());

            // Find overlapping rectangles of other nets.
            region_query
                .locate_intersect_rect(layer, &rect)
                // Take only geometric objects which cause a short.
                .filter_map(move |geo_obj| {
                    // Extract shape and net of colliding object.
                    let (marker, maybe_other_subnet) = match geo_obj {
                        GeometricObject::RouteSegment(RouteSegment { shape, net_id })
                        | GeometricObject::Obstacle(Obstacle { shape, net_id }) => {
                            // Use the intersection of the both rectangles as marker.
                            let marker = shape
                                .intersection(&rect_enlarged)
                                .expect("rectangles must intersect");
                            (marker, net_id)
                        }
                        GeometricObject::PatchMetal(patch) => {
                            let marker = patch
                                .shape
                                .intersection(&rect_enlarged)
                                .expect("rectangles must intersect")
                                .add_rect(&patch.origin); // Also penalize the graph nodes which created the marker.
                            (marker, &patch.net_id)
                        }
                    };

                    // If the nets are different => short circuit.
                    let other_net = maybe_other_subnet.as_ref().map(|subnet| &subnet.net);
                    if other_net == Some(&net.net) {
                        // Same net, no short circuit.
                        None
                    } else {
                        // Short circuit.
                        Some((marker, maybe_other_subnet.clone()))
                    }
                })
                // Associate with layer ID and violation marker.
                .map(move |(marker, maybe_net)| (layer, marker, maybe_net))
        })
        .collect()
}

/// A shape of a net which should be checked for design rule violations.
#[derive(Copy, Clone)]
pub(super) struct DrcCheckShape<Coord> {
    /// Layer of the shape to be checked.
    pub layer: LayerId,
    /// Shape to be checked.
    pub shape: db::Rect<Coord>,
    /// Layer and shape on which the DRC marker costs should be added.
    pub origin: Option<(LayerId, db::Rect<Coord>)>,
}

pub(super) fn spacing_check<LN, Rules, NetId>(
    layout: &LN,
    spacing_rules: &Rules,
    net: &SubnetId<NetId>,
    net_shapes: impl Iterator<Item = DrcCheckShape<LN::Coord>>,
    region_query: &RegionQuery<LN::Coord, SubnetId<NetId>>,
    db_layer_mapping: &[LN::LayerId],
) -> Vec<(LayerId, db::Rect<LN::Coord>, Option<SubnetId<NetId>>)>
where
    LN: db::L2NBase,
    LN::Coord: PrimInt + Signed + std::fmt::Debug,
    NetId: Clone + PartialEq,
    Rules: db::DistanceRuleBase<Distance = LN::Coord, LayerId = LN::LayerId> + db::MinimumSpacing,
{
    net_shapes
        // Find short circuits for each shape of the net.
        .flat_map(|check_shape| {
            let layer = check_shape.layer;
            // Enlarge the rectangle by the minimum spacing.
            let db_layer = &db_layer_mapping[layer.to_usize()];
            let rect = check_shape.shape;

            let min_spacing_x = spacing_rules
                .min_spacing(
                    db_layer,
                    rect.height(), // Be pessimistic with the parallel runlength.
                    rect.width(),
                )
                .unwrap_or(LN::Coord::zero());

            let min_spacing_y = spacing_rules
                .min_spacing(
                    db_layer,
                    rect.width(), // Be pessimistic with the parallel runlength.
                    rect.height(),
                )
                .unwrap_or(LN::Coord::zero());

            // Enlarge by 1, to make sure the intersections are not empty.
            let rect_enlarged = rect.sized(min_spacing_x, min_spacing_y);

            // Find overlapping rectangles of other nets.
            region_query
                .locate_intersect_rect(layer, &rect_enlarged)
                // Take only geometric objects which cause a violation.
                .flat_map(move |geo_obj| {
                    // Extract shape and net of colliding object.
                    let (marker, marker_layer, maybe_other_subnet) = match geo_obj {
                        GeometricObject::RouteSegment(RouteSegment { shape, net_id })
                        | GeometricObject::Obstacle(Obstacle { shape, net_id }) => {
                            // Use the intersection of the both rectangles as marker.
                            let marker = shape.intersection(&rect_enlarged);
                            (marker, layer, net_id) // Marker is on the same layer as the segment/obstacle.
                        }
                        GeometricObject::PatchMetal(patch) => {
                            let marker = patch
                                .shape
                                .intersection(&rect_enlarged)
                                .unwrap_or(patch.origin)
                                .add_rect(&patch.origin); // Also penalize the graph nodes which created the marker.
                            (Some(marker), patch.origin_layer, &patch.net_id) // Marker is possibly on a specific layer.
                        }
                    };

                    // If the nets are different => short circuit.
                    let other_net = maybe_other_subnet.as_ref().map(|subnet| &subnet.net);
                    let (marker, self_marker) = if other_net == Some(&net.net) {
                        // Same net => ignore violation.
                        // TODO: Handle same-net spacing violations.
                        (None, None)
                    } else {
                        // Different nets => Spacing violation with an obstacle.

                        // Create a marker which increases the cost on the origin of the current object.
                        let self_marker = check_shape.origin.map(|(origin_layer, origin_shape)| {
                            (Some(origin_shape), origin_layer, maybe_other_subnet.clone())
                        });

                        let marker = Some((marker, marker_layer, maybe_other_subnet.clone()));

                        (self_marker, marker)
                    };

                    marker.into_iter().chain(self_marker)
                })
                .filter_map(|(marker, layer, net)| marker.map(|m| (m, layer, net)))
                // Intersection area must be larger than zero.
                .filter(|(marker, _marker_layer, _net)| {
                    // Check if intersection area is > 0.
                    marker.width() * marker.height() > Zero::zero()
                })
                // Associate with layer ID and violation marker.
                .map(move |(marker, marker_layer, maybe_net)| (marker_layer, marker, maybe_net))
        })
        .collect()
}
