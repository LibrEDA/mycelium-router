// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Geometric objects used for detail routing.

use db::traits::BoundingBox;
use libreda_pnr::db;
use num_traits::{PrimInt, Signed};
use rstar;

use super::LayerId;

type SInt = i32;

#[derive(Debug, Clone, PartialEq)]
pub struct PathSegment<Crd> {
    segment: db::REdge<Crd>,
    half_width: Crd,
    net: Option<()>,
}

impl<Crd> db::BoundingBox<Crd> for PathSegment<Crd>
where
    Crd: PrimInt,
{
    fn bounding_box(&self) -> db::Rect<Crd> {
        let extension = db::Vector::new(self.half_width, self.half_width);
        let bbox1 = db::Rect::new(self.segment.start(), self.segment.end());
        db::Rect::new(
            bbox1.lower_left() - extension,
            bbox1.upper_right() + extension,
        )
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Obstacle<Crd, NetId> {
    pub(super) shape: db::Rect<Crd>,
    pub(super) net_id: Option<NetId>,
}

impl<Crd, NetId> db::BoundingBox<Crd> for Obstacle<Crd, NetId>
where
    Crd: PrimInt,
{
    fn bounding_box(&self) -> db::Rect<Crd> {
        self.shape
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct RoutingGuide<Crd, NetId, LayerId> {
    pub(super) shape: db::Rect<Crd>,
    pub(super) layer: LayerId,
    pub(super) net_id: NetId,
}

impl<Crd, NetId, LayerId> db::BoundingBox<Crd> for RoutingGuide<Crd, NetId, LayerId>
where
    Crd: PrimInt,
{
    fn bounding_box(&self) -> db::Rect<Crd> {
        self.shape
    }
}

impl<Crd, NetId, LayerId> rstar::RTreeObject for RoutingGuide<Crd, NetId, LayerId>
where
    Crd: PrimInt + Signed + std::fmt::Debug,
{
    type Envelope = rstar::AABB<[Crd; 2]>;

    fn envelope(&self) -> Self::Envelope {
        let bbox = self.bounding_box();

        rstar::AABB::from_corners(bbox.lower_left().into(), bbox.upper_right().into())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Iroute<Crd, NetId, LayerId> {
    pub(super) edge: db::REdge<Crd>,
    pub(super) layer: LayerId,
    pub(super) net_id: NetId,
}

impl<Crd, NetId, LayerId> db::BoundingBox<Crd> for Iroute<Crd, NetId, LayerId>
where
    Crd: PrimInt,
{
    fn bounding_box(&self) -> db::Rect<Crd> {
        self.edge.bounding_box()
    }
}

impl<Crd, NetId, LayerId> rstar::RTreeObject for Iroute<Crd, NetId, LayerId>
where
    Crd: PrimInt + Signed + std::fmt::Debug,
{
    type Envelope = rstar::AABB<[Crd; 2]>;

    fn envelope(&self) -> Self::Envelope {
        let bbox = self.bounding_box();

        rstar::AABB::from_corners(bbox.lower_left().into(), bbox.upper_right().into())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct RouteSegment<Crd, NetId> {
    pub(super) shape: db::Rect<Crd>,
    pub(super) net_id: Option<NetId>,
}

impl<Crd, NetId> db::BoundingBox<Crd> for RouteSegment<Crd, NetId>
where
    Crd: PrimInt,
{
    fn bounding_box(&self) -> db::Rect<Crd> {
        self.shape
    }
}

/// A piece of metal which cannot be described by a center line and a width.
/// A patch metal can for instance be the enclosure around a via, or a minimum-area fix.
/// In order to know which node costs to increase such that the patch metal will be moved it also contains an `origin` shape.
/// The origin marks the region which is responsible for creating the patch metal.
#[derive(Debug, Clone, PartialEq)]
pub struct PatchMetal<Crd, NetId> {
    pub(super) shape: db::Rect<Crd>,
    /// Region/anchor responsible for creating the patch. For example: If the patch forms an enclosure for a via, then the origin marks the via center.
    pub(super) origin: db::Rect<Crd>,
    /// The 'anchor' of the patch metal might be on another layer.
    pub(super) origin_layer: LayerId,
    pub(super) net_id: Option<NetId>,
}

impl<Crd, NetId> db::BoundingBox<Crd> for PatchMetal<Crd, NetId>
where
    Crd: PrimInt,
{
    fn bounding_box(&self) -> db::Rect<Crd> {
        self.shape
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum GeometricObject<Crd, NetId> {
    RouteSegment(RouteSegment<Crd, NetId>),
    PatchMetal(PatchMetal<Crd, NetId>),
    Obstacle(Obstacle<Crd, NetId>),
}

impl<Crd, NetId> db::BoundingBox<Crd> for GeometricObject<Crd, NetId>
where
    Crd: PrimInt,
{
    fn bounding_box(&self) -> db::Rect<Crd> {
        match self {
            Self::Obstacle(o) => o.bounding_box(),
            Self::RouteSegment(seg) => seg.bounding_box(),
            Self::PatchMetal(patch) => patch.bounding_box(),
        }
    }
}

impl<Crd, NetId> rstar::RTreeObject for GeometricObject<Crd, NetId>
where
    Crd: PrimInt + Signed + std::fmt::Debug,
{
    type Envelope = rstar::AABB<[Crd; 2]>;

    fn envelope(&self) -> Self::Envelope {
        let bbox = self.bounding_box();

        rstar::AABB::from_corners(bbox.lower_left().into(), bbox.upper_right().into())
    }
}
