// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Analyze rectangular routing guides which come from global routing. This is used as precomputation steps before using the guides in detail routing.
//! * Reconstruct the global routing grid
//! * Find connected components withing a detail routing clip. (A global route might be split into multiple parts by the clip)

use db::traits::*;
use fnv::FnvHashSet;
use iron_shapes::isotropy::Orientation2D;
use libreda_pnr::db;
use num_traits::{Num, PrimInt, Signed};
use std::hash::Hash;

/// Extract the pitch of the rectangle boundaries and the bounding box of all rectangles.
///
/// Return a tuple `(grid pitch as a (pitch_x, pitch_y) vector, grid area)`.
pub fn reconstruct_global_routing_grid<Coord>(
    guide_shapes: impl Iterator<Item = db::Rect<Coord>>,
) -> Option<(db::Vector<Coord>, db::Rect<Coord>)>
where
    Coord: Num + PrimInt + Hash,
{
    // Collect used coordinates.
    let mut xs: FnvHashSet<_> = Default::default();
    let mut ys: FnvHashSet<_> = Default::default();

    for r in guide_shapes {
        xs.insert(r.lower_left().x);
        xs.insert(r.upper_right().x);

        ys.insert(r.lower_left().y);
        ys.insert(r.upper_right().y);
    }

    let mut xs: Vec<_> = xs.into_iter().collect();
    let mut ys: Vec<_> = ys.into_iter().collect();

    xs.sort_unstable();
    ys.sort_unstable();

    // Find the minimal used pitch in the coordinates.
    let x_pitch = extract_pitch(&xs)?;
    let y_pitch = extract_pitch(&ys)?;

    // Find the bounding box.
    let x_start = *xs.first()?;
    let y_start = *ys.first()?;
    let x_end = *xs.last()?;
    let y_end = *ys.last()?;

    let bbox = db::Rect::new((x_start, y_start), (x_end, y_end));
    let pitch = db::Vector::new(x_pitch, y_pitch);

    Some((pitch, bbox))
}

#[test]
fn test_reconstruct_grid() {
    let rectangles = [
        db::Rect::new((0, 0), (10, 20)),
        db::Rect::new((100, 100), (1000, 110)),
    ];

    assert_eq!(
        reconstruct_global_routing_grid(rectangles.iter().copied()),
        Some((db::Vector::new(10, 10), db::Rect::new((0, 0), (1000, 110))))
    );
}

/// Extract the common pitch from a set of sorted coordinates.
fn extract_pitch<Coord>(arr: &[Coord]) -> Option<Coord>
where
    Coord: Num + PrimInt + Hash,
{
    assert!(
        arr.windows(2).all(|w| w[0] <= w[1]),
        "coordinates must be sorted"
    );

    let diffs: FnvHashSet<_> = arr.windows(2).map(|w| w[1] - w[0]).collect();

    let min_diff = diffs.iter().copied().min()?;

    let is_all_multiples_of_min_diff = diffs.iter().all(|d| *d % min_diff == Coord::zero());

    is_all_multiples_of_min_diff.then_some(min_diff)
}

#[test]
fn test_extract_pitch() {
    let coords = [10, 100, 150, 155, 200];
    assert_eq!(extract_pitch(&coords), Some(5));
}

/// Given a set of rectangle shapes on different layers group them by electrical connectivity.
///
/// Rectangles on two adjacent layers are considered connected if they overlap.
/// Rectangles on the same layer are considered connected if their intersection is larger than a single point.
///
/// Runs in `O(n^2)`.
///
/// To be used for routing guides of a single net within a detail routing clip. This should not be many,
/// so `O(n^2)` should be fine.
///
/// Returns indices into the `guide_shape` array.
///
/// Alternatively, ironshapes-booleanop could be used to extract the connectivity. It has lower complexity.
pub fn connected_components<Coord, Layer>(
    layer_stack: &[Layer],
    guide_shapes: &[(Layer, db::Rect<Coord>)],
) -> Vec<Vec<usize>>
where
    Coord: Num + PrimInt,
    Layer: PartialEq,
{
    // Graph which represents the connectivity between rectangles.
    let mut connectivity_graph: petgraph::graphmap::UnGraphMap<usize, ()> = Default::default();

    // Associate shapes with their index.
    let guides_with_index = guide_shapes.iter().enumerate();

    // Add all guide indices as graph nodes.
    guides_with_index.clone().for_each(|(idx, _)| {
        connectivity_graph.add_node(idx);
    });

    // Buffers.
    let mut guides_this_layer = vec![];
    let mut guides_upper_layer = vec![];

    // Create edges between guide touching guide shapes.
    for layer_idx in 0..layer_stack.len() {
        let layer = &layer_stack[layer_idx];
        let upper_layer = layer_stack.get(layer_idx + 1);

        guides_this_layer.clear();
        guides_upper_layer.clear();
        // Extract same-layer connectivity.
        {
            {
                let guides_on_this_layer = guides_with_index
                    .clone()
                    .filter(|(_, (l, _))| l == layer)
                    .map(|(i, (_, rect))| (i, rect));
                guides_this_layer.extend(guides_on_this_layer);
            }

            // Check all pairs of rectangles for intersection.
            for i in 0..guides_this_layer.len() {
                let (a_id, a_rect) = &guides_this_layer[i];
                for j in i + 1..guides_this_layer.len() {
                    let (b_id, b_rect) = &guides_this_layer[j];

                    // Check if rectangle touch each other. 'Kissing corner' does not count.
                    let touches = {
                        let intersection_hplw = a_rect
                            .intersection_inclusive_bounds(b_rect)
                            .map(|r| r.width() + r.height())
                            .unwrap_or(Coord::zero());
                        intersection_hplw > Coord::zero()
                    };

                    if touches {
                        // Connection between the rectangles.
                        connectivity_graph.add_edge(*a_id, *b_id, ());
                    }
                }
            }
        }

        // Extract connectivity to upper layer.
        if let Some(upper_layer) = upper_layer {
            let guides_on_upper_layer = guides_with_index
                .clone()
                .filter(|(_, (l, _))| l == upper_layer)
                .map(|(i, (_, rect))| (i, rect));
            guides_upper_layer.extend(guides_on_upper_layer);

            // Check all pairs of rectangles for intersection.
            for i in 0..guides_this_layer.len() {
                let (a_id, a_rect) = &guides_this_layer[i];
                for j in 0..guides_upper_layer.len() {
                    let (b_id, b_rect) = &guides_upper_layer[j];

                    if a_rect.intersection(b_rect).is_some() {
                        // Connection between the rectangles.
                        connectivity_graph.add_edge(*a_id, *b_id, ());
                    }
                }
            }
        }
    }

    // Find connected components.
    petgraph::algo::tarjan_scc(&connectivity_graph)
}

#[test]
fn test_connected_components() {
    #[derive(Debug, PartialEq)]
    struct LayerId(u8);
    let layer_stack = [LayerId(0), LayerId(1)];

    let guides = [
        // Three connected guides.
        (LayerId(0), db::Rect::new((0, 0), (10, 10))),
        (LayerId(0), db::Rect::new((10, 0), (20, 10))),
        (LayerId(1), db::Rect::new((0, 0), (10, 10))),
        // An unconnected guide.
        (LayerId(1), db::Rect::new((100, 100), (110, 110))),
        // An unconnected guide.
        (LayerId(0), db::Rect::new((110, 100), (120, 110))),
        // Unconnected but 'kissing edge'.
        (LayerId(1), db::Rect::new((110, 110), (120, 120))),
    ];

    let connected = connected_components(&layer_stack, &guides);

    assert_eq!(connected.len(), 4);
}

/// Find the location where a routing guide crosses a boundary of a clip.
///
/// * `guide`: Routing guide as a rectangle
/// * `clip`: Clipping area
/// * `direction`: Specifies which boundary of the clip should be checked for intersection with the routing guide.
pub fn find_clip_crossing<Coord>(
    guide: &db::Rect<Coord>,
    clip: &db::Rect<Coord>,
    direction: db::Direction2D,
) -> Option<db::REdge<Coord>>
where
    Coord: Num + PrimInt + Signed + std::fmt::Debug,
{
    // Create the direct neighbour tile (clip) in the given direction.
    let neighbour_clip = {
        let shift = match direction.orientation() {
            Orientation2D::Horizontal => clip.width(),
            Orientation2D::Vertical => clip.height(),
        };
        let sign: Coord = direction.sign();
        let mut v = db::Point::zero();
        v.set(direction.orientation(), shift * sign);
        clip.translate(v.v())
    };

    let i1 = clip.intersection(guide)?;
    let i2 = neighbour_clip.intersection(guide)?;

    // Intersection of i1 and i2 will either be empty or a line segment.
    let e = i1.intersection_inclusive_bounds(&i2)?;

    Some(db::REdge::new(e.lower_left(), e.upper_right()))
}

#[test]
fn test_find_clip_crossing() {
    let clip = db::Rect::new((0, 0), (10, 10));

    {
        let guide_from_left = db::Rect::new((-5, 1), (5, 2));

        assert_eq!(
            find_clip_crossing(&guide_from_left, &clip, db::Direction2D::Left),
            Some(db::REdge::new((0, 1), (0, 2)))
        );

        assert_eq!(
            find_clip_crossing(&guide_from_left, &clip, db::Direction2D::Right),
            None
        );
        assert_eq!(
            find_clip_crossing(&guide_from_left, &clip, db::Direction2D::Up),
            None
        );
        assert_eq!(
            find_clip_crossing(&guide_from_left, &clip, db::Direction2D::Down),
            None
        );
    }

    {
        let guide_from_right = db::Rect::new((5, 1), (15, 2));

        assert_eq!(
            find_clip_crossing(&guide_from_right, &clip, db::Direction2D::Right),
            Some(db::REdge::new((10, 1), (10, 2)))
        );

        assert_eq!(
            find_clip_crossing(&guide_from_right, &clip, db::Direction2D::Left),
            None
        );
        assert_eq!(
            find_clip_crossing(&guide_from_right, &clip, db::Direction2D::Up),
            None
        );
        assert_eq!(
            find_clip_crossing(&guide_from_right, &clip, db::Direction2D::Down),
            None
        );
    }
}
