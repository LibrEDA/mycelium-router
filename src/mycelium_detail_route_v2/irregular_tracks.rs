// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Representation of parallel track lines.
//!
//! In a typical use case most of the tracks are placed on a regular grid. Some 'irregular' tracks are placed off the grid.
//!
//! The orientation of the track lines is not specified in this data structures.

use crate::db;
use num_traits::PrimInt;

pub struct IrregularTracks<Coord> {
    /// Offset of the first regular track.
    regular_tracks_start: Coord,
    /// Offset of the last+1 regular track.
    regular_tracks_end: Coord,
    /// Spacing between regular tracks.
    regular_track_pitch: Coord,

    /// Sorted offsets of all tracks, regular and irregular ones.
    track_offsets: Vec<Coord>,

    /// Index of i-th regular track in `track_offsets`.
    /// This is used to quickly find the index of a track in order to find its neighbours.
    regular_track_indices: Vec<usize>,
}

/// Index for a specific track in an irregular track pattern.
/// Used for fast lookup of the track and its neighbours.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, PartialOrd, Ord)]
pub struct TrackIndex(usize);

impl<Coord> IrregularTracks<Coord>
where
    Coord: PrimInt + Ord + PartialEq + Copy,
{
    /// Create new irregular tracks.
    pub fn new(
        regular_tracks_start: Coord,
        regular_tracks_end: Coord,
        regular_track_pitch: Coord,
        mut irregular_tracks: Vec<Coord>,
    ) -> Self {
        assert!(
            regular_track_pitch > Coord::zero(),
            "track pitch must be positive"
        );
        assert!(
            regular_tracks_start <= regular_tracks_end,
            "track pitch must be positive"
        );

        // Generate offsets of regular tracks.
        let regular_track_offsets = (0..)
            .scan(regular_tracks_start, |offset, _| {
                let current_offset = *offset;
                *offset = *offset + regular_track_pitch;
                Some(current_offset)
            })
            .take_while(|off| *off < regular_tracks_end);

        let num_regular_tracks = {
            let num_irregular_tracks = irregular_tracks.len();
            irregular_tracks.extend(regular_track_offsets);
            irregular_tracks.len() - num_irregular_tracks
        };

        // Sort and deduplicate the tracks tracks.
        irregular_tracks.sort_unstable();
        irregular_tracks.dedup();

        let is_regular_track = |off: Coord| -> bool {
            (off - regular_tracks_start) % regular_track_pitch == Coord::zero()
                && off >= regular_tracks_start
                && off < regular_tracks_end
        };

        // Store indices of regular tracks.
        let regular_track_indices: Vec<usize> = irregular_tracks
            .iter()
            .enumerate()
            .filter(|(_idx, offset)| is_regular_track(**offset))
            .map(|(idx, _)| idx)
            .collect();

        assert_eq!(regular_track_indices.len(), num_regular_tracks);

        Self {
            regular_tracks_start,
            regular_tracks_end,
            regular_track_pitch,

            track_offsets: irregular_tracks,
            regular_track_indices,
        }
    }

    // /// Get the index of the closest regular track with an offset <= `offset`.
    // fn grid_floor_index(&self, offset: Coord) -> usize {
    //     let distance_to_start = offset - self.regular_tracks_start;
    //     if distance_to_start < Coord::zero() {
    //         0
    //     } else {
    //         let regular_track_idx = distance_to_start
    //     }
    // }

    /// Find the index `i` such that `self.track_offsets[i] == *offset`.
    pub fn find_track_index(&self, offset: &Coord) -> Option<TrackIndex> {
        // TODO: Do something faster than binary search (use index table build from regular tracks).
        match self.track_offsets.binary_search(offset) {
            Ok(idx) => Some(TrackIndex(idx)),
            Err(_) => None,
        }
    }

    /// Get the track offset of the track at `index`.
    pub fn track_offset(&self, index: TrackIndex) -> Coord {
        self.track_offsets[index.0]
    }

    /// Check if a track at a given `offset` exists.
    pub fn contains_track(&self, offset: &Coord) -> bool {
        self.find_track_index(offset).is_some()
    }

    /// Get the next neighbour of the track.
    ///
    /// Returns `None` if there's no next track.
    pub fn next(&self, index: TrackIndex) -> Option<TrackIndex> {
        let next_idx = TrackIndex(index.0 + 1);

        (next_idx.0 < self.track_offsets.len()).then_some(next_idx)
    }

    /// Get the next neighbour of the track based on its offset.
    ///
    /// Returns `None` if there's no track at `offset` or there's no next track.
    pub fn next_by_offset(&self, offset: &Coord) -> Option<Coord> {
        self.find_track_index(offset)
            .and_then(|idx| self.next(idx))
            .map(|idx| self.track_offset(idx))
    }

    /// Get the previous neighbour track of the track, if any.
    ///
    /// Returns `None` if there's no previous track.
    pub fn previous(&self, index: TrackIndex) -> Option<TrackIndex> {
        (index.0 > 0).then(|| TrackIndex(index.0 - 1))
    }

    /// Get the previous neighbour track of the track at `offset`, if any.
    ///
    /// Returns `None` if there's no track at `offset` or there's no previous track.
    pub fn previous_by_offset(&self, offset: &Coord) -> Option<Coord> {
        self.find_track_index(offset)
            .and_then(|idx| self.previous(idx))
            .map(|idx| self.track_offset(idx))
    }

    /// Get the neighbour of the given track in the given direction.
    pub fn neighbour(&self, direction: db::Direction1D, index: TrackIndex) -> Option<TrackIndex> {
        match direction {
            db::Direction1D::Low => self.previous(index),
            db::Direction1D::High => self.next(index),
        }
    }

    /// Check if the track at the given offset is a regular track.
    pub fn is_regular_track(&self, offset: Coord) -> bool {
        (offset - self.regular_tracks_start) % self.regular_track_pitch == Coord::zero()
            && offset >= self.regular_tracks_start
            && offset < self.regular_tracks_end
    }

    /// Get the number of tracks.
    pub fn len(&self) -> usize {
        self.track_offsets.len()
    }

    /// Test if there are no tracks.
    pub fn is_empty(&self) -> bool {
        self.track_offsets.is_empty()
    }

    /// Get the offset of the first track, if any.
    pub fn first(&self) -> Option<Coord> {
        self.track_offsets.get(0).copied()
    }

    /// Get the offset of the last track, if any.
    pub fn last(&self) -> Option<Coord> {
        self.track_offsets.get(self.len() - 1).copied()
    }

    /// Iterate over track offsets which are in the interval `[start, end]`.
    /// `end` is included.
    pub fn get_tracks_in_interval(
        &self,
        (start, end): (Coord, Coord),
    ) -> impl Iterator<Item = Coord> + '_ {
        assert!(start <= end);

        // Find index of first track offset which is equal or larger than `start`.
        let idx = {
            // TODO: Can be faster than a binary search by using `regular_track_indices`.
            let maybe_idx = self.track_offsets.binary_search(&start);

            match maybe_idx {
                Ok(idx) => idx,
                Err(idx) => idx,
            }
        };

        self.track_offsets[idx..]
            .iter()
            .copied()
            .take_while(move |off| *off <= end)
    }
}

#[test]
fn test_irregular_tracks() {
    let tracks = IrregularTracks::new(0, 100, 10, vec![7, 13, 13, 15, 20]);

    assert_eq!(tracks.len(), 10 + 3); // `13` and `20` are counted only once.
    assert_eq!(tracks.first(), Some(0));
    assert_eq!(tracks.last(), Some(90));

    assert_eq!(tracks.next_by_offset(&10), Some(13));
    assert_eq!(tracks.next_by_offset(&13), Some(15));
    assert_eq!(tracks.next_by_offset(&15), Some(20));

    assert_eq!(tracks.next_by_offset(&1), None);
    assert_eq!(tracks.previous_by_offset(&10), Some(7));
    assert_eq!(tracks.previous_by_offset(&7), Some(0));
}
