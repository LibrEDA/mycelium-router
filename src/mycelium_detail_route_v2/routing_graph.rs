// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Routing graph used for detail routing.
//! The routing graph is defined by routing tracks of the layer stack.

use std::hash::Hash;

use super::LayerId;
use crate::graph::{CreateNodeAttributes, NodeAttributes, TraverseGraph};
use crate::mycelium_detail_route_v2::node_attribute::GridDirection;
use iron_shapes::isotropy::{Direction1D, Direction2D, Orientation2D};
use libreda_pnr::db;
use num_traits::{PrimInt, Signed};

/// 2.5D point used as node in the routing graph.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Ord, PartialOrd)]
pub(super) struct Node {
    index_x: u32,
    index_y: u32,
    /// Layer of the node.
    layer: LayerId,
}

impl Node {
    /// Get the layer of the node.
    pub fn layer(&self) -> LayerId {
        self.layer
    }

    /// Get index of the node in x direction.
    pub fn index_x(&self) -> usize {
        self.index_x as usize
    }

    /// Get index of the node in y direction.
    pub fn index_y(&self) -> usize {
        self.index_y as usize
    }
}

/// Multi-layer grid graph.
#[derive(Clone, Debug)]
pub(super) struct RoutingGraph<Crd> {
    preferred_routing_direction_on_first_layer: db::Orientation2D,
    num_layers: usize,
    xs: Vec<Crd>,
    ys: Vec<Crd>,
    /// Tell if the horizontal grid line at the given index is a main track.
    track_type_x: Vec<Vec<TrackType>>,
    /// Tell if the vertical grid line at the given index is a main track.
    track_type_y: Vec<Vec<TrackType>>,
}

/// Mark the type of a track represented by lines of the routing grid.
/// Used for favouring on-track and half-track routes.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub(super) enum TrackType {
    /// Not on a main track and also not in the middle of two main tracks.
    OffTrack,
    /// In the middle of two main tracks.
    HalfTrack,
    /// A main track.
    OnTrack,
}

impl<Crd> Default for RoutingGraph<Crd> {
    /// Create an empty graph.
    fn default() -> Self {
        Self {
            preferred_routing_direction_on_first_layer: db::Orientation2D::Horizontal,
            num_layers: 0,
            xs: vec![],
            ys: vec![],
            track_type_x: vec![],
            track_type_y: vec![],
        }
    }
}

impl<Crd> RoutingGraph<Crd>
where
    Crd: Ord + PartialEq,
{
    /// Create new routing graph based on track patterns.
    pub fn new(
        preferred_routing_direction_on_first_layer: db::Orientation2D,
        mut xs: Vec<Crd>,
        mut ys: Vec<Crd>,
        num_layers: usize,
    ) -> Self {
        xs.sort_unstable();
        xs.dedup();
        ys.sort_unstable();
        ys.dedup();

        // By default mark tracks as 'off-track'.
        let track_type_x = vec![vec![TrackType::OffTrack; xs.len()]; num_layers];
        let track_type_y = vec![vec![TrackType::OffTrack; ys.len()]; num_layers];

        Self {
            preferred_routing_direction_on_first_layer,
            num_layers,
            xs,
            ys,
            track_type_x,
            track_type_y,
        }
    }

    /// Mark the routing grid lines with given coordinates as 'on-track' lines.
    pub fn set_on_track_coordinates(
        &mut self,
        layer: LayerId,
        on_track_xs: &[Crd],
        on_track_ys: &[Crd],
    ) {
        self.mark_track_types(layer, on_track_xs, on_track_ys, TrackType::OnTrack);
    }

    /// Mark the routing grid lines with given coordinates as 'half-track' lines.
    pub fn set_half_track_coordinates(
        &mut self,
        layer: LayerId,
        on_track_xs: &[Crd],
        on_track_ys: &[Crd],
    ) {
        self.mark_track_types(layer, on_track_xs, on_track_ys, TrackType::HalfTrack);
    }

    fn mark_track_types(
        &mut self,
        layer: LayerId,
        on_track_xs: &[Crd],
        on_track_ys: &[Crd],
        track_type: TrackType,
    ) {
        for x in on_track_xs {
            if let Some(idx) = self.xs.binary_search(x).ok() {
                self.track_type_x[layer.to_usize()][idx] = track_type;
            }
        }

        for y in on_track_ys {
            if let Some(idx) = self.ys.binary_search(y).ok() {
                self.track_type_y[layer.to_usize()][idx] = track_type;
            }
        }
    }
}

impl<Crd> RoutingGraph<Crd>
where
    Crd: PrimInt + Signed,
{
    /// Get a graph node from a point in the plane and a layer.
    /// Returns `None` if the routing graph has no node at this location.
    pub fn node_from_coordinates(&self, p: db::Point<Crd>, layer: LayerId) -> Option<Node> {
        assert!(layer.is_routing_layer());

        let index_x = self.find_index(p.x, db::Orientation2D::Horizontal)? as u32;
        let index_y = self.find_index(p.y, db::Orientation2D::Vertical)? as u32;
        let n = Node {
            layer,
            index_x,
            index_y,
        };

        self.contains_node(&n).then_some(n)
    }

    pub fn find_index(&self, coord: Crd, orient: db::Orientation2D) -> Option<usize> {
        let coords = match orient {
            Orientation2D::Horizontal => &self.xs,
            Orientation2D::Vertical => &self.ys,
        };

        // TODO: Create index based on regular tracks instead of using brute binary search.
        coords.binary_search(&coord).ok()
    }

    pub fn grid_floor(&self, coord: Crd, orient: db::Orientation2D) -> Option<usize> {
        let coords = match orient {
            Orientation2D::Horizontal => &self.xs,
            Orientation2D::Vertical => &self.ys,
        };

        // TODO: Create index based on regular tracks instead of using brute binary search.
        match coords.binary_search(&coord) {
            Ok(idx) => Some(idx),
            Err(idx) => Some(idx.min(coords.len() - 1)),
        }
    }
    pub fn grid_ceil(&self, coord: Crd, orient: db::Orientation2D) -> Option<usize> {
        let coords = match orient {
            Orientation2D::Horizontal => &self.xs,
            Orientation2D::Vertical => &self.ys,
        };

        // TODO: Create index based on regular tracks instead of using brute binary search.
        match coords.binary_search(&coord) {
            Ok(idx) => Some(idx),
            Err(idx) => Some(idx.min(coords.len() - 1)),
        }
    }

    /// Convert the graph node into a `(point, layer)` coordinate.
    pub fn node_to_coordinates(&self, n: &Node) -> (db::Point<Crd>, LayerId) {
        let coord = db::Point::new(self.xs[n.index_x()], self.ys[n.index_y()]);

        (coord, n.layer)
    }

    /// Get the layer of a node in the routing graph.
    pub fn node_layer(&self, n: &Node) -> LayerId {
        self.node_to_coordinates(n).1
    }

    /// Get the xy coordinates of a node in the routing graph.
    pub fn node_coord(&self, n: &Node) -> db::Point<Crd> {
        self.node_to_coordinates(n).0
    }

    /// Get the track types of a node.
    /// Returns a typle `(track type of x-coordinate, track type of y-coordinate)`.
    pub fn track_types(&self, n: &Node) -> (TrackType, TrackType) {
        (
            self.track_type_x[n.layer.to_usize()][n.index_x()],
            self.track_type_y[n.layer.to_usize()][n.index_y()],
        )
    }

    fn num_layers(&self) -> usize {
        self.num_layers
    }

    /// Get the bounding box of all tracks.
    fn boundary(&self) -> Option<db::Rect<Crd>> {
        let min_x = *self.xs.first()?;
        let min_y = *self.ys.first()?;
        let max_x = *self.xs.last()?;
        let max_y = *self.ys.last()?;

        Some(db::Rect::new((min_x, min_y), (max_x, max_y)))
    }

    fn has_layer(&self, layer: &LayerId) -> bool {
        (layer.0 as usize) < self.num_layers()
    }

    /// Get lower metal layer. Ignores via layers.
    pub fn lower_layer(&self, layer: &LayerId) -> Option<LayerId> {
        debug_assert!(layer.is_routing_layer());
        (self.has_layer(layer) && layer.0 >= 2).then(|| LayerId(layer.0 - 2))
    }

    /// Get upper metal layer. Ignores via layers.
    pub fn upper_layer(&self, layer: &LayerId) -> Option<LayerId> {
        debug_assert!(layer.is_routing_layer());
        let upper = LayerId(layer.0 + 2);
        self.has_layer(&upper).then_some(upper)
    }

    /// Get the via just above the node `n`.
    pub fn upper_via(&self, n: &Node) -> Option<Node> {
        let metal_layer = n.layer;
        assert!(metal_layer.is_routing_layer());
        let upper = LayerId(metal_layer.0 + 1);
        self.has_layer(&upper)
            .then_some(Node { layer: upper, ..*n })
    }

    // Get neighbour routing layer, excludes via layers.
    fn get_neighbour_layer(&self, direction: db::Direction1D, layer: &LayerId) -> Option<LayerId> {
        debug_assert!(layer.is_routing_layer());
        match direction {
            Direction1D::Low => self.lower_layer(layer),
            Direction1D::High => self.upper_layer(layer),
        }
    }

    fn track_orientation_on_layer(&self, layer: &LayerId) -> db::Orientation2D {
        if layer.0 % 2 == 0 {
            self.preferred_routing_direction_on_first_layer
        } else {
            self.preferred_routing_direction_on_first_layer.other()
        }
    }

    /// Check if the routing graph contains the given node.
    pub fn contains_node(&self, n: &Node) -> bool {
        let contains_layer = (n.layer.0 as usize) < self.num_layers() && n.layer.is_routing_layer();

        let contains_xy = n.index_x() < self.xs.len() && n.index_y() < self.ys.len();

        contains_layer && contains_xy
    }

    /// Find the closest neighbour node of `n` in the given three-dimensional direction.
    pub fn get_neighbour(&self, direction: GridDirection, n: &Node) -> Option<Node> {
        use GridDirection::*;
        match direction {
            NoDirection => None,
            XUp => self.get_neighbour_xy(db::Direction2D::Right, n),
            XDown => self.get_neighbour_xy(db::Direction2D::Left, n),
            YUp => self.get_neighbour_xy(db::Direction2D::Up, n),
            YDown => self.get_neighbour_xy(db::Direction2D::Down, n),
            ZUp => self.get_neighbour_z(db::Direction1D::High, n),
            ZDown => self.get_neighbour_z(db::Direction1D::Low, n),
        }
    }

    /// Get neighbour of a node in z direction.
    pub fn get_neighbour_z(&self, direction: db::Direction1D, n: &Node) -> Option<Node> {
        let other_layer = self.get_neighbour_layer(direction, &n.layer)?;
        let neighbour = Node {
            layer: other_layer,
            ..*n
        };
        self.contains_node(&neighbour).then_some(neighbour)
    }

    /// Get neighbour of a node in x or y direction.
    pub fn get_neighbour_xy(&self, direction: db::Direction2D, n: &Node) -> Option<Node> {
        debug_assert!(self.contains_node(n), "node is not in the graph");

        let (mut x, mut y) = (n.index_x, n.index_y);

        match direction {
            Direction2D::Right => x += 1,
            Direction2D::Up => y += 1,
            Direction2D::Left => {
                let (x_new, overflow) = x.overflowing_sub(1);
                x = (!overflow).then_some(x_new)?;
            }
            Direction2D::Down => {
                let (y_new, overflow) = y.overflowing_sub(1);
                y = (!overflow).then_some(y_new)?;
            }
        };

        let n = Node {
            layer: n.layer,
            index_x: x,
            index_y: y,
        };

        self.contains_node(&n).then_some(n)
    }

    /// Iterate over all nodes in the graph.
    pub fn each_node(&self) -> impl Iterator<Item = Node> + '_ {
        self.boundary().into_iter().flat_map(move |boundary| {
            (0..self.num_layers())
                .step_by(2)
                .into_iter()
                .map(|l| LayerId(l as u8))
                .flat_map(move |layer| self.nodes_in_rect(layer, &boundary))
        })
    }

    /// Iterate over all nodes contained in the rectangle `rect` on a specific layer.
    pub fn nodes_in_rect(
        &self,
        layer: LayerId,
        rect: &db::Rect<Crd>,
    ) -> impl Iterator<Item = Node> + '_ {
        let (ll, ur) = (rect.lower_left(), rect.upper_right());
        let (dx, dy) = ((ll.x, ur.x), (ll.y, ur.y));

        let x_start = self
            .grid_floor(ll.x, db::Orientation2D::Horizontal)
            .unwrap_or(0);
        let x_end = self
            .grid_ceil(ur.x, db::Orientation2D::Horizontal)
            .unwrap_or(self.xs.len());

        let y_start = self
            .grid_floor(ll.y, db::Orientation2D::Vertical)
            .unwrap_or(0);
        let y_end = self
            .grid_ceil(ur.y, db::Orientation2D::Vertical)
            .unwrap_or(self.ys.len());

        let x_start = if self.xs[x_start] < ll.x {
            x_start + 1
        } else {
            x_start
        };

        let y_start = if self.ys[y_start] < ll.y {
            y_start + 1
        } else {
            y_start
        };

        let x_end = if self.xs[x_end] > ur.x {
            x_end.saturating_sub(1)
        } else {
            x_end
        };

        let y_end = if self.ys[y_end] > ur.y {
            y_end.saturating_sub(1)
        } else {
            y_end
        };

        let rect = *rect;
        (y_start..=y_end)
            .into_iter()
            .flat_map(move |y| {
                (x_start..=x_end).into_iter().map(move |x| Node {
                    layer,
                    index_x: x as u32,
                    index_y: y as u32,
                })
            })
            .filter(move |n| rect.contains_point(self.node_coord(n))) // TODO: Get correct points by construction instead of by filtering.
            .inspect(move |n| debug_assert!(rect.contains_point(self.node_coord(n))))
    }
}

impl<Crd> TraverseGraph<Node> for RoutingGraph<Crd>
where
    Crd: PrimInt + Signed,
{
    fn neighbours(&self, buffer: &mut Vec<Node>, n: &Node) {
        debug_assert!(self.contains_node(n));
        debug_assert!(buffer.is_empty(), "Buffer must be cleared.");

        // Get neighbours on same layer.
        use db::Direction2D::*;
        let directions = [Right, Up, Left, Down];
        buffer.extend(
            directions
                .iter()
                .flat_map(|dir| self.get_neighbour_xy(*dir, n)),
        );

        buffer.extend(self.get_neighbour_z(db::Direction1D::Low, n));
        buffer.extend(self.get_neighbour_z(db::Direction1D::High, n));
    }

    fn contains_node(&self, n: &Node) -> bool {
        self.contains_node(n)
    }
}

#[test]
fn test_neighbours() {
    let xs = (0..=100).step_by(10).collect();
    let ys = (0..=100).step_by(10).collect();
    let num_layers = 3 + 2; // 3 metal layers + 2 via layers
    let graph = RoutingGraph {
        preferred_routing_direction_on_first_layer: db::Orientation2D::Horizontal,
        xs,
        ys,
        num_layers,
        track_type_x: vec![],
        track_type_y: vec![],
    };

    {
        let n = graph
            .node_from_coordinates((0, 0).into(), LayerId(0))
            .unwrap();

        assert_eq!(
            graph.get_neighbour_xy(db::Direction2D::Right, &n),
            graph.node_from_coordinates((10, 0).into(), LayerId(0))
        );

        assert_eq!(graph.get_neighbour_xy(db::Direction2D::Left, &n), None);

        assert_eq!(
            graph.get_neighbour_xy(db::Direction2D::Up, &n),
            graph.node_from_coordinates((0, 10).into(), LayerId(0))
        );

        assert_eq!(graph.get_neighbour_xy(db::Direction2D::Down, &n), None);

        assert_eq!(
            graph.get_neighbour_z(db::Direction1D::High, &n),
            graph.node_from_coordinates((0, 0).into(), LayerId(2))
        );
    }

    {
        let n = graph
            .node_from_coordinates((10, 10).into(), LayerId(0))
            .unwrap();

        assert_eq!(
            graph.get_neighbour_xy(db::Direction2D::Right, &n),
            graph.node_from_coordinates((20, 10).into(), LayerId(0))
        );

        assert_eq!(
            graph.get_neighbour_xy(db::Direction2D::Left, &n),
            graph.node_from_coordinates((0, 10).into(), LayerId(0))
        );

        assert_eq!(
            graph.get_neighbour_xy(db::Direction2D::Up, &n),
            graph.node_from_coordinates((10, 20).into(), LayerId(0))
        );

        assert_eq!(
            graph.get_neighbour_xy(db::Direction2D::Down, &n),
            graph.node_from_coordinates((10, 0).into(), LayerId(0))
        );
    }
}

#[test]
fn test_connectivity() {
    use std::collections::HashSet;

    let xs = (0..100).step_by(10).collect();
    let ys = (0..100).step_by(10).collect();
    let num_layers = 3 + 2; // 3 metal layers + 2 via layers
    let graph = RoutingGraph {
        preferred_routing_direction_on_first_layer: db::Orientation2D::Horizontal,
        xs,
        ys,
        num_layers,
        track_type_x: vec![],
        track_type_y: vec![],
    };

    assert_eq!(graph.each_node().count(), 10 * 10 * 3);

    let n = graph
        .node_from_coordinates((0, 0).into(), LayerId(0))
        .unwrap();

    let mut stack = vec![n];
    let mut visited = HashSet::new();
    let mut buffer = vec![];
    while let Some(n) = stack.pop() {
        if visited.contains(&n) {
            continue;
        }

        visited.insert(n);

        buffer.clear();
        graph.neighbours(&mut buffer, &n);
        buffer.retain(|n| !visited.contains(n));
        stack.extend(buffer.iter().copied());
        buffer.clear();
    }

    assert_eq!(
        graph.each_node().count(),
        visited.len(),
        "each node should be visited"
    );
}

#[test]
fn test_nodes_in_rectangle() {
    let xs = (0..=100).step_by(10).collect();
    let ys = (0..=100).step_by(5).collect();

    let graph = RoutingGraph {
        preferred_routing_direction_on_first_layer: db::Orientation2D::Horizontal,
        xs,
        ys,
        num_layers: 3,
        track_type_x: vec![],
        track_type_y: vec![],
    };

    let rect = db::Rect::new((20, 20), (40, 40));

    let points: Vec<_> = graph
        .nodes_in_rect(LayerId(1), &rect)
        .map(|n| {
            let c = graph.node_coord(&n);
            (c.x, c.y)
        })
        .collect();

    assert_eq!(
        points,
        vec![
            (20, 20),
            (30, 20),
            (40, 20),
            (20, 25),
            (30, 25),
            (40, 25),
            (20, 30),
            (30, 30),
            (40, 30),
            (20, 35),
            (30, 35),
            (40, 35),
            (20, 40),
            (30, 40),
            (40, 40),
        ]
    )
}

#[test]
fn test_nodes_in_rectangle_single() {
    let xs = (0..=100).step_by(10).collect();
    let ys = (0..=100).step_by(5).collect();

    let graph = RoutingGraph {
        preferred_routing_direction_on_first_layer: db::Orientation2D::Horizontal,
        xs,
        ys,
        num_layers: 3,
        track_type_x: vec![],
        track_type_y: vec![],
    };

    let rect = db::Rect::new((19, 19), (21, 21));

    let points: Vec<_> = graph
        .nodes_in_rect(LayerId(1), &rect)
        .map(|n| {
            let c = graph.node_coord(&n);
            (c.x, c.y)
        })
        .collect();

    assert_eq!(points, vec![(20, 20)])
}

impl<Crd, Attr> CreateNodeAttributes<Node, Attr> for RoutingGraph<Crd>
where
    Crd: Copy + Hash + Eq + PrimInt + Signed,
    Attr: Clone,
{
    type NodeAttrs = RoutingGraphNodeAttributes<Attr>;

    fn create_attributes(&self, default_value: Attr) -> Self::NodeAttrs {
        let preferred_directions: Vec<_> = (0..self.num_layers)
            .map(|i| {
                let l = LayerId(i as u8);
                self.track_orientation_on_layer(&l)
            })
            .collect();

        Self::NodeAttrs::new(
            self.xs.len(),
            self.ys.len(),
            self.num_layers,
            &preferred_directions,
            default_value,
        )
    }
}

#[derive(Debug, Default, Clone)]
pub struct RoutingGraphNodeAttributes<Attr> {
    /// One 2D-array of node data per layer.
    data: Vec<Array2D<Attr>>,
}

impl<Attr: Clone> RoutingGraphNodeAttributes<Attr> {
    pub fn new(
        num_x: usize,
        num_y: usize,
        num_layers: usize,
        preferred_directions: &[db::Orientation2D],
        default_value: Attr,
    ) -> Self {
        assert_eq!(num_layers, preferred_directions.len());

        let data = (0..num_layers)
            .map(|i| {
                let dir = preferred_directions[i];
                Array2D::new(num_x, num_y, dir, default_value.clone())
            })
            .collect();

        Self { data }
    }

    /// Iterate over all node values.
    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut Attr> {
        self.data.iter_mut().flat_map(|arr| arr.data.iter_mut())
    }
}

impl<Attr> NodeAttributes<Node, Attr> for RoutingGraphNodeAttributes<Attr>
where
    Attr: Clone,
{
    fn get(&self, node: &Node) -> &Attr {
        self.data[node.layer.0 as usize].get(node.index_x(), node.index_y())
    }

    fn get_mut(&mut self, node: &Node) -> &mut Attr {
        self.data[node.layer.0 as usize].get_mut(node.index_x(), node.index_y())
    }

    fn set(&mut self, node: &Node, value: Attr) {
        *self.get_mut(node) = value;
    }
}

#[derive(Debug, Clone)]
struct Array2D<T> {
    num_x: usize,
    num_y: usize,
    data: Vec<T>,
    cache_optimized_direction: db::Orientation2D,
}

impl<T> Default for Array2D<T> {
    fn default() -> Self {
        Self {
            num_x: 0,
            num_y: 0,
            data: vec![],
            cache_optimized_direction: db::Orientation2D::Horizontal,
        }
    }
}

impl<T: Clone> Array2D<T> {
    fn new(
        num_x: usize,
        num_y: usize,
        cache_optimized_direction: db::Orientation2D,
        default_value: T,
    ) -> Self {
        let size = num_x * num_y;
        let data = vec![default_value; size];

        Self {
            num_x,
            num_y,
            data,
            cache_optimized_direction,
        }
    }

    fn compute_index(&self, i: usize, j: usize) -> usize {
        assert!(i < self.num_x, "index out of bounds");
        assert!(j < self.num_y, "index out of bounds");

        let (i, j, dimension) = match self.cache_optimized_direction {
            Orientation2D::Horizontal => (i, j, self.num_x),
            Orientation2D::Vertical => (j, i, self.num_y),
        };

        j * dimension + i
    }

    fn get(&self, i: usize, j: usize) -> &T {
        let idx = self.compute_index(i, j);
        &self.data[idx]
    }

    fn get_mut(&mut self, i: usize, j: usize) -> &mut T {
        let idx = self.compute_index(i, j);
        &mut self.data[idx]
    }

    fn set(&mut self, i: usize, j: usize, value: T) -> T {
        std::mem::replace(self.get_mut(i, j), value)
    }
}

#[test]
fn test_attributes() {
    let xs = (0..=100).step_by(20).collect();
    let ys = (0..=100).step_by(10).collect();

    let graph = RoutingGraph {
        preferred_routing_direction_on_first_layer: db::Orientation2D::Horizontal,
        xs,
        ys,
        num_layers: 2,
        track_type_x: vec![],
        track_type_y: vec![],
    };

    let n = graph
        .node_from_coordinates(db::Point::new(0, 0), LayerId(0))
        .unwrap();

    let mut int_attr = graph.create_attributes(7);

    // Test default value:
    assert_eq!(int_attr.get(&n), &7);

    // Test `set()`:
    int_attr.set(&n, 42);
    assert_eq!(int_attr.get(&n), &42);

    // Test `get_mut()`:
    let attr_mut = int_attr.get_mut(&n);
    *attr_mut += 1;

    assert_eq!(int_attr.get(&n), &43);
}
