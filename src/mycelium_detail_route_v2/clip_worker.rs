// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Worker which does detail routing in a rectangular region ('clip') of the chip.
use fnv::FnvHashMap;
use itertools::Itertools;
use num_traits::{PrimInt, Signed};
use rstar::{RTreeObject, AABB};
use smallvec::SmallVec;

use std::collections::HashMap;
use std::ops::Deref;

use iron_shapes::prelude::IntoEdges;
use libreda_pnr::db;

use super::geometric_objects::*;
use super::node_attribute::*;
use super::region_query::*;
use super::routing_graph::*;
use super::*;
use crate::graph::*;

/// Identifier for a subnet of a net.
/// Used for nets that get split into multiple unconnected parts by clipping the routing guides.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub(super) struct SubnetId<NetId> {
    pub net: NetId,
    subnet: u32,
}

/// Context data of the clip worker.
pub(super) struct ClipWorkerTask<'a, LN, Rules, RoutingProblem, Drc>
where
    LN: db::L2NEdit,
    LN::Coord: PrimInt + Signed + std::fmt::Debug,
    GeometricObject<LN::Coord, SubnetId<LN::NetId>>: RTreeObject,
{
    /// Reference to the top-level router configuration.
    pub router: &'a MyceliumDR2<'a, LN, Rules, Drc>,

    pub routing_problem: &'a RoutingProblem,

    /// Track definitions.
    pub routing_tracks: HashMap<LN::LayerId, pa::Tracks<LN::Coord>>,

    /// Precomputed data which is shared among clip workers and is not modified by them.
    pub precomputed: &'a PrecomputedRoutingData<LN>,

    /// (x,y) index of this clip.
    pub clip_index: (usize, usize),

    /// Part of the chip which can be modified by this worker.
    pub route_box: db::Rect<LN::Coord>,
    /// Region which is used for DRC checks. Should be larger than the `route_box` in order to check
    /// design rules towards neighbour clips.
    pub drc_box: db::Rect<LN::Coord>,
    /// Box which extends over the `drc_box`.
    pub ext_box: db::Rect<LN::Coord>,

    /// Routing guides which intersect this clip.
    pub local_routing_guides: FnvHashMap<LN::NetId, Vec<(LN::LayerId, db::Rect<LN::Coord>)>>,
    /// Routing guides which touch this clip but don't intersect. This may include routing guides of neighbour clips.
    pub neighbour_routing_guides: FnvHashMap<LN::NetId, Vec<(LN::LayerId, db::Rect<LN::Coord>)>>,
    /// Routing guides of subnets in this clip.
    pub local_subnet_routing_guides:
        FnvHashMap<SubnetId<LN::NetId>, Vec<(LN::LayerId, db::Rect<LN::Coord>)>>,

    /// Lookup-table to find subnets of a net.
    pub subnets: FnvHashMap<LN::NetId, Vec<SubnetId<LN::NetId>>>,

    /// Layout related data which is often modified.
    pub geometry_data: GeometryData<LN>,

    /// Default cost increment for a node blocked by an object.
    pub object_cost: i32,
    /// Const increment for nodes covered by a DRC marker.
    pub drc_marker_cost: i32,
    /// Data associated with nets (shapes, temporary route, etc).
    pub net_data: FnvHashMap<SubnetId<LN::NetId>, NetData<LN>>,

    /// Shapes which form obstacles for all nets.
    pub obstacles: Vec<(LN::LayerId, db::Rect<LN::Coord>)>,

    /// Shapes which are an obstacle to all other nets but not to this net.
    pub non_obstacles: FnvHashMap<LN::NetId, Vec<(LayerId, db::Rect<LN::Coord>)>>,
}

impl<'a, LN, Rules, RP, Drc> ClipWorkerTask<'a, LN, Rules, RP, Drc>
where
    LN: db::L2NEdit<Coord = db::SInt> + db::L2NBaseMT,
    LN::Coord: PrimInt + Signed + std::fmt::Debug,
    Rules: db::RoutingLayerStack<LayerId = LN::LayerId>
        + db::RoutingRules<LayerId = LN::LayerId, Distance = LN::Coord>,
    RP: DetailRoutingProblem<LN>,
    Drc: DrcEngine<LN>,
{
    /// Perform some checks on input data and design rules.
    /// Used to abort routing if some inputs are not good.
    pub fn sanity_checks(&self) -> Result<(), ()> {
        // TODO
        Ok(())
    }

    /// Shortcut to access the routing graph data structure.
    /// # Panics
    /// Panics if the routing graph is not built yet.
    fn routing_graph(&self) -> &RoutingGraph<LN::Coord> {
        self.geometry_data.routing_graph()
    }

    /// Shortcut to access the node attributes.
    fn node_costs_mut(&mut self) -> &mut RoutingGraphNodeAttributes<NodeAttribute> {
        self.geometry_data.node_costs_mut()
    }

    /// Main routing flow.
    pub fn do_route(
        mut self,
        routed_neighbour_clips: &HashMap<(usize, usize), &ClipRoutingResult<LN>>,
    ) -> Result<ClipRoutingResult<LN>, ()> {
        let routing_layers = self.router.design_rules.routing_layer_stack();
        let via_layers = self.router.design_rules.via_layer_stack();

        assert_eq!(
            routing_layers.len(),
            via_layers.len() + 1,
            "There must be exactly one more routing layer than via layers."
        );

        assert!(
            !self.routing_tracks.is_empty(),
            "No routing tracks defined, call `set_routing_tracks()` first."
        );

        self.init_region_search();

        self.init_local_routing_guides();

        self.init_subnets();

        // Create routing graph.
        self.geometry_data.routing_graph = Some(self.build_routing_graph(routed_neighbour_clips));

        // Create data-structure for holding node attributes.
        self.init_node_costs();

        // Initialize data associated with nets.
        self.init_net_data();

        self.init_net_terminals();
        self.init_boundary_net_terminals(routed_neighbour_clips);

        self.mark_access_points();

        // Write obstacle costs to the graph nodes and register
        // obstacles for region search.
        self.init_obstacles();

        self.init_neighbour_obstacles(routed_neighbour_clips);

        let (drc_hotspots, clip_perf) = self.do_maze_routing();

        let result = ClipRoutingResult {
            clip_box: self.route_box,
            routing_graph: self.geometry_data.routing_graph.unwrap(),
            net_data: self.net_data,
            subnets: self.subnets,
            drc_hotspots: drc_hotspots.into_iter().collect(),
            performance_stats: clip_perf,
            obstacles: self.obstacles,
        };

        Ok(result)
    }

    /// Build a 3D graph based on the regular track patterns and the irregular tracks defined by the
    /// net terminals.
    ///
    /// # Parameters
    /// * `routed_neighbour_clips`: Routing results of the neighbour clips.
    /// Used to find the center-lines of the incoming wire-segments.
    fn build_routing_graph(
        &self,
        routed_neighbour_clips: &HashMap<(usize, usize), &ClipRoutingResult<LN>>,
    ) -> RoutingGraph<LN::Coord> {
        log::trace!("build routing graph");

        let chip = self.routing_problem.fused_layout_netlist();

        let routing_layers = self.router.design_rules.layer_stack_ids();

        let lowest_layer_preferred_direction = self.routing_tracks[&routing_layers[0]].orientation;

        let via_locations = self
            .precomputed
            .pin_access_oracle
            .final_access_patterns
            .iter()
            .flat_map(|(cell_inst, cell_ap)| {
                let inst_location = chip.get_transform(&cell_inst);

                cell_ap.access_points.iter().map(move |(pin_id, ap)| {
                    let location =
                        ap.rel_location + inst_location.transform_point(db::Point::zero()).v();

                    // let via_def = &ap.possible_vias[0]; // Pick first via only. TODO: Use more than one possible via?
                    // let via_cell = chip.cell_by_name(via_def.via_name.as_str())
                    //     .expect("via cell not found");

                    (ap.layer.clone(), location)
                })
            })
            // Use only vias which are inside this clip.
            .filter(|(_, location)| self.route_box.contains_point(*location));

        // Find all x/y coordinates used by pins, route-segments and tracks.
        let (xs, ys) = {
            let mut xs = HashSet::new();
            let mut ys = HashSet::new();

            // Add potentially irregular grids.
            for (layer, point) in via_locations {
                xs.insert(point.x);
                ys.insert(point.y);
            }

            // Add regular grid lines.
            for (layer, track_definition) in &self.routing_tracks {
                let layer_id = self.precomputed.get_router_layer_id(layer);

                // Generate offsets according to the track pitch.
                let track_offsets = track_definition.all_offsets_in_region(&self.route_box);

                let dest = match track_definition.orientation {
                    db::Orientation2D::Horizontal => &mut ys,
                    db::Orientation2D::Vertical => &mut xs,
                };

                dest.extend(track_offsets);
            }

            // Add tracks along clip boundaries.
            {
                xs.insert(self.route_box.lower_left().x);
                xs.insert(self.route_box.upper_right().x);
                ys.insert(self.route_box.lower_left().y);
                ys.insert(self.route_box.upper_right().y);
            }

            // Add center-lines of incoming route segments.
            {
                let (x, y) = self.get_boundary_terminal_coordinates(routed_neighbour_clips);
                xs.extend(x);
                ys.extend(y);
            }

            let mut xs: Vec<_> = xs.into_iter().collect();
            let mut ys: Vec<_> = ys.into_iter().collect();

            // Use only tracks which are within the clip.
            {
                let bounds = &self.route_box;
                xs.retain(|&x| bounds.lower_left().x <= x && x <= bounds.upper_right().x);
                ys.retain(|&y| bounds.lower_left().y <= y && y <= bounds.upper_right().y);
            }

            xs.sort_unstable();
            ys.sort_unstable();

            (xs, ys)
        };

        let num_layers = routing_layers.len();

        log::trace!(
            "routing graph size: {}x{}x({} layers), ",
            xs.len(),
            ys.len(),
            num_layers
        );

        let mut routing_graph =
            RoutingGraph::new(lowest_layer_preferred_direction, xs, ys, num_layers);

        // Mark on-track grid lines in the routing graph.
        for (layer, track_definition) in &self.routing_tracks {
            let layer_id = self.precomputed.get_router_layer_id(layer);

            // Generate offsets according to the track pitch.
            let track_offsets: Vec<_> = track_definition
                .all_offsets_in_region(&self.route_box)
                .collect();

            let (xs, ys) = match track_definition.orientation {
                db::Orientation2D::Horizontal => (vec![], track_offsets),
                db::Orientation2D::Vertical => (track_offsets, vec![]),
            };

            routing_graph.set_on_track_coordinates(layer_id, &xs, &ys);

            // Compute half-track coordinates.
            let two = LN::Coord::one() + LN::Coord::one();
            let xs_half_track: Vec<_> = xs.windows(2).map(|w| (w[0] + w[1]) / two).collect();
            let ys_half_track: Vec<_> = ys.windows(2).map(|w| (w[0] + w[1]) / two).collect();
            routing_graph.set_half_track_coordinates(layer_id, &xs_half_track, &ys_half_track);
        }

        routing_graph
    }

    /// Set the per-layer definitions of routing tracks.
    pub fn set_routing_tracks(
        &mut self,
        routing_tracks: HashMap<LN::LayerId, pa::Tracks<LN::Coord>>,
    ) {
        self.routing_tracks = routing_tracks
    }

    /// Fill the region query structure with already present shapes from the layout.
    fn init_region_search(&mut self) {
        let routing_layers = self.router.design_rules.layer_stack();
        let num_routing_layers = routing_layers.len();

        // Initialize empty data structure.
        self.geometry_data.drc_region_search = RegionQuery::new(num_routing_layers);
    }

    /// Use shapes from the DB as obstacles.
    fn init_obstacles(&mut self) {
        let routing_layers = self.router.design_rules.routing_layer_stack();
        let chip = self.routing_problem.fused_layout_netlist();
        let top = self.routing_problem.top_cell();

        for layer in &routing_layers {
            let layer_id = self.precomputed.get_router_layer_id(layer);

            // Process obstacles within the extended clip region.
            self.precomputed
                .obstacles
                .locate_intersect_rect(layer_id, &self.ext_box)
                .for_each(|geometry| match geometry {
                    GeometricObject::Obstacle(obs) => {
                        self.register_obstacle(layer_id, obs.shape, obs.net_id.clone())
                    }
                    _ => {}
                })
        }
    }

    /// Save an obstacle shape in the RTree and if it is associated with a net, store it with the net.
    fn register_obstacle(
        &mut self,
        layer_id: LayerId,
        rect: db::Rect<LN::Coord>,
        net: Option<LN::NetId>,
    ) {
        let layer = self.precomputed.get_db_layer_id(&layer_id);

        // Bloat the rectangles to enforce minimum-distance rules.
        let (bloat_x, bloat_y) = self
            .router
            //            .bloat_extension_for_short_circuit(layer, Some(&rect));
            .bloat_extension(layer, Some(&rect));
        let bloated = rect.sized(bloat_x, bloat_y);

        // If the obstacle shapes are associated with a net, then store them with the net.
        // They will be used to unreserve pins before to routing.
        if let Some(net) = net.as_ref() {
            // Store `(layer, shape)` tuples.
            self.non_obstacles
                .entry(net.clone())
                .or_insert(vec![])
                .push((layer_id, bloated));
        } else {
            // TODO:
            // Store obstacle for debug output.
            self.obstacles.push((layer.clone(), bloated));
            self.geometry_data
                .reserve_obstacle(layer_id, &bloated, true);
        }

        // Store obstacle in local RTree.
        {
            let geometry_with_subnet =
                // Convert the net ID of obstacles into a subnet ID.
                GeometricObject::Obstacle(Obstacle {
                    net_id: net.map(|net| SubnetId { net, subnet: 0 }),
                    shape: rect,
                });
            self.geometry_data
                .drc_region_search
                .insert(layer_id, geometry_with_subnet);
        }
    }

    /// Load net shapes from routed neighbour clips and store them in the local region query struct.
    fn init_neighbour_obstacles(
        &mut self,
        routed_neighbour_clips: &HashMap<(usize, usize), &ClipRoutingResult<LN>>,
    ) {
        // Borrow outside of the closure to help the borrow checker.
        let precomputed = &self.precomputed;
        let drc_box = self.drc_box;

        // Get an iterator over all neighbour shapes which interact with the local DRC-box.
        let neighbour_shapes = routed_neighbour_clips
            .values()
            .flat_map(|n| n.net_data.iter())
            .flat_map(|(subnet, net_data)| {
                // Get all net shapes with their layer.
                let wire_shapes = net_data.wires.iter().map(|w| (w.layer, w.to_rectangle()));
                let via_shapes = net_data
                    .vias
                    .iter()
                    .flat_map(|v| Self::get_via_metal_shapes(precomputed, v));

                let all_interacting_shapes = wire_shapes
                    .chain(via_shapes)
                    // Use only rectangles which interact with the DRC box.
                    .filter(|(_layer, rect)| rect.intersection(&drc_box).is_some());

                // Associate shapes with their layer.
                all_interacting_shapes.map(|shape| (subnet.clone(), shape))
            });

        // Register the neighbour net shapes as obstacles.
        neighbour_shapes.for_each(|(subnet, (layer, rect))| {
            self.register_obstacle(layer, rect, Some(subnet.net.clone()))
        });
    }

    /// Find routing guides which touch this clip and copy them to the data structure of this clip.
    fn init_local_routing_guides(&mut self) {
        self.local_routing_guides.clear();

        // Select routing guides which intersect with this box.
        // Use the enlarged routing box to detect all guides which cross the boundary.
        let route_box_enlarged = self.route_box.sized_isotropic(LN::Coord::one());

        let p1 = route_box_enlarged.lower_left().into();
        let p2 = route_box_enlarged.upper_right().into();

        // Copy routing guides which intersect with the clip.
        self.precomputed
            .routing_guides_tree
            // Find guides which touch or intersect this clip.
            .locate_in_envelope_intersecting(&AABB::from_corners(p1, p2))
            .for_each(|guide_object| {
                // Skip guides which only touch the clip but are not inside the clip.
                let is_completely_inside_clip = guide_object
                    .bounding_box()
                    .intersection(&self.route_box)
                    .is_some();

                if is_completely_inside_clip {
                    // Store guides which are completely inside this clip.
                    self.local_routing_guides
                        .entry(guide_object.net_id.clone())
                        .or_insert(vec![])
                        .push((guide_object.layer.clone(), guide_object.shape));
                } else {
                    // Store guides which intersect or touch the clip.
                    self.neighbour_routing_guides
                        .entry(guide_object.net_id.clone())
                        .or_insert(vec![])
                        .push((guide_object.layer.clone(), guide_object.shape));
                }
            });

        log::debug!(
            "number of local routing guides in this clip: {}",
            self.local_routing_guides.len()
        );
    }

    /// Split some nets into subnets.
    /// A routing guide of a net might get disconnected when being clipped by the clip boundary as illustrated below.
    /// The net A gets split into two parts when it is clipped with the rectangle.
    /// Each of the parts will be assigned a separate subnet.
    ///
    /// ```txt
    ///            |
    ///        -----------------
    ///        |   |           |
    ///      +-|---+ A.0       |
    ///  A---| |               |
    ///      +-|---+ A.1       |
    ///        |   |           |
    ///        -----------------  <- clipping rectangle
    ///            |
    /// ```
    fn init_subnets(&mut self) {
        let layer_stack = self.router.design_rules.routing_layer_stack();

        log::debug!(
            "number of local routing guides: {}",
            self.local_routing_guides.len()
        );

        let subnet_guides = self
            .local_routing_guides
            .iter()
            .flat_map(|(net, net_guide)| {
                let connected_component_indices =
                    analyze_global_routes::connected_components(&layer_stack, net_guide);

                connected_component_indices.into_iter().enumerate().map(
                    move |(subnet_id, guide_indices)| {
                        // Resolve the indices of the guide shapes.
                        let guides: Vec<_> = guide_indices
                            .into_iter()
                            .map(|idx| net_guide[idx].clone())
                            .collect();

                        let subnet_id = SubnetId {
                            net: net.clone(),
                            subnet: subnet_id as u32,
                        };

                        (subnet_id, guides)
                    },
                )
            });

        self.local_subnet_routing_guides = subnet_guides.collect();

        log::debug!(
            "number of sub-nets in this clip: {}",
            self.local_subnet_routing_guides.len()
        );

        // Sanity check: Can't have less subnets than nets.
        assert!(self.local_subnet_routing_guides.len() >= self.local_routing_guides.len());

        // Create lookup table to find subnets of a net.
        {
            self.subnets.clear();
            for subnet in self.local_subnet_routing_guides.keys() {
                self.subnets
                    .entry(subnet.net.clone())
                    .or_insert(vec![])
                    .push(subnet.clone());
            }
        }
    }

    fn init_node_costs(&mut self) {
        let node_costs = self.routing_graph().create_attributes(Default::default());
        self.geometry_data.node_costs = Some(node_costs);
    }

    /// Initialize empty data-structures of nets.
    fn init_net_data(&mut self) {
        self.net_data = self
            .local_subnet_routing_guides
            .keys()
            .cloned()
            .map(|subnet| (subnet, NetData::new()))
            .collect();
    }

    /// For each net: store the graph nodes which make up the routing terminals.
    fn init_net_terminals(&mut self) {
        let chip = self.routing_problem.fused_layout_netlist();

        // Create lookup-table: pin instance -> access point (with relative location)
        let pin_inst_access_points: FnvHashMap<LN::PinInstId, &pa::PinAccessPoint<_>> = self
            .precomputed
            .pin_access_oracle
            .final_access_patterns
            .iter()
            .flat_map(|(cell_inst, cell_ap)| {
                let inst_location = chip.get_transform(&cell_inst);

                cell_ap.access_points.iter().map(move |(pin_id, ap)| {
                    let pin_inst_id = chip.pin_instance(cell_inst, pin_id);
                    (pin_inst_id, ap)
                })
            })
            .collect();

        // Convert pin access points into graph nodes.
        let routing_graph = self.routing_graph();
        let nets = self.local_routing_guides.keys().cloned();

        let routing_terminals: Vec<(LN::NetId, Vec<Node>)> = nets
            .map(|net| {
                let terminals: Vec<_> = chip.each_pin_instance_of_net(&net)
                    .filter_map(|pin_inst| {
                        let cell_inst = chip.parent_of_pin_instance(&pin_inst);
                        let inst_location = chip.get_transform(&cell_inst);

                        // Convert the access point into a node in the routing graph.
                        match pin_inst_access_points.get(&pin_inst) {
                            Some(ap) => {
                                // Shift the access point location to the absolute location.
                                let absolute_location = ap.rel_location
                                    + inst_location.transform_point(db::Point::zero()).v();

                                // Translate the layer ID.
                                let layer = self.precomputed.get_router_layer_id(&ap.layer);

                                if self.route_box.contains_point(absolute_location) {
                                    // TODO: A pin on the boundary of the rectangle might end up in two clips. Fix: include lower boundaries, exclude upper boundaries.
                                    Some(
                                        routing_graph.node_from_coordinates(absolute_location, layer)
                                            .expect("no routing node exists for this access point")
                                    )
                                } else {
                                    // Terminal is outside of the clip.
                                    None
                                }
                            }
                            None => {
                                let cell = chip.template_cell(&cell_inst);

                                log::warn!("no access point found for pin instance. cell = '{}', pin = '{}'",
                                    chip.cell_name(&cell),
                                    chip.pin_name(&chip.template_pin(&pin_inst)),
                                );

                                None
                            }
                        }
                    })
                    .collect();

                (net, terminals)
            })
            // Skip nets which have no terminals in this clip.
            .filter(|(_net, terminals)| !terminals.is_empty())
            .collect();

        // Sanity check: all terminals must be nodes of the routing graph.
        debug_assert!(
            {
                let routing_graph = self.routing_graph();

                routing_terminals
                    .iter()
                    .flat_map(|(_, nodes)| nodes)
                    .all(|n| routing_graph.contains_node(n))
            },
            "terminal nodes must be nodes in the routing graph"
        );

        // Sanity check.
        self.check_routing_guides(&routing_terminals);

        // Store terminals in net_data structure.
        let graph = self.geometry_data.routing_graph.as_ref().unwrap();
        for (net, nodes) in &routing_terminals {
            let subnets = &self.subnets[net];
            assert!(!subnets.is_empty());

            let subnet_guides: SmallVec<[_; 8]> = subnets
                .iter() // A net is likely split into a small number of subnets.
                .map(|subnet| &self.local_subnet_routing_guides[subnet])
                .collect();

            let mut subnet_nodes: SmallVec<[_; 8]> = smallvec![vec![]; subnets.len()];

            for node in nodes {
                let (coord, layer) = graph.node_to_coordinates(node);
                let layer_id = self.precomputed.get_db_layer_id(&layer);

                // Find the subnet whose guide covers this terminal node.
                let subnet_of_node = subnet_guides.iter().enumerate().find(|(idx, guide)| {
                    guide
                        .iter()
                        .any(|(l, rect)| l == layer_id && rect.contains_point(coord))
                });

                if let Some((idx, _)) = subnet_of_node {
                    // node is contained by the guide at `idx` => assign the node to subnet `idx`
                    subnet_nodes[idx].push(*node);
                } else {
                    // Node is not covered by a guide. Assign it to the closest subnet.

                    // Compute rectilinear distance between `coord` and `rect`.
                    let distance_fn = |rect: &db::Rect<LN::Coord>| -> LN::Coord {
                        rect.distance_to_point(coord).norm1()
                    };

                    let distances = subnet_guides.iter().enumerate().filter_map(|(idx, guide)| {
                        // Get the smallest distance to a guide.
                        guide
                            .iter()
                            .map(|(_l, rect)| distance_fn(rect))
                            .min()
                            .map(|min_distance| (idx, min_distance))
                    });

                    let closest_subnet_idx = distances
                        .min_by_key(|(_idx, dist)| *dist)
                        .map(|(idx, _)| idx)
                        .unwrap_or(0); // Take first subnet by default.

                    subnet_nodes[closest_subnet_idx].push(*node);
                }
            }

            // Store terminals in NetData structures.
            for (idx, nodes) in subnet_nodes.iter().enumerate() {
                let subnet = &subnets[idx];

                let pins = nodes
                    .iter()
                    .copied()
                    .map(|n| Pin::new(smallvec![n]))
                    .collect();

                self.net_data
                    .get_mut(subnet)
                    .expect("net_data is not initialized yet")
                    .pins = pins;
            }
        }
    }

    /// Check that all routing terminals are contained in a routing guide.
    fn check_routing_guides(&self, routing_terminals: &[(LN::NetId, Vec<Node>)]) {
        let graph = self.geometry_data.routing_graph();
        let chip = self.routing_problem.fused_layout_netlist();

        for (net, terminals) in routing_terminals {
            let guide = &self.local_routing_guides[net];

            let all_terminals_covered = terminals
                .iter()
                // For all terminal nodes...
                .all(|node| {
                    let (coord, layer) = graph.node_to_coordinates(node);
                    let layer_id = self.precomputed.get_db_layer_id(&layer);

                    guide
                        .iter()
                        // ... at least one guide must contain the terminal node.
                        .any(|(l, rect)| l == layer_id && rect.contains_point(coord))
                });

            // Print a warining if the routing guides don't cover all terminals.
            if !all_terminals_covered {
                log::warn!(
                    "Not all terminals of net '{:?}' are covered by the routing guide.",
                    chip.net_name(net)
                );
            }
        }
    }

    /// Find the the index of the neighbour clip. The neighbour clip may or may not exist.
    fn neighbour_clip_index(
        clip_index: (usize, usize),
        direction: db::Direction2D,
    ) -> Option<(usize, usize)> {
        let (x, y) = clip_index;
        use db::Direction2D::*;
        match direction {
            Left => (x > 0).then(|| (x - 1, y)),
            Up => Some((x, y + 1)),
            Right => Some((x + 1, y)),
            Down => (y > 0).then(|| (x, y - 1)),
        }
    }

    /// Get x and y coordinates of boundary pins coming from the routed neighbour clip.
    ///
    /// Used to add the grid-lines to the routing graph such that the centerlines
    /// of the incoming routes are contained in the graph.
    /// Returns a tuple of `(x coordinates, y coordinates)`.
    /// The coordinates are de-duplicated and sorted.
    fn get_boundary_terminal_coordinates(
        &self,
        routed_neighbour_clips: &HashMap<(usize, usize), &ClipRoutingResult<LN>>,
    ) -> (Vec<LN::Coord>, Vec<LN::Coord>) {
        log::trace!("find x/y coordinates of incoming routes");

        let chip = self.routing_problem.fused_layout_netlist();

        use db::Direction2D::*;
        let directions = [Right, Up, Left, Down];

        // Get routing results from all routed neighbour clips.
        let neighbour_clip_results = directions.iter().filter_map(|direction| {
            Self::neighbour_clip_index(self.clip_index, *direction)
                .and_then(|idx| routed_neighbour_clips.get(&idx))
        });

        let mut xs: FnvHashSet<_> = Default::default();
        let mut ys: FnvHashSet<_> = Default::default();

        let mut boundary_nodes_buffer = vec![];

        for neighbour_clip in neighbour_clip_results {
            // Get only wire shapes which match local nets.
            let local_subnets = self.local_subnet_routing_guides.keys();
            // Translate local subnets to subnets in the neighbour clip.
            let neighbour_subnets = local_subnets
                .filter_map(move |subnet| neighbour_clip.subnets.get(&subnet.net))
                .flatten();

            for neighbour_subnet in neighbour_subnets {
                let used_boundary_nodes = self.get_used_boundary_nodes_of_neighbour_clip(
                    neighbour_clip,
                    &neighbour_subnet,
                    &mut boundary_nodes_buffer,
                );

                used_boundary_nodes
                    .filter(|(point, _layer)| self.route_box.contains_point(*point))
                    .for_each(|(point, _layer)| {
                        xs.insert(point.x);
                        ys.insert(point.y);
                    });
            }
        }

        let mut xs: Vec<_> = xs.into_iter().collect();
        let mut ys: Vec<_> = ys.into_iter().collect();
        xs.sort_unstable();
        ys.sort_unstable();

        log::trace!(
            "Number of x coordinates used by incoming wire segments: {}",
            xs.len()
        );
        log::trace!(
            "Number of y coordinates used by incoming wire segments: {}",
            ys.len()
        );

        (xs, ys)
    }

    /// Create virtual terminals towards neighbour clips.
    /// * If the neighbour clip is *not routed*, then create a row of virtual terminals where the routing guide
    /// intersects the clip boundary.
    /// * If the neighbour clip is *routed*, create a terminal at the incoming route.
    fn init_boundary_net_terminals(
        &mut self,
        routed_neighbour_clips: &HashMap<(usize, usize), &ClipRoutingResult<LN>>,
    ) {
        log::trace!("create virtual pins at clip boundaries");

        use db::Direction2D::*;
        let directions = [Right, Up, Left, Down];

        // Check all four boundaries.
        for direction in directions {
            // Find the routed neighbour clip, if any.
            let routed_neighbour_clip = Self::neighbour_clip_index(self.clip_index, direction)
                .and_then(|idx| routed_neighbour_clips.get(&idx));

            if let Some(neighbour_clip) = routed_neighbour_clip {
                self.init_boundary_net_terminals_with_routed_neighbour(direction, neighbour_clip);
            } else {
                // No routed neighbour.
                self.init_boundary_net_terminals_with_unrouted_neighbour(direction);
            }
        }

        // // Deduplicate boundary pins of all nets.
        // // TODO: not correct yet
        // self.net_data.iter_mut()
        //     .for_each(|(_, net_data)| {
        //         let boundary_pins = std::mem::replace(&mut net_data.boundary_pins, vec![]);
        //         net_data.boundary_pins = boundary_pins.into_iter().unique().collect();
        //     })
    }

    /// Create horizontal or vertical slices of guide which potentially cross the clip boundary.
    fn get_boundary_crossing_guide_slices(
        &self,
        direction: db::Direction2D, // Direction of the neighbour clip.
        subnet: &SubnetId<LN::NetId>,
    ) -> impl Iterator<Item = (LN::LayerId, db::Rect<LN::Coord>)> + '_ {
        // Group guide shapes by layer.
        let guide_shapes_per_layer = {
            let mut per_layer = HashMap::new();
            let guide = self
                .local_subnet_routing_guides
                .get(subnet)
                .expect("no guide found for subnet");

            // Add local guide shapes.
            for (db_layer, guide_rect) in guide {
                per_layer.entry(db_layer).or_insert(vec![]).push(guide_rect);
            }

            // Add neighbour guide shapes of this subnet.
            // Get subnets in the neighbour clip which correspond to the current subnet.
            if let Some(guide) = self.neighbour_routing_guides.get(&subnet.net) {
                for (db_layer, guide_rect) in guide {
                    per_layer.entry(db_layer).or_insert(vec![]).push(guide_rect);
                }
            }

            per_layer
        };

        // Create horizontal or vertical slices of guide shapes.
        let guide_slices =
            guide_shapes_per_layer
                .into_iter()
                .flat_map(move |(layer, rectangles)| {
                    let redges = rectangles.iter().flat_map(|rect| rect.into_edges());

                    // Cut the layer shapes into slices.
                    let slices = match direction.orientation() {
                        db::Orientation2D::Horizontal => decompose_rectangles(redges),
                        db::Orientation2D::Vertical => decompose_rectangles_vertical(redges),
                    };

                    // Associate the slices with the layer.
                    slices.into_iter().map(move |slice| (layer.clone(), slice))
                });

        guide_slices
    }

    /// Get all boundary nodes which are used by the given subnet in the given neighbour clip.
    /// Note: this graph nodes are only valid in the neighbour routing graph but not in the local routing graph.
    fn get_used_boundary_nodes_of_neighbour_clip<'b>(
        &'b self,
        neighbour_clip: &'b ClipRoutingResult<LN>,
        neighbour_subnet: &'b SubnetId<LN::NetId>,
        boundary_nodes_buffer: &'b mut Vec<Node>, // Temporary buffer.
    ) -> impl Iterator<Item = (db::Point<LN::Coord>, LayerId)> + 'b {
        let net_data = &neighbour_clip.net_data[&neighbour_subnet];

        // Get all nodes in the neighbour which are marked as boundary nodes.
        let boundary_nodes = net_data
            .boundary_pins
            .iter()
            .flat_map(|pin| pin.nodes.iter())
            .copied();
        boundary_nodes_buffer.clear();
        boundary_nodes_buffer.extend(boundary_nodes);

        // Get boundary nodes actually used by the neighbour.
        // Note: this graph nodes are only valid in the neighbour routing graph but not in the local routing graph.
        let used_boundary_nodes = {
            // Get all start and end-points of wire segments.
            let used_wire_nodes = net_data.wires.iter().flat_map(|w| w.graph_nodes.iter());

            // Get all start and end-points of vias.
            let used_via_nodes = net_data.vias.iter().flat_map(|w| w.graph_nodes.iter());

            // Concatenate wire and via nodes.
            let used_nodes = used_wire_nodes.chain(used_via_nodes);

            let used_boundary_nodes = used_nodes
                // Take only nodes which are at the boundary to the current clip.
                .filter(|n| {
                    let coord = neighbour_clip.routing_graph.node_coord(n);
                    self.route_box.contains_point(coord)
                                // Take only boundary terminals.
                                && boundary_nodes_buffer.contains(n)
                })
                .copied();

            used_boundary_nodes
        };

        // Convert to (point, layer) tuples. They are valid also in the routing graph of the local clip, while the node indices are not.
        used_boundary_nodes.map(|n| neighbour_clip.routing_graph.node_to_coordinates(&n))
    }

    fn init_boundary_net_terminals_with_routed_neighbour(
        &mut self,
        direction: db::Direction2D,
        neighbour_clip: &ClipRoutingResult<LN>,
    ) {
        let chip = self.routing_problem.fused_layout_netlist();
        let graph = self.geometry_data.routing_graph();

        let mut boundary_nodes_buffer = vec![]; // Reused buffer for avoiding allocations.

        // Find boundary terminals net by net.
        for local_net in self.subnets.keys() {
            // Get subnets in the neighbour clip which correspond to the current subnet.
            let neighbour_subnets = neighbour_clip.subnets.get(local_net).into_iter().flatten();

            // Sanity check.
            debug_assert!(
                neighbour_subnets.clone().unique().count() == neighbour_subnets.clone().count(),
                "subnets must not be duplicated"
            );

            // Find boundary terminals of this subnet which are used by the neighbour clip.
            let used_boundary_terminals_of_neighbour = {
                for neighbour_subnet in neighbour_subnets.clone() {
                    let net_data = &neighbour_clip.net_data[&neighbour_subnet];

                    // Get all nodes in the neighbour which are marked as boundary nodes.
                    let boundary_nodes = net_data
                        .boundary_pins
                        .iter()
                        .flat_map(|pin| pin.nodes.iter())
                        .copied();
                    boundary_nodes_buffer.clear();
                    boundary_nodes_buffer.extend(boundary_nodes);

                    // Get boundary nodes actually used by the neighbour.
                    // Note: this graph nodes are only valid in the neighbour routing graph but not in the local routing graph.
                    let used_boundary_nodes: Vec<_> = self
                        .get_used_boundary_nodes_of_neighbour_clip(
                            neighbour_clip,
                            &neighbour_subnet,
                            &mut boundary_nodes_buffer,
                        )
                        .collect();

                    // Assign the each boundary terminal to a local subnet.
                    for (coord, boundary_node_layer) in used_boundary_nodes {
                        // Choose the local subnet whose routing guide is closest to the boundary terminal.
                        let closest_subnet: Option<_> = self.subnets[local_net]
                            .iter()
                            .flat_map(|subnet| {
                                self.local_subnet_routing_guides[subnet]
                                    .iter()
                                    // Compute distance to guide shape.
                                    .map(move |(layer, guide_shape)| {
                                        let distance = guide_shape.distance_to_point(coord).norm1();
                                        (subnet, distance)
                                    })
                            })
                            // Take closest subnet.
                            .min_by_key(|(_subnet, distance)| *distance)
                            .map(|(subnet, _distance)| subnet);

                        // Store the boundary node with the closest subnet.
                        if let Some(closest_subnet) = closest_subnet {
                            let subnet_data = self
                                .net_data
                                .get_mut(closest_subnet)
                                .expect("net data is not initialized");

                            // Translate to the node index in the local graph.
                            let local_boundary_node = graph.node_from_coordinates(coord, boundary_node_layer)
                                .expect("boundary node of neighbour does not exist in local routing graph");

                            // Create a pin from the boundary nodes.
                            subnet_data.boundary_pins.push(Pin {
                                nodes: smallvec!(local_boundary_node),
                            })
                        } else {
                            log::warn!(
                                "Boundary node could not be assigned to a subnet: {:?}",
                                coord
                            );
                        }
                    }
                }
            };
        }
    }

    /// Create routing terminals towards the neighbour clip.
    fn init_boundary_net_terminals_with_unrouted_neighbour(&mut self, direction: db::Direction2D) {
        let graph = self.geometry_data.routing_graph();

        let chip = self.routing_problem.fused_layout_netlist();

        for (subnet, guide) in &self.local_subnet_routing_guides {
            // Create horizontal or vertical slices of guide shapes.
            let guide_slices = self.get_boundary_crossing_guide_slices(direction, subnet);

            /// Find locations where the routing guides cross the clip boundary.
            let mut boundary_crossings: Vec<(LayerId, db::REdge<_>)> = guide_slices
                .filter_map(|(db_layer, guide_rect)| {
                    let layer = self.precomputed.get_router_layer_id(&db_layer);

                    let boundary_crossing = analyze_global_routes::find_clip_crossing(
                        &guide_rect,
                        &self.route_box,
                        direction,
                    );

                    boundary_crossing.map(|edge| (layer, edge))
                })
                .collect();

            // Deduplicate crossings.
            boundary_crossings.sort_by_key(|(layer, redge)| (redge.start, redge.end, *layer));
            boundary_crossings.dedup();

            /// Convert boundary crossings into virtual pins which connect nets accross clips.
            /// Merge boundary crossings at the same location (but different layers) into a single pin.
            let groups = boundary_crossings
                .iter()
                // Group crossings by location. `redge` marks the location.
                .group_by(|(layer, redge)| *redge);

            let pins = groups
                .into_iter()
                // Convert to a pin with multiple terminals.
                .map(|(redge, group)| {
                    // Get graph nodes at the crossings.
                    let mut boundary_nodes: SmallVec<_> = group
                        .flat_map(|(layer, boundary_crossing)| {
                            // Find graph nodes at the boundary crossing which form the terminals of the pin.
                            let r = boundary_crossing
                                .bounding_box()
                                .sized(One::one(), One::one());

                            graph.nodes_in_rect(*layer, &r)
                        })
                        .collect();

                    // Deduplicate (maybe not necessary).
                    boundary_nodes.sort();
                    boundary_nodes.dedup();

                    Pin {
                        nodes: boundary_nodes,
                    }
                })
                // Skip empty pins.
                .filter(|pin| !pin.nodes.is_empty());

            let subnet_data = self.net_data.get_mut(subnet).unwrap();

            // Store the pins.
            subnet_data.boundary_pins.extend(pins);
        }
    }

    /// Returns locations of design rule violations (for debugging) and performance numbers.
    fn do_maze_routing(
        &mut self,
    ) -> (
        HashSet<(LN::LayerId, db::Rect<LN::Coord>)>,
        ClipPerformanceStats,
    ) {
        log::trace!("do maze routing");

        // Store locations with design-rule violations for debugging purposes.
        let mut drc_hotspots = HashSet::new();

        let mut marked_nets: Vec<SubnetId<_>> =
            self.local_subnet_routing_guides.keys().cloned().collect();

        // 4) Reserve pin access for the nets.
        self.reserve_obstacles_of_all_nets();
        for subnet in &marked_nets {
            self.reserve_pin_access(subnet, true);
        }

        // Measure performance of detail routing.
        let mut clip_performance = ClipPerformanceStats::default();

        let mut iter_count = 0;
        let routes = loop {
            if iter_count >= self.router.max_iter {
                log::warn!("reached maximum number of iterations");
                break None;
            }

            log::trace!(
                "routing iteration {} of max. {}",
                iter_count + 1,
                self.router.max_iter
            );

            // Sort nets by ascending half-perimeter wire-length.
            {
                let graph = self.routing_graph();

                marked_nets.sort_by_cached_key(|subnet| {
                    if self.net_data.get(subnet).is_none() {
                        dbg!(&subnet);
                    }
                    let mut pin_locations = self
                        .net_data
                        .get(subnet)
                        .expect("net_data not initialized for subnet")
                        .pins
                        .iter()
                        .flat_map(|pin| pin.nodes.iter())
                        .map(|node| graph.node_coord(node));

                    let mut bbox = pin_locations
                        .next()
                        .unwrap_or(db::Point::new(Zero::zero(), Zero::zero()))
                        .bounding_box();
                    pin_locations.for_each(|p| bbox = bbox.add_point(p));

                    // Compute HPLW
                    bbox.width() + bbox.height()
                });
            }

            // 2) Get nets which are marked by a DRC.
            let subnets = &marked_nets;

            // 3) Rip-up up marked nets.
            for net in subnets {
                self.ripup_net(net);
            }

            // 5) Route each net.
            let num_nets = subnets.len();

            for (i, subnet) in subnets.into_iter().enumerate() {
                // Print progress.
                log::debug!("route subnet {}/{}", i + 1, num_nets);

                // 5.2) Subtract boundary costs.
                // TODO

                // 5.3) Route this net.
                {
                    // 5.1) Un-reserve pin access for this net.
                    self.reserve_obstacles_of_net(&subnet.net, false);
                    self.reserve_pin_access(&subnet, false);
                    self.enable_routing_guide(&subnet, true);
                    self.enable_assigned_tracks(&subnet.net, true);
                    self.set_boundary_terminal_costs(&subnet, true);

                    let result = self.do_route_single_net(&subnet);

                    self.set_boundary_terminal_costs(&subnet, false);
                    self.enable_routing_guide(&subnet, false);
                    self.enable_assigned_tracks(&subnet.net, false);

                    // Reserve the nodes which where unreserved specifically for this net.
                    self.reserve_obstacles_of_net(&subnet.net, true);
                    self.reserve_pin_access(&subnet, true);

                    match result {
                        Ok(stats) => {
                            // Update performance counters.
                            clip_performance =
                                ClipPerformanceStats::reduce(&clip_performance, &stats);
                        }
                        Err(_) => {
                            if let Some(net_name) = self
                                .routing_problem
                                .fused_layout_netlist()
                                .net_name(&subnet.net)
                            {
                                log::error!("failed to route net: {}", net_name);
                            } else {
                                log::error!("failed to route unnamed net")
                            }
                        }
                    }
                    // TODO: Pass error
                }

                // 5.4) Add object costs of the newly routed net.
                self.add_net_object_costs(&subnet, self.object_cost);

                self.register_net_shapes_in_region_query(&subnet, false);

                // 5.5) Add boundary costs of the newly routed net.
                // TODO
            }

            // 5) DRC check on the newly routed nets.
            {
                // Clear old DRV markers.
                drc_hotspots.clear();

                let drc_markers = self.do_drc(subnets);

                if drc_markers.is_empty() {
                    log::debug!("DRC Ok.");
                    break Some(());
                } else {
                    log::info!("Number of DRC violations: {}", drc_markers.len());
                }

                // Remember markers for debugging.
                {
                    let new_markers =
                        drc_markers
                            .iter()
                            .map(|(layer, rectangle, _net, _other_net)| {
                                let layer_id = self.precomputed.get_db_layer_id(layer).clone();
                                (layer_id, *rectangle)
                            });

                    drc_hotspots.extend(new_markers);
                }

                // Process DRC markers.
                marked_nets = {
                    // Find all nets involved in a DRC violation.
                    let mut new_marked_nets: FnvHashSet<_> = Default::default();
                    for (layer, rectangle, net, other_net) in &drc_markers {
                        new_marked_nets.insert(net);
                        if let Some(n) = other_net {
                            if self.net_data.contains_key(&n) {
                                // Re-route only nets which we are allowed to route.
                                new_marked_nets.insert(n);
                            }
                        }
                    }

                    // Add DRC marker costs.
                    // Marker costs are not removed between iterations. This way the history of violations is taken into account.
                    {
                        let node_costs = self.geometry_data.node_costs.as_mut().unwrap();
                        let routing_graph = self.geometry_data.routing_graph.as_mut().unwrap();

                        for (layer, rectangle, _net, _other_net) in &drc_markers {
                            // Enlarge the marker to make sure it contains routing nodes.
                            // TODO: Enlarge by how much?
                            let (enlarge_x, enlarge_y) = self
                                .router
                                .design_rules
                                .default_pitch(&self.precomputed.get_db_layer_id(layer))
                                .unwrap_or((Zero::zero(), Zero::zero()));
                            let two = LN::Coord::one() + LN::Coord::one();
                            let enlarge_x = enlarge_x / two;
                            let enlarge_y = enlarge_y / two;

                            debug_assert!(enlarge_x >= Zero::zero());
                            debug_assert!(enlarge_y >= Zero::zero());
                            let marker = rectangle.sized(enlarge_x, enlarge_y);

                            let marked_nodes = routing_graph.nodes_in_rect(*layer, &marker);

                            for n in marked_nodes {
                                node_costs.get_mut(&n).add_marker_cost(self.drc_marker_cost)
                            }
                        }
                    }

                    // Remember marked nets to be ripped up in next iteration.
                    new_marked_nets.into_iter().cloned().collect()
                };
            }

            iter_count += 1;
        };

        (drc_hotspots, clip_performance)
    }

    /// Mark graph nodes as obstacles according to all registered
    /// net shapes.
    fn reserve_obstacles_of_all_nets(&mut self) {
        let chip = self.routing_problem.fused_layout_netlist();
        let reserve = true;
        for (net, non_obstacles) in &self.non_obstacles {
            for (layer, obstacle_shape) in non_obstacles {
                let db_layer = self.precomputed.get_db_layer_id(layer);
                let (bloat_x, bloat_y) = self
                    .router
                    .bloat_extension_for_short_circuit(db_layer, Some(&obstacle_shape));
                let bloated = obstacle_shape.sized(bloat_x, bloat_y);
                self.geometry_data
                    .reserve_obstacle(*layer, obstacle_shape, reserve);
            }
        }
    }

    /// Reserve/unreserve obstacle shapes which are associated with the net.
    /// This includes pin shapes. They need to be unreserved before routing such that only the correct net can access them.
    fn reserve_obstacles_of_net(&mut self, net: &LN::NetId, reserve: bool) {
        if let Some(non_obstacles) = self.non_obstacles.get(net) {
            for (layer, obstacle_shape) in non_obstacles {
                let db_layer = self.precomputed.get_db_layer_id(layer);
                let (bloat_x, bloat_y) = self
                    .router
                    .bloat_extension_for_short_circuit(db_layer, Some(&obstacle_shape));
                let bloated = obstacle_shape.sized(bloat_x, bloat_y);
                self.geometry_data
                    .reserve_obstacle(*layer, obstacle_shape, reserve);
            }
        }
    }

    /// Mark all net terminal nodes which are selected as access points to pins.
    /// This helps the path search to quickly determine if a node is an access point.
    fn mark_access_points(&mut self) {
        let graph = self.geometry_data.routing_graph.as_ref().unwrap();
        let node_costs = self.geometry_data.node_costs.as_mut().unwrap();

        self.net_data
            .values()
            .flat_map(|net_data| net_data.pins.iter().flat_map(|pin| pin.nodes.iter()))
            // Take only access points. (Could be that there's other terminals, such as boundary nodes).
            .filter(|node| {
                let coords = graph.node_to_coordinates(node);
                self.precomputed
                    .access_points_by_location
                    .contains_key(&coords)
            })
            // Set the 'access point' bit.
            .for_each(|node| node_costs.get_mut(node).set_access_point(true));
    }

    /// Reserve or unreserve graph nodes around the access points of the net.
    /// Sets the costs to prevent other nets from coming too close to the access point.
    fn reserve_pin_access(&mut self, subnet_id: &SubnetId<LN::NetId>, reserve: bool) {
        // let pin_instances = self.routing_problem.fused_layout_netlist()
        //     .each_pin_instance_of_net(net);

        let precomputed = &self.precomputed;
        let node_costs = self.geometry_data.node_costs.as_mut().unwrap();
        let graph = self.geometry_data.routing_graph.as_ref().unwrap();

        let subnet_data = &self.net_data[subnet_id];
        let pin_nodes = subnet_data.pins.iter().flat_map(|pin| pin.nodes.iter());

        // Find all pin access points used by this subnet.
        let access_points = pin_nodes.filter_map(|node| {
            let coord = graph.node_to_coordinates(node);
            precomputed
                .access_points_by_location
                .get(&coord)
                .map(|ap_ref| (coord.0, ap_ref.get_access_point()))
        });

        // Use a high cost to prevent other nets from coming close to the access point.
        let cost = 100 * if reserve { 1 } else { -1 };

        for (ap_location, ap) in access_points {
            let ap: &pa::PinAccessPoint<_> = ap;

            // Get the favourite via.
            let via_def = &ap.possible_vias[0];

            // Get metal shapes of the via.
            let via_rectangles = via_def
                .shapes
                .iter()
                // Skip CUT layers.
                .filter(move |(layer, _)| {
                    layer == &via_def.low_layer || layer == &via_def.high_layer
                })
                .map(move |(layer, rect)| {
                    let rect_translated = rect.translate(ap_location.v());
                    (precomputed.get_router_layer_id(layer), rect_translated)
                });

            // Increase the costs around the via.
            for (layer, rect) in via_rectangles {
                let layer_id = &precomputed.get_db_layer_id(&layer);

                let (bloat_x, bloat_y) = self.router.bloat_extension(layer_id, Some(&rect));
                let rect_bloated = rect.sized(bloat_x, bloat_y);

                // Get all nodes affected by the route.
                let nodes = graph.nodes_in_rect(layer, &rect_bloated);

                for n in nodes {
                    node_costs.get_mut(&n).add_cost(cost);
                }
            }
        }

        // for pin_instance in pin_instances {
        //
        //     // Get access point to pin instance.
        //
        //     // TODO
        //
        //     // let nodes = self.routing_graph
        //     //     .as_ref().unwrap()
        //     //     .nodes_in_rect(*layer, rect);
        //     //
        //     // for n in nodes {
        //     //     node_costs.get_mut(&n).add_cost(1)
        //     // }
        // }
    }

    /// Remove the object costs caused by a net.
    fn ripup_net(&mut self, subnet: &SubnetId<LN::NetId>) {
        // 3.2) Subtract object costs of the nets.
        self.add_net_object_costs(&subnet, -self.object_cost);

        // Remove net shapes from the region query.
        self.register_net_shapes_in_region_query(&subnet, true);

        // Clear the net wires.
        let net_data = self.net_data.get_mut(subnet).unwrap();

        net_data.wires.clear();
        net_data.vias.clear();
    }

    /// Mark/unmark nodes covered by the routing guide.
    /// This way the path search is channeled by the routing guide.
    fn enable_routing_guide(&mut self, subnet: &SubnetId<LN::NetId>, enable: bool) {
        let graph = self.geometry_data.routing_graph.as_ref().unwrap();
        let node_attrs = self.geometry_data.node_costs.as_mut().unwrap();

        if let Some(guide) = self.local_subnet_routing_guides.get(subnet).as_ref() {
            for (db_layer, rect) in guide.iter() {
                // Get all nodes affected by the guide shape.
                let layer = self.precomputed.get_router_layer_id(db_layer);

                // Allow routing also one layer next to the routing guide.
                let layers = std::iter::once(layer)
                    .chain(graph.lower_layer(&layer))
                    .chain(graph.upper_layer(&layer));

                for layer in layers {
                    let nodes = graph.nodes_in_rect(layer, rect);

                    for n in nodes {
                        node_attrs.get_mut(&n).enable_routing_guide(enable);
                    }
                }
            }
        }
    }

    /// Set costs of boundary terminals such that assigned tracks are prioritized.
    /// Must be run after marking the nodes covered by assigned iroutes.
    fn set_boundary_terminal_costs(&mut self, subnet: &SubnetId<LN::NetId>, enable: bool) {
        let enable_factor = if enable { 1 } else { -1 };

        let graph = self.geometry_data.routing_graph.as_ref().unwrap();
        let node_attrs = self.geometry_data.node_costs.as_mut().unwrap();
        let subnet_data = &self.net_data[subnet];

        let mut node_buf: SmallVec<[_; 4]> = smallvec![];

        // Compute and set costs of boundary terminals.
        for boundary_pin in &subnet_data.boundary_pins {
            // Find boundary nodes which are on an assigned track.
            let nodes_on_assigned_tracks = boundary_pin
                .nodes
                .iter()
                .filter(|n| node_attrs.get(n).is_assigned_track())
                // Convert to a point.
                .map(|n| graph.node_coord(n));
            node_buf.clear();
            node_buf.extend(nodes_on_assigned_tracks);

            // Set node costs according to the distance to assigned iroutes.
            for node in &boundary_pin.nodes {
                let dist_to_iroute = node_buf
                    .iter()
                    // Compute distances.
                    .map(|p1| {
                        let p2 = graph.node_coord(node);
                        (*p1 - p2).norm1()
                    })
                    // Take the closest distance.
                    .min();

                // Convert the distance into a cost.
                let cost = dist_to_iroute.map(|d| d).unwrap_or(0);

                // Update the cost.
                node_attrs.get_mut(node).add_cost(cost * enable_factor);
            }
        }
    }

    /// Mark/unmark nodes which are part of an assigned track of the net.
    /// Assigned tracks are prioritized for the path search.
    fn enable_assigned_tracks(&mut self, net: &LN::NetId, enable: bool) {
        let graph = self.geometry_data.routing_graph.as_ref().unwrap();
        let node_attrs = self.geometry_data.node_costs.as_mut().unwrap();

        // TODO: Extract iroutes within this clip only. Currently the iroutes of the whole routing problem are used.
        if let Some(guide) = self.precomputed.assigned_iroutes.get(net).as_ref() {
            for (db_layer, redge) in guide.iter() {
                // Get all nodes affected by the assigned track.
                let layer = self.precomputed.get_router_layer_id(db_layer);

                // Convert the edge into a slightly enlarged rectangle.
                let rect = db::Rect::new(redge.start(), redge.end()).sized(One::one(), One::one());

                let nodes = graph.nodes_in_rect(layer, &rect);

                for n in nodes {
                    node_attrs.get_mut(&n).set_assigned_track(enable);
                }
            }
        }
    }

    /// Add object costs of a net to the grid nodes.
    fn add_net_object_costs(&mut self, subnet: &SubnetId<LN::NetId>, cost: i32) {
        let net_data = self.net_data.get(subnet).unwrap();

        {
            // Do borrows outside of loops and closures to help the borrow checker.
            let precomputed = self.precomputed;
            let design_rules = self.router.design_rules;
            let via_defs = &self.precomputed.via_definitions_by_cell_id;
            let graph = self.geometry_data.routing_graph.as_ref().unwrap();

            let node_costs = self.geometry_data.node_costs.as_mut().unwrap();

            // Add object costs of wire shapes.
            let wire_rectangles = net_data
                .wires
                .iter()
                .map(|wire_segment| (wire_segment.layer, wire_segment.to_rectangle()));

            // Add object costs of via shapes.
            let via_rectangles = net_data
                .vias
                .iter()
                .flat_map(|via_instance| Self::get_via_shapes(&precomputed, via_instance));

            let rectangles = wire_rectangles.chain(via_rectangles);

            for (layer, rect) in rectangles {
                let layer_id = &self.precomputed.get_db_layer_id(&layer);

                let (bloat_x, bloat_y) = self.router.bloat_extension(layer_id, Some(&rect));
                let rect_bloated = rect.sized(bloat_x, bloat_y);

                // Get all nodes affected by the route.
                let nodes = graph.nodes_in_rect(layer, &rect_bloated);

                for n in nodes {
                    node_costs.get_mut(&n).add_cost(cost);
                }
            }
        }
    }

    /// Get all rectangle shapes of a via (without cut layer).
    fn get_via_metal_shapes<'l>(
        precomputed: &'l PrecomputedRoutingData<LN>,
        via_instance: &'l WireVia<LN::Coord, LN::CellId>,
    ) -> impl Iterator<Item = (LayerId, db::Rect<LN::Coord>)> + Clone + 'l {
        let layer_lut = &precomputed.dblayer2id;
        let via_defs = &precomputed.via_definitions_by_cell_id;
        let via_def = &via_defs[&via_instance.via_id];

        via_def
            .shapes
            .iter()
            // Skip CUT layers.
            .filter(move |(layer, _)| layer == &via_def.low_layer || layer == &via_def.high_layer)
            .map(move |(layer, rect)| {
                let rect_translated = rect.translate(via_instance.location.v());
                (layer_lut[&layer], rect_translated)
            })
    }

    /// Get all rectangle shapes of a via (including cut layers).
    fn get_via_shapes<'l>(
        precomputed: &'l PrecomputedRoutingData<LN>,
        via_instance: &'l WireVia<LN::Coord, LN::CellId>,
    ) -> impl Iterator<Item = (LayerId, db::Rect<LN::Coord>)> + Clone + 'l {
        let layer_lut = &precomputed.dblayer2id;
        let via_defs = &precomputed.via_definitions_by_cell_id;
        let via_def = &via_defs[&via_instance.via_id];

        via_def.shapes.iter().map(move |(layer, rect)| {
            let rect_translated = rect.translate(via_instance.location.v());
            (layer_lut[&layer], rect_translated)
        })
    }

    /// Add or remove all shapes of the net for fast region search.
    /// This is used for quicker DRC checks of single nets.
    fn register_net_shapes_in_region_query(
        &mut self,
        subnet: &SubnetId<LN::NetId>,
        unregister: bool,
    ) {
        let net_data = self.net_data.get(subnet).unwrap();

        let wire_objects = net_data.wires.iter().map(|w| {
            let geo = GeometricObject::RouteSegment(RouteSegment {
                shape: w.to_rectangle(),
                net_id: Some(subnet.clone()),
            });

            (w.layer, geo)
        });

        // Do borrows outside of loops and closures to help the borrow checker.
        let precomputed = self.precomputed;
        let via_defs = &self.precomputed.via_definitions_by_cell_id;
        let region_search = &mut self.geometry_data.drc_region_search;
        let layer_lut = &self.precomputed.dblayer2id;

        // Collect shapes of vias and convert them into objects which can be stored in the RTree.
        // Via enclosing shapes are treated differently: They are stored together with information from which route segment they stem from (called 'origin').
        let via_objects = net_data.vias.iter().flat_map(|via_instance| {
            let via_shapes = Self::get_via_shapes(&precomputed, via_instance);

            // Get bounding box around via cut shapes.
            let cut_bbox = via_shapes
                .clone()
                .filter(|(layer, _shape)| layer.is_via_layer())
                .reduce(|(acc_layer, acc_shape), (layer, shape)| {
                    (layer, acc_shape.add_rect(&shape))
                });

            // Convert via shapes into RTree objects.
            via_shapes.map(move |(layer, shape)| {
                let geo = if layer.is_via_layer() {
                    GeometricObject::RouteSegment(RouteSegment {
                        shape,
                        net_id: Some(subnet.clone()),
                    })
                } else {
                    if let Some((cut_layer, via_origin_shape)) = cut_bbox {
                        // Use the via cut shape as a DRC anchor of the via enclosure shapes.
                        // This indicates that the location of the via cut must be penalized when a DRC violation happens with the enclosure shapes.

                        GeometricObject::PatchMetal(PatchMetal {
                            shape,
                            origin: via_origin_shape,
                            origin_layer: cut_layer,
                            net_id: Some(subnet.clone()),
                        })
                    } else {
                        // Via has no shapes on a cut layer. That should not happen.
                        GeometricObject::RouteSegment(RouteSegment {
                            shape,
                            net_id: Some(subnet.clone()),
                        })
                    }
                };

                (layer, geo)
            })
        });

        let net_objects = wire_objects.chain(via_objects);

        for (layer, obj) in net_objects {
            if unregister {
                region_search
                    .remove(layer, &obj)
                    .expect("net shape was not registered");
            } else {
                region_search.insert(layer, obj);
            }
        }
    }

    /// Reset node attributes used for the path search.
    fn reset_node_direction_flags(&mut self) {
        self.geometry_data
            .node_costs
            .as_mut()
            .unwrap()
            .iter_mut()
            .for_each(|attr| attr.set_trace_back_direction(GridDirection::NoDirection))
    }

    fn do_route_single_net(
        &mut self,
        subnet: &SubnetId<LN::NetId>,
    ) -> Result<ClipPerformanceStats, ()> {
        // self.reset_node_direction_flags();
        let chip = self.routing_problem.fused_layout_netlist();
        let net_data = &self.net_data[subnet];
        let graph = self.geometry_data.routing_graph();

        let preferred_routing_directions = self.precomputed.preferred_routing_directions.clone();

        // Cache routing pitches. Used to compute via costs.
        let default_pitch: Vec<_> = self
            .precomputed
            .layerid2dblayer
            .iter()
            .map(|layer| {
                self.router
                    .design_rules
                    .default_pitch_preferred_direction(layer)
                    .unwrap_or(LN::Coord::zero())
            })
            .collect();

        // Precompute via base costs.
        let via_base_costs: Vec<_> = {
            let num_layers = self.precomputed.layerid2dblayer.len() as u8;
            (0..num_layers)
                // Convert to layer IDs.
                .map(|i| {
                    let layer = LayerId(i);
                    let lower_layer = (i > 0).then(|| LayerId(i - 1));
                    (lower_layer, layer)
                })
                .map(|(lower_layer, layer)| {
                    if let Some(lower_layer) = lower_layer {
                        if layer.is_via_layer() {
                            // Via base cost: Use the routing pitch on the lower layer.
                            assert!(lower_layer.is_routing_layer());
                            4 * default_pitch[lower_layer.to_usize()]
                        } else {
                            0
                        }
                    } else {
                        0
                    }
                })
                .collect()
        };

        let mut unconnected_pins: Vec<Pin> = net_data
            .pins
            .iter()
            .chain(&net_data.boundary_pins)
            .cloned()
            .collect();

        if let Some(net_name) = chip.net_name(&subnet.net) {
            log::debug!(
                "routing subnet net '{}/{}' with {} pins",
                net_name,
                subnet.subnet,
                unconnected_pins.len()
            );
        }

        if unconnected_pins.is_empty() {
            log::debug!("subnet has no pins");
            return Ok(Default::default());
        };

        if unconnected_pins.len() == 1 {
            let coord = graph.node_coord(&unconnected_pins[0].center());
            log::warn!(
                "subnet has only one pin: {:?}, (at {:?})",
                chip.net_name(&subnet.net),
                coord
            ); // TODO: Something is probably not right if that happens.
            return Ok(Default::default());
        };

        debug_assert!(!unconnected_pins.is_empty());

        // Extract terminal locations to find the source node (node furthest away from the center of mass).
        let terminal_locations = unconnected_pins
            .iter()
            .map(|pin| graph.node_coord(&pin.center()));

        let source_pin_idx: usize = route_one_net::select_source_node(terminal_locations)
            .ok_or_else(|| {
                log::error!("failed to select source node");
                ()
            })?;

        // Remove source node from the unconnected pins.
        let source_pin = unconnected_pins.remove(source_pin_idx);

        let mut source_nodes = vec![];
        source_nodes.extend(source_pin.nodes);

        let mut paths = vec![];

        // Performance counters.
        let mut num_path_nodes = 0;
        let mut num_visited_nodes = 0;

        // Do path search until all pins are connected.
        while !unconnected_pins.is_empty() {
            // Get all grid nodes of the yet unconnected pins.
            // They are the routing destinations.
            let unconnected_nodes: Vec<Node> = unconnected_pins
                .iter()
                .flat_map(|p| p.nodes.iter().copied())
                .collect();

            // Set unconnected nodes as destinations.
            {
                let node_attributes = self.geometry_data.node_costs.as_mut().unwrap();
                for n in &unconnected_nodes {
                    node_attributes.get_mut(n).enable_destination(true);
                }
            }

            log::trace!("number of unconnected pins: {}", unconnected_pins.len());
            log::trace!("number of unconnected nodes: {}", unconnected_nodes.len());

            let routing_graph = self.geometry_data.routing_graph.as_ref().unwrap();
            let node_attributes = self.geometry_data.node_costs.as_ref().unwrap();

            let edge_cost_fn = |n1: &Node, n2: &Node| -> i32 {
                let (p1, layer1) = routing_graph.node_to_coordinates(n1);
                let (p2, layer2) = routing_graph.node_to_coordinates(n2);
                let is_via = layer1 != layer2;

                let node_attr1 = node_attributes.get(n1);
                let node_attr2 = node_attributes.get(n2);

                if is_via {
                    // Get graph node which represents the via. Used for spacing penalties.
                    let lower_node = if layer1 < layer2 { n1 } else { n2 };
                    let via_node = routing_graph
                        .upper_via(lower_node)
                        // Via node must exist because the lower and upper nodes also exist.
                        .unwrap();

                    let via_node_attr = node_attributes.get(&via_node);

                    // Compute via cost
                    let via_between_iroutes =
                        node_attr1.is_assigned_track() && node_attr2.is_assigned_track();

                    let is_access_point =
                        node_attr1.is_access_point() || node_attr2.is_access_point();

                    // Via base cost: Use the routing pitch on the lower layer.
                    let via_base_cost = via_base_costs[via_node.layer().to_usize()];

                    let off_iroute_penalty = if via_between_iroutes || is_access_point {
                        // A via which connects two assigned tracks is cheaper to favour usage of assigned tracks.
                        -1
                    } else {
                        1
                    };

                    let node_cost = node_attr1.cost() + node_attr2.cost() + via_node_attr.cost();

                    let penalty = 1 + off_iroute_penalty + node_cost;
                    let penalty = penalty.max(1);

                    penalty * via_base_cost
                } else {
                    // Planar edge (not a via).

                    let edge_direction = if p1.x != p2.x {
                        db::Orientation2D::Horizontal
                    } else {
                        db::Orientation2D::Vertical
                    };

                    let distance = (p2 - p1).norm1();

                    let is_preferred_direction = edge_direction
                        == preferred_routing_directions[layer1.to_usize()]
                            // If no preferred direction is defined, assume the edge points towards preferred direction.
                            .unwrap_or(edge_direction);
                    let direction_penalty = if is_preferred_direction { 0 } else { 4 };

                    // Allow/disallow planar access to pins.
                    let planar_access_penalty = if self.router.allow_planar_pin_access {
                        0
                    } else {
                        if node_attr1.is_access_point() || node_attr2.is_access_point() {
                            100000 // Strongly discourage using planar access.
                        } else {
                            0
                        }
                    };

                    // Penalize routes that don't follow the assigned tracks.
                    let is_on_iroute =
                        node_attr1.is_assigned_track() && node_attr2.is_assigned_track();
                    let off_iroute_penalty = if is_on_iroute { -1 } else { 1 };

                    // Penalize routes that don't follow the routing guides.
                    let is_on_guide = node_attr1.is_routing_guide_enabled()
                        && node_attr2.is_routing_guide_enabled();
                    let off_guide_penalty = if is_on_guide || is_on_iroute { 0 } else { 10 };

                    let off_track_penalty = {
                        let (track_type_x, track_type_y) = routing_graph.track_types(n1);

                        let track_type = match edge_direction {
                            Orientation2D::Horizontal => track_type_y,
                            Orientation2D::Vertical => track_type_x,
                        };

                        match track_type {
                            TrackType::OffTrack => 2,
                            TrackType::HalfTrack => 1,
                            TrackType::OnTrack => 0,
                        }
                    };

                    let node_cost = node_attr2.cost();

                    let penalty = 1
                        + off_iroute_penalty
                        + direction_penalty
                        + node_cost
                        + off_guide_penalty
                        + planar_access_penalty;
                    let penalty = penalty.max(1);

                    let distance_cost = penalty * distance;

                    distance_cost
                }
            };

            // Remove reserved nodes from the graph on the fly.
            let filtered_graph = FilterNodes::new(self.routing_graph(), |node| {
                let node_attr = node_attributes.get(node);
                !node_attr.is_reserved()
            });

            let graph = self.routing_graph();

            /// Compute the lower bound distance from `n` to any of the destination nodes.
            let astar_heuristic_fn = |n: &Node| -> i32 {
                let (coord1, layer1) = graph.node_to_coordinates(n);
                unconnected_nodes
                    .iter()
                    .map(|dest| {
                        let (coord2, layer2) = graph.node_to_coordinates(dest);
                        let dist_xy = (coord1 - coord2).norm1();
                        let dist_z: i32 = {
                            let low_layer = layer1.to_usize().min(layer2.to_usize());
                            let high_layer = layer1.to_usize().max(layer2.to_usize());
                            // layer1 and layer2 are metal layers, they don't need to be included in the sum
                            via_base_costs[low_layer..high_layer].iter().sum()
                        };
                        dist_xy + dist_z
                    })
                    .min()
                    .unwrap_or(0)
            };
            // let astar_heuristic_fn = |_n: &Node| -> i32 {
            //     0
            // };

            let mut visited_nodes = Default::default();
            let path = route_one_net::path_search(
                &filtered_graph,
                &source_nodes,
                &|n| node_attributes.get(n).is_destination(),
                &edge_cost_fn,
                &astar_heuristic_fn,
                &mut visited_nodes,
            );

            // Keep track of performance statistics.
            num_visited_nodes += visited_nodes.len();

            // Unset unconnected nodes as destinations.
            {
                let node_attributes = self.geometry_data.node_costs.as_mut().unwrap();
                for n in &unconnected_nodes {
                    node_attributes.get_mut(n).enable_destination(false);
                }
            }

            let path = path.ok_or_else(|| {
                log::error!("path search failed");
                ()
            })?;

            num_path_nodes += path.len();

            // Sanity check
            assert!(
                !path.is_empty(),
                "result of path search must not be an empty path"
            );

            // 5) Update
            self.route_single_net_update(subnet, &path, &mut source_nodes, &mut unconnected_pins);

            // Materialize the new path.
            self.materialize_path(subnet, &path)?;

            paths.push(path);
        }

        log::debug!("routed net with {} paths", paths.len());

        Ok(ClipPerformanceStats {
            num_subnets: 1,
            num_explored_graph_nodes: num_visited_nodes,
            avg_explored_node_to_path_length_ratio: (num_visited_nodes as f64)
                / (num_path_nodes.max(1) as f64),
        })
    }

    /// Convert the sequence of graph nodes into wire shapes and
    /// store them with the net.
    fn materialize_path(&mut self, subnet: &SubnetId<LN::NetId>, path: &[Node]) -> Result<(), ()> {
        if path.is_empty() {
            // Nothing to be done.
            return Ok(());
        }

        let chip = self.routing_problem.fused_layout_netlist();
        let graph = self.geometry_data.routing_graph();

        let segments = extract_path_corners(self.routing_graph(), path);

        // Convert segments into wires or vias.

        let net_data = self.net_data.get_mut(subnet).unwrap();

        // Used to determine routing direction of previous segment.
        let mut previous_diff = None;

        for i in 1..segments.len() {
            let start = segments[i - 1];
            let end = segments[i];

            let diff = coord_diff(&start, &end);
            let (diff_x, diff_y, diff_z) = diff;

            // Used to determine routing direction of next segement.
            let next_diff = (i + 1 < segments.len()).then(|| {
                let next_start = &end;
                let next_end = &segments[i + 1];
                coord_diff(next_start, next_end)
            });

            // Sanity check: Only one coordinate changes at a time. (All corners are rectangular)
            debug_assert_eq!(hamming_distance(&start, &end), 1);

            let is_via = diff_z;

            if is_via {
                let start_layer = start.layer();
                let end_layer = end.layer();
                let via_location = graph.node_coord(&start);

                let prev_routing_direction = previous_diff.and_then(|d| match d {
                    (true, false, false) => Some(db::Orientation2D::Horizontal),
                    (false, true, false) => Some(db::Orientation2D::Vertical),
                    _ => None,
                });

                let next_routing_direction = next_diff.and_then(|d| match d {
                    (true, false, false) => Some(db::Orientation2D::Horizontal),
                    (false, true, false) => Some(db::Orientation2D::Vertical),
                    _ => None,
                });

                let access_point_ref = {
                    self.precomputed
                        .access_points_by_location
                        .get(&(via_location, start_layer))
                        .or_else(|| {
                            self.precomputed
                                .access_points_by_location
                                .get(&(via_location, end_layer))
                        })
                };

                // Find the best via for this location.
                let via_def: Option<&pa::ViaDef<_, _>> = if let Some(access_point_ref) =
                    access_point_ref
                {
                    // Find routing directions below and upon the via.
                    let (low_dir, high_dir) = {
                        if start_layer.to_usize() < end_layer.to_usize() {
                            (prev_routing_direction, next_routing_direction)
                        } else {
                            (next_routing_direction, prev_routing_direction)
                        }
                    };

                    // Pick one of the vias assigned to this access point.
                    let possible_vias = &access_point_ref.get_access_point().possible_vias;

                    // Pick via according to routing directions.
                    let access_point_via: &pa::ViaDef<_, _> = possible_vias
                        .iter()
                        .find(|via_def| {
                            let via_low_dir = via_def.low_routing_direction();
                            let via_high_dir = via_def.high_routing_direction();

                            (low_dir.is_none() || via_low_dir.is_none() || low_dir == via_low_dir)
                                && (high_dir.is_none()
                                    || via_high_dir.is_none()
                                    || high_dir == via_high_dir)
                        })
                        .unwrap_or(&possible_vias[0]); // Default to first via in the list.

                    Some(access_point_via)
                } else {
                    // Pick via based on the preferred routing directions.
                    // Pick via based on the lower and upper route. Or TODO: materialize vias once all paths of the net are known.
                    let start_layer_id = self.precomputed.get_db_layer_id(&start_layer);
                    let end_layer_id = self.precomputed.get_db_layer_id(&end_layer);

                    let default_direction_start = self
                        .router
                        .design_rules
                        .preferred_routing_direction(&start_layer_id);

                    let default_direction_end = self
                        .router
                        .design_rules
                        .preferred_routing_direction(&end_layer_id);

                    // Use actual routing directions to select the via.
                    // Fall-back to default routing directions if they are not known.
                    let start_direction =
                        prev_routing_direction.or_else(|| default_direction_start);
                    let end_direction = next_routing_direction.or_else(|| default_direction_end);

                    let mut possible_vias =
                        self.precomputed.via_lookup_table.get_vias_with_directions(
                            start_layer_id,
                            end_layer_id,
                            start_direction,
                            end_direction,
                        );

                    // TODO: Pick the via with smallest enclosure.
                    let best_via = possible_vias.next();

                    if best_via.is_none() {
                        log::error!("No via found from '{:?}' to '{:?}' with routing directions {:?} and {:?}",
                            chip.layer_info(start_layer_id).name, chip.layer_info(end_layer_id).name,
                            start_direction, end_direction
                        );
                    }

                    best_via.map(|v| v.deref())
                };

                // Resolve CellId of the via.
                if let Some(via_def) = via_def {
                    let via_cell =
                        chip.cell_by_name(via_def.via_name.as_str())
                            .ok_or_else(|| {
                                log::error!("Via cell not found: {}", via_def.via_name);
                                ()
                            })?;

                    net_data.vias.push(WireVia {
                        via_id: via_cell,
                        location: via_location,
                        graph_nodes: [start, end],
                    });
                } else {
                    // Failed to find via.

                    // TODO: Abort and return an error.
                    // return Err(());
                }
            } else {
                // Wire segment.
                debug_assert_eq!(start.layer(), end.layer());

                let layer = start.layer();

                // Construct shape of the wire segment.
                let edge =
                    db::REdge::try_from_points(graph.node_coord(&start), graph.node_coord(&end))
                        .expect("graph edges must be rectilinear");

                // Get correct wire width.
                let half_width = {
                    let wire_width = self
                        .router
                        .design_rules
                        .default_width(self.precomputed.get_db_layer_id(&layer), None)
                        .expect("no wire width defined");
                    let one = LN::Coord::one();
                    let two = one + one;
                    wire_width / two
                };

                net_data.wires.push(WireSegment {
                    edge,
                    half_width,
                    layer,
                    graph_nodes: [start, end],
                });
            }

            previous_diff = Some(diff);
        }

        Ok(())
    }

    /// Algorithm 5: Update
    ///
    /// * create new source nodes for the next path search
    /// * remove the found pin from the list of unconnected pins.
    fn route_single_net_update(
        &self,
        subnet: &SubnetId<LN::NetId>,
        path: &[Node],
        source_nodes: &mut Vec<Node>,
        unconnected_pins: &mut Vec<Pin>,
    ) {
        let net_data = &self.net_data[subnet];

        source_nodes.extend(path.iter().copied());

        // Remove the newly connected pin from the list of unconnected pins.
        {
            let path_end = &path[path.len() - 1];
            let (destination_pin_idx, _) = unconnected_pins
                .iter()
                .enumerate()
                .find(|(_i, pin)| pin.nodes.contains(path_end))
                .expect("the path end must be a grid node of an unconnected pin");

            let destination_pin = unconnected_pins.remove(destination_pin_idx);

            if self.router.allow_pin_feedthrough {
                // Set other grid nodes of the destination pin as sources.
                let other_pin_nodes = destination_pin
                    .nodes
                    .iter()
                    .copied()
                    .filter(|n| n != path_end);
                source_nodes.extend(other_pin_nodes)
            }
        }

        // Remove other grid nodes of the source pin to avoid feed-through.
        {
            let mut pins = net_data
                .pins
                .iter()
                .take_while(|_| !self.router.allow_pin_feedthrough) // Skip the normal pins when feedthrough is allowed.
                .chain(&net_data.boundary_pins);

            let path_begin = &path[0];
            if let Some(begin_pin) = pins.find(|pin| pin.nodes.contains(path_begin)) {
                let other_pin_nodes = begin_pin.nodes.iter().filter(|&n| n != path_begin);

                // Set the other nodes of the pin as non-source nodes.
                for node in other_pin_nodes {
                    source_nodes.retain(|n| n != node);
                }
            }
        }
    }

    /// Do a DRC check for the supplied nets.
    fn do_drc(
        &self,
        nets: &[SubnetId<LN::NetId>],
    ) -> Vec<(
        LayerId,
        db::Rect<LN::Coord>,
        SubnetId<LN::NetId>,
        Option<SubnetId<LN::NetId>>,
    )> {
        nets.into_iter()
            .map(|net| self.do_drc_single_net(net))
            .flatten()
            .collect()
    }

    /// Do DRC checks for the given net.
    ///
    /// Check for spacing violations (and short circuit).
    ///
    /// Return violations in the form of `(layer, rectangle, offending net, offended other net)`.
    fn do_drc_single_net(
        &self,
        subnet: &SubnetId<LN::NetId>,
    ) -> Vec<(
        LayerId,
        db::Rect<LN::Coord>,
        SubnetId<LN::NetId>,
        Option<SubnetId<LN::NetId>>,
    )> {
        let mut markers = vec![];

        let net_data = &self.net_data[subnet];
        let net_wire_shapes = net_data.wires.iter().map(|w| drc_checks::DrcCheckShape {
            shape: w.to_rectangle(),
            layer: w.layer,
            origin: None,
        });

        // Do borrows outside of loops and closures to help the borrow checker.
        let precomputed = self.precomputed;

        // Collect shapes of vias and convert them into objects which can be stored in the RTree.
        // Via enclosing shapes are treated differently: They are stored together with information from which route segment they stem from (called 'origin').
        let via_check_shapes = net_data.vias.iter().flat_map(|via_instance| {
            let via_shapes = Self::get_via_shapes(&precomputed, via_instance);

            // Get bounding box around via cut shapes.
            let cut_bbox = via_shapes
                .clone()
                .filter(|(layer, _shape)| layer.is_via_layer())
                .reduce(|(acc_layer, acc_shape), (layer, shape)| {
                    (layer, acc_shape.add_rect(&shape))
                });

            // Convert via shapes into `DrcCheckShape`s
            via_shapes.map(move |(layer, shape)| {
                let check_shape = if layer.is_via_layer() {
                    drc_checks::DrcCheckShape {
                        shape,
                        layer,
                        origin: None,
                    }
                } else {
                    if let Some((cut_layer, via_origin_shape)) = cut_bbox {
                        // Use the via cut shape as a DRC anchor of the via enclosure shapes.
                        // This indicates that the location of the via cut must be penalized when a DRC violation happens with the enclosure shapes.
                        drc_checks::DrcCheckShape {
                            shape,
                            layer,
                            origin: Some((cut_layer, via_origin_shape)),
                        }
                    } else {
                        // Via has no shapes on a cut layer. That should not happen.
                        drc_checks::DrcCheckShape {
                            shape,
                            layer,
                            origin: None,
                        }
                    }
                };

                check_shape
            })
        });

        let net_shapes = net_wire_shapes.chain(via_check_shapes);

        // Detect short circuits.
        {
            let shorts = drc_checks::detect_shorts(
                &subnet,
                net_shapes.clone(),
                &self.geometry_data.drc_region_search,
            );

            markers.extend(
                shorts
                    .into_iter()
                    .map(|(layer, rect, other_net)| (layer, rect, subnet.clone(), other_net)),
            );
        }

        // Detect spacing violations.
        {
            let chip = self.routing_problem.fused_layout_netlist();

            let spacing_violations = drc_checks::spacing_check(
                chip,
                self.router.design_rules,
                &subnet,
                net_shapes,
                &self.geometry_data.drc_region_search,
                &self.precomputed.layerid2dblayer,
            );

            markers.extend(
                spacing_violations
                    .into_iter()
                    .map(|(layer, rect, other_net)| (layer, rect, subnet.clone(), other_net)),
            );
        }

        markers
    }
}

/// Result of detail routing within a clip.
pub(super) struct ClipRoutingResult<LN: db::L2NIds>
//where    LN::Coord: PrimInt + Signed + std::fmt::Debug,
{
    /// Region in which this clip can modify the layout.
    pub clip_box: db::Rect<LN::Coord>,
    routing_graph: RoutingGraph<LN::Coord>,
    /// Data associated with nets (shapes, temporary route, etc).
    /// This is the actual result of the detail routing.
    pub net_data: FnvHashMap<SubnetId<LN::NetId>, NetData<LN>>,
    subnets: FnvHashMap<LN::NetId, Vec<SubnetId<LN::NetId>>>,
    /// Save locations of design rule violations for debugging.
    pub drc_hotspots: Vec<(LN::LayerId, db::Rect<LN::Coord>)>,
    /// Region query filled with all net shapes. This is used to import net shapes in neighbour clips.
    //drc_region_search: RegionQuery<LN::Coord, SubnetId<LN::NetId>>,

    /// Statistics on the performance of the detail routing in this clip.
    pub performance_stats: ClipPerformanceStats,

    /// Shapes which form obstacles for all nets. Used for debugging to see if any obstacle
    /// blocks a pin access or the like.
    pub obstacles: Vec<(LN::LayerId, db::Rect<LN::Coord>)>,
}

impl<LN: db::L2NBase> ClipRoutingResult<LN>
where
    LN::Coord: PrimInt + Signed + std::fmt::Debug,
{
    /// Empty result.
    fn new() -> Self {
        Self {
            clip_box: db::Rect::new(db::Point::zero(), db::Point::zero()),
            routing_graph: Default::default(),
            net_data: Default::default(),
            subnets: Default::default(),
            drc_hotspots: Default::default(),
            performance_stats: Default::default(),
            obstacles: Default::default(),
            //drc_region_search: RegionQuery::new(0),
        }
    }
}

/// Statistics on the performance of the detail routing.
#[derive(Debug, Clone, Default)]
pub(super) struct ClipPerformanceStats {
    num_subnets: usize,
    num_explored_graph_nodes: usize,
    /// Average number of explored nodes during path search divided by the number of nodes in the found paths.
    /// The ratio is `>=1`. A low ratio means that the path search is efficient, i.e. well directed.
    /// A high ratio hints that the path search is not well directed.
    avg_explored_node_to_path_length_ratio: f64,
}

impl ClipPerformanceStats {
    /// Summarize two performance statistics into one.
    pub(super) fn reduce(s1: &Self, s2: &Self) -> Self {
        let sum_subnets = s1.num_subnets + s2.num_subnets;
        let avg_explore_ratio = if sum_subnets == 0 {
            0.
        } else {
            (s1.avg_explored_node_to_path_length_ratio * (s1.num_subnets as f64)
                + s2.avg_explored_node_to_path_length_ratio * (s2.num_subnets as f64))
                / (sum_subnets as f64)
        };
        Self {
            num_subnets: sum_subnets,
            num_explored_graph_nodes: s1.num_explored_graph_nodes + s2.num_explored_graph_nodes,
            avg_explored_node_to_path_length_ratio: avg_explore_ratio,
        }
    }
}

/// Store layout related data of a routing task.
pub(super) struct GeometryData<LN>
where
    LN: db::L2NEdit,
    LN::Coord: PrimInt + Signed + std::fmt::Debug,
{
    pub routing_graph: Option<RoutingGraph<LN::Coord>>,
    pub node_costs: Option<RoutingGraphNodeAttributes<NodeAttribute>>,
    pub drc_region_search: RegionQuery<LN::Coord, SubnetId<LN::NetId>>,
}

impl<LN> GeometryData<LN>
where
    LN: db::L2NEdit,
    LN::Coord: PrimInt + Signed + std::fmt::Debug,
{
    pub fn routing_graph(&self) -> &RoutingGraph<LN::Coord> {
        self.routing_graph
            .as_ref()
            .expect("routing graph is not initialize yet")
    }

    pub(crate) fn node_costs_mut(&mut self) -> &mut RoutingGraphNodeAttributes<NodeAttribute> {
        self.node_costs
            .as_mut()
            .expect("node costs are not initialize yet")
    }

    /// Reserve/unreserve nodes in the rectangle such that they cannot be used by the path search.
    pub(crate) fn reserve_obstacle(
        &mut self,
        layer: LayerId,
        rect: &db::Rect<LN::Coord>,
        reserve: bool,
    ) {
        let graph = self.routing_graph.as_ref().unwrap();
        let node_attrs = self.node_costs.as_mut().unwrap();

        let nodes = graph.nodes_in_rect(layer, rect);

        for n in nodes {
            let attr = node_attrs.get_mut(&n);
            if reserve {
                // attr.reserve();
                attr.add_cost(10000); // Give a very high cost instead of completely blocking the node.
            } else {
                // attr.unreserve();
                attr.add_cost(-10000);
            }
        }
    }
}
