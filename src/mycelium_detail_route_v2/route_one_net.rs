// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Route a single net.

use fnv::FnvHashMap;
use libreda_pnr::db;
use num_traits::Num;

use std::cmp::Ordering;
use std::hash::Hash;

use std::collections::BinaryHeap;

use crate::graph::TraverseGraph;

/// Find the point which is furthest away from the center of mass.
/// Return the index of the selected point.
pub(super) fn select_source_node<C>(
    node_locations: impl Iterator<Item = db::Point<C>> + Clone,
) -> Option<usize>
where
    C: Num + Copy + Ord,
{
    // Compute center of mass. Not normalized, i.e. MULTIPLIED by the number of points (avoid divisions).
    let (num_points, center_of_mass) = node_locations
        .clone()
        .fold((C::zero(), db::Point::zero()), |(count, acc), p| {
            (count + C::one(), acc + p)
        });

    let closest_to_center_of_mass_index = node_locations
        .enumerate()
        .max_by_key(|(_i, p)| {
            let p_scaled = *p * num_points;
            let diff = p_scaled - center_of_mass;
            diff.norm1()
        })
        .map(|(i, _p)| i);

    closest_to_center_of_mass_index
}

#[test]
fn test_select_source_node() {
    let points = vec![(0, 0).into(), (1, 0).into(), (0, 10).into()];
    assert_eq!(select_source_node(points.iter().copied()), Some(2));
}

/// Find path from the source node to one of the sink nodes.
pub(super) fn path_search<FE, FH, FDest, Node, G, Cost>(
    graph: &G,
    source_nodes: &[Node],
    is_destination_fn: &FDest,
    edge_cost_fn: &FE,
    heuristic_fn: &FH, // A-star heuristic. Lower-bound distance to destination nodes.
    visited_nodes: &mut FnvHashMap<Node, Node>, // Visited nodes associated with their previous nodes.
) -> Option<Vec<Node>>
where
    G: TraverseGraph<Node>,
    Node: PartialEq + Eq + Hash + Copy,
    Cost: Ord + Num + Copy,
    FE: Fn(&Node, &Node) -> Cost,
    FH: Fn(&Node) -> Cost,
    FDest: Fn(&Node) -> bool,
{
    // Visited nodes.
    let trace = visited_nodes;
    trace.reserve(1 << 10);

    let mut front = BinaryHeap::new();
    front.reserve(1 << 10);

    // Populate front with the initial node.
    for source in source_nodes {
        let element = PQElement {
            cost: Cost::zero(),
            node: *source,
            previous: *source,
            pin_id: 0,
            remaining_cost_lower_bound: Cost::zero(),
        };
        front.push(element);
    }

    // Buffer for neighbour nodes.
    // This buffer helps avoiding allocations.
    let mut neighbours = Vec::new();
    neighbours.reserve(16);

    // Work through priority queue.
    let mut target_hit = None; // The sink node which was found.
    let mut processed_node_counter = 0; // Statistics on processed nodes.

    while let Some(e) = front.pop() {
        processed_node_counter += 1;

        if is_destination_fn(&e.node) {
            // Found a target!
            target_hit = Some(e.node);
            trace.insert(e.node, e.previous);
            break;
        }

        if let Some(_previous) = trace.get(&e.node) {
        } else {
            // Node was not yet visited.

            // Clear buffer.
            neighbours.clear();
            graph.neighbours(&mut neighbours, &e.node);

            // Create next nodes.
            for &n in &neighbours {
                if n != e.previous {
                    // Skip the node where we come from.
                    let distance = edge_cost_fn(&e.node, &n);

                    let heuristic = heuristic_fn(&n);

                    let new = PQElement {
                        node: n,
                        cost: e.cost + distance,
                        pin_id: e.pin_id,
                        previous: e.node,
                        remaining_cost_lower_bound: heuristic,
                    };
                    if !trace.contains_key(&n) {
                        front.push(new);
                    }
                }
            }

            trace.insert(e.node, e.previous);
        }
    }

    log::trace!("processed nodes: {}", processed_node_counter);

    if let Some(hit) = target_hit {
        let trace_back = |p: Node| -> Vec<Node> {
            let mut path = Vec::new();
            let mut p = p;
            loop {
                path.push(p);
                let prev = trace[&p];
                if prev == p {
                    break path;
                }
                p = prev;
            }
        };
        let mut path = trace_back(hit);
        path.reverse();

        log::trace!(
            "processed nodes / path-length: {:.2}",
            (processed_node_counter as f64) / (path.len() as f64)
        );

        Some(path)
    } else {
        // No path was found.
        log::warn!("No path found.");
        None
    }
}

/// Priority queue element.
#[derive(Hash, PartialEq)]
struct PQElement<Node, Cost> {
    node: Node,
    previous: Node,
    // TODO: Use cost for tracing back.
    pin_id: u32,
    cost: Cost,
    // A-star heuristic
    remaining_cost_lower_bound: Cost,
}

impl<Node, Cost> Eq for PQElement<Node, Cost>
where
    Node: PartialEq,
    Cost: PartialEq,
{
}

impl<Node, Cost: Num + Copy> PQElement<Node, Cost> {
    fn cost_lower_bound(&self) -> Cost {
        self.cost + self.remaining_cost_lower_bound
    }
}

impl<Node, Cost> Ord for PQElement<Node, Cost>
where
    Cost: PartialOrd + Num + Copy,
    Node: PartialEq + Eq,
{
    fn cmp(&self, other: &Self) -> Ordering {
        let cost_bound_other = other.cost_lower_bound();
        let cost_bound_self = self.cost_lower_bound();

        cost_bound_other
            .partial_cmp(&cost_bound_self)
            .unwrap_or(Ordering::Equal)
    }
}

impl<Node, Cost> PartialOrd for PQElement<Node, Cost>
where
    Cost: PartialOrd + Num + Copy,
    Node: PartialEq + Eq,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
