// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Fast search for geometric objects based on their location.
//! Uses the `rstar` crate in the background.

use super::geometric_objects::GeometricObject;
use super::LayerId;
use libreda_pnr::db;
use num_traits::{Bounded, PrimInt, Signed};
use rstar;
use rstar::{RTree, RTreeObject};

/// RTrees for multiple layers.
pub(super) struct RegionQuery<Crd, NetId>
where
    GeometricObject<Crd, NetId>: RTreeObject,
{
    /// One RTree per layer.
    pub(super) rtrees: Vec<RTree<GeometricObject<Crd, NetId>>>,
}

impl<Crd, NetId> RegionQuery<Crd, NetId>
where
    Crd: Copy + PrimInt + Bounded + Signed + PartialOrd + PartialEq + std::fmt::Debug,
    NetId: PartialEq,
{
    /// Create a new `RegionQuery` struct for the given number of layers.
    pub fn new(num_layers: usize) -> Self {
        Self {
            rtrees: (0..num_layers).map(|_| RTree::new()).collect(),
        }
    }

    /// Insert an object on the layer.
    pub fn insert(&mut self, layer: LayerId, obj: GeometricObject<Crd, NetId>) {
        self.rtrees[layer.0 as usize].insert(obj)
    }

    /// Remove an object from a layer.
    pub fn remove(
        &mut self,
        layer: LayerId,
        obj: &GeometricObject<Crd, NetId>,
    ) -> Option<GeometricObject<Crd, NetId>> {
        self.rtrees[layer.0 as usize].remove(obj)
    }

    /// Iterate over all geometric objects on the given layer which are contained in the rectangle.
    pub fn locate_in_rect(
        &self,
        layer: LayerId,
        rect: &db::Rect<Crd>,
    ) -> impl Iterator<Item = &GeometricObject<Crd, NetId>> {
        self.rtrees[layer.0 as usize].locate_in_envelope(&rect2aabb(rect))
    }

    /// Iterate over all geometric objects on the given layer which are intersecting the rectangle.
    pub fn locate_intersect_rect(
        &self,
        layer: LayerId,
        rect: &db::Rect<Crd>,
    ) -> impl Iterator<Item = &GeometricObject<Crd, NetId>> {
        self.rtrees[layer.0 as usize].locate_in_envelope_intersecting(&rect2aabb(rect))
    }
}

/// Convert a rectangle into an axis aligned bounding box used by RStar.
fn rect2aabb<Crd>(r: &db::Rect<Crd>) -> rstar::AABB<[Crd; 2]>
where
    Crd: Copy + Bounded + Signed + PartialOrd + std::fmt::Debug,
{
    rstar::AABB::from_corners(r.lower_left().into(), r.upper_right().into())
}

#[test]
fn rstar_learning_test() {
    // Test the API of `rstar`.

    #[derive(Debug, Copy, Clone, PartialEq)]
    struct TreeElement {
        rect: db::Rect<i32>,
    }

    impl rstar::RTreeObject for TreeElement {
        type Envelope = rstar::AABB<[i32; 2]>;

        fn envelope(&self) -> Self::Envelope {
            let bbox = self.rect;

            rstar::AABB::from_corners(bbox.lower_left().into(), bbox.upper_right().into())
        }
    }

    let mut tree = rstar::RTree::new();

    tree.insert(TreeElement {
        rect: db::Rect::new((0, 0), (10, 10)),
    });

    tree.insert(TreeElement {
        rect: db::Rect::new((100, 100), (101, 101)),
    });

    let r3 = db::Rect::new((140, 140), (160, 160));
    let t3 = TreeElement { rect: r3 };
    tree.insert(t3);

    assert_eq!(tree.size(), 3);

    // Do a region query for *fully contained* objects.

    let region = db::Rect::new((50, 50), (150, 150));

    {
        let in_envelope: Vec<_> = tree
            .locate_in_envelope(&rstar::AABB::from_corners(
                region.lower_left().into(),
                region.upper_right().into(),
            ))
            .collect();

        assert_eq!(in_envelope.len(), 1);
        assert_eq!(
            in_envelope,
            vec![&TreeElement {
                rect: db::Rect::new((100, 100), (101, 101))
            }]
        );
    }

    // Do a region query for intersecting objects.
    {
        let in_envelope: Vec<_> = tree
            .locate_in_envelope_intersecting(&rstar::AABB::from_corners(
                region.lower_left().into(),
                region.upper_right().into(),
            ))
            .collect();

        assert_eq!(in_envelope.len(), 2);
    }

    // Test removing objects.

    // Add r3 a second time.
    tree.insert(t3);

    // Can remove t3 twice because it was inserted twice.
    assert_eq!(tree.remove(&t3), Some(t3));
    assert_eq!(tree.remove(&t3), Some(t3));

    // Removing t3 the third time should fail.
    assert_eq!(tree.remove(&t3), None);
}
