// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Measure time spent by selected parts of a program.

use std::collections::HashMap;
use std::time::Duration;

#[derive(Default)]
pub struct PerfCounter {
    total_time: Duration,
    num_calls: usize,
    sub_counters: HashMap<&'static str, PerfCounter>,
}

impl std::fmt::Display for PerfCounter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        self.pretty_print(f)
    }
}

impl std::fmt::Debug for PerfCounter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        self.pretty_print(f)
    }
}

impl PerfCounter {
    pub fn pretty_print(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        self.pretty_print_indent(f, 0)
    }

    fn pretty_print_indent(
        &self,
        f: &mut std::fmt::Formatter<'_>,
        indent_level: usize,
    ) -> Result<(), std::fmt::Error> {
        let indent_width = 4 * indent_level;
        writeln!(
            f,
            "{:indent$}total time: {:?}",
            "",
            self.total_time,
            indent = indent_width
        )?;
        writeln!(
            f,
            "{:indent$}number of calls: {:?}",
            "",
            self.num_calls,
            indent = indent_width
        )?;

        let mut sorted_sub_counters: Vec<_> = self.sub_counters.iter().collect();
        sorted_sub_counters.sort_by_key(|(_name, sub_counter)| sub_counter.total_time);
        sorted_sub_counters.reverse();

        for (name, sub_counter) in sorted_sub_counters {
            writeln!(f, "{:indent$}- {}:", "", name, indent = indent_width);
            sub_counter.pretty_print_indent(f, indent_level + 1)?;
        }

        Ok(())
    }
}

/// Context manager for a performance counter.
/// This is used to track when the measured function exits.
#[derive(Debug)]
pub struct PerfCounterGuard<'a> {
    /// Instant then the time measurement was started.
    start_time: std::time::Instant,
    counter: &'a mut PerfCounter,
}

impl<'a> PerfCounterGuard<'a> {
    /// Stop measuring the time and add the duration to the total spent time.
    pub fn stop(self) {
        // `drop()` will be called automatically now.
    }

    pub fn child(&mut self, name: &'static str) -> &mut PerfCounter {
        self.counter.sub_counters.entry(name).or_default()
    }

    #[must_use]
    pub fn start_child(&mut self, name: &'static str) -> PerfCounterGuard {
        let mut child = self.child(name);
        child.start()
    }

    /// Execute a function `f` and measure the spent time.
    /// Account the spent time to the specified child counter.
    pub fn measure_child<F, R>(&mut self, child_name: &'static str, f: F) -> R
    where
        F: FnOnce(&mut PerfCounterGuard) -> R,
    {
        let r = self.child(child_name).measure(f);
        r
    }
}

impl<'a> Drop for PerfCounterGuard<'a> {
    fn drop(&mut self) {
        let elapsed = self.start_time.elapsed();
        self.counter.total_time += elapsed;
    }
}

impl PerfCounter {
    /// Create a new performance counter.
    pub fn new() -> Self {
        Default::default()
    }

    /// Start measuring the spent time.
    /// Call `stop()` on the return value to stop the timer.
    /// Otherwise the timer runs until the guard exits the scope.
    #[must_use]
    pub fn start(&mut self) -> PerfCounterGuard {
        self.num_calls += 1;
        PerfCounterGuard {
            start_time: std::time::Instant::now(),
            counter: self,
        }
    }

    /// Execute a function `f` and measure the spent time.
    pub fn measure<F, R>(&mut self, f: F) -> R
    where
        F: FnOnce(&mut PerfCounterGuard) -> R,
    {
        let mut guard = self.start();
        let r = f(&mut guard);
        guard.stop();
        r
    }

    /// Execute a function `f` and measure the spent time.
    /// Account the spent time to the specified child counter.
    pub fn measure_child<F, R>(&mut self, child_name: &'static str, f: F) -> R
    where
        F: FnOnce(&mut PerfCounterGuard) -> R,
    {
        let mut guard = self.start();
        let r = guard.child(child_name).measure(f);
        guard.stop();
        r
    }
}

#[test]
fn test_nested_perf_counter() {
    use std::thread::sleep;
    use std::time::Duration;

    let mut counter = PerfCounter::new();
    {
        let mut guard1 = counter.start();
        {
            let mut child1 = guard1.child("child1");
            let guard = child1.start();
            sleep(Duration::from_millis(1));
        }
        {
            let mut child2 = guard1.child("child2");
            let guard = child2.start();
            sleep(Duration::from_millis(1));
            guard.stop();
            sleep(Duration::from_millis(1));
        }
    }

    assert!(counter.total_time >= Duration::from_millis(3));
    assert!(counter.sub_counters["child1"].total_time >= Duration::from_millis(1));
    assert!(counter.sub_counters["child2"].total_time >= Duration::from_millis(1));
}
