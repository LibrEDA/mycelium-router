// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Mutable context of a routing task and implementation of routing flow.

use num_traits::{FromPrimitive, ToPrimitive};
use rstar::RTree;

use crate::mycelium_detail_route_v2::perf_counter::PerfCounter;
use iron_shapes::prelude::{Geometry, IntoEdges};
use iron_shapes::CoordinateType;

use super::clip_worker::{ClipRoutingResult, ClipWorkerTask, GeometryData};
use super::geometric_objects::*;
use super::region_query::*;
use super::routing_graph::*;
use super::via_lookup_table::*;
use super::*;
use crate::pin_access_analysis::access_pattern::CellAccessPointReference;
use crate::pin_access_analysis::pin_access_oracle::{PinAccessOracle, PinAccessOracleBuilder};
use crate::task_scheduler::TaskScheduler;
use crate::track_assignment::TrackAssignmentRouter;

/// Context data of the routing algorithm.
pub struct RoutingTask<'a, LN, Rules, RoutingProblem, Drc>
where
    LN: db::L2NBase,
    LN::Coord: PrimInt + Signed + std::fmt::Debug,
{
    router: &'a MyceliumDR2<'a, LN, Rules, Drc>,

    routing_problem: RoutingProblem,
    routing_tracks: HashMap<LN::LayerId, pa::Tracks<LN::Coord>>,

    precomputed: PrecomputedRoutingData<LN>,

    /// Size of panels used for track assignment.
    /// Numbers of tracks per panel.
    track_assignment_panel_size: usize,

    /// Size of routing clips in terms of global routing cells.
    /// A routing clip will contain `clip_size * clip_size` global routing cells.
    clip_size: usize,

    /// Default cost increment for a node blocked by an object.
    object_cost: i32,
    /// Const increment for nodes covered by a DRC marker.
    drc_marker_cost: i32,
}

impl<'a, LN, Rules, RP, Drc> RoutingTask<'a, LN, Rules, RP, Drc>
where
    LN: db::L2NEdit,
    LN::Coord: PrimInt + Signed,
{
    /// Create a new routing task.
    pub fn new(
        router: &'a MyceliumDR2<'a, LN, Rules, Drc>,
        routing_problem: RP,
    ) -> RoutingTask<'a, LN, Rules, RP, Drc> {
        RoutingTask {
            router,
            routing_problem,
            routing_tracks: Default::default(),
            precomputed: PrecomputedRoutingData {
                dblayer2id: Default::default(),
                layerid2dblayer: Default::default(),
                via_lookup_table: Default::default(),
                via_definitions_by_cell_id: Default::default(),
                routing_guides: Default::default(),
                routing_guides_tree: Default::default(),
                iroutes: Default::default(),
                assigned_iroutes: Default::default(),
                assigned_iroutes_tree: Default::default(),
                preferred_routing_directions: Default::default(),
                pin_access_oracle: Default::default(),
                access_points_by_location: Default::default(),
                obstacles: RegionQuery::new(0),
            },
            // TODO: Choose good panel size. Reference paper says 50 times the height of a global routing cell.
            track_assignment_panel_size: 16,
            clip_size: 8,
            object_cost: 10,
            drc_marker_cost: 4,
        }
    }
}

impl<'a, LN, Rules, RP, Drc> RoutingTask<'a, LN, Rules, RP, Drc>
where
    LN: db::L2NEdit<Coord = db::SInt> + db::L2NBaseMT,
    LN::Coord: PrimInt + Signed + std::fmt::Debug + Sync + Send,
    Rules: db::RoutingLayerStack<LayerId = LN::LayerId>
        + db::RoutingRules<LayerId = LN::LayerId, Distance = LN::Coord>
        + Sync,
    RP: DetailRoutingProblem<LN> + Sync,
    Drc: DrcEngine<LN> + Sync,
{
    /// Perform some checks on input data and design rules.
    /// Used to abort routing if some inputs are not good.
    pub fn sanity_checks(&self) -> Result<(), ()> {
        let mut success = true;
        let chip = self.routing_problem.fused_layout_netlist();
        let rules = self.router.design_rules;
        let routing_layers = rules.routing_layer_stack();

        // Wire widths must be defined.
        {
            for l in &routing_layers {
                let default_width = rules.default_width(l, None);
                if default_width.is_none() {
                    log::error!(
                        "No default routing width defined for layer '{:?}'",
                        &chip.layer_info(l).name
                    );
                    success = false;
                }
            }
        }

        // Minimum spacing must be defined.
        {
            for l in &routing_layers {
                let min_spacing = rules.min_spacing_absolute(l);
                if min_spacing.is_none() {
                    log::error!(
                        "Minimum spacing is not defined for layer '{:?}'",
                        &chip.layer_info(l).name
                    );
                    success = false;
                }
            }
        }

        // Pitch must be defined.
        {
            for l in &routing_layers {
                let default_pitch = rules.default_pitch_preferred_direction(l);
                if default_pitch.is_none() {
                    log::error!(
                        "Default routing pitch is not defined for layer '{:?}'",
                        &chip.layer_info(l).name
                    );
                    success = false;
                }
            }
        }

        // Check the routing guides.
        // Each routing guide must be one connected piece.
        {
            let layer_stack = self.router.design_rules.routing_layer_stack();
            for (net, guide) in &self.precomputed.routing_guides {
                let connected_components_indices =
                    analyze_global_routes::connected_components(&layer_stack, guide);
                let num_connected_components = connected_components_indices.len();

                if num_connected_components != 1 {
                    log::warn!(
                        "Routing guide is not connected ({} parts), net: '{:?}'",
                        num_connected_components,
                        chip.net_name(net)
                    );
                }
            }
        }

        if success {
            Ok(())
        } else {
            Err(())
        }
    }

    /// Get the rectangular region which can be used for routing.
    pub(super) fn routing_region(&self) -> db::Rect<LN::Coord> {
        self.routing_problem
            .boundary()
            .and_then(|b| b.try_bounding_box())
            .expect("no routing boundary defined")
    }

    /// Subdivide the routing region in square-shaped 'clips'.
    fn create_clips(&self) -> Result<RoutingClips<LN::Coord, ()>, ()> {
        log::debug!("create routing clips");
        let routing_problem = &self.routing_problem;
        let chip = routing_problem.fused_layout_netlist();

        // Reconstruct global routing grid.
        let (global_grid_pitch, global_grid_bbox) = {
            let nets: Vec<_> = routing_problem
                .nets()
                .filter(|net| !chip.is_constant_net(net))
                .collect();

            log::debug!("reconstruct routing grid");
            let all_guide_shapes = nets
                .iter()
                .flat_map(|net| {
                    routing_problem
                        .routing_guide(net)
                        .map(|r| r.to_rectangles())
                        .into_iter()
                        .flatten()
                })
                .map(|(shape, _layer)| shape);

            let result =
                super::analyze_global_routes::reconstruct_global_routing_grid(all_guide_shapes);

            result.ok_or(()).map_err(|e| {
                log::error!("failed to reconstruct global routing grid");
                e
            })?
        };

        log::debug!("global routing grid pitch: {}", global_grid_pitch);
        log::debug!("global routing grid bbox: {:?}", global_grid_bbox);

        let clip_size = global_grid_pitch * LN::Coord::from_usize(self.clip_size).unwrap();

        let num_clips_x = ((global_grid_bbox.width() + clip_size.x - LN::Coord::one())
            / clip_size.x)
            .to_usize()
            .unwrap();

        let num_clips_y = ((global_grid_bbox.height() + clip_size.y - LN::Coord::one())
            / clip_size.y)
            .to_usize()
            .unwrap();

        log::debug!("Number of clips: {}x{}", num_clips_x, num_clips_y);

        assert!(
            global_grid_bbox.width() <= LN::Coord::from_usize(num_clips_x).unwrap() * clip_size.x,
            "clips must cover full routing region"
        );

        assert!(
            global_grid_bbox.height() <= LN::Coord::from_usize(num_clips_y).unwrap() * clip_size.y,
            "clips must cover full routing region"
        );

        let clips = RoutingClips::new(
            num_clips_x,
            num_clips_y,
            clip_size,
            global_grid_bbox.lower_left(),
        );

        Ok(clips)
    }

    fn create_clip_worker(
        &self,
        clip_index: (usize, usize),
        route_box: db::Rect<LN::Coord>,
    ) -> ClipWorkerTask<LN, Rules, RP, Drc> {
        let drc_box = route_box.sized_isotropic(self.drc_safe_distance()); // Make DRC box larger than route box.
        let ext_box = drc_box; // TODO: Make ext_box larger than route box.

        let mut worker = ClipWorkerTask {
            router: self.router,
            routing_problem: &self.routing_problem,
            routing_tracks: self.routing_tracks.clone(),
            precomputed: &self.precomputed,
            clip_index,
            route_box,
            drc_box,
            ext_box,
            local_routing_guides: Default::default(),
            local_subnet_routing_guides: Default::default(),
            subnets: Default::default(),
            geometry_data: GeometryData {
                routing_graph: None,
                node_costs: None,
                drc_region_search: RegionQuery::new(0),
            },
            object_cost: self.object_cost,
            drc_marker_cost: self.drc_marker_cost,
            net_data: Default::default(),
            non_obstacles: Default::default(),
            neighbour_routing_guides: Default::default(),
            obstacles: Default::default(),
        };

        worker
    }

    /// Compute maximal distance at which DRC interactions can happen.
    fn drc_safe_distance(&self) -> LN::Coord {
        let rules = self.router.design_rules;
        let routing_layers = rules.routing_layer_stack();

        let spacings = routing_layers
            .iter()
            .filter_map(|l| rules.min_spacing_absolute(l));

        // TODO: Include other design rules.

        spacings.max().unwrap_or(LN::Coord::zero())
    }

    /// Choose the ordering of the clips.
    /// Route congested clips first.
    fn sort_clip_indices_by_congestion(
        &self,
        clips: &RoutingClips<LN::Coord, ()>,
    ) -> Vec<(usize, usize)> {
        let mut clip_indices: Vec<_> = clips.all_clip_indices().collect();

        log::info!("Sort routing clips by congestion.");
        // Compute clip congestions.
        let clip_congestion: HashMap<_, _> = clip_indices
            .iter()
            .copied()
            .map(|clip_idx| {
                let route_box = clips.clip_shape(clip_idx);
                // Take the area covered by routing guides as a congestion metric.
                let guides = self
                    .precomputed
                    .routing_guides_tree
                    .locate_in_envelope_intersecting(&rstar::AABB::from_corners(
                        route_box.lower_left().into(),
                        route_box.upper_right().into(),
                    ));

                let clip_area =
                    route_box.width().to_i64().unwrap() * route_box.height().to_i64().unwrap();

                let total_guide_area: i64 = guides
                    // Get rectangle shapes of guides clipped with the routing box.
                    .filter_map(|guide_object| {
                        let rect = guide_object.shape;
                        rect.intersection(&route_box)
                    })
                    .map(|rect| {
                        // Compute area.
                        rect.width().to_i64().unwrap() * rect.height().to_i64().unwrap()
                    })
                    .fold(0, |acc, x| acc + x);

                // Compute relative area covered by routing guides.
                let relative_guide_area = 1000 * total_guide_area / clip_area; // Multiply by 1000 to get 'fix-point' numbers.

                (clip_idx, relative_guide_area)
            })
            .collect();

        clip_indices.sort_by_key(|clip_idx| -clip_congestion[clip_idx]);

        clip_indices
    }

    /// Main routing flow.
    pub fn do_route(mut self) -> Result<MyceliumDR2RoutingResult<LN>, RoutingError<LN>> {
        let routing_layers = self.router.design_rules.routing_layer_stack();
        let via_layers = self.router.design_rules.via_layer_stack();

        assert_eq!(
            routing_layers.len(),
            via_layers.len() + 1,
            "There must be exactly one more routing layer than via layers."
        );

        assert!(
            !self.routing_tracks.is_empty(),
            "No routing tracks defined, call `set_routing_tracks()` first."
        );

        // Measure time spent in functions.
        let mut perf_counter = PerfCounter::new();
        let mut perf_counter_guard = perf_counter.start();

        // Perform some sanity checks for early detection of errors.
        perf_counter_guard.measure_child("sanity_checks", |_| {
            self.sanity_checks().map_err(|_| {
                log::error!("Sanity checks failed. See log for details.");
                RoutingError::Other
            })
        })?;

        self.init_layer_mapping()?;
        perf_counter_guard.measure_child("initialization", |_| {
            self.init_region_search();
            self.init_via_lookup_table();
            self.init_viadef_table();
            self.init_routing_directions();
        });

        // Pin Analysis
        // Find good access points to pins.

        perf_counter_guard.measure_child("pin access analysis", |_| {
            self.do_pin_access_analysis();
        });

        perf_counter_guard.measure_child("initialization", |_| {
            self.precomputed.access_points_by_location = self.init_access_point_lookup_table();

            // Extract center-lines from global routes.
            // They will be used as 'iroutes' for track assignment.
            self.precomputed.routing_guides = self.init_routing_guides();
            // Enable fast region search of the global routes.
            self.init_routing_guides_tree();
            self.precomputed.iroutes = self.extract_iroutes_from_routing_guides();
        });

        // Do track assignment.

        {
            let g = perf_counter_guard.start_child("track assignment");
            self.precomputed.assigned_iroutes = self.do_track_assignment();
            g.stop();
        }

        perf_counter_guard.measure_child("initialization", |_| {
            self.init_iroutes_tree();
        });

        // Collect obstacles in an RTree.
        // The clip workers will fetch them from there.
        perf_counter_guard.measure_child("initialize obstacles", |_| {
            self.precomputed.obstacles = self.init_obstacles();
        });

        // Do maze routing in clips.
        let mut routing_result = {
            let mut routing_result = MyceliumDR2RoutingResult::new();

            let mut clips = match self.create_clips() {
                Ok(c) => c,
                Err(_) => {
                    log::error!("failed to create clips");
                    return Err(RoutingError::InitializationFailed);
                }
            };

            let clip_indices = self.sort_clip_indices_by_congestion(&clips);

            // // // For debugging, route only the first few clips.
            //let clip_indices: Vec<_> = clip_indices.into_iter().take(5).collect();

            let num_clips = clip_indices.len();

            // Setup tasks which can be run in parallel.
            let mut task_scheduler =
                perf_counter_guard.measure_child("create clip tasks", |_| {
                    let mut task_scheduler = TaskScheduler::new();

                    // Propagate the number of threads from the router configuration.
                    if self.router.max_num_threads > 0 {
                        task_scheduler.set_num_threads(std::num::NonZeroUsize::new(self.router.max_num_threads).unwrap());
                    }

                    let mut already_processed_clip_indices = HashSet::new();

                    for (i, clip_index) in clip_indices.iter().copied().enumerate() {
                        let route_box = clips.clip_shape(clip_index);

                        let clip_worker = self.create_clip_worker(clip_index, route_box);

                        task_scheduler.register_task(clip_index, move |_task_id, neighbour_results: HashMap<_, &Result<ClipRoutingResult<LN>, _>>| {

                            log::debug!("route clip {:?}", clip_index);

                            let mut neighbour_clip_results = HashMap::new();

                            // Resolve eventual errors in the dependencies.
                            for (dependency_clip_idx, clip_result) in neighbour_results {
                                match clip_result.as_ref() {
                                    Err(_) => {
                                        log::error!("abort routing clip {:?} because dependency failed (clip {:?})", clip_index, dependency_clip_idx);
                                        return Err(());
                                    }
                                    Ok(r) => {
                                        neighbour_clip_results.insert(dependency_clip_idx, r);
                                    }
                                }
                            }

                            let clip_result: Result<ClipRoutingResult<LN>, ()> = clip_worker.do_route(&neighbour_clip_results);

                            match &clip_result {
                                Err(_) => {
                                    log::error!("failed to route clip {:?}", clip_index);
                                }
                                _ => {}
                            };

                            clip_result
                        });

                        // Register dependencies.
                        // Tells which clips depend which other clips.
                        {
                            let neighbour_indices = {
                                let x = clip_index.0 as i32;
                                let y = clip_index.1 as i32;
                                (y - 1..y + 2)
                                    .flat_map(move |y| (x - 1..x + 2).map(move |x| (x, y)))
                                    .filter(move |(x, y)| *x >= 0 && *y >= 0)
                                    .filter(move |n| n != &(x, y))
                                    .map(|(x, y)| (x as usize, y as usize))
                            };

                            let dependencies = neighbour_indices
                                .filter(|idx| already_processed_clip_indices.contains(idx));

                            for dependency in dependencies {
                                task_scheduler.register_dependency(clip_index, dependency);
                            }
                        }

                        // Mark this clip as processed.
                        // The yet unrouted neighbours will now depend on its routing result.
                        already_processed_clip_indices.insert(clip_index);
                    }

                    task_scheduler
                });

            // Run tasks.
            log::info!("Start clip routing tasks");

            let results =
                perf_counter_guard.measure_child("detail routing", |_| task_scheduler.run());

            // Collect routing results of the clips.
            let mut clip_results = HashMap::new();

            // Store clip result.
            for (clip_idx, result) in results.into_iter() {
                let result = match result {
                    Err(_) => {
                        log::error!("failed to route clip {:?}", clip_idx);
                        return Err(RoutingError::Other);
                    }
                    Ok(r) => clip_results.insert(clip_idx, r),
                };
            }

            // Print performance statistics.
            {
                // Summarize performance statistics of all clips.
                let perf_stats = clip_results
                    .values()
                    .map(|r| r.performance_stats.clone())
                    .reduce(|acc, stats| clip_worker::ClipPerformanceStats::reduce(&acc, &stats));

                if let Some(perf_stats) = perf_stats {
                    log::info!("Detail routing performance: {:?}", perf_stats);
                }
            }

            // Store clip results in final routing result.
            routing_result.clip_routing_results = clip_results;

            routing_result
        };

        // Collect all via instances at pin access points.
        // Used for displaying them for debugging purpose.
        routing_result.debug_access_points = {
            let chip = self.routing_problem.fused_layout_netlist();

            let all_access_vias: Vec<(LN::PinId, db::Point<LN::Coord>, LN::LayerId, LN::CellId)> = {
                self.precomputed
                    .pin_access_oracle
                    .final_access_patterns
                    .iter()
                    .flat_map(|(cell_inst, cell_ap)| {
                        let inst_location = chip.get_transform(&cell_inst);

                        cell_ap.access_points.iter().map(move |(pin_id, ap)| {
                            let rel_location = ap.rel_location; // Relative location of access point.
                            let location =
                                rel_location + inst_location.transform_point(db::Point::zero()); // Absolute location of access point.

                            let via_def = &ap.possible_vias[0];
                            let via_cell = chip
                                .cell_by_name(via_def.via_name.as_str())
                                .expect("via cell not found");

                            (pin_id.clone(), location, ap.layer.clone(), via_cell)
                        })
                    })
                    .collect()
            };

            all_access_vias
        };
        routing_result.iroutes = self.precomputed.assigned_iroutes;
        routing_result.layer_mapping = self.precomputed.layerid2dblayer;
        routing_result.routing_guides = self.precomputed.routing_guides;

        perf_counter_guard.stop();
        log::info!("Spent time: {}", &perf_counter);

        Ok(routing_result)
    }

    /// Find good access points to pins.
    fn do_pin_access_analysis(&mut self) {
        log::info!("Run pin access analysis.");

        let builder = PinAccessOracleBuilder::new(
            self.routing_problem.fused_layout_netlist(),
            self.routing_problem.top_cell(),
            self.router.design_rules,
            self.router.drc_engine,
            Box::new(|cell| self.router.cell_outlines.get(cell).cloned()),
            self.router.via_definitions.clone(),
            self.routing_tracks.clone(),
        );

        let oracle = builder.build();

        // Store final access patterns.
        self.precomputed.pin_access_oracle = oracle;
    }

    /// Convert the routing guides into sets of rectangles.
    fn init_routing_guides(&self) -> HashMap<LN::NetId, Vec<(LN::LayerId, db::Rect<LN::Coord>)>> {
        let routing_problem = &self.routing_problem;
        let chip = routing_problem.fused_layout_netlist();

        let nets: Vec<_> = routing_problem
            .nets()
            .filter(|net| !chip.is_constant_net(net))
            .collect();

        log::debug!(
            "pre-process rectangular routing guides for {} nets",
            nets.len()
        );

        // Warn if there are nets without routing guides.
        {
            let nets_without_routing_guide: Vec<_> = nets
                .iter()
                .filter(|net| routing_problem.routing_guide(net).is_none())
                .collect();

            if !nets_without_routing_guide.is_empty() {
                log::warn!(
                    "Some nets have no routing guide: {}",
                    nets_without_routing_guide.len()
                );
                for net in nets_without_routing_guide {
                    if let Some(name) = chip.net_name(net) {
                        log::warn!("Net without routing guide: '{}'", name);
                    }
                }
            }
        }

        nets.into_iter()
            .filter(|net| routing_problem.routing_guide(&net).is_some())
            .map(|net| {
                log::debug!(
                    "pre-process routing guide of net '{:?}'",
                    chip.net_name(&net)
                );

                let routing_guide = routing_problem
                    .routing_guide(&net)
                    .expect("net has no routing guide");

                let rectangles = routing_guide.to_rectangles();

                // 1) Group by layers.
                let guide_shapes = {
                    let mut guide_shapes: FnvHashMap<LN::LayerId, Vec<db::Rect<LN::Coord>>> =
                        Default::default();
                    rectangles.into_iter().for_each(|(rect, layer)| {
                        guide_shapes.entry(layer).or_default().push(rect);
                    });
                    guide_shapes
                };

                // 2) Merge rectangles to polygons.
                let merged_guide_shapes = guide_shapes.into_iter().map(|(layer, rectangles)| {
                    let edges = rectangles
                        .iter()
                        .flat_map(|r| r.into_edges())
                        // Convert rectilinear edge to general edge.
                        .map(|redge| redge.into());

                    let merged = boolop::edges_boolean_op(
                        boolop::edge_intersection_integer,
                        edges,
                        std::iter::empty(),
                        boolop::Operation::Union,
                        boolop::PolygonSemantics::Union,
                    );

                    {
                        // Sanity check: number of polygons should not increase when merging them.
                        let num_polygons_before_merge = rectangles.len();
                        let num_polygons_after_merge = merged.len();
                        assert!(num_polygons_after_merge <= num_polygons_before_merge);
                    }

                    (layer, merged)
                });

                // 3) Create rectangular slices with direction in the preferred routing direction of the layer.
                let slices = merged_guide_shapes
                    .flat_map(|(layer, merged)| {
                        // Get preferred routing direction on the current layer.
                        let preferred_direction = self
                            .router
                            .design_rules
                            .preferred_routing_direction(&layer)
                            .unwrap_or_else(|| {
                                log::warn!(
                                    "No preferred routing direction specified for layer '{:?}'",
                                    chip.layer_info(&layer).name
                                );
                                Orientation2D::Horizontal
                            });

                        // Convert into rectilinear edges.
                        let redges = merged.all_edges_iter().map(|e| {
                            db::REdge::try_from(&e).expect("all edges must be rectilinear")
                        });

                        let slices = match preferred_direction {
                            Orientation2D::Horizontal => decompose_rectangles(redges),
                            Orientation2D::Vertical => decompose_rectangles_vertical(redges),
                        };

                        // let slices_wrong_direction = match preferred_direction {
                        //     Orientation2D::Vertical => decompose_rectangles(redges),
                        //     Orientation2D::Horizontal => decompose_rectangles_vertical(redges)
                        // };

                        slices.into_iter().map(move |slice| (layer.clone(), slice))
                    })
                    .collect();

                (net, slices)
            })
            .collect()
    }

    /// Store routing guides in an RTree for fast region query.
    fn init_routing_guides_tree(&mut self) {
        log::debug!(
            "number of routing guides: {}",
            self.precomputed.routing_guides.len()
        );

        let guide_objects: Vec<geometric_objects::RoutingGuide<_, _, _>> = self
            .precomputed
            .routing_guides
            .iter()
            .flat_map(|(net, guide)| {
                guide
                    .iter()
                    .map(move |(layer, rect)| geometric_objects::RoutingGuide {
                        shape: *rect,
                        layer: layer.clone(),
                        net_id: net.clone(),
                    })
            })
            .collect();

        self.precomputed.routing_guides_tree = RTree::bulk_load(guide_objects);
    }

    /// Store iroutes in an RTree for fast region query.
    fn init_iroutes_tree(&mut self) {
        let iroute_objects: Vec<geometric_objects::Iroute<_, _, _>> = self
            .precomputed
            .assigned_iroutes
            .iter()
            .flat_map(|(net, guide)| {
                guide
                    .iter()
                    .map(move |(layer, edge)| geometric_objects::Iroute {
                        edge: *edge,
                        layer: layer.clone(),
                        net_id: net.clone(),
                    })
            })
            .collect();

        self.precomputed.assigned_iroutes_tree = RTree::bulk_load(iroute_objects);
    }

    /// Pre-process the global routing guides (set of rectangles) to form
    /// 'iroutes' which are needed for the track assignment algorithm.
    fn extract_iroutes_from_routing_guides(
        &self,
    ) -> HashMap<LN::NetId, Vec<(LN::LayerId, db::REdge<LN::Coord>)>> {
        let routing_problem = &self.routing_problem;
        let chip = routing_problem.fused_layout_netlist();

        log::debug!("initialize iroutes from global routes");

        // Process the routing guide of each net.
        // Convert the routing guides into center lines.

        self.precomputed
            .routing_guides
            .iter()
            .map(|(net, slices)| {
                // 4) Get the center-lines of the slices.
                let center_lines: Vec<(LN::LayerId, db::REdge<LN::Coord>)> = slices
                    .iter()
                    .filter_map(|(layer, rectangle)| {
                        let direction = self
                            .router
                            .design_rules
                            .preferred_routing_direction(&layer)
                            .unwrap_or(Orientation2D::Horizontal);

                        let ll = rectangle.lower_left();
                        let ur = rectangle.upper_right();
                        let center = rectangle.center();

                        let two = LN::Coord::one() + LN::Coord::one();
                        // Shorten the center line by half of the guide width from both sides.
                        let ext = match direction {
                            Orientation2D::Horizontal => rectangle.height() / two,
                            Orientation2D::Vertical => rectangle.width() / two,
                        };

                        let center_line = match direction {
                            Orientation2D::Horizontal => {
                                db::REdge::new((ll.x + ext, center.y), (ur.x - ext, center.y))
                            }
                            Orientation2D::Vertical => {
                                db::REdge::new((center.x, ll.y + ext), (center.x, ur.y - ext))
                            }
                        };

                        // Drop center-lines of zero length
                        if center_line.end - center_line.start > Zero::zero() {
                            Some((layer.clone(), center_line))
                        } else {
                            None
                        }
                    })
                    .collect();

                (net.clone(), center_lines)
            })
            .collect()
    }

    /// Experimental: Use steiner minimal trees as global routes.
    fn create_iroutes_from_steiner_trees(
        &self,
    ) -> HashMap<LN::NetId, Vec<(LN::LayerId, db::REdge<LN::Coord>)>> {
        // Create steiner-tree routes.
        let chip = self.routing_problem.fused_layout_netlist();

        // Extract net terminal locations from the pin access oracle.
        let net_terminals: HashMap<_, Vec<_>> = self
            .routing_problem
            .nets()
            .map(|net| {
                // Get access location for each pin instance attached to the net.
                let terminals: Vec<_> = chip
                    .each_pin_instance_of_net(&net)
                    .map(|pin_inst| {
                        self.precomputed
                            .pin_access_oracle
                            .get_pin_access_location(chip, &pin_inst)
                    })
                    .collect();
                (net, terminals)
            })
            .collect();

        let steiner_lut = steiner_tree::gen_lut::gen_full_lut(8);

        log::info!("Generate steiner minimal trees.");

        let steiner_trees_2d = net_terminals.iter().map(|(net, terminals)| {
            let terminals_2d: Vec<_> = terminals
                .iter()
                .map(|(p, _layer)| steiner_tree::Point::new(p.x, p.y))
                .collect();

            let acurracy = 3;
            let (tree, _cost) = steiner_lut.rsmt_medium_degree(terminals_2d, acurracy);
            // Convert back to db::Point
            let edges = tree.into_iter().map(|(start, end)| {
                let p1 = db::Point::new(start.x, start.y);
                let p2 = db::Point::new(end.x, end.y);
                db::REdge::new(p1, p2)
            });

            (net, edges)
        });

        // Simple layer assignment:
        //
        // * Don't use the lowest layer
        // * Use layer according to routing direction.
        // * Use an upper layer for long edges.
        let layers = self.router.design_rules.routing_layer_stack();
        let steiner_trees_3d = steiner_trees_2d.map(|(net, edges)| {
            let edges_with_layer: Vec<_> = edges
                .map(|edge| {
                    let edge_length = edge.length();
                    let skip_layers = if edge_length < chip.dbu() / 10 {
                        0
                    } else if edge_length < chip.dbu() {
                        1
                    } else if edge_length < 10 * chip.dbu() {
                        2
                    } else if edge_length < 100 * chip.dbu() {
                        3
                    } else {
                        3
                    };

                    let possible_layers = layers
                        .iter()
                        .skip(skip_layers) // Skip the lowest routing layers.
                        .filter(|l| {
                            let pref_dir = self
                                .router
                                .design_rules
                                .preferred_routing_direction(l)
                                .expect("no preferred routing direction specified");
                            pref_dir.is_horizontal() == edge.orientation.is_horizontal()
                        });

                    let layer = possible_layers.cloned().next().expect("no layer found");

                    (layer, edge)
                })
                .collect();
            (net.clone(), edges_with_layer)
        });

        steiner_trees_3d.collect()
    }

    /// Create a lookup-table to find access point definitions by their location.
    /// This is used during detail routing to check if a via connects to an access point.
    fn init_access_point_lookup_table(
        &self,
    ) -> FnvHashMap<(db::Point<LN::Coord>, LayerId), CellAccessPointReference<LN>> {
        let chip = self.routing_problem.fused_layout_netlist();

        self.precomputed
            .pin_access_oracle
            .final_access_patterns
            .iter()
            .flat_map(|(cell_inst, cell_access_pattern)| {
                let inst_location = chip.get_transform(&cell_inst);

                cell_access_pattern.access_points.iter().enumerate().map(
                    move |(ap_index, (pin_id, ap))| {
                        let rel_location = ap.rel_location; // Relative location of access point.
                        let location =
                            rel_location + inst_location.transform_point(db::Point::zero()); // Absolute location of access point.
                                                                                             // Take favourite via.
                                                                                             // let via_def = &ap.possible_vias[0];
                                                                                             // let via_cell = chip.cell_by_name(via_def.via_name.as_str())
                                                                                             //     .expect("via cell not found");

                        let layer = self.precomputed.get_router_layer_id(&ap.layer);
                        let ap_reference = CellAccessPointReference {
                            cell_access_pattern: cell_access_pattern.clone(),
                            access_point_index: ap_index,
                        };
                        ((location, layer), ap_reference)
                    },
                )
            })
            .collect()
    }

    fn do_track_assignment(&self) -> HashMap<LN::NetId, Vec<(LN::LayerId, db::REdge<LN::Coord>)>> {
        let chip = self.routing_problem.fused_layout_netlist(); // Needed to extract blockages and get layer names for debugging output.
        let top_cell = self.routing_problem.top_cell();

        log::debug!("Prepare for track assignment.");

        let track_assigner = TrackAssignmentRouter::new(self.track_assignment_panel_size);

        // Convert iroutes into the required data structure.
        let iroutes_iter = self.precomputed.iroutes.iter().flat_map(|(net, routes)| {
            routes
                .iter()
                .map(move |(layer, edge)| (net.clone(), layer.clone(), *edge))
        });

        // Convert access points into the required data structure.
        let pins_iter = self
            .precomputed
            .pin_access_oracle
            .final_access_patterns
            .iter()
            .flat_map(|(cell_inst, cell_ap)| {
                let inst_location = chip.get_transform(&cell_inst);

                cell_ap.access_points.iter().map(move |(pin_id, ap)| {
                    let rel_location = ap.rel_location; // Relative location of access point.
                    let location = rel_location + inst_location.transform_point(db::Point::zero()); // Absolute location of access point.

                    let net = chip
                        .net_of_pin_instance(&chip.pin_instance(cell_inst, pin_id))
                        .expect("Pin with an access-point must be connected to a net.");

                    (net, location)
                })
            });

        log::info!("Run track assignment.");

        let assigned_iroutes = track_assigner.do_track_assignment(
            chip,
            &top_cell,
            &self.routing_tracks,
            iroutes_iter,
            pins_iter,
        );

        log::info!("Track assignment done.");

        assigned_iroutes
    }

    /// Set the per-layer definitions of routing tracks.
    pub fn set_routing_tracks(
        &mut self,
        routing_tracks: HashMap<LN::LayerId, pa::Tracks<LN::Coord>>,
    ) {
        self.routing_tracks = routing_tracks
    }

    /// Initialize the mapping between layer IDs used by the data base and [`LayerId`]
    /// used by the router.
    fn init_layer_mapping(&mut self) -> Result<(), RoutingError<LN>> {
        // self.precomputed.layerid2dblayer = self.router.design_rules.routing_layer_stack();
        self.precomputed.layerid2dblayer = {
            // TODO: Also include via layers.
            let layer_stack = self.router.design_rules.layer_stack();
            // Check that the routing layers start with a metal layer and alternate between via/metal layer.
            let is_metal_via_alternated = layer_stack.iter().enumerate().all(|(i, l)| {
                if i % 2 == 0 {
                    l.layer_type == db::RoutingLayerType::Routing
                } else {
                    l.layer_type == db::RoutingLayerType::Cut
                }
            });
            if !is_metal_via_alternated {
                log::error!("Routing layer stack must start with a metal layer and must have one via layer between two metal layers.");
                return Err(RoutingError::ConfigError);
            }

            layer_stack.into_iter().map(|l| l.id).collect()
        };

        // Create translation from data-base layer IDs to layer IDs used by the router.
        self.precomputed.dblayer2id = self
            .precomputed
            .layerid2dblayer
            .iter()
            .cloned()
            .enumerate()
            .map(|(i, layer)| (layer, LayerId(i as u8)))
            .collect();

        Ok(())
    }

    /// Initialize fast search for routing vias.
    fn init_via_lookup_table(&mut self) {
        self.precomputed.via_lookup_table =
            ViaLookupTable::new(self.router.via_definitions.iter().cloned());
    }

    /// Initialize lookup from cell ID of a via to a via definition struct.
    fn init_viadef_table(&mut self) {
        self.precomputed.via_definitions_by_cell_id = Default::default();
        let chip = self.routing_problem.fused_layout_netlist();

        for via_def in &self.router.via_definitions {
            if let Some(via_cell) = chip.cell_by_name(via_def.via_name.as_str()) {
                let old_entry = self
                    .precomputed
                    .via_definitions_by_cell_id
                    .insert(via_cell, via_def.clone());
                if old_entry.is_some() {
                    log::warn!("Multiple definitions of via '{}'", &via_def.via_name);
                }
            } else {
                log::error!(
                    "Via cell not found in layout but present in via definitions: {}",
                    &via_def.via_name
                );
            }
        }
    }

    /// Fill the region query structure with already present shapes from the layout.
    fn init_region_search(&mut self) {
        let routing_layers = self.router.design_rules.layer_stack_ids();
        let num_routing_layers = routing_layers.len();

        self.precomputed.obstacles = RegionQuery::new(num_routing_layers);
    }

    /// Use shapes from the DB as obstacles and store them in an RTree.
    fn init_obstacles(&self) -> RegionQuery<LN::Coord, LN::NetId> {
        log::trace!("init_obstacles()");
        let routing_layers = self.router.design_rules.layer_stack_ids();
        let chip = self.routing_problem.fused_layout_netlist();
        let top = self.routing_problem.top_cell();

        let mut obstacle_rtree = RegionQuery::new(routing_layers.len());

        // Get shapes on all routing layers.
        let mut shape_buffer = vec![]; // Collect shapes in this buffer.
        for layer in &routing_layers {
            shape_buffer.clear();

            let layer_id = self.precomputed.get_router_layer_id(layer);

            // Process all shapes on this layer (recurse into sub-cells).
            for_each_shape_recursive(
                chip,
                &top,
                layer,
                |transform, maybe_cell_inst, shape_id, geometry| {
                    // Move geometry to absolute position.
                    let transformed_geometry = geometry.transformed(&transform);

                    // Check if obstacle is a pin or assigned to a net.
                    // For DRC checks the obstacle must be associated with its net (if any).
                    let net = if let Some(cell_inst) = maybe_cell_inst {
                        if chip.parent_cell(&cell_inst) == top {
                            // Shape lives inside a sub-cell: Could be a pin instance.
                            let pin_inst = chip
                                .get_pin_of_shape(&shape_id)
                                .map(|pin| chip.pin_instance(&cell_inst, &pin));
                            // Find the net connected to the pin instance.
                            pin_inst.and_then(|pin_inst| chip.net_of_pin_instance(&pin_inst))
                        } else {
                            // Instance is deeply nested.
                            None
                        }
                    } else {
                        // Shape lives inside the top cell: Could be a pin or a specific net.
                        chip.get_net_of_shape(&shape_id)
                    };

                    shape_buffer.push((transformed_geometry, net));
                },
            );

            // Process the collected obstacle shapes.
            // Add the obstacle to the region query such that it will be respected for DRC checks.
            {
                let all_obstacles_on_layer = shape_buffer.drain(0..).flat_map(|(geometry, net)| {
                    // Decompose obstacle into rectangles.
                    let rectangles = geometry_to_rectangles(&geometry);

                    // Convert to obstacle structs.
                    rectangles.into_iter().map(move |r| {
                        GeometricObject::Obstacle(Obstacle {
                            shape: r,
                            net_id: net.clone(),
                        })
                    })
                });

                obstacle_rtree.rtrees[layer_id.0 as usize] =
                    RTree::bulk_load(all_obstacles_on_layer.collect());
            }
        }

        obstacle_rtree
    }

    /// Create lookup-table of preferred routing direction of each metal layer.
    fn init_routing_directions(&mut self) {
        self.precomputed.preferred_routing_directions = self
            .precomputed
            .layerid2dblayer
            .iter()
            .map(|l| self.router.design_rules.preferred_routing_direction(l))
            .collect();
    }

    /// Translate the layer ID of the data-base into a layer ID used by the router.
    ///
    /// `init_layer_mapping()` must have been called first.
    fn get_router_layer_id(&self, layer: &LN::LayerId) -> LayerId {
        self.precomputed.dblayer2id[layer]
    }

    /// Translate the layer ID used by the router into a layer ID used by the data-base.
    ///
    /// `init_layer_mapping()` must have been called first.
    fn get_db_layer_id(&self, layer: &LayerId) -> &LN::LayerId {
        &self.precomputed.layerid2dblayer[layer.0 as usize]
    }
}

/// Compute the hamming distance of two grid nodes.
pub(super) fn hamming_distance(n1: &Node, n2: &Node) -> usize {
    let (diff_x, diff_y, diff_z) = coord_diff(n1, n2);

    (diff_x as usize) + (diff_y as usize) + (diff_z as usize)
}

pub(super) fn coord_diff(n1: &Node, n2: &Node) -> (bool, bool, bool) {
    (
        n1.index_x() != n2.index_x(),
        n1.index_y() != n2.index_y(),
        n1.layer() != n2.layer(),
    )
}

/// From a path with rectilinear segments extract only the corner nodes (including start and end point).
pub(super) fn extract_path_corners<Crd>(
    routing_graph: &RoutingGraph<Crd>,
    path: &[Node],
) -> Vec<Node>
where
    Crd: PartialEq + Copy,
{
    // Sanity check:
    debug_assert!(
        path.windows(2).all(|w| hamming_distance(&w[0], &w[1]) <= 1),
        "path should have rectilinear segments only"
    );

    // Decompose the sequence of nodes into intervals.

    let mut segment_start = path[0];
    let mut segment_end = segment_start;
    let mut segments = vec![segment_start];
    for node in &path[1..] {
        let diff_x = segment_start.index_x() != node.index_x();
        let diff_y = segment_start.index_y() != node.index_y();
        let diff_z = segment_start.layer() != node.layer();

        if diff_z {
            // Create a via.
            segments.push(segment_end);
            segment_start = segment_end;
        }

        if diff_x && diff_y {
            // Create a path segment.
            segments.push(segment_end);
            segment_start = segment_end;
        }

        segment_end = *node;
    }

    // Last vertex of the path may need to be added.
    if segments.last() != path.last() {
        segments.extend(path.last());
    }
    segments.dedup(); // TODO: Make the algorithm correct instead of deduplicating the result.

    segments
}

/// Recursively iterate over all shapes in a cell.
/// `f` is called with tuples of the form `(transform of the shape relative to the top cell, the cell instance, ID of the shape, geometry of the shape)`
pub(super) fn for_each_shape_recursive<LN: db::L2NBase, F>(
    layout_netlist: &LN,
    cell: &LN::CellId,
    layer: &LN::LayerId,
    mut f: F,
) where
    F: FnMut(
        db::SimpleTransform<LN::Coord>,
        Option<&LN::CellInstId>,
        &LN::ShapeId,
        &db::Geometry<LN::Coord>,
    ) -> (),
{
    // This recursive iteration through the cells is implemented iteratively.
    // A plain recursive implementation is more difficult to handle due to the type system.

    // Stack for resolved recursion.
    let mut stack = Vec::new();
    stack.push((cell.clone(), None, db::SimpleTransform::identity()));

    while let Some((cell, inst, tf)) = stack.pop() {
        // Push child instances.
        layout_netlist.for_each_cell_instance(&cell, |inst| {
            let template = layout_netlist.template_cell(&inst);
            let transform = layout_netlist.get_transform(&inst);
            let tf2 = transform.then(&tf);
            stack.push((template, Some(inst), tf2));
        });

        // Process shapes of this cell.
        layout_netlist.for_each_shape(&cell, layer, |id, g| f(tf.clone(), inst.as_ref(), id, g));
    }
}

/// Decompose a geometric shape into rectangles.
///
// TODO: How to handle non-rectilinear shapes? For now they are skipped.
pub(super) fn geometry_to_rectangles<Coord>(geometry: &db::Geometry<Coord>) -> Vec<db::Rect<Coord>>
where
    Coord: PrimInt,
{
    match geometry {
        Geometry::Point(_) => vec![],
        Geometry::Edge(_) => vec![],
        Geometry::Rect(r) => vec![*r],
        Geometry::SimplePolygon(p) => {
            // Try to convert into rectliniear edges.
            let redges: Option<Vec<db::REdge<_>>> = p
                .edges_iter()
                .map(|e| db::REdge::try_from(&e).ok())
                .collect();

            if let Some(redges) = redges {
                decompose_rectangles(redges.into_iter())
            } else {
                log::warn!("failed to convert non-rectilinear shape into rectangles");
                vec![]
            }
        }
        Geometry::SimpleRPolygon(p) => decompose_rectangles(p.edges()),
        Geometry::Polygon(p) => {
            // Try to convert into rectliniear edges.
            let redges: Option<Vec<db::REdge<_>>> = p
                .all_edges_iter()
                .map(|e| db::REdge::try_from(&e).ok())
                .collect();

            if let Some(redges) = redges {
                decompose_rectangles(redges.into_iter())
            } else {
                log::warn!("failed to convert non-rectilinear shape into rectangles");
                vec![]
            }
        }
        Geometry::Path(p) => {
            // TODO
            unimplemented!("conversion of path to rectangles");
        }
        Geometry::Text(_) => vec![],
    }
}

/// Results of computation steps which are completed before the actual maze-routing.
/// This includes for example via-analysis and track assignment.
pub(super) struct PrecomputedRoutingData<LN>
where
    LN: db::L2NBase,
    LN::Coord: PrimInt + Signed + std::fmt::Debug,
{
    /// Lookup-table to convert layer IDs of the data-base into layer indices used by the router.
    pub(super) dblayer2id: FnvHashMap<LN::LayerId, LayerId>,
    /// Inverse lookup-table of `dblayer2id`.
    pub(super) layerid2dblayer: Vec<LN::LayerId>,

    /// For each metal layer a group of vias which connect to it.
    /// This vias are used for regular routing. Pin access points have their own list of possible vias.
    pub(super) via_lookup_table: ViaLookupTable<LN>,

    /// Find via definitions by the cell ID of the via.
    /// Used to quickly get via shapes based on the via ID.
    pub(super) via_definitions_by_cell_id:
        FnvHashMap<LN::CellId, Arc<pa::ViaDef<LN::Coord, LN::LayerId>>>,

    /// Routing guides in form of rectangles.
    pub(super) routing_guides: HashMap<LN::NetId, Vec<(LN::LayerId, db::Rect<LN::Coord>)>>,
    pub(super) routing_guides_tree:
        RTree<geometric_objects::RoutingGuide<LN::Coord, LN::NetId, LN::LayerId>>,

    /// Iroutes (skeletons of global routes)
    pub(super) iroutes: HashMap<LN::NetId, Vec<(LN::LayerId, db::REdge<LN::Coord>)>>,

    /// iroutes after track assignment
    pub(super) assigned_iroutes: HashMap<LN::NetId, Vec<(LN::LayerId, db::REdge<LN::Coord>)>>,
    /// track-assigned iroutes in an RTree for fast region search
    pub(super) assigned_iroutes_tree:
        RTree<geometric_objects::Iroute<LN::Coord, LN::NetId, LN::LayerId>>,

    /// Lookup-table for preferred routing directions.
    /// The index is the layer number as defined by a [`LayerId`].
    pub(crate) preferred_routing_directions: Vec<Option<db::Orientation2D>>,
    /// Selected access patterns to cell pins.
    pub(super) pin_access_oracle: PinAccessOracle<LN>,
    /// Lookup-table to find access points by their location and layer.
    /// Used during detail routing to select the correct via for access points.
    pub(super) access_points_by_location:
        FnvHashMap<(db::Point<LN::Coord>, LayerId), CellAccessPointReference<LN>>,

    /// Obstacle shapes, extracted from the DB.
    pub obstacles: RegionQuery<LN::Coord, LN::NetId>,
}

impl<LN: db::L2NBase> PrecomputedRoutingData<LN>
where
    LN::Coord: PrimInt + Signed + std::fmt::Debug,
{
    /// Translate the layer ID of the data-base into a layer ID used by the router.
    ///
    /// `init_layer_mapping()` must have been called first.
    pub fn get_router_layer_id(&self, layer: &LN::LayerId) -> LayerId {
        self.dblayer2id[layer]
    }

    /// Translate the layer ID used by the router into a layer ID used by the data-base.
    ///
    /// `init_layer_mapping()` must have been called first.
    pub fn get_db_layer_id(&self, layer: &LayerId) -> &LN::LayerId {
        &self.layerid2dblayer[layer.0 as usize]
    }
}

/// Defines the tiling of the routing region into square-shaped 'clips'.
struct RoutingClips<Coord, T> {
    // Number of routing clips in x-direction.
    num_x: usize,
    // Number of routing clips in y-direction.
    num_y: usize,
    // Dimensions of a single clip.
    clip_size: db::Rect<Coord>,
    // Lower-left corner of the routing region.
    lower_left: db::Point<Coord>,
    /// Data associated with a clip.
    clip_data: Vec<Option<T>>,
}

impl<Coord, T> RoutingClips<Coord, T>
where
    Coord: CoordinateType + FromPrimitive + ToPrimitive,
{
    pub fn new(
        num_x: usize,
        num_y: usize,
        clip_size: db::Vector<Coord>,
        lower_left: db::Point<Coord>,
    ) -> Self {
        Self {
            num_x,
            num_y,
            clip_size: db::Rect::new(db::Point::new(Zero::zero(), Zero::zero()), clip_size.into()),
            lower_left,
            clip_data: (0..num_x * num_y).map(|_| None).collect(),
        }
    }

    /// Get the number of clips.
    fn num_clips(&self) -> usize {
        self.num_x * self.num_y
    }

    /// Get upper right corner of the region covered by the clips.
    fn upper_right(&self) -> db::Point<Coord> {
        let x = Coord::from_usize(self.num_x).expect("failed to convert usize to coordinate");
        let y = Coord::from_usize(self.num_y).expect("failed to convert usize to coordinate");
        let w = self.clip_size.width();
        let h = self.clip_size.height();
        let v = db::Vector::new(w * x, h * y);
        self.lower_left + v
    }

    /// Get bounding-box of all clips.
    fn bbox(&self) -> db::Rect<Coord> {
        db::Rect::new(self.lower_left, self.upper_right())
    }

    /// Convert a 2D index into a 1D index which points into the data array.
    fn array_index(&self, index: (usize, usize)) -> usize {
        index.0 + index.1 * self.num_x
    }

    fn get_clip_data_mut(&mut self, index: (usize, usize)) -> Option<&mut T> {
        let idx = self.array_index(index);
        let maybe_data = self.clip_data.get_mut(idx)?;
        maybe_data.as_mut()
    }

    fn take_clip_data_mut(&mut self, index: (usize, usize)) -> Option<T> {
        let idx = self.array_index(index);
        let maybe_data = self.clip_data.get_mut(idx)?;
        maybe_data.take()
    }

    fn get_clip_data(&self, index: (usize, usize)) -> Option<&T> {
        let idx = self.array_index(index);
        let maybe_data = self.clip_data.get(idx)?;
        maybe_data.as_ref()
    }

    fn set_clip_data(&mut self, index: (usize, usize), data: T) {
        let idx = self.array_index(index);

        *self
            .clip_data
            .get_mut(idx)
            .expect("array index out of bounds") = Some(data);
    }

    /// Compute the lower-left corner of the clip with the given `index`.
    fn clip_offset(&self, index: (usize, usize)) -> db::Point<Coord> {
        let x = Coord::from_usize(index.0).expect("failed to convert usize to coordinate");
        let y = Coord::from_usize(index.1).expect("failed to convert usize to coordinate");
        let w = self.clip_size.width();
        let h = self.clip_size.height();

        self.lower_left + db::Vector::new(w * x, h * y)
    }

    /// Get the shape of a specific clip.
    fn clip_shape(&self, index: (usize, usize)) -> db::Rect<Coord> {
        self.clip_size.translate(self.clip_offset(index).v())
    }

    /// Get indices of clips which intersect the given rectangle.
    fn intersecting_clip_indices(
        &self,
        rect: db::Rect<Coord>,
    ) -> impl Iterator<Item = (usize, usize)> {
        let rect = rect
            .intersection(&self.bbox())
            .unwrap_or(db::Rect::new(self.lower_left, self.lower_left));

        let w = self.clip_size.width();
        let h = self.clip_size.height();

        let start_p = rect.lower_left() - self.lower_left;
        let end_p = rect.upper_right() - self.lower_left;

        let start_x = (start_p.x / w).to_usize().unwrap();
        let start_y = (start_p.y / h).to_usize().unwrap();
        let end_x = ((end_p.x + w - Coord::one()) / w).to_usize().unwrap();
        let end_y = ((end_p.y + h - Coord::one()) / h).to_usize().unwrap();

        (start_y..end_y).flat_map(move |y| (start_x..end_x).map(move |x| (x, y)))
    }

    /// Iterate over all clip indices.
    fn all_clip_indices(&self) -> impl Iterator<Item = (usize, usize)> {
        let num_x = self.num_x;
        let num_y = self.num_y;

        (0..num_x).flat_map(move |x| (0..num_y).map(move |y| (x, y)))
    }
}

#[test]
fn test_routing_clip_indices() {
    let clips: RoutingClips<_, ()> =
        RoutingClips::new(1, 2, db::Vector::new(10, 10), db::Point::new(0, 0));

    let actual_indices: Vec<_> = clips.all_clip_indices().collect();
    let expected_indices = vec![(0, 0), (0, 1)];
    assert_eq!(actual_indices, expected_indices);
}
