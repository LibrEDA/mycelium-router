// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Version 2 of the Mycelium detail router.
//!
//! [`MyceliumDR2`] is a maze router which supports parallelization and spacing rules.
//!
#![warn(unused_imports)]

mod analyze_global_routes;
mod clip_worker;
mod drc_checks;
mod geometric_objects;
mod irregular_tracks;
mod node_attribute;
mod perf_counter;
mod region_query;
mod route_one_net;
mod routing_graph;
mod routing_task;
mod via_lookup_table;

pub use libreda_drc::{DummyDrcEngine, SimpleDrcEngine};

use fnv::{FnvHashMap, FnvHashSet};
use iron_shapes_booleanop as boolop;

use std::collections::{HashMap, HashSet};
use std::convert::TryFrom;
use std::hash::Hash;
use std::marker::PhantomData;
use std::sync::Arc;

use db::traits::*;
use libreda_pnr::db;
use libreda_pnr::db::RoutingLayerStack;
use libreda_pnr::route::prelude::{
    DetailRouter, DetailRoutingProblem, DrawDetailRoutes, RoutingGuide,
};

use libreda_drc::DrcEngine;

use crate::pin_access_analysis as pa;

use iron_shapes::prelude::Orientation2D;
use iron_shapes_algorithms::prelude::{decompose_rectangles, decompose_rectangles_vertical};
use num_traits::{Num, One, PrimInt, Signed, Zero};
use smallvec::{smallvec, SmallVec};

use routing_graph::Node;

use crate::mycelium_detail_route_v2::clip_worker::ClipRoutingResult;
use routing_task::*;

/// Signed integer used for coordinates.
pub(crate) type Coord = i32;

/// Identifier for a net.
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub(super) struct NetId(pub(super) u32);

/// Identifier for a metal layer. The lowest layer has ID 0.
/// Used to keep track of IRoutes even when their location is shifted.
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
pub(super) struct LayerId(pub(super) u8);

impl LayerId {
    /// Get the layer index as an `usize`.
    pub fn to_usize(&self) -> usize {
        self.0 as usize
    }

    /// Check if this layer ID represents a via layer.
    pub fn is_via_layer(self) -> bool {
        self.0 % 2 == 1
    }

    /// Check if this layer ID represents a routing/metal layer.
    pub fn is_routing_layer(self) -> bool {
        self.0 % 2 == 0
    }
}

/// Mycelium detail router.
/// # Overview
/// The algorithm of this router consists of the following steps:
/// * pin-access analysis: Finds valid locations and vias which can be used to connect a wire to the pins of standard-cells or macros.
/// * Pre-processing global routes: The provided global routes get merged into larger rectangles and sliced in the preferred routing direction for each layer.
/// * Track assignment refines the global routes down to routing tracks which should be used for the wires.
/// * Create clips: Subdivides the chip into disjoint regions which will be used for parallel detail routing.
/// * Maze routing: Detail routing happens on the clip level. Each clip is routed on its own while respecting the results of already routed neighbour clips. The track assignment helps to know where nets should cross the clip boundaries.
///     * Design rule check: After maze routing the routes are checked for short circuits and spacing violations.
///     * Rip-up & re-route: Nets which have design-rule violations are routed again with adjusted costs such that they may avoid the violation.
///
/// # References
///
/// * This work is largely inspired by [TritonRoute](https://vlsicad.ucsd.edu/Publications/Journals/j133.pdf), [archived](https://web.archive.org/web/20221009090613/https://vlsicad.ucsd.edu/Publications/Journals/j133.pdf)
#[derive(Debug, Clone)]
pub struct MyceliumDR2<'a, LN: L2NIds, Rules, DrcEngine> {
    /// Rules used for detail routing. This includes the routing stack, spacing rules, preferred routing directions, default width and pitch.
    design_rules: &'a Rules,
    /// DRC checker used for pin-access analysis.
    drc_engine: &'a DrcEngine,
    /// A list of vias which can be used for detail routing.
    via_definitions: Vec<Arc<pa::ViaDef<LN::Coord, LN::LayerId>>>,
    /// Outlines of the used standard cells. This is only used for clustering the cells according to the standard-cell rows.
    cell_outlines: &'a HashMap<LN::CellId, db::Rect<LN::Coord>>,
    /// Allow using the metal shape of a pin to connect two or more parts of a net.
    allow_pin_feedthrough: bool,
    /// Allow accessing a pin from the same layer without useing a direct via.
    allow_planar_pin_access: bool,
    /// Maximum number of threads to use.
    /// When set to 0 the number of threads will be chosen automatically.
    max_num_threads: usize,
    /// Maximum number of iterations for DRC fixing.
    max_iter: usize,
}

impl<'a, LN, DR, DrcEngine> MyceliumDR2<'a, LN, DR, DrcEngine>
where
    LN: db::L2NBase<Coord = db::SInt>,
    DR: RoutingLayerStack,
{
    /// Create a new router engine based on given design rules.
    ///
    /// # Parameters
    /// * `design_rules`: Must provide necessary design rules such as the routing layer stack, spacing rules, default-width, preferred routing directions.
    /// * `drc_engine`: Algorithm for checking design rules. This is currently used only for pin-access analysis.
    /// * `via_definitions`: A list of vias which can be used for routing. Use for example [`super::extract_via_defs_from_lef`] to get such via definitions based on a LEF file.
    /// * `cell_outlines`: Abutment boxes of the standard-cells. This is used for detecting the arrangement of standard-cells during pin-access analysis.
    pub fn new(
        design_rules: &'a DR,
        drc_engine: &'a DrcEngine,
        via_definitions: Vec<Arc<pa::ViaDef<LN::Coord, LN::LayerId>>>,
        cell_outlines: &'a HashMap<LN::CellId, db::Rect<LN::Coord>>,
    ) -> Self {
        Self {
            design_rules,
            drc_engine,
            via_definitions,
            cell_outlines,
            allow_pin_feedthrough: false,
            allow_planar_pin_access: true,
            max_num_threads: 0,
            max_iter: 2,
        }
    }

    /// Set the maximal number of threads to use for detail routing.
    /// If set to zero then the maximal number of threads is chosen automatically.
    pub fn set_num_threads(&mut self, max_num_threads: usize) {
        self.max_num_threads = max_num_threads
    }
}

/// Result of detail routing.
pub struct MyceliumDR2RoutingResult<LN: db::L2NIds> {
    /// All access points. Used for displaying them in the layout for debugging purpose.
    debug_access_points: Vec<(LN::PinId, db::Point<LN::Coord>, LN::LayerId, LN::CellId)>,
    /// Routing guides for visualization.
    routing_guides: HashMap<LN::NetId, Vec<(LN::LayerId, db::Rect<LN::Coord>)>>,
    /// Center-lines of the global routes. Used for debugging purpose.
    iroutes: HashMap<LN::NetId, Vec<(LN::LayerId, db::REdge<LN::Coord>)>>,

    /// Detail routes of each clip.
    clip_routing_results: HashMap<(usize, usize), ClipRoutingResult<LN>>,

    /// Translation from layer IDs used by the router to layer IDs used by the data-base.
    layer_mapping: Vec<LN::LayerId>,
    /// Create text labels with the net name on wire segments.
    /// Used for inspecting and debugging.
    enable_net_labels: bool,
    /// Display routing guides for debugging.
    draw_routing_guides: bool,
    /// Display chosen pin access points for debugging.
    draw_debug_access_points: bool,
    /// Draw `iroutes` which mark the track assignment.
    draw_iroutes: bool,
    /// For debugging: Mark regions with DRC violations in the layout.
    draw_drc_hotspots: bool,
    /// For debugging: Draw the boundaries of the routing clips.
    draw_routing_clip_boxes: bool,
    /// For debugging: Draw shapes which form obstacles for all nets.
    draw_obstacles: bool,
    _ln: PhantomData<LN>,
}

/// An error encountered during routing or setup of routing.
pub enum RoutingError<LN: db::L2NBase> {
    /// Some configuration is wrong.
    ConfigError,
    /// Unspecified error. Check log output for details.
    Other,
    /// Something failed prior to actual detail routing.
    InitializationFailed,
    /// The routing did not complete, returns a partial result and a list of failed nets.
    RoutingFailed((MyceliumDR2RoutingResult<LN>, Vec<LN::NetId>)),
}

impl<LN: db::L2NBase> std::fmt::Display for RoutingError<LN> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use RoutingError::*;
        match self {
            ConfigError => write!(f, "Configuration error."),
            Other => write!(f, "Unspecified error."),
            InitializationFailed => write!(f, "Something failed prior to actual detail routing."),
            RoutingFailed((_, failed_nets)) => {
                write!(f, "Routing failed. {} failed nets.", failed_nets.len())
            }
        }
    }
}

impl<LN: db::L2NBase> MyceliumDR2RoutingResult<LN> {
    /// Empty result.
    fn new() -> Self {
        Self {
            debug_access_points: Default::default(),
            routing_guides: Default::default(),
            iroutes: Default::default(),
            clip_routing_results: Default::default(),
            layer_mapping: Default::default(),
            enable_net_labels: true,
            draw_routing_guides: true,
            draw_iroutes: true,
            draw_debug_access_points: true,
            draw_drc_hotspots: true,
            draw_routing_clip_boxes: true,
            _ln: Default::default(),
            draw_obstacles: true,
        }
    }
}

impl<LN: db::L2NEdit> DrawDetailRoutes<LN> for MyceliumDR2RoutingResult<LN> {
    type Error = ();

    fn draw_detail_routes(&self, chip: &mut LN, top: &LN::CellId) -> Result<(), Self::Error> {
        //let draw_single_net = "pad_net_wakeup_addr_i.7"; // For debugging draw only this net.

        // Draw access points (for debugging).
        if self.draw_debug_access_points {
            log::info!("Draw pin access points to layout.");
            for (pin_id, location, layer, via_cell) in &self.debug_access_points {
                // let via_inst = chip.create_cell_instance(top, via_cell, None);
                // // Set location of via.
                // chip.set_transform(&via_inst, db::SimpleTransform::translate(location.v()));

                // // Add text with pin name.
                let pin_name = chip.pin_name(pin_id);
                let text = db::Text::new(pin_name.to_string(), *location);
                chip.insert_shape(top, layer, text.into());
            }
        }

        // Draw routing guides (for debugging).
        if self.draw_routing_guides {
            log::info!("Draw routing guides to layout.");

            // Draw guides to separate cell.
            let guides_cell = chip.create_cell("__routing_guides".to_string().into());
            chip.create_cell_instance(top, &guides_cell, None);

            for (net, guide) in &self.routing_guides {
                //if Some(net) != chip.net_by_name(top, draw_single_net).as_ref() {
                //    continue;
                //}
                for (layer, rect) in guide {
                    let rect = *rect;
                    let shape_id = chip.insert_shape(&guides_cell, layer, rect.into());

                    // Associate shape with the net.
                    chip.set_net_of_shape(&shape_id, Some(net.clone()));
                }
            }
        }
        // Draw obstacles (for debugging).
        if self.draw_obstacles {
            log::info!("Draw obstacles to the layout.");

            // Draw to a separate cell.
            let cell = chip.create_cell("__routing_obstacles".to_string().into());
            chip.create_cell_instance(top, &cell, None);

            for (_, clip_routing_result) in &self.clip_routing_results {
                for (layer, shape) in &clip_routing_result.obstacles {
                    chip.insert_shape(&cell, &layer, (*shape).into());
                }
            }
        }

        // Draw center-lines of global routes (for debugging).
        if self.draw_iroutes {
            // Draw iroutes to separate cell.
            let iroutes_cell = chip.create_cell("__iroutes".to_string().into());
            chip.create_cell_instance(top, &iroutes_cell, None);

            for (net, iroutes) in &self.iroutes {
                //if Some(net) != chip.net_by_name(top, draw_single_net).as_ref() {
                //    continue;
                //}
                for (layer, redge) in iroutes {
                    // Convert iroute into a thin rectangle.
                    let rect = db::Rect::new(redge.start(), redge.end());
                    let one = LN::Coord::one();
                    let grow_direction = db::Vector::new(one, one);
                    let rect_sized = db::Rect::new(
                        rect.lower_left() - grow_direction,
                        rect.upper_right() + grow_direction,
                    );

                    let shape_id = chip.insert_shape(&iroutes_cell, layer, rect_sized.into());

                    // Associate shape with the net.
                    //chip.set_net_of_shape(&shape_id, Some(net.clone()));
                }
            }
        }

        if self.draw_drc_hotspots {
            // Draw markers to separate cell.
            let drc_marker_cell = chip.create_cell("__drc_hotspots".to_string().into());
            chip.create_cell_instance(top, &drc_marker_cell, None);

            for (clip_idx, clip_routing_result) in &self.clip_routing_results {
                for (layer_id, marker) in &clip_routing_result.drc_hotspots {
                    let _shape_id = chip.insert_shape(&drc_marker_cell, layer_id, (*marker).into());
                }
            }
        }

        if self.draw_routing_clip_boxes {
            // Draw markers to separate cell.
            let clip_cell = chip.create_cell("__routing_clips".to_string().into());
            let clip_layer = chip.create_layer(255, 1);
            chip.create_cell_instance(top, &clip_cell, None);

            for (clip_idx, clip_routing_result) in &self.clip_routing_results {
                let clip_box = clip_routing_result.clip_box;
                let _shape_id = chip.insert_shape(&clip_cell, &clip_layer, clip_box.into());
            }
        }

        // Draw detail routes.
        for (clip_idx, clip_routing_result) in &self.clip_routing_results {
            // Draw routes inside the clip.
            for (subnet, subnet_data) in &clip_routing_result.net_data {
                let net = &subnet.net;

                //if Some(net) != chip.net_by_name(top, draw_single_net).as_ref() {
                //    continue;
                //}

                // Draw wires.
                for wire_segment in &subnet_data.wires {
                    let rect = wire_segment.to_rectangle();
                    let layer_id = &self.layer_mapping[wire_segment.layer.to_usize()];
                    let shape_id = chip.insert_shape(top, layer_id, rect.into());

                    // Associate shape with the net.
                    chip.set_net_of_shape(&shape_id, Some(net.clone()));

                    if self.enable_net_labels {
                        if let Some(net_name) = chip.net_name(net) {
                            for p in [wire_segment.edge.start(), wire_segment.edge.end()] {
                                let text = db::Text::new(format!("{}", net_name), p);
                                chip.insert_shape(top, layer_id, text.into());
                            }
                        }
                    }
                }

                // Create vias.
                for via in &subnet_data.vias {
                    let via_inst = chip.create_cell_instance(top, &via.via_id, None);
                    chip.set_transform(&via_inst, db::SimpleTransform::translate(via.location.v()));
                }
            }
        }

        Ok(())
    }
}

/// An electrical pin which is used as a routing terminal.
#[derive(Debug, Default, Clone, Hash, PartialEq, Eq)]
struct Pin {
    /// Nodes in the grid graph which represent access-points to this pin.
    // Use smallvec: Most pins of standard-cells have only few access locations.
    nodes: SmallVec<[Node; 4]>,
}

impl Pin {
    fn new(nodes: SmallVec<[Node; 4]>) -> Self {
        assert!(!nodes.is_empty(), "pin must have at least one grid node");
        Self { nodes }
    }

    /// Get a representative node. Used as a summary of the pin location.
    fn center(&self) -> Node {
        self.nodes[0]
    }
}

/// Data of a (maybe) routed net.
pub(super) struct NetData<L: db::LayoutIds> {
    /// End-points for routing.
    pins: Vec<Pin>,
    /// Pins created at the clip boundary.
    boundary_pins: Vec<Pin>,
    /// Shapes which are an obstacle to all other nets but not to this net.
    non_obstacles: Vec<(LayerId, db::Rect<L::Coord>)>,
    pub(super) wires: Vec<WireSegment<L::Coord>>,
    pub(super) vias: Vec<WireVia<L::Coord, L::CellId>>,
}

impl<LN: db::L2NBase> NetData<LN> {
    fn new() -> Self {
        Self {
            pins: Default::default(),
            boundary_pins: Default::default(),
            non_obstacles: Default::default(),
            wires: Default::default(),
            vias: Default::default(),
        }
    }
}

/// Representation of a rectilinear metal wire which connects two nodes of the routing graph.
#[derive(Copy, Clone, Debug)]
pub(super) struct WireSegment<Coord> {
    /// Start and end-point of the wire segment.
    pub edge: db::REdge<Coord>,
    /// Half the width of the wire.
    pub half_width: Coord,
    /// Layer of the wire.
    pub layer: LayerId,
    /// Store the endpoints of the graph edge which forms the center line of this wire segment.
    /// Note: The graph nodes are only valid for in the grid graph of the correct routing clip.
    graph_nodes: [Node; 2],
}

impl<Coord> WireSegment<Coord>
where
    Coord: Copy + PartialOrd + Num,
{
    /// Get the rectangular shape of the wire segment.
    pub fn to_rectangle(&self) -> db::Rect<Coord> {
        db::Rect::new(self.edge.start(), self.edge.end()).sized(self.half_width, self.half_width)
    }
}

/// Representation of a via.
#[derive(Clone, Debug)]
pub(super) struct WireVia<Coord, ViaId> {
    /// Via type to be used.
    pub via_id: ViaId,
    /// Coordinates of the via center.
    pub location: db::Point<Coord>,
    /// Store the endpoints of the graph edge which forms the center line of this via.
    /// Note: The graph nodes are only valid for in the grid graph of the correct routing clip.
    graph_nodes: [Node; 2],
}

impl<'a, LN, Rules, Drc> MyceliumDR2<'a, LN, Rules, Drc>
where
    LN: db::L2NBase,
    Rules: db::RoutingLayerStack<LayerId = LN::LayerId>
        + db::RoutingRules<LayerId = LN::LayerId, Distance = LN::Coord>,
{
    /// Get the amount by which a rectangle needs to be bloated such that it covers
    /// all routing nodes which get occupied by it (includes minimum spacing).
    ///
    /// Returns a tuple `(bloating in x-directions, bloating in y-directions)`.
    fn bloat_extension(
        &self,
        layer: &LN::LayerId,
        shape: Option<&db::Rect<LN::Coord>>,
    ) -> (LN::Coord, LN::Coord) {
        // let (pitch_x, pitch_y) = self.router.design_rules.default_pitch(layer)
        //     .unwrap_or((Zero::zero(), Zero::zero()));

        let shape_length = shape.map(|r| {
            let h = r.height();
            let w = r.width();
            if h > w {
                h
            } else {
                w
            }
        });

        let w = self
            .design_rules
            .default_width(layer, shape_length)
            .or_else(|| self.design_rules.min_width(layer, shape_length))
            .unwrap_or(Zero::zero());

        let d = self
            .design_rules
            .min_spacing_absolute(layer)
            .unwrap_or(Zero::zero());

        let one = LN::Coord::one();
        let two = one + one;

        let ext = w / two + d;

        // Subtract one to exclude the boundary.
        let ext = if ext > Zero::zero() { ext - one } else { ext };

        let ext_x = ext;
        let ext_y = ext;

        // (pitch_x/two, pitch_y/two)
        (ext_x, ext_y)
    }

    /// Relaxed version of [`MyceliumDR2::bloat_extension`]. Does not account for spacing
    /// but only makes sure that enough space is kept free such that there's no short circuit.
    ///
    /// Get the amount by which a rectangle needs to be bloated such that it covers
    /// all routing nodes which get occupied by it.
    ///
    /// Returns a tuple `(bloating in x-directions, bloating in y-directions)`.
    fn bloat_extension_for_short_circuit(
        &self,
        layer: &LN::LayerId,
        shape: Option<&db::Rect<LN::Coord>>,
    ) -> (LN::Coord, LN::Coord) {
        // let (pitch_x, pitch_y) = self.router.design_rules.default_pitch(layer)
        //     .unwrap_or((Zero::zero(), Zero::zero()));

        let shape_length = shape.map(|r| {
            let h = r.height();
            let w = r.width();
            if h > w {
                h
            } else {
                w
            }
        });

        let w = self
            .design_rules
            .default_width(layer, shape_length)
            .or_else(|| self.design_rules.min_width(layer, shape_length))
            .unwrap_or(Zero::zero());

        let one = LN::Coord::one();
        let two = one + one;

        let ext = w / two;

        let ext_x = ext;
        let ext_y = ext;

        // (pitch_x/two, pitch_y/two)
        (ext_x, ext_y)
    }
}

impl<'a, LN, Rules, Drc> DetailRouter<LN> for MyceliumDR2<'a, LN, Rules, Drc>
where
    LN: db::L2NEdit<Coord = db::SInt> + db::L2NBaseMT,
    LN::Coord: PrimInt + Sync + Send,
    Rules: db::RoutingLayerStack<LayerId = LN::LayerId>
        + db::RoutingRules<LayerId = LN::LayerId, Distance = LN::Coord>
        + Sync,
    Drc: DrcEngine<LN> + Sync,
{
    type RoutingResult = MyceliumDR2RoutingResult<LN>;
    /// On error: return the successfully routed nets and a vector of unrouted nets.
    type Error = RoutingError<LN>;

    fn name(&self) -> String {
        "MyceliumDR2".into()
    }

    fn route<RP>(&self, routing_problem: RP) -> Result<Self::RoutingResult, Self::Error>
    where
        RP: DetailRoutingProblem<LN> + Sync,
    {
        let mut routing_task = RoutingTask::new(self, routing_problem);

        // Construct routing track definitions from design rules.
        // TODO: Pass routing tracks as arguments to the router.
        let routing_tracks =
            create_routing_tracks(self.design_rules, routing_task.routing_region());

        routing_task.set_routing_tracks(routing_tracks);

        routing_task.do_route()
    }

    fn route_with_pin_access_oracle(
        &self,
        routing_problem: impl DetailRoutingProblem<LN> + Sync,
        pin_access_oracle: &impl libreda_pnr::prelude::PinAccessOracle<LN>,
    ) -> Result<Self::RoutingResult, Self::Error> {
        let mut routing_task = RoutingTask::new(self, routing_problem);

        // Construct routing track definitions from design rules.
        // TODO: Pass routing tracks as arguments to the router.
        let routing_tracks =
            create_routing_tracks(self.design_rules, routing_task.routing_region());

        routing_task.set_routing_tracks(routing_tracks);
        // TODO routing_task.set_pin_access_oracle(Some(pin_access_oracle));

        routing_task.do_route()
    }
}

/// Create routing track definitions based on the routing pitch specified in the design rules.
pub fn create_routing_tracks<Rules>(
    design_rules: &Rules,
    routing_region: db::Rect<Rules::Distance>,
) -> HashMap<Rules::LayerId, pa::Tracks<Rules::Distance>>
where
    Rules: db::RoutingRules + db::RoutingLayerStack,
    Rules::LayerId: Hash + Eq,
{
    design_rules
        .routing_layer_stack()
        .into_iter()
        .map(|layer| {
            let pitch = design_rules
                .default_pitch_preferred_direction(&layer)
                .expect("no routing pitch defined");
            let orientation = design_rules
                .preferred_routing_direction(&layer)
                .expect("no routing direction defined");

            let tracks = pa::Tracks {
                region: routing_region,
                pitch,
                offset: Zero::zero(),
                orientation,
            };

            (layer, tracks)
        })
        .collect()
}
