// Copyright (c) 2022-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Estimate the routing capacities of the global routing cells based on the obstacle geometries.

use std::convert::TryFrom;

use libreda_pnr::libreda_db::iron_shapes;
use libreda_pnr::libreda_db::iron_shapes_booleanop;

use iron_shapes::prelude::{
    DoubledOrientedArea, IntoEdges, MultiPolygon, REdge, Rect, SimpleRPolygon, ToPolygon,
    TryBoundingBox, Vector,
};
use iron_shapes::CoordinateType;
// use iron_shapes_booleanop::{
//     edges_boolean_op, edge_intersection_integer, BooleanOp, Operation, PolygonSemantics,
// };

use super::GlobalRoutingGraph;
use iron_shapes::point::Point;
use iron_shapes_algorithms::rectangle_decomposition::decompose_rectangles;
use num_traits::{FromPrimitive, Num, PrimInt, ToPrimitive, Zero};
use std::collections::HashMap;
use std::hash::Hash;

use crate::graph::{GridGraph, GridGraphEdgeId, Node3D};

/// Estimate edge and node capacities of a global grid graph based on obstacle shapes.
pub fn estimate_gcell_capacities<T>(
    global_routing_graph: &mut GlobalRoutingGraph<T>,
    obstacles: &[(SimpleRPolygon<T>, u8)],
    routing_pitch: &[T],
    via_pitch: &[T],
) where
    T: PrimInt + Hash + std::fmt::Debug + FromPrimitive,
{
    log::info!(
        "Estimate GCell capacities on a {:?}x{:?} grid.",
        global_routing_graph.grid_graph().num_x(),
        global_routing_graph.grid_graph().num_y()
    );
    let num_layers = global_routing_graph.grid_graph().num_layers;
    // dbg!(global_routing_graph.grid_graph());
    assert_eq!(routing_pitch.len(), num_layers as usize);
    assert_eq!(via_pitch.len(), num_layers as usize);

    // Put shapes in bins. A bin spans all layers.
    let binned_shapes = put_shapes_in_bins(global_routing_graph.grid_graph(), obstacles);

    let mut gcell_capacities = HashMap::new();

    // Compute the capacities of each graph cell.
    for (bin_location, shapes) in binned_shapes {
        let bin_shape = bin_shape(global_routing_graph.grid_graph().grid_pitch, bin_location);

        for layer in 0..num_layers {
            // Select shapes on this layer.
            let shapes_on_layer = shapes
                .iter()
                .filter(|(_, l)| *l == layer)
                .map(|(shape, _layer)| shape);

            let relative_capacities = compute_bin_capacity(bin_shape, shapes_on_layer);

            let capacities = compute_absolute_bin_capacities(
                relative_capacities,
                bin_shape,
                routing_pitch[layer as usize],
                via_pitch[layer as usize],
            );

            let graph_node = Node3D(bin_location, layer);
            gcell_capacities.insert(graph_node, capacities);
        }
    }

    // Compute capacities of GCells without obstacles.
    // TODO: This overestimates the capacity.
    let default_capacity = {
        let grid_pitch = global_routing_graph.grid_graph().grid_pitch;
        let gcell_width = grid_pitch.x;
        let gcell_height = grid_pitch.y;
        let total_area = gcell_width * gcell_height;

        let total_area = gcell_width * gcell_height;
        let full_horizontal_capacity = gcell_height / routing_pitch[0];
        let full_vertical_capacity = gcell_width / routing_pitch[0];
        let full_via_capacity = total_area / via_pitch[0] / via_pitch[0];

        GCellCapacities {
            east: full_horizontal_capacity.to_u32().unwrap(),
            north: full_vertical_capacity.to_u32().unwrap(),
            west: full_horizontal_capacity.to_u32().unwrap(),
            south: full_vertical_capacity.to_u32().unwrap(),
            area: full_via_capacity.to_u32().unwrap(),
        }
    };

    global_routing_graph.update_capacities(&gcell_capacities, default_capacity);
}

#[test]
fn test_estimate_gcell_capacities() {
    let grid: GridGraph<i32> = GridGraph::new(
        Rect::new((0, 0), (100, 100)),
        3,
        Vector::new(10, 10),
        Vector::new(0, 0),
    );

    let mut graph = GlobalRoutingGraph::new(grid);

    let obstacles = vec![
        (Rect::new((0, 0), (5, 5)).into(), 0), // Block the lowest left GCell by a quarter of it's area.
    ];

    let routing_pitch = vec![1, 1, 1];
    let via_pitch = vec![1, 1, 1];

    estimate_gcell_capacities(&mut graph, &obstacles, &routing_pitch, &via_pitch);

    // Node capacity of non-blocked node.
    assert_eq!(
        graph.get_node_data(&Node3D::new(10, 0, 0)).capacity,
        (10 * 10 / via_pitch[0] / via_pitch[0]) as u32
    );

    // Node capacity of 1/4 blocked node.
    assert_eq!(
        graph.get_node_data(&Node3D::new(0, 0, 0)).capacity,
        (10 * 10 / via_pitch[0] / via_pitch[0] * 3 / 4) as u32
    );

    {
        // Check capacity between two cells where one cell border is half blocked.
        let a = Node3D::new(0, 0, 0);
        let b = Node3D::new(10, 0, 0);
        let edge = GridGraphEdgeId::new_undirected(a, b);
        assert_eq!(graph.get_edge_data(&edge).capacity, 5);
    }
}

/// Edge capacities. All values are relative to the total cell area.
#[derive(Copy, Clone, Debug)]
struct GCellCapacities<T> {
    /// Edge capacity.
    east: T,
    /// Edge capacity.
    north: T,
    /// Edge capacity.
    west: T,
    /// Edge capacity.
    south: T,
    /// Free area.
    area: T,
}

impl<T: PrimInt + FromPrimitive + Hash> GlobalRoutingGraph<T> {
    fn update_capacities(
        &mut self,
        capacities: &HashMap<Node3D<T>, GCellCapacities<u32>>,
        default_capacity: GCellCapacities<u32>,
    ) {
        // Update node capacities.
        for n in self.grid_graph().clone().all_nodes() {
            self.get_node_data_mut(&n).capacity =
                capacities.get(&n).unwrap_or(&default_capacity).area
        }

        // Update edge capacities.
        for e in self.grid_graph().clone().all_edges() {
            let (capacity_a, capacity_b) = {
                let (a, b) = e.into();
                assert!(a < b, "Nodes must be ordered.");
                let capacities_a = capacities.get(&a).unwrap_or(&default_capacity);
                let capacities_b = capacities.get(&b).unwrap_or(&default_capacity);

                let is_horizontal = a.0.y == b.0.y && a.1 == b.1;
                let is_vertical = a.0.x == b.0.x && a.1 == b.1;
                let is_via = a.1 != b.1;

                if is_horizontal {
                    (capacities_a.east, capacities_b.west)
                } else if is_vertical {
                    (capacities_a.north, capacities_b.south)
                } else {
                    debug_assert!(is_via);
                    (capacities_a.area, capacities_b.area)
                }
            };

            // The capacity of the graph edge is bounded by the two GCell boundary capacities.
            let edge_capacity = capacity_a.min(capacity_b);

            // Store the capacity.
            self.get_edge_data_mut(&e).capacity = edge_capacity;
        }
    }
}

/// Convert relative bin capacities into absolute capacities (number of routes which can pass).
fn compute_absolute_bin_capacities<T>(
    cap: GCellCapacities<T>,
    bin_shape: Rect<T>,
    routing_pitch: T,
    via_pitch: T,
) -> GCellCapacities<u32>
where
    T: Copy + PrimInt + ToPrimitive,
{
    // Need to convert to u64 to avoid overflows.
    let routing_pitch = routing_pitch.to_u64().unwrap();
    let via_pitch = via_pitch.to_u64().unwrap();
    let w = bin_shape.width().to_u64().unwrap();
    let h = bin_shape.height().to_u64().unwrap();

    let total_area = w * h;
    let full_horizontal_capacity = h / routing_pitch;
    let full_vertical_capacity = w / routing_pitch;
    let full_via_capacity = total_area / via_pitch / via_pitch;

    assert!(cap.north >= Zero::zero());
    assert!(cap.south >= Zero::zero());
    assert!(cap.east >= Zero::zero());
    assert!(cap.west >= Zero::zero());
    assert!(cap.area >= Zero::zero());

    let available_capacity_north =
        full_vertical_capacity * cap.north.to_u64().unwrap() / total_area;
    let available_capacity_south =
        full_vertical_capacity * cap.south.to_u64().unwrap() / total_area;
    let available_capacity_west =
        full_horizontal_capacity * cap.west.to_u64().unwrap() / total_area;
    let available_capacity_east =
        full_horizontal_capacity * cap.east.to_u64().unwrap() / total_area;
    let available_via_capacity = full_via_capacity * cap.area.to_u64().unwrap() / total_area;

    GCellCapacities {
        east: available_capacity_east.to_u32().unwrap(),
        north: available_capacity_north.to_u32().unwrap(),
        west: available_capacity_west.to_u32().unwrap(),
        south: available_capacity_south.to_u32().unwrap(),
        area: available_via_capacity.to_u32().unwrap(),
    }
}

/// Compute the relative routing capacities of the bin edges.
/// Capacities are given relative to the area. The value of the total bin area equals full capacity.
fn compute_bin_capacity<'a, T, S>(bin_bbox: Rect<T>, shapes: S) -> GCellCapacities<T>
where
    T: std::fmt::Debug + PrimInt + 'a,
    S: Iterator<Item = &'a SimpleRPolygon<T>>,
{
    // Clip to bin. Decompose into non-overlapping rectangles and then to boolean intersection of rectangles.
    let obstacle_edges = shapes.flat_map(|s| s.edges());
    let rectangles: Vec<Rect<T>> = decompose_rectangles(obstacle_edges);

    // Clip rectangles to the bin size.
    let clipped = rectangles.iter().filter_map(|r| r.intersection(&bin_bbox));

    // Area used by obstacles inside the bin.
    let used_area = clipped
        .clone()
        .map(|r| r.width() * r.height())
        .fold(T::zero(), |a, b| a + b);
    debug_assert!(used_area >= T::zero());
    let available_area = bin_bbox.width() * bin_bbox.height() - used_area;
    debug_assert!(available_area >= T::zero());

    // Get edges of clipped rectangles.
    let clipped_obstacle_edges = clipped.flat_map(|r| r.into_edges());

    let capacity_west = compute_edge_capacity(
        clipped_obstacle_edges.clone(),
        Vector::new(T::one(), T::zero()),
        bin_bbox,
    );
    let capacity_south = compute_edge_capacity(
        clipped_obstacle_edges.clone(),
        Vector::new(T::zero(), T::one()),
        bin_bbox,
    );
    let capacity_east = compute_edge_capacity(
        clipped_obstacle_edges.clone(),
        Vector::new(T::zero() - T::one(), T::zero()),
        bin_bbox,
    );
    let capacity_north = compute_edge_capacity(
        clipped_obstacle_edges.clone(),
        Vector::new(T::zero(), T::zero() - T::one()),
        bin_bbox,
    );

    GCellCapacities {
        east: capacity_east,
        north: capacity_north,
        west: capacity_west,
        south: capacity_south,
        area: available_area,
    }
}

#[test]
fn test_compute_bin_capacity_quarter_blocked() {
    let bin_bbox = Rect::new((0, 0), (10, 10));
    let obstacles = vec![Rect::new((5, -1000), (1000, 5)).into()];

    let edge_cap = compute_bin_capacity(bin_bbox, obstacles.iter());
    let bin_area = bin_bbox.width() * bin_bbox.height();

    assert_eq!(edge_cap.east, bin_area - bin_area / 2);
    assert_eq!(edge_cap.north, bin_area - bin_area / 4);
    assert_eq!(edge_cap.west, bin_area - bin_area / 4);
    assert_eq!(edge_cap.south, bin_area - bin_area / 2);
}

/// Return the area which is accessible by the given routing direction.
fn compute_edge_capacity<T, Edges>(obstacles: Edges, route_direction: Vector<T>, bbox: Rect<T>) -> T
where
    T: PrimInt + std::fmt::Debug,
    Edges: Iterator<Item = REdge<T>>,
{
    assert!(
        route_direction.x.is_zero() || route_direction.y.is_zero(),
        "Routing direction must be axis-aligned."
    );
    let v = route_direction * bbox.width().max(bbox.height());

    // Compute region which is blocked for routes of given direction.
    let unaccessible_region = minkowsky_sum(obstacles, v, bbox);

    let unaccessible_area: T = unaccessible_region
        .iter()
        .map(|r| r.width() * r.height())
        .fold(T::zero(), |a, b| a + b);

    debug_assert!(unaccessible_area >= T::zero());
    let total_area = bbox.width() * bbox.height();
    debug_assert!(unaccessible_area <= total_area);
    let accessible_area = total_area - unaccessible_area;
    debug_assert!(accessible_area >= T::zero());

    accessible_area
}

#[test]
fn test_compute_edge_capacity_no_obstacles() {
    let bin_bbox = Rect::new((0, 0), (10, 10));
    let obstacles = vec![
        Rect::new((100, 0), (110, 110)), // Shape does not intersect bin.
    ];

    let routing_direction = Vector::new(1, 0);

    let capacity_west = compute_edge_capacity(
        obstacles.iter().flat_map(|r| r.into_edges()),
        routing_direction,
        bin_bbox,
    );
    assert_eq!(capacity_west, bin_bbox.width() * bin_bbox.height())
}

#[test]
fn test_compute_edge_capacity_quarter_blocked() {
    let bin_bbox = Rect::new((0, 0), (10, 10));
    let obstacles = vec![Rect::new((5, 0), (10, 5))];

    let routing_direction = Vector::new(1, 0);

    let capacity_west = compute_edge_capacity(
        obstacles.iter().flat_map(|r| r.into_edges()),
        routing_direction,
        bin_bbox,
    );

    let bin_area = bin_bbox.width() * bin_bbox.height();
    // A quarter is blocked. 3/4 remain.
    assert_eq!(capacity_west, bin_area - bin_area / 4)
}

/// Compute the minkowsky sum of a rectilinear (!) multi-polygon and a vector,
/// clip the result to the `bbox` rectangle.
/// This 'smears' the polygon along the vector.
fn minkowsky_sum<T, Edges>(edges: Edges, v: Vector<T>, bbox: Rect<T>) -> Vec<Rect<T>>
where
    T: PrimInt + std::fmt::Debug,
    Edges: Iterator<Item = REdge<T>>,
{
    // let edges = poly.polygons.iter()
    //     .flat_map(|p| p.exterior.edges_iter());
    // // Convert to rectilinear edges.
    // let redges = edges.map(|e| {
    //     REdge::try_from(&e).expect("Must be a rectilinear edge.")
    // });

    // Decompose the multipolygon into non-overlapping rectangles.
    let rectangles = decompose_rectangles(edges);
    let minkowsky_sums = rectangles.iter().map(|r| {
        r.add_point(r.lower_left() + v)
            .add_point(r.upper_left() + v)
            .add_point(r.lower_right() + v)
            .add_point(r.upper_right() + v)
    });
    let clipped = minkowsky_sums.filter_map(|r| r.intersection(&bbox));

    let rectangles = decompose_rectangles(clipped.flat_map(|r| r.into_edges()));
    let clipped_rectangles = rectangles
        .iter()
        .filter_map(|r| r.intersection(&bbox))
        .collect();

    clipped_rectangles

    // Same but use full boolean operation algorithm (which is complicated and broken):
    // // Compute the minkowsky sum of edges separately.
    // let minkowsky_sum_of_edges = redges.map(|e| {
    //
    //     let a = e.start();
    //     let b = e.end();
    //     dbg!(a, b);
    //     Rect::new(a, b)
    //         .add_point(a + v)
    //         .add_point(b + v)
    // });
    // // Remove zero-area rectangles to speed-up boolean operation.
    // let minkowsky_sum_of_edges = minkowsky_sum_of_edges.filter(|r| {
    //     let diagonal = r.upper_right() - r.lower_left();
    //     let has_zero_area = diagonal.x.is_zero() || diagonal.y.is_zero();
    //     !has_zero_area
    // });
    // // Convert to polygons.
    // let polygons: Vec<_> = minkowsky_sum_of_edges.map(|r| r.to_polygon()).collect();
    // dbg!(&polygons);
    //
    //
    // // Compute the boolean union of the partial minkowsky sums and clip to the bounding box.
    // let minkowsky_sum = boolean_op(
    //     edge_intersection_integer,
    //     polygons.iter(),
    //     std::iter::once(&bbox.to_polygon()),
    //     Operation::Intersection,
    //     PolygonSemantics::Union,
    // );
    // dbg!(&minkowsky_sum);
    //
    // minkowsky_sum
}

#[test]
fn test_compute_bin_capacity_no_intersecting_shape() {
    let bin_bbox = Rect::new((0, 0), (10, 10));
    let shapes = vec![
        Rect::new((100, 100), (110, 110)).into(), // Shape does not intersect bin.
    ];

    let cap = compute_bin_capacity(bin_bbox, shapes.iter());
}

/// Get the rectangular shape of a bin (also known as GCell) at a certain grid location.
fn bin_shape<T>(grid_pitch: Vector<T>, bin_center: Point<T>) -> Rect<T>
where
    T: Copy + Num + PartialOrd,
{
    let _1 = T::one();
    let _2 = _1 + _1;

    let half_pitch = grid_pitch / _2;
    let lower_left = bin_center - half_pitch;
    Rect::new(lower_left, lower_left + grid_pitch)
}

/// Associate all shapes with cells of the grid graph.
/// A bin spans all layers.
fn put_shapes_in_bins<'a, T: PrimInt + Hash>(
    grid: &GridGraph<T>,
    shapes: &'a [(SimpleRPolygon<T>, u8)],
) -> HashMap<Point<T>, Vec<&'a (SimpleRPolygon<T>, u8)>> {
    let mut bins = HashMap::new();

    // Associate shapes with overlapping bin centers.
    let binned_shapes = shapes.into_iter().flat_map(|shape| {
        let (s, layer) = shape;
        s.try_bounding_box()
            .into_iter()
            .flat_map(move |bbox| find_overlapping_grid_cells(grid, bbox).map(move |p| (p, shape)))
    });

    // Fill the bins.
    for (bin, shape) in binned_shapes {
        bins.entry(bin).or_insert(vec![]).push(shape);
    }

    debug_assert!(bins.keys().all(|p| grid.contains_point(*p)));

    bins
}

/// Find center points of grid cells which touch the rectangle.
fn find_overlapping_grid_cells<T: PrimInt>(
    grid: &GridGraph<T>,
    rect: Rect<T>,
) -> impl Iterator<Item = Point<T>> + '_ {
    let _1 = T::one();
    let _2 = _1 + _1;

    // Bloat the rectangle.
    let half_pitch = grid.grid_pitch / _2;
    let rect = Rect::new(
        rect.lower_left - half_pitch,
        rect.upper_right + grid.grid_pitch,
    );

    grid.all_nodes_xy(&rect)
}
