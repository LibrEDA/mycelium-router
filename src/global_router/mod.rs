// Copyright (c) 2022-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! A global router finds approximate routes which facilitate the work of the detail router.

#[derive(Debug, Default, Copy, Clone)]
pub struct NodeData {
    pub capacity: u32,
    pub usage: u32,
}

#[derive(Debug, Default, Copy, Clone)]
pub struct EdgeData {
    pub capacity: u32,
    pub usage: u32,
}

pub type GlobalRoutingGraph<T> = crate::graph::grid_graph::GridGraphWithData<T, NodeData, EdgeData>;

pub mod capacity_estimation;
