// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Abstractions for single-signal routers and multi-signal routers which operate on graph structures.

use num_traits::Num;
use std::collections::HashMap;

/// Representation of the routing result for a single net.
pub trait RoutedSignal
where
    Self: Sized,
{
    /// Type of a graph node.
    type GraphNode: Copy;
    /// Cost type of graph edges and graph nodes.
    type Cost: Copy + PartialEq + Num;

    /// Iterator over graph edges used by the route.
    type EdgeIter: Iterator<Item = (Self::GraphNode, Self::GraphNode)>;
    /// Iterator over graph nodes used by the route.
    type NodeIter: Iterator<Item = Self::GraphNode>;

    /// Iterate over all graph edges used by this route.
    fn edges(&self) -> Self::EdgeIter;
    /// Iterate over all graph nodes used by this route.
    fn nodes(&self) -> Self::NodeIter;

    /// Get the total cost of this route.
    fn total_cost(&self) -> Self::Cost;

    /// Merge two partial routes.
    fn merge(self, other: Self) -> Self;

    /// Create a trivial route which covers only a single node.
    fn from_node(node: Self::GraphNode) -> Self;

    /// Create an empty route.
    fn empty() -> Self;
}

/// Router for a single signal with multiple terminals.
pub trait SignalRouter {
    /// Graph datatype.
    type Graph;
    /// Graph node type.
    type GraphNode;
    /// Representation of a routed signal.
    type RoutingResult: RoutedSignal<GraphNode = Self::GraphNode, Cost = Self::Cost>;
    /// Edge and node cost types.
    type Cost: Copy + PartialOrd + Num;

    /// Enable or disable pin feed-through.
    /// If pin feed-through is enabled, then two subnets can be connected by attaching to two different nodes of the same pin.
    fn enable_pin_feed_through(&mut self, enable_feed_through: bool);

    /// Check if router has pin feed-through enabled.
    fn is_pin_feed_through_enabled(&self) -> bool;

    /// Connect all terminals of the signal.
    ///
    /// # Parameters
    /// * `graph`: The routing graph.
    /// * `node_cost_fn`: Function which tells the cost of a node.
    /// * `edge_cost_fn`: Function which tells the cost of an edge.
    /// * `terminals`: List of pins that need to be connected. A pin consists of one or more graph nodes which are considered to be connected already.
    /// * `previous_solution`: A previous solution to the routing problem with the same terminals but potentially different costs.
    /// This is intended to be used as a hint for accelerated graph search.
    fn route<NodeCostFn, EdgeCostFn>(
        &self,
        graph: &Self::Graph,
        node_cost_fn: &NodeCostFn,
        edge_cost_fn: &EdgeCostFn,
        terminals: &Vec<Vec<Self::GraphNode>>,
        previous_solution: Option<&Self::RoutingResult>,
    ) -> Option<Self::RoutingResult>
    where
        NodeCostFn: Fn(&Self::GraphNode) -> Self::Cost,
        EdgeCostFn: Fn(&Self::GraphNode, &Self::GraphNode) -> Self::Cost;
}

/// Router which finds routes for multiple signals on a graph while respecting edge and node capacity constraints.
pub trait MultiSignalRouter {
    /// Graph datatype.
    type Graph;
    /// Graph node type.
    type GraphNode;
    /// Identifier of a net.
    type NetId: Clone;
    /// Representation of a routed signal.
    type RoutingResult: RoutedSignal<GraphNode = Self::GraphNode, Cost = Self::Cost>;
    /// Edge and node cost types.
    type Cost: Copy + PartialOrd + Num;

    /// Enable or disable pin feed-through.
    /// If pin feed-through is enabled, then two subnets can be connected by attaching to two different nodes of the same pin.
    fn enable_pin_feed_through(&mut self, enable_feed_through: bool);

    /// Check if router has pin feed-through enabled.
    fn is_pin_feed_through_enabled(&self) -> bool;

    /// Find routes for multiple nets while respecting capacity constraints.
    fn route_multisignal<NodeCostFn, EdgeCostFn>(
        &self,
        graph: &Self::Graph,
        node_cost_fn: &NodeCostFn,
        edge_cost_fn: &EdgeCostFn,
        node_capacity_fn: &(dyn Fn(&Self::GraphNode) -> u32 + Sync),
        edge_capacity_fn: &(dyn Fn(&Self::GraphNode, &Self::GraphNode) -> u32 + Sync),
        can_use_node_fn: &(dyn Fn(&Self::NetId, &Self::GraphNode) -> bool + Sync),
        nets: &Vec<(Self::NetId, Vec<Vec<Self::GraphNode>>)>,
        net_weights: &HashMap<Self::NetId, f64>,
    ) -> Result<Vec<(Self::NetId, Self::RoutingResult)>, ()>
    where
        NodeCostFn: Fn(&Self::GraphNode) -> Self::Cost + Sync,
        EdgeCostFn: Fn(&Self::GraphNode, &Self::GraphNode) -> Self::Cost + Sync;
}
