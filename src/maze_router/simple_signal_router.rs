// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Route single signals in a graph using a maze-routing algorithm.

use fnv::FnvHashMap;
use itertools::Itertools;
use num_traits::{Num, PrimInt, Zero};

use iron_shapes::prelude::{REdge, Rect, TryIntoBoundingBox, Vector};

use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::hash::Hash;
use std::io::Write;
use std::marker::PhantomData;

use crate::visualize::TermPlot;

use super::router_traits::{RoutedSignal, SignalRouter};
use crate::graph::{GridGraph, Node3D, OffGridGraph, TraverseGraph};

/// Route a single signal with multiple terminals.
/// Assume that the first terminal is the signal source, i.e. the driver.
/// Routes from the driver to the other terminals are found sequentially.
/// Start with the terminal that is closest to the source.
fn route_single_signal_from_source<FE, FH, G, T, Cost>(
    graph: &G,
    pins: &Vec<Vec<Node3D<T>>>,
    wire_half_width: T,
    min_spacing: T,
    edge_cost_fn: &FE,
    heuristic_fn: &FH,
) -> Option<Vec<Vec<Node3D<T>>>>
where
    T: PrimInt + Hash,
    Cost: Num + Copy + PartialOrd + Ord,
    G: TraverseGraph<Node3D<T>>,
    FE: Fn(&Node3D<T>, &Node3D<T>) -> Cost,
    FH: Fn(&Node3D<T>, &Node3D<T>) -> Cost,
{
    if pins.len() <= 1 {
        return Some(vec![]);
    }

    let mut sources = pins[0].clone();

    assert!(!sources.is_empty(), "List of source nodes cannot be empty.");

    // Find all non-empty sinks.
    let mut sinks = pins[1..].iter().filter(|p| !p.is_empty()).collect_vec();

    // Sort the sinks by ascending distance to the source (take the lower bound of the distance as
    // given by the heuristic function).
    sinks.sort_by_cached_key(|sink| {
        // Find the minimal distance between the sink nodes and source nodes.
        let distances = sink.iter().flat_map(|sink_node| {
            sources
                .iter()
                .map(move |source_node| heuristic_fn(sink_node, source_node))
        });
        let min_distance = distances.min().unwrap(); // Unwrap is safe because sources and all sinks are not empty by assertion before.
        min_distance
    });

    // Create buffer to store found paths.
    let mut paths = Vec::new();

    for sink in sinks {
        let path = route_single_source_sink_pair(
            graph,
            &sources,
            sink,
            wire_half_width,
            min_spacing,
            edge_cost_fn,
            heuristic_fn,
        );
        if let Some(path) = path {
            sources.extend(&path);
            paths.push(path);
        } else {
            // Failed to find route.
            return None;
        }
    }

    Some(paths)
}

/// Connect all pins of a single signal.
/// Return a list of paths that create the routing tree.
fn route_single_signal_with_net_decomposition<FE, FH, G, T>(
    graph: &G,
    pins: &Vec<Vec<Node3D<T>>>,
    wire_half_width: T,
    min_spacing: T,
    edge_cost_fn: &FE,
    heuristic_fn: &FH,
) -> Option<Vec<Vec<Node3D<T>>>>
where
    T: PrimInt + Hash,
    G: TraverseGraph<Node3D<T>>,
    FE: Fn(&Node3D<T>, &Node3D<T>) -> T,
    FH: Fn(&Node3D<T>, &Node3D<T>) -> T,
{
    // Create local mutable copy of the pins.
    // Copy only pins which have terminals.
    let mut pins = pins
        .iter()
        .filter(|terms| !terms.is_empty())
        .cloned()
        .collect_vec();

    // Create buffer to store found paths.
    let mut paths = Vec::new();

    // As long as there is more than one pin left, connect the two closest pins and merge them.
    while pins.len() > 1 {
        // Find the two subnets which are closest (by manhattan distance of their bounding boxes).
        let (pin_id_1, pin_id_2) = {
            // Find bounding boxes of the pins.
            let pin_bounding_boxes: Vec<_> = pins
                .iter()
                .map(
                    |pin_locations| {
                        pin_locations
                            .iter()
                            .map(|Node3D(point, _layer)| point)
                            .try_into_bounding_box()
                            .unwrap()
                    }, // Pins without shapes have been removed already, to there should be no case without bounding box.
                )
                .collect();

            crate::multi_pin_decomposition::find_closest_rectangles(&pin_bounding_boxes).unwrap()
        };

        // Sanity check.
        assert_ne!(pin_id_1, pin_id_2, "Need two different pins.");

        let closest_subnets = vec![pins[pin_id_1].clone(), pins[pin_id_2].clone()];

        // Try to find a route between the closest pins.
        let path = route_single_signal_closest_terminals(
            graph,
            &closest_subnets,
            wire_half_width,
            min_spacing,
            edge_cost_fn,
            heuristic_fn,
        );

        if let Some((_, _, path)) = path {
            // Found a path between closest pins.

            // Merge the nodes of the connected pins. They form now a new pin.
            let pins2 = std::mem::replace(&mut pins[pin_id_2], vec![]);
            pins[pin_id_1].extend(pins2);

            // Construct new pin nodes.
            // Nodes of the two connected pins are now merged.
            pins.remove(pin_id_2);

            // Remember this route.
            paths.push(path);
        } else {
            // Routing failed.
            return None;
        }
    }

    Some(paths)
}

/// Very simple router for single nets.
/// Creates a routing tree between terminals.
///
pub struct SimpleSignalRouter<T, Graph, Cost> {
    /// Width of a wire.
    pub wire_half_width: T,
    /// Minimum spacing between routes.
    pub min_spacing: T,
    pin_feed_through_enabled: bool,
    _graph: PhantomData<Graph>,
    _cost: PhantomData<Cost>,
}

impl<T, Graph, Cost> SimpleSignalRouter<T, Graph, Cost>
where
    T: Zero,
{
    /// Create a new router instance.
    /// Wire width and minimum spacing are set to zero.
    pub fn new() -> Self {
        Self {
            wire_half_width: T::zero(),
            min_spacing: T::zero(),
            _graph: Default::default(),
            _cost: Default::default(),
            pin_feed_through_enabled: false,
        }
    }
}

/// Store the route of a net.
#[derive(Debug, Clone)]
pub struct SimpleSignalRouterResult<Node, Cost> {
    /// List of tree segments which make the routes between the net terminals.
    pub routes: Vec<Vec<Node>>,
    /// Cost of the routing tree.
    pub total_cost: Cost,
}

impl<Node, Cost> SimpleSignalRouterResult<Node, Cost>
where
    Node: Hash + Eq + Copy,
{
    /// Create a lookup table to quickly find the segments which contains a node.
    fn create_node_to_segment_lookup_table(&self) -> FnvHashMap<Node, Vec<&Vec<Node>>> {
        let mut segments_by_node: FnvHashMap<Node, Vec<&Vec<Node>>> = Default::default();

        for segment in &self.routes {
            for node in segment {
                segments_by_node
                    .entry(*node)
                    .or_insert(vec![])
                    .push(segment);
            }
        }

        segments_by_node
    }
}

impl<N, Cost> RoutedSignal for SimpleSignalRouterResult<N, Cost>
where
    N: Ord + Hash + Eq + Copy,
    Cost: Num + Copy,
{
    type GraphNode = N;
    type Cost = Cost;
    type EdgeIter = std::vec::IntoIter<(N, N)>;
    type NodeIter = std::collections::hash_set::IntoIter<N>;

    fn edges(&self) -> Self::EdgeIter {
        let all_edges = self.routes.iter().flat_map(|route| {
            let edges = route
                .iter()
                .zip(route.iter().skip(1))
                .map(|(a, b)| (*a, *b))
                .filter(|(a, b)| a != b);
            edges
        });

        // TODO: Avoid conversion to vector.
        let all_edges_vec: Vec<_> = all_edges.collect();

        all_edges_vec.into_iter()
    }

    fn nodes(&self) -> Self::NodeIter {
        // Nodes might occur multiple times (at branching points).
        // Create a hash-set of the nodes to iterate over unique nodes.
        let all_nodes: HashSet<_> = self.routes.iter().flatten().copied().collect();
        all_nodes.into_iter()
    }

    fn total_cost(&self) -> Self::Cost {
        self.total_cost
    }

    fn merge(self, other: Self) -> Self {
        // Simply join the segments. This might introduce overlaps but that should be fine for now.
        // let routes = self.routes.iter()
        //     .chain(&other.routes)
        //     .cloned()
        //     .collect();
        let total_cost = self.total_cost + other.total_cost;

        let mut routes = self.routes;
        routes.extend(other.routes);
        routes.sort();
        routes.dedup();

        Self { routes, total_cost }
    }

    fn from_node(node: Self::GraphNode) -> Self {
        Self {
            routes: vec![vec![node]],
            total_cost: Cost::zero(),
        }
    }

    fn empty() -> Self {
        Self {
            routes: vec![],
            total_cost: Cost::zero(),
        }
    }
}

impl<T, G, Cost> SignalRouter for SimpleSignalRouter<T, G, Cost>
where
    T: PrimInt + Hash,
    G: TraverseGraph<Node3D<T>>,
    Cost: Num + Ord + Copy,
{
    type Graph = G;
    type GraphNode = Node3D<T>;
    type RoutingResult = SimpleSignalRouterResult<Node3D<T>, Cost>;
    type Cost = Cost;

    fn route<NodeCostFn, EdgeCostFn>(
        &self,
        graph: &Self::Graph,
        node_cost_fn: &NodeCostFn,
        edge_cost_fn: &EdgeCostFn,
        terminals: &Vec<Vec<Self::GraphNode>>,
        previous_solution: Option<&Self::RoutingResult>,
    ) -> Option<Self::RoutingResult>
    where
        NodeCostFn: Fn(&Self::GraphNode) -> Self::Cost,
        EdgeCostFn: Fn(&Self::GraphNode, &Self::GraphNode) -> Self::Cost,
    {
        // Dummy a-star heuristic function.
        // Returning always 0 as lower bound turns the a-star algorithm into Dijkstra's algorithm.
        let heuristic_fn =
            |n1: &Self::GraphNode, n2: &Self::GraphNode| -> Self::Cost { Zero::zero() };

        // Route the signal.
        let routing_solution = route_single_signal(
            // route_single_signal_from_source(
            graph,
            terminals,
            self.wire_half_width,
            self.min_spacing,
            edge_cost_fn,
            &heuristic_fn,
            self.pin_feed_through_enabled,
        );

        // Convert routed signal into correct output format.
        routing_solution.map(|sol| SimpleSignalRouterResult {
            routes: sol,
            total_cost: Zero::zero(),
        })
    }

    fn enable_pin_feed_through(&mut self, enable_feed_through: bool) {
        self.pin_feed_through_enabled = enable_feed_through
    }

    fn is_pin_feed_through_enabled(&self) -> bool {
        self.pin_feed_through_enabled
    }
}

/// Connect all pins of a single signal.
/// Return a list of paths that create the routing tree.
pub fn route_single_signal<FE, FH, G, Node, T, Cost>(
    graph: &G,
    pins: &Vec<Vec<Node>>,
    wire_half_width: T,
    min_spacing: T,
    edge_cost_fn: &FE,
    heuristic_fn: &FH,
    enable_pin_feed_through: bool,
) -> Option<Vec<Vec<Node>>>
where
    T: PrimInt + Hash,
    Cost: Num + Ord + Copy,
    G: TraverseGraph<Node>,
    Node: PartialEq + Eq + Hash + Copy,
    FE: Fn(&Node, &Node) -> Cost,
    FH: Fn(&Node, &Node) -> Cost,
{
    // Create local mutable copy of the pins.
    // Copy only pins which have terminals.
    let mut pins = pins
        .iter()
        .filter(|terms| !terms.is_empty())
        .cloned()
        .collect_vec();

    // // Restrict search to the slightly enlarged bounding box around all pins.
    // let bounding_box = pins.iter()
    //     .flatten()
    //     .map(|(point, _layer)| point)
    //     .try_into_bounding_box()
    //     .unwrap_or(Rect::new((T::zero(), T::zero()), (T::zero(), T::zero())));
    //
    // let bounded_graph = FilterNodes::new(
    //     graph,
    //     |(point, _layer)| bounding_box.contains_point(*point),
    // );

    // Create buffer to store found paths.
    let mut paths = Vec::new();

    // As long as there is more than one pin left, connect the two closest pins and merge them.
    while pins.len() > 1 {
        // Try to find a route between the closest pins.
        let path = route_single_signal_closest_terminals(
            graph,
            &pins,
            wire_half_width,
            min_spacing,
            edge_cost_fn,
            heuristic_fn,
        );

        if let Some((pin_id_1, pin_id_2, path)) = path {
            // Found a path between closest pins.

            // Merge the nodes of the connected pins. They form now a new pin.
            let pins2 = std::mem::replace(&mut pins[pin_id_2], vec![]);
            pins[pin_id_1].extend(pins2);

            if !enable_pin_feed_through {
                // Remove all except the used terminals from the pins.
                let used_terminals = [path.first(), path.last()];
                pins[pin_id_1].retain(|t| used_terminals.contains(&Some(t)));
            }

            // Construct new pin nodes.
            // Nodes of the two connected pins are now merged.
            pins.remove(pin_id_2);

            // Remember this route.
            paths.push(path);
        } else {
            // Routing failed.
            return None;
        }
    }

    Some(paths)
}

#[test]
fn test_route_single_signal() {
    let graph = GridGraph::new(
        Rect::new((0, 0), (10, 10)),
        1,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    let pins = vec![
        vec![Node3D::new(2, 2, 0)],
        vec![Node3D::new(8, 8, 0)],
        vec![Node3D::new(2, 8, 0)],
        vec![Node3D::new(0, 0, 0)],
        vec![Node3D::new(9, 0, 0)],
        vec![Node3D::new(9, 5, 0)],
    ];

    type T = i32;

    // A-star heuristic.
    let heuristic_fn = |n1: &Node3D<_>, n2: &Node3D<T>| -> T { (n1.0 - n2.0).norm1() };

    let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
        if n1.1 != n2.1 {
            1000 as T // Via cost
        } else {
            (n1.0 - n2.0).norm1()
        }
    };

    let result = route_single_signal(&graph, &pins, 1, 1, &edge_cost_fn, &heuristic_fn, false);

    assert!(result.is_some());
    // assert_eq!(result.unwrap().3.len(), 8 * 2 + 1);

    let result = result.unwrap();

    // Plot the result.
    let mut plt = TermPlot::new();

    for path in result {
        path.iter().zip(path.iter().skip(1)).for_each(|(a, b)| {
            let Node3D(p1, l1) = a;
            let Node3D(p2, l2) = b;
            if l1 == l2 {
                let edge = REdge::new(p1, p2);
                plt.draw_edge(*l1 as i32, edge);
            }
        });
    }

    // Plot pin locations.
    for pin in &pins {
        for &Node3D(p, layer) in pin {
            plt.set_char(3, p, 'x');
        }
    }

    println!("Resulting routes:");
    plt.enable_colors(false);
    plt.print()
}

#[test]
fn test_route_single_signal_with_shared_terminals() {
    let graph = GridGraph::new(
        Rect::new((0, 0), (10, 10)),
        1,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    let pins = vec![
        vec![Node3D::new(2, 2, 0), Node3D::new(2, 2, 0)],
        // vec![Node3D::new(8, 8, 0)],
        vec![Node3D::new(2, 8, 0), Node3D::new(2, 2, 0)],
    ];

    type T = i32;

    // A-star heuristic.
    let heuristic_fn = |n1: &Node3D<_>, n2: &Node3D<T>| -> T { (n1.0 - n2.0).norm1() };

    let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
        if n1.1 != n2.1 {
            1000 as T // Via cost
        } else {
            (n1.0 - n2.0).norm1()
        }
    };

    let result = route_single_signal(&graph, &pins, 1, 1, &edge_cost_fn, &heuristic_fn, false);

    assert_eq!(
        result,
        Some(vec![vec![Node3D::new(2, 2, 0), Node3D::new(2, 2, 0)]])
    );
}

/// Find path between the two closest pins.
/// Returns: `(pin ID 1, pin ID 2, path)`.
fn route_single_signal_closest_terminals<FE, FH, G, Node, T, Cost>(
    graph: &G,
    pins: &Vec<Vec<Node>>,
    _wire_half_width: T,
    _min_spacing: T,
    edge_cost_fn: &FE,
    heuristic_fn: &FH,
) -> Option<(usize, usize, Vec<Node>)>
where
    T: PrimInt + Hash,
    Cost: Num + Ord + Copy,
    G: TraverseGraph<Node>,
    Node: PartialEq + Eq + Hash + Copy,
    FE: Fn(&Node, &Node) -> Cost,
    FH: Fn(&Node, &Node) -> Cost,
{
    // Sanity check: all pins should have at least one terminal.
    {
        let num_pins_without_terminals = pins.iter().filter(|pins| pins.is_empty()).count();
        if num_pins_without_terminals > 0 {
            log::warn!("Some pins have no terminal nodes.");
        }

        let num_pins_with_terminals = pins.len() - num_pins_without_terminals;
        if num_pins_with_terminals < 2 {
            // There's one or less pins with terminals. So no route can be found.
            return None;
        }
    }

    // Visited nodes.
    let mut trace: FnvHashMap<Node, PQElement<_, Cost>> = Default::default();

    let mut touch_points = None; // Points where to paths to terminals touch.
    let mut processed_node_counter = 0; // Statistics on processed nodes.

    {
        let mut front = BinaryHeap::new();

        // Populate front with initial nodes.
        for (pin_id, nodes) in pins.iter().enumerate() {
            for &node in nodes {
                let element = PQElement {
                    cost: Cost::zero(),
                    node: node,
                    previous: node,
                    pin_id: pin_id as u32,
                    remaining_cost_lower_bound: Cost::zero(),
                };
                front.push(element);
            }
        }

        // For each pin find a terminal that is closest to it.
        // The distance to this terminal is then used as A-star heuristic.
        // let reference_targets = pins.iter()
        //     .map(|terminals| {
        //         let center = terminals.iter().map(|(p, _)| p).sum();
        //     })
        //     .collect_vec();

        // Find a lower bound of the distance from the node to the closest target terminal.
        // TODO: This is not efficient.
        let multi_target_heuristic = |pin_id: usize, node: &Node| -> Cost {
            pins.iter()
                .enumerate()
                .filter(|(i, _)| *i != pin_id)
                .flat_map(|(_, terminals)| {
                    terminals
                        .iter()
                        .map(|terminal| heuristic_fn(node, terminal))
                })
                .min()
                .unwrap_or(Cost::zero())
        };

        // Buffer for neighbour nodes.
        // This buffer helps avoiding allocations.
        let mut neighbours = Vec::new();

        // Work through priority queue.
        while let Some(pq_element) = front.pop() {
            processed_node_counter += 1;
            if processed_node_counter % 10_000_000 == 0 {
                // When so many nodes are processed, it is likely that something went wrong.
                // Therefore print some information.
                log::info!(
                    "Number of processed nodes: {}e6",
                    processed_node_counter / 1_000_000
                );
                log::info!("Priority queue size: {}", front.len());
            }

            if let Some(existing) = trace.get(&pq_element.node) {
                // Node was already visited.

                if pq_element.pin_id != existing.pin_id {
                    // Found another terminal!
                    // Remember the nodes to be able to trace back to the terminals.
                    touch_points = Some((
                        pq_element.pin_id,
                        pq_element.previous,
                        existing.pin_id,
                        existing.node,
                    ));
                    break;
                } else {
                    // Same pin.
                    if pq_element.cost >= existing.cost {
                        // Skip
                    } else {
                        // Replace with the lower-cost element.
                        trace.insert(pq_element.node, pq_element);
                    }
                }
            } else {
                // Node was not yet visited.

                // Clear buffer.
                neighbours.clear();
                graph.neighbours(&mut neighbours, &pq_element.node);

                // Create next nodes.
                for &n in &neighbours {
                    if n != pq_element.previous {
                        // Skip the node where we come from.
                        let distance = edge_cost_fn(&pq_element.node, &n);

                        let heuristic = multi_target_heuristic(pq_element.pin_id as usize, &n);

                        let new = PQElement {
                            node: n,
                            cost: pq_element.cost + distance,
                            pin_id: pq_element.pin_id,
                            previous: pq_element.node,
                            remaining_cost_lower_bound: heuristic,
                        };
                        front.push(new);
                    }
                }

                trace.insert(pq_element.node, pq_element);
            }
        }
    }

    if let Some((pin_id1, p1, pin_id2, p2)) = touch_points {
        let trace_back = |p: Node| -> Vec<Node> {
            let mut path = Vec::new();
            let mut p = p;
            loop {
                path.push(p);
                let prev = trace[&p].previous;
                if prev == p {
                    break;
                }
                p = prev;
            }
            path
        };

        // Trace back and join the half pathes.
        let mut path1 = trace_back(p1);
        let path2 = trace_back(p2);

        path1.reverse();
        path1.extend(path2);
        let path = path1;

        Some((pin_id1 as usize, pin_id2 as usize, path))
    } else {
        // No path was found.
        log::debug!("No path found (closest-terminal connection).");
        None
    }
}

#[test]
fn test_route_single_signal_closest_terminals() {
    let graph = GridGraph::new(
        Rect::new((0, 0), (10, 10)),
        1,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    let pins = vec![vec![Node3D::new(1, 1, 0)], vec![Node3D::new(9, 9, 0)]];

    type T = i32;

    // A-star heuristic.
    let heuristic_fn = |n1: &Node3D<_>, n2: &Node3D<T>| -> T { (n1.0 - n2.0).norm1() };

    let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
        if n1.1 != n2.1 {
            1000 as T // Via cost
        } else {
            (n1.0 - n2.0).norm1()
        }
    };

    let result =
        route_single_signal_closest_terminals(&graph, &pins, 1, 1, &edge_cost_fn, &heuristic_fn);

    dbg!(&result);

    assert!(result.is_some());
    assert_eq!(result.unwrap().2.len(), 8 * 2 + 1);
}

#[test]
fn test_route_single_signal_closest_terminals_with_already_connected_pins() {
    let graph = GridGraph::new(
        Rect::new((0, 0), (10, 10)),
        1,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    // Both pins share the terminal `(2, 2)`.
    let pins = vec![
        vec![Node3D::new(1, 1, 0)],
        vec![Node3D::new(2, 2, 0), Node3D::new(3, 2, 0)],
        vec![Node3D::new(2, 2, 0)],
    ];

    type T = i32;

    // A-star heuristic.
    let heuristic_fn = |n1: &Node3D<_>, n2: &Node3D<T>| -> T { (n1.0 - n2.0).norm1() };

    let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
        if n1.1 != n2.1 {
            1000 as T // Via cost
        } else {
            (n1.0 - n2.0).norm1()
        }
    };

    let result =
        route_single_signal_closest_terminals(&graph, &pins, 1, 1, &edge_cost_fn, &heuristic_fn);

    dbg!(&result);

    assert!(result.is_some());
    assert_eq!(result.unwrap().2.len(), 2);
}

/// Find path from source nodes to one of the sink nodes.
/// This is most efficient if there's only few sink nodes.
fn route_single_source_sink_pair<FE, FH, Node, G, T, Cost>(
    graph: &G,
    sources: &Vec<Node>,
    sinks: &Vec<Node>,
    _wire_half_width: T,
    _min_spacing: T,
    edge_cost_fn: &FE,
    heuristic_fn: &FH,
) -> Option<Vec<Node>>
where
    T: PrimInt + Hash,
    G: TraverseGraph<Node>,
    Node: PartialEq + Eq + Hash + Copy,
    Cost: Ord + Num + Copy,
    FE: Fn(&Node, &Node) -> Cost,
    FH: Fn(&Node, &Node) -> Cost,
{
    assert!(!sources.is_empty(), "Source nodes cannot be empty.");
    assert!(!sinks.is_empty(), "Sink nodes cannot be empty.");

    // Visited nodes.
    let mut trace: FnvHashMap<Node, PQElement<_, Cost>> = Default::default();

    let mut front = BinaryHeap::new();

    // Populate front with initial nodes.
    for &node in sources {
        let element = PQElement {
            cost: Cost::zero(),
            node,
            previous: node,
            pin_id: 0,
            remaining_cost_lower_bound: Cost::zero(),
        };
        front.push(element);
    }

    // Find a lower bound of the distance from the node to the closest target terminal.
    let multi_target_heuristic = |pin_id: usize, node: &Node| -> Cost {
        sinks
            .iter()
            .map(|terminal| heuristic_fn(node, terminal))
            .min()
            .unwrap_or(Cost::zero())
    };

    // Buffer for neighbour nodes.
    // This buffer helps avoiding allocations.
    let mut neighbours = Vec::new();

    // Work through priority queue.
    let mut target_hit = None; // The sink node which was found.
    let mut processed_node_counter = 0; // Statistics on processed nodes.
    while let Some(e) = front.pop() {
        processed_node_counter += 1;

        if sinks.contains(&e.node) {
            // Found a target!
            target_hit = Some(e.node);
            trace.insert(e.node, e);
            break;
        }

        if let Some(existing) = trace.get(&e.node) {
        } else {
            // Node was not yet visited.

            // Clear buffer.
            neighbours.clear();
            graph.neighbours(&mut neighbours, &e.node);

            // Create next nodes.
            for &n in &neighbours {
                if n != e.previous {
                    // Skip the node where we come from.
                    let distance = edge_cost_fn(&e.node, &n);

                    let heuristic = multi_target_heuristic(e.pin_id as usize, &n);

                    let new = PQElement {
                        node: n,
                        cost: e.cost + distance,
                        pin_id: e.pin_id,
                        previous: e.node,
                        remaining_cost_lower_bound: heuristic,
                    };
                    front.push(new);
                }
            }

            trace.insert(e.node, e);
        }
    }

    if let Some(hit) = target_hit {
        let trace_back = |p: Node| -> Vec<Node> {
            let mut path = Vec::new();
            let mut p = p;
            loop {
                path.push(p);
                let prev = trace[&p].previous;
                if prev == p {
                    break path;
                }
                p = prev;
            }
        };
        let mut path = trace_back(hit);
        path.reverse();
        Some(path)
    } else {
        // No path was found.
        log::warn!("No path found.");
        None
    }
}

/// Priority queue element.
#[derive(Hash, PartialEq)]
struct PQElement<Node, Cost> {
    node: Node,
    previous: Node,
    // TODO: Use cost for tracing back.
    pin_id: u32,
    cost: Cost,
    // A-star heuristic
    remaining_cost_lower_bound: Cost,
}

impl<Node, Cost> Eq for PQElement<Node, Cost>
where
    Node: PartialEq,
    Cost: PartialEq,
{
}

impl<Node, Cost: Num + Copy> PQElement<Node, Cost> {
    fn cost_lower_bound(&self) -> Cost {
        self.cost + self.remaining_cost_lower_bound
    }
}

impl<Node, Cost> Ord for PQElement<Node, Cost>
where
    Cost: PartialOrd + Num + Copy,
    Node: PartialEq + Eq,
{
    fn cmp(&self, other: &Self) -> Ordering {
        let cost_bound_other = other.cost_lower_bound();
        let cost_bound_self = self.cost_lower_bound();

        cost_bound_other
            .partial_cmp(&cost_bound_self)
            .unwrap_or(Ordering::Equal)
    }
}

impl<Node, Cost> PartialOrd for PQElement<Node, Cost>
where
    Cost: PartialOrd + Num + Copy,
    Node: PartialEq + Eq,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[test]
fn test_route_single_source_sink_pair() {
    let graph = GridGraph::new(
        Rect::new((0, 0), (10, 10)),
        1,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    let sources = vec![
        Node3D::new(7, 0, 0),
        Node3D::new(8, 0, 0),
        Node3D::new(9, 0, 0),
    ];
    let sinks = vec![Node3D::new(8, 8, 0), Node3D::new(9, 9, 0)];

    type T = i32;

    // A-star heuristic.
    let heuristic_fn = |n1: &Node3D<_>, n2: &Node3D<T>| -> T { (n1.0 - n2.0).norm1() };

    let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
        if n1.1 != n2.1 {
            1000 as T // Via cost
        } else {
            (n1.0 - n2.0).norm1()
        }
    };

    let result =
        route_single_source_sink_pair(&graph, &sources, &sinks, 1, 1, &edge_cost_fn, &heuristic_fn);

    dbg!(&result);

    assert!(result.is_some());
    let path = result.unwrap();
    assert_eq!(path[0], Node3D::new(8, 0, 0));
    assert_eq!(path[path.len() - 1], Node3D::new(8, 8, 0));
}
