// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use itertools::Itertools;
use rayon::prelude::*;

use iron_shapes_algorithms::rectangle_decomposition::decompose_rectangles;
use libreda_pnr::db;
use libreda_pnr::db::*;
use libreda_pnr::route::simple_router::{
    extract_net_terminal_geometries, SimpleRoutedNet, SimpleRouter,
};

use log;
use num_traits::{FromPrimitive, PrimInt, Signed, ToPrimitive};

use std::collections::{HashMap, HashSet};
use std::hash::Hash;

use crate::util::simplify_point_string;

use crate::graph::*;

use super::route_multi_signal::graph_route_multi_signal;
use super::router_traits::RoutedSignal;
use super::simple_signal_router;
use crate::global_router::GlobalRoutingGraph;
use crate::maze_router::monotonic_signal_router::MonotonicSignalRouter;
use crate::maze_router::pathfinder_parallel::PathFinder;
use crate::maze_router::pathfinder_sequential::PathFinderSequential;
use crate::maze_router::router_traits::MultiSignalRouter;
use crate::maze_router::simple_signal_router::{SimpleSignalRouter, SimpleSignalRouterResult};
use crate::multilevel_maze_router::MultilevelMazeRouter;
use fnv::{FnvHashMap, FnvHashSet};
use iron_shapes::prelude::Angle;
use libreda_pnr::route::prelude::GlobalRouter;
use libreda_pnr::route::routing_problem::{GlobalRoutingProblem, RoutingGuide, RoutingProblem};
use std::fmt::Debug;
use std::marker::PhantomData;

/// Example implementation of a simple global router.
#[derive(Clone, Debug)]
pub struct SimpleGlobalRouterV2<L: LayoutIds> {
    /// Routing pitch in the preferred direction for each layer.
    routing_pitch: Vec<L::Coord>,
    /// Horizontal and vertical wire weights for each layer.
    /// Starting with the lowest routing layer (closest to the transistors).
    /// The cost of a wire segment is computed by the product of the length and the wire weight.
    wire_weights: Vec<(L::Coord, L::Coord)>,
    /// Number of fine routing tracks per coarse grid cell.
    coarsening_factor: u32,
    /// Cost of a via.
    via_cost: L::Coord,
    /// IDs of metal routing layers. Starts with lowest layer (closest to silicon) to top layer.
    routing_layers: Vec<L::LayerId>,
    /// Via layers. Starting from lowest via layer (via1).
    via_layers: Vec<L::LayerId>,
    _layout_netlist_type: PhantomData<L>,
}

/// Representation of a global route, i.e. routing guide.
/// This representation stores the coarse grid nodes which belong to the route.
/// A point in the layout can be efficiently checked to be part of the routing guide.
#[derive(Clone)]
pub struct SimpleGlobalRoute<L: LayoutIds> {
    /// Global routing grid.
    // TODO: Fields like this should not be stored per route but per routing solution.
    grid_graph: GridGraph<L::Coord>,
    nodes: FnvHashSet<(db::Point<L::Coord>, L::LayerId)>,
    /// Routing nodes, flattened to two dimensions.
    nodes_2d: FnvHashSet<db::Point<L::Coord>>,
}

impl<LN: L2NBase> SimpleGlobalRoute<LN>
where
    LN::Coord: Eq,
{
    fn new(
        grid_graph: GridGraph<LN::Coord>,
        nodes: FnvHashSet<(db::Point<LN::Coord>, LN::LayerId)>,
    ) -> Self {
        let nodes_2d = nodes.iter().map(|(p, _layer)| *p).collect();
        Self {
            grid_graph,
            nodes,
            nodes_2d,
        }
    }
}

impl<LN: L2NBase> RoutingGuide<LN> for SimpleGlobalRoute<LN>
where
    LN::Coord: PrimInt + FromPrimitive + ToPrimitive,
{
    fn contains_point(&self, layer: &LN::LayerId, p: Point<LN::Coord>) -> bool {
        // Snap to closest grid point.
        let coarse_point = self.grid_graph.closest_grid_point(p);

        // Check if closest grid point is included in the routing guide.
        self.nodes_2d.contains(&coarse_point)
        // self.nodes.contains(&(coarse_point, layer.clone()))
    }

    fn to_rectangles(&self) -> Vec<(db::Rect<LN::Coord>, LN::LayerId)> {
        // Convert route paths into rectangles.
        let grid_pitch = self.grid_graph.grid_pitch;
        let grid_pitch_half = grid_pitch / LN::Coord::from_usize(2).unwrap();

        // Store rectangles by layer.
        let mut raw_rectangles = HashMap::new();
        for (p, layer) in self.nodes.iter().cloned() {
            let p1 = p - grid_pitch_half;
            let p2 = p1 + grid_pitch;

            // // TODO: For debugging TritonRoute: Enlarge rectangles such that pins are covered for sure.
            // let p1 = p1 - grid_pitch;
            // let p2 = p2 + grid_pitch;

            let r = Rect::new(p1, p2);

            raw_rectangles.entry(layer).or_insert(vec![]).push(r);
        }

        // Simplify by merging the raw rectangles into bigger rectangles.
        let rectangles = raw_rectangles
            .into_iter()
            .flat_map(|(layer, rectangles)| {
                let horizontal_rectangles =
                    decompose_rectangles(rectangles.iter().flat_map(|r| r.into_edges()));

                let vertical_rectangles: Vec<_> = {
                    // Rotate everything by 90 degrees.
                    let rects = decompose_rectangles(
                        rectangles
                            .iter()
                            .flat_map(|r| r.into_edges())
                            .map(|edge| edge.rotate_ortho(Angle::R90)),
                    );

                    // Rotate back by -90 degrees.
                    rects
                        .into_iter()
                        .map(|r| r.rotate_ortho(Angle::R270))
                        .collect()
                };

                // Find the decomposition with lower number of rectangles.
                let rectangles = if horizontal_rectangles.len() < vertical_rectangles.len() {
                    horizontal_rectangles
                } else {
                    vertical_rectangles
                };

                rectangles.into_iter().map(move |r| (r, layer.clone()))
            })
            .collect();

        rectangles
    }
}

#[derive(Clone)]
pub struct SimpleGlobalRoutes<LN: L2NIds> {
    global_routing_nodes: HashMap<LN::NetId, SimpleGlobalRoute<LN>>,
}

impl<T, LN> SimpleGlobalRouterV2<LN>
where
    LN::Coord: CoordinateType
        + PrimInt
        + Signed
        + std::fmt::Display
        + std::fmt::Debug
        + Hash
        + ToPrimitive
        + FromPrimitive
        + Send
        + Sync,
    LN::NetId: Sync + Send,
    LN: L2NBase<Coord = T>,
    T: 'static,
{
    /// Create a new router instance.
    pub fn new(
        routing_pitch: Vec<LN::Coord>,
        wire_weights: Vec<(T, T)>,
        routing_layers: Vec<LN::LayerId>,
        via_layers: Vec<LN::LayerId>,
    ) -> Self {
        Self {
            routing_pitch,
            wire_weights,
            coarsening_factor: 16,
            via_cost: LN::Coord::from(100).unwrap(),
            routing_layers,
            via_layers,
            _layout_netlist_type: Default::default(),
        }
    }

    /// Find global routes for the specified nets.
    fn compute_routes<RP>(
        &self,
        routing_problem: &RP,
    ) -> Result<HashMap<LN::NetId, SimpleGlobalRoute<LN>>, Vec<LN::NetId>>
    where
        RP: GlobalRoutingProblem<LN>,
    {
        log::info!("Start router '{}'.", self.name());

        assert_eq!(
            self.routing_layers.len(),
            self.via_layers.len() + 1,
            "There must be exactly one more routing layer than via layers."
        );

        let chip = routing_problem.fused_layout_netlist();
        let top_cell = routing_problem.top_cell();
        let nets: Vec<_> = routing_problem.nets().collect();

        // Find geometries of terminals for each net.
        let net_terminals = extract_net_terminal_geometries(chip, &top_cell);

        // Take only nets that should be routed.
        let nets_set: HashSet<_> = nets.iter().cloned().collect();
        let net_terminals: HashMap<_, _> = net_terminals
            .into_iter()
            .filter(|(n, _)| nets_set.contains(n))
            .collect();

        // Print some statistics on terminals.
        {
            let num_terminals: usize = net_terminals.values().map(|t| t.len()).sum();
            let num_one_terminal_nets = net_terminals.iter().filter(|(_, t)| t.len() <= 1).count();
            log::info!("Number of nets with terminals: {}", net_terminals.len());
            log::info!("Number of net terminals: {}", num_terminals);
            if num_one_terminal_nets > 0 {
                log::warn!(
                    "Number of nets with less than two terminals: {}",
                    num_one_terminal_nets
                );
            }
        }

        // Mapping from layer IDs to subsequent layer numbers.
        let layer_numbers: HashMap<_, _> = self
            .routing_layers
            .iter()
            .enumerate()
            .map(|(i, layer)| (layer.clone(), i as u8))
            .collect();

        // Convert terminals into rectilinear polygons.
        let net_terminals: HashMap<_, _> = net_terminals
            .into_iter()
            .map(|(net, terminals)| {
                let r_terminals: Vec<_> = terminals
                    .into_iter()
                    .map(|(t, layer)| {
                        assert_eq!(
                            t.interiors.len(),
                            0,
                            "Polygons with holes are not supported."
                        );
                        vec![(
                            SimpleRPolygon::try_new(t.exterior.points())
                                .expect("Only rectilinear polygons are supported"),
                            layer_numbers[&layer],
                        )] // Assign correct layer.
                    })
                    .collect();
                (net, r_terminals)
            })
            .collect();

        // Create a set of obstacles.
        // For testing everything is put on a single layer.
        let mut obstacles: Vec<_> = net_terminals
            .values()
            .flat_map(|terminals| terminals.iter().flatten().cloned())
            .collect();

        debug_assert!(
            obstacles
                .iter()
                .all(|(poly, _)| poly.orientation().is_counter_clock_wise()),
            "Obstacle polygons must have counter-clock-wise orientation."
        );

        // Get the bounding box of the top cell.
        let top_cell_bounding_box = chip.bounding_box(&top_cell).unwrap();

        assert!(self.routing_layers.len() <= u8::MAX as usize);
        let num_layers = self.routing_layers.len() as u8;

        // Register existing shapes such as the power grid as obstacles.

        for (layer_num, layer) in self.routing_layers.iter().enumerate() {
            let layer_info = chip.layer_info(layer);
            log::info!(
                "Find obstacles on layer {}/{} (name={:?})",
                layer_info.index,
                layer_info.datatype,
                layer_info.name
            );
            // let mut debug_shapes = Vec::new();
            chip.for_each_shape_recursive(&top_cell, layer, |tf, _id, shape| {
                let poly = shape.transformed(&tf).to_polygon();
                assert_eq!(
                    poly.interiors.len(),
                    0,
                    "Polygons with holes are not supported."
                );

                let rpoly = SimpleRPolygon::try_new(poly.exterior.points())
                    .expect("Only rectilinear polygons are supported");

                // debug_shapes.push((rpoly.clone(), layer.clone()));
                obstacles.push((rpoly, layer_num as u8));
            });
        }
        log::info!("Number of obstacle shapes: {}", obstacles.len());

        // TODO: This sizing is arbitrary. Use provided boundary.
        let margin = LN::Coord::from(1000).unwrap();
        let boundary = top_cell_bounding_box.sized(margin, margin);

        assert!(num_layers <= u8::MAX);

        let result = self.compute_global_routes(boundary, &net_terminals, &obstacles);

        // Find nets that have not been routed.
        let unrouted_nets: Vec<_> = nets
            .iter()
            .filter(|n| !chip.is_constant_net(n)) // Ignore constant 0 and 1 nets.
            .filter(|&n| !result.contains_key(n))
            .cloned()
            .collect();

        if unrouted_nets.is_empty() {
            Ok(result)
        } else {
            Err(unrouted_nets)
        }
    }

    /// Find global routes for all the nets.
    fn compute_global_routes<NetId>(
        &self,
        boundary: Rect<T>,
        net_terminals: &HashMap<NetId, Vec<Vec<(SimpleRPolygon<T>, u8)>>>,
        obstacles: &[(SimpleRPolygon<T>, u8)],
    ) -> HashMap<NetId, SimpleGlobalRoute<LN>>
    where
        NetId: Eq + Hash + Clone + Debug + Sync + Send + 'static,
    {
        let wire_weights = &self.wire_weights;
        let num_layers = wire_weights.len() as u8;

        assert_eq!(wire_weights.len(), num_layers as usize);
        assert_eq!(self.routing_pitch.len(), num_layers as usize);

        // Cost of a via.
        let via_cost = self.via_cost;

        // Compute minimal costs for both routing directions.
        // This is used to compute the lower bound on the cost. Used for the A-star heuristic.
        let min_weight_horizontal = wire_weights.iter().map(|&(h, _)| h).min().unwrap();
        let min_weight_vertical = wire_weights.iter().map(|&(_, v)| v).min().unwrap();

        // A-star heuristic.
        let heuristic_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
            let diff = n1.0 - n2.0;
            let weighted: Vector<_> =
                (diff.x * min_weight_horizontal, diff.y * min_weight_vertical).into();
            weighted.norm1()
        };

        // Cost estimate from node `n1` to node `n2`.
        let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
            if n1.1 != n2.1 {
                // Layers are not the same, must be a via.
                via_cost // Via cost
            } else {
                // Edge on same layer.
                let layer = n1.1;
                // Get wire costs for horizontal and vertical directions.
                let (wh, wv) = wire_weights[layer as usize];
                let diff = n1.0 - n2.0;
                let weighted: Vector<_> = (diff.x * wh, diff.y * wv).into();
                weighted.norm1()
            }
        };

        let fine_grid_pitch = self
            .routing_pitch
            .iter()
            .copied()
            .min()
            .expect("no routing pitch defined");

        // Fine routing grid.
        let fine_grid = grid_graph::GridGraph::new(
            boundary,
            num_layers,
            Vector::new(fine_grid_pitch, fine_grid_pitch),
            Vector::zero(),
        )
        .set_preferred_routing_directions(grid_graph::PreferredRoutingDirection::XY);

        log::info!("Fine grid: {}x{}", fine_grid.num_x(), fine_grid.num_y());
        log::info!(
            "Preferred routing direction on layer 0: {:?}",
            fine_grid.routing_directions
        );

        let mut off_grid_graph = OffGridGraph::new(fine_grid);

        // Translate the terminal shapes into nodes in the graph.
        // TODO: Use result from pin access analysis.
        log::info!("Find pin nodes.");
        let pin_nodes =
            self.convert_terminals_to_grid_nodes(net_terminals, &fine_grid, &mut off_grid_graph);

        let coarsening_factor = T::from_u32(self.coarsening_factor).unwrap();

        log::info!(
            "Create global routing grid. A GCell contains {} tracks.",
            coarsening_factor
        );

        let via_pitch = &self.routing_pitch; // TODO: Treat via pitch separately.
        let coarse_routing_graph = self.create_global_routing_grid(
            &fine_grid,
            coarsening_factor,
            &self.routing_pitch,
            via_pitch,
            obstacles,
        );

        let coarse_guide_nodes = self.find_coarse_routes(
            &coarse_routing_graph,
            coarsening_factor,
            &pin_nodes,
            &edge_cost_fn,
            &heuristic_fn,
        );

        let coarse_graph = *coarse_routing_graph.grid_graph();

        let guides = coarse_guide_nodes
            .into_iter()
            .map(|(net, route)| {
                let nodes = route
                    .nodes()
                    .map(|Node3D(p, layer)| (p, self.routing_layers[layer as usize].clone()))
                    .collect();
                (net, SimpleGlobalRoute::new(coarse_graph, nodes))
            })
            .collect();

        guides
    }

    /// Find grid nodes for the terminal shapes.
    /// If necessary, create off-grid nodes in the `off_grid_graph`..
    fn convert_terminals_to_grid_nodes<NetId>(
        &self,
        net_terminals: &HashMap<NetId, Vec<Vec<(SimpleRPolygon<T>, u8)>>>,
        fine_grid: &grid_graph::GridGraph<T>,
        off_grid_graph: &mut OffGridGraph<T>,
    ) -> Vec<(NetId, Vec<Vec<Node3D<T>>>)>
    where
        NetId: Eq + Hash + Clone,
    {
        let mut pin_nodes: Vec<_> = net_terminals
            .iter()
            .map(|(net, terminals)| {
                let nodes: Vec<_> = terminals
                    .iter()
                    .enumerate()
                    .map(|(i, shapes)| {
                        let pin_nodes = shapes
                            .iter()
                            .flat_map(|(shape, layer)| {
                                let bbox = shape.try_bounding_box().unwrap();
                                // Find all nodes that interact with the terminal shape.
                                // Candidate nodes are nodes that interact with the bounding box.
                                log::debug!("Find on-grid nodes.");
                                let on_grid_nodes = fine_grid
                                    .all_nodes_xy(&bbox)
                                    .filter(move |p| shape.contains_point(*p))
                                    .map(move |xy| Node3D(xy, *layer))
                                    .collect_vec();

                                if on_grid_nodes.is_empty() {
                                    log::debug!("Find off-grid nodes.");
                                    // There's no on-grid nodes. Find off-grid nodes instead.
                                    let mut off_grid_nodes = vec![];

                                    // Decompose the pin shape into rectangles. Take the center
                                    // of the biggest rectangle as terminal node.
                                    log::debug!("Decompose rectangles.");
                                    let rects = decompose_rectangles(shape.edges());
                                    let rects90 = decompose_rectangles(
                                        shape
                                            .transformed(&SimpleTransform::rotate90(Angle::R90))
                                            .edges(),
                                    )
                                    .into_iter()
                                    .map(|r| r.rotate_ortho(Angle::R270));

                                    log::debug!("Find largest rectangle.");
                                    let biggest_rect = rects
                                        .into_iter()
                                        .chain(rects90)
                                        .max_by_key(|r| r.area_doubled_oriented())
                                        .expect("Pin has no area.");

                                    // Create a terminal node in the center.
                                    let center = biggest_rect.center();
                                    let n = Node3D(center, *layer);
                                    // Remember the off-grid node.
                                    off_grid_graph.insert_node(n);
                                    // Store node as terminal.
                                    off_grid_nodes.push(n);

                                    // let _2 = T::one() + T::one();
                                    // // Use the middle points of the polygon edges as terminals.
                                    // shape.edges()
                                    //     .for_each(|edge| {
                                    //         let center = (edge.start() + edge.end()) / _2;
                                    //         let n = (center, *layer);
                                    //         // Remember the off-grid node.
                                    //         off_grid_graph.insert_node(n);
                                    //         // Store node as terminal.
                                    //         off_grid_nodes.push(n)
                                    //     });

                                    off_grid_nodes.into_iter()
                                } else {
                                    // No need for off-grid nodes, take on-grid nodes.
                                    on_grid_nodes.into_iter()
                                }
                            })
                            .unique()
                            .collect_vec();
                        if pin_nodes.is_empty() {
                            // TODO
                            log::warn!("Pin without routing nodes.");
                        }
                        pin_nodes
                    })
                    .collect();

                (net.clone(), nodes)
            })
            .collect();

        // Sort nets by the perimeter of their bounding boxes such that small nets are routed first.
        self.sort_pin_nodes_by_bounding_box(&mut pin_nodes);

        pin_nodes
    }

    /// Sort nets by the perimeter of their bounding boxes.
    fn sort_pin_nodes_by_bounding_box<NetId>(
        &self,
        pin_nodes: &mut Vec<(NetId, Vec<Vec<Node3D<T>>>)>,
    ) {
        pin_nodes.sort_by_cached_key(|(net, node)| {
            if let Some(Node3D(p, _layer)) = node.iter().flatten().nth(0) {
                let mut x_min = p.x;
                let mut x_max = p.x;
                let mut y_min = p.y;
                let mut y_max = p.y;

                for Node3D(p, _layer) in node.iter().flatten() {
                    x_min = x_min.min(p.x);
                    x_max = x_max.max(p.x);
                    y_min = y_min.min(p.y);
                    y_max = y_max.max(p.y);
                }

                let dx = x_max - x_min;
                let dy = y_max - y_min;

                dx + dy
            } else {
                T::zero()
            }
        });
    }

    /// Create the coarse global routing grid and estimate its routing capacities
    /// based on the obstacle shapes.
    fn create_global_routing_grid(
        &self,
        fine_grid: &grid_graph::GridGraph<T>,
        coarsening_factor: T,
        routing_pitch: &[T],
        via_pitch: &[T],
        obstacles: &[(SimpleRPolygon<T>, u8)],
    ) -> GlobalRoutingGraph<T> {
        log::info!("Fine grid: {}x{}", fine_grid.num_x(), fine_grid.num_y());

        // Find routing guides on a coarser grid.
        // Create a coarser grid.
        let coarse_grid = grid_graph::GridGraph {
            grid_pitch: fine_grid.grid_pitch * coarsening_factor,
            ..*fine_grid
        };

        {
            // Sanity check.
            let num_layers = coarse_grid.num_layers as usize;
            assert_eq!(routing_pitch.len(), num_layers);
            assert_eq!(via_pitch.len(), num_layers);
        }

        log::info!(
            "Coarse grid: {}x{}",
            coarse_grid.num_x(),
            coarse_grid.num_y()
        );

        let mut coarse_graph = GlobalRoutingGraph::new(coarse_grid);

        log::info!("Estimate routing capacities of the global routing grid.");
        crate::global_router::capacity_estimation::estimate_gcell_capacities(
            &mut coarse_graph,
            obstacles,
            routing_pitch,
            via_pitch,
        );

        coarse_graph
    }

    /// Find coarse routes based on the Pathfinder negotiation algorithm.
    fn find_coarse_routes<NetId, FE, FH>(
        &self,
        coarse_graph: &GlobalRoutingGraph<T>,
        coarsening_factor: T,
        pin_nodes: &Vec<(NetId, Vec<Vec<Node3D<T>>>)>,
        edge_cost_fn: &FE,
        heuristic_fn: &FH,
    ) -> Vec<(NetId, SimpleSignalRouterResult<Node3D<T>, T>)>
    where
        NetId: Hash + Eq + Clone + Sync + Send + Debug + 'static,
        FE: Fn(&Node3D<T>, &Node3D<T>) -> T + Sync,
        FH: Fn(&Node3D<T>, &Node3D<T>) -> T + Sync,
    {
        let coarse_grid = *coarse_graph.grid_graph();

        // Find coarse routing terminals first.
        let coarse_pin_nodes: Vec<(NetId, Vec<Vec<_>>)> = pin_nodes
            .iter()
            .map(|(net, pins)| {
                let coarse_pins: Vec<_> = pins
                    .iter()
                    .map(|terminals| {
                        // Map the terminal nodes to the closest nodes on the coarse grid.
                        terminals
                            .iter()
                            .map(|&Node3D(p, layer)| {
                                Node3D(coarse_grid.closest_grid_point(p), layer)
                            })
                            .unique()
                            .collect_vec()
                    })
                    .collect();

                // let coarse_pins: Vec<_> = pins.iter()
                //     .flatten()
                //     .map(|&(p, layer)| (coarse_grid.closest_grid_point(p), layer))
                //     .unique()
                //     .map(|node| vec![node])
                //     .collect_vec();

                (net.clone(), coarse_pins)
            })
            .collect();

        // Setup pathfinder router.
        let single_signal_router = SimpleSignalRouter::new();
        // let single_signal_router = MonotonicSignalRouter::new();

        // let pathfinder = PathFinder::new(&single_signal_router);
        let pathfinder = PathFinderSequential::new(&single_signal_router);
        let mut multi_signal_router = MultilevelMazeRouter::new(&pathfinder);
        // multi_signal_router.set_global_congestion_output_directory("/tmp/".into());
        // let multi_signal_router = pathfinder;

        log::info!("Start coarse routing to find routing guides.");

        let node_cost_fn = |n: &Node3D<T>| -> T { Zero::zero() };

        let edge_cost_float_fn = |a: &Node3D<T>, b: &Node3D<T>| -> T { edge_cost_fn(a, b) };

        let node_capacity_fn =
            |n: &Node3D<T>| -> u32 { (coarsening_factor * coarsening_factor).to_u32().unwrap() };

        let edge_capacity_fn = |a: &Node3D<T>, b: &Node3D<T>| -> u32 {
            let is_same_layer = a.1 == b.1;
            if is_same_layer {
                coarsening_factor.to_u32().unwrap() //* 3 / 4
            } else {
                // Via
                (coarsening_factor * coarsening_factor).to_u32().unwrap() // / 4
            }
        };

        // Run pathfinder.
        let routing_result = multi_signal_router.route_multisignal(
            // coarse_graph.grid_graph(),
            &coarse_graph,
            &node_cost_fn,
            &edge_cost_float_fn,
            &node_capacity_fn,
            &edge_capacity_fn,
            &|_, _| true,
            &coarse_pin_nodes,
            &HashMap::new(),
        );

        if routing_result.is_err() {
            log::error!("Failed to find global routes.");
        }
        let routing_result = routing_result.expect("Failed to find global routes.");

        // Sanity check: All pin nodes must be included in the found route.
        for ((net_id, route), (net_id_orig, pins)) in routing_result.iter().zip(coarse_pin_nodes) {
            assert!(
                net_id == &net_id_orig,
                "Ordering of routing results is not matching the ordering of the input nets."
            );

            let all_route_nodes: HashSet<_> = route.nodes().collect();

            debug_assert!(
                pins.iter()
                    .all(|ports| { ports.iter().any(|port| all_route_nodes.contains(port)) }),
                "Route does not contain all pins of the net."
            );
        }

        routing_result
    }
}

impl<LN> GlobalRouter<LN> for SimpleGlobalRouterV2<LN>
where
    LN: L2NBase,
    LN::Coord: CoordinateType
        + PrimInt
        + Signed
        + std::fmt::Display
        + std::fmt::Debug
        + Hash
        + ToPrimitive
        + FromPrimitive
        + Send
        + Sync
        + 'static,
    LN::NetId: Sync + Send + 'static,
{
    type RoutingResult = HashMap<LN::NetId, SimpleGlobalRoute<LN>>;
    type Error = Vec<LN::NetId>;

    fn name(&self) -> String {
        "SimpleGlobalRouterV2".into()
    }

    fn route<RP>(&self, routing_problem: &RP) -> Result<Self::RoutingResult, Self::Error>
    where
        RP: GlobalRoutingProblem<LN>,
    {
        self.compute_routes(routing_problem)
    }
}
