// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! **Work in progress**
//!
//! Implementation of the 'pathfinder' algorithm.
//! Pathfinder routes multiple nets in a graph by solving conflicts with a negotiation scheme.
//!
//! # Literature
//! * Pathfinder: <http://www.cecs.uci.edu/~papers/compendium94-03/papers/1995/fpga95/pdffiles/6a.pdf>
//! * FGR (good summary about Lagrange multipliers): <https://web.eecs.umich.edu/~imarkov/pubs/jour/tcad08-fgr.pdf>

use itertools::Itertools;
use log;
use num_traits::{Float, FromPrimitive, Num, NumCast, One, PrimInt, ToPrimitive, Zero};

use std::collections::HashMap;
use std::hash::Hash;
use std::marker::PhantomData;
use std::ops::Add;
use std::time;

// Use an alternative hasher that has better performance for small keys.
use fnv::FnvHashMap;

use super::router_traits::*;
use crate::graph::Node3D;
use crate::graph::TraverseGraph;

/// A negotiation-based multi-signal router based on a single-signal router.
pub struct PathFinderSequential<'a, R, NetId> {
    /// Maximum number of routing iterations.
    /// If no solution within this number of iterations, an error is returned.
    pub max_iterations: usize,
    /// Underlying single-signal router which will be called from the Pathfinder algorithm.
    single_signal_router: &'a R,
    _net_id_type: PhantomData<NetId>,
}

impl<'a, R, N> PathFinderSequential<'a, R, N> {
    /// Create a Pathfinder algorithm based on a given single-net routing algorithm.
    /// Maximum number of iterations is set to `1000`.
    pub fn new(single_signal_router: &'a R) -> Self {
        Self {
            max_iterations: 1000,
            single_signal_router,
            _net_id_type: Default::default(),
        }
    }

    /// Set the maximum number of iterations after which the routing is aborted.
    pub fn set_max_iterations(&mut self, max_iterations: usize) -> &mut Self {
        self.max_iterations = max_iterations;
        self
    }
}

fn logistic_function<F: Float>(x: F) -> F {
    F::one() / (F::one() + (-x).exp())
}

impl<'a, R, NetId> MultiSignalRouter for PathFinderSequential<'a, R, NetId>
where
    R: SignalRouter,
    R: Sync,
    R::Graph: TraverseGraph<R::GraphNode> + Sync,
    R::GraphNode: Eq + Hash + Copy + Sync + std::fmt::Debug,
    R::Cost: Num + Ord + FromPrimitive + ToPrimitive + Copy + Send + Sync,
    R::RoutingResult: Sync + Send,
    NetId: Clone + Sync + std::fmt::Debug,
{
    type Graph = R::Graph;
    type GraphNode = R::GraphNode;
    type NetId = NetId;
    type RoutingResult = R::RoutingResult;
    type Cost = R::Cost;

    /// Negotiation-based multi-signal routing.
    ///
    /// Uses the signal router to route each signal individually, then iteratively increases
    /// the node costs and edge costs at locations where the capacity constraints are violated.
    fn route_multisignal<NodeCostFn, EdgeCostFn>(
        &self,
        graph: &R::Graph,
        node_cost_fn: &NodeCostFn,
        edge_cost_fn: &EdgeCostFn,
        node_capacity_fn: &(dyn Fn(&Self::GraphNode) -> u32 + Sync),
        edge_capacity_fn: &(dyn Fn(&Self::GraphNode, &Self::GraphNode) -> u32 + Sync),
        can_use_node_fn: &(dyn Fn(&Self::NetId, &Self::GraphNode) -> bool + Sync),
        signal_pins: &Vec<(Self::NetId, Vec<Vec<R::GraphNode>>)>,
        _net_weights: &HashMap<Self::NetId, f64>,
    ) -> Result<Vec<(Self::NetId, R::RoutingResult)>, ()>
    where
        NodeCostFn: Fn(&R::GraphNode) -> R::Cost + Sync,
        EdgeCostFn: Fn(&R::GraphNode, &R::GraphNode) -> R::Cost + Sync,
    {
        type Edge<N> = (N, N);

        let num_signals = signal_pins.len();

        let edge_history_cost_increment = R::Cost::from_f64(1.0).unwrap();
        let node_history_cost_increment = R::Cost::from_f64(1.0).unwrap();

        // History costs. They are iteratively increased for congested regions.
        let mut node_history_costs: FnvHashMap<R::GraphNode, R::Cost> = Default::default();
        let mut edge_history_costs: FnvHashMap<Edge<R::GraphNode>, R::Cost> = Default::default();

        // Store the absolute usage of edges.
        let mut edge_usage: FnvHashMap<Edge<R::GraphNode>, u32> = Default::default();

        let mut intermediary_routing_solutions: Vec<Option<R::RoutingResult>> =
            (0..num_signals).map(|_| None).collect();
        let mut route_costs: Vec<Option<R::Cost>> = (0..num_signals).map(|_| None).collect();

        let mut slack_ratios: Vec<f64> = signal_pins.iter().map(|_| 1.0).collect();

        // Routing iterations.
        // 1) Route all signals in every iteration.
        // 2) Increase node and edge costs to move routes away from congestions in the next iteration.
        // Because costs are only increased, but not decreased, only routes need to be recomputed which are affected by the cost increase.
        // 3) Iterate until all capacity requirements are met or the maximum number of iterations is reached.
        let mut iteration = 0;
        let mut last_log_output_time = time::Instant::now();
        let routing_solution = loop {
            if iteration >= self.max_iterations {
                log::warn!("Abort: Maximal number of iterations reached.");
                break None;
            }
            iteration += 1;

            // Print progress not more often than every 10s.
            if last_log_output_time.elapsed() > time::Duration::from_secs(10) {
                log::info!(
                    "Routing iteration {} of at most {}.",
                    iteration,
                    self.max_iterations
                );
                last_log_output_time = time::Instant::now();
            } else {
                log::debug!(
                    "Routing iteration {} of at most {}.",
                    iteration,
                    self.max_iterations
                );
            }

            // 1) Find a new solution for each signal.
            let new_routing_solutions: Vec<_> = {
                // Print statistics.
                // {
                //     let rel_usages: Vec<_> = edge_usage.keys()
                //         .map(|(a, b)| edge_relative_usage(a, b))
                //         .collect();
                //     let average_relative_usage = if rel_usages.len() > 0 {
                //         rel_usages.iter().sum::<f64>() / (rel_usages.len() as f64)
                //     } else {
                //         0.
                //     };
                //     log::info!("Average relative edge usage: {}", average_relative_usage);
                // }

                let mut new_routes: Vec<_> = signal_pins.iter().map(|_| None).collect();

                // Costs of resource-sharing in current iteration.
                let mut node_present_sharing_cost: FnvHashMap<R::GraphNode, u32> =
                    Default::default();
                let mut edge_present_sharing_cost: FnvHashMap<Edge<R::GraphNode>, u32> =
                    Default::default();

                // Determine which nets get routed first.
                // Route nets with low slack ratio first (tendentially short nets).
                let routing_order = {
                    let mut routing_order: Vec<usize> = (0..num_signals).into_iter().collect();
                    routing_order.sort_by_key(|&i| ((slack_ratios[i] * 1e6) as i64, i));
                    routing_order
                };

                // Compute routes for each signal.
                for i in routing_order {
                    let (net_id, pins) = &signal_pins[i];
                    let previous_solution =
                        std::mem::replace(&mut intermediary_routing_solutions[i], None);
                    let previous_cost = &mut route_costs[i];
                    let slack_ratio = slack_ratios[i];

                    // Compute relative usage of an edge: 0.0 = no usage. 1.0 = full usage.
                    let edge_relative_usage = |n1: &R::GraphNode, n2: &R::GraphNode| -> f64 {
                        let edge = (*n1, *n2);

                        let past_usage = edge_usage.get(&edge).copied().unwrap_or(0);
                        let present_usage =
                            edge_present_sharing_cost.get(&edge).copied().unwrap_or(0);

                        let usage = (past_usage + present_usage + 1) / 2;

                        let capacity = edge_capacity_fn(n1, n2);
                        if capacity.is_zero() {
                            One::one()
                        } else {
                            usage.to_f64().unwrap() / capacity.to_f64().unwrap()
                        }
                    };

                    // Compute node cost from the base cost and the history cost.
                    let new_node_cost_fn = |node: &R::GraphNode| -> R::Cost {
                        // Base cost.
                        let b = node_cost_fn(node);
                        // History cost.
                        let h = node_history_costs
                            .get(node)
                            .copied()
                            .unwrap_or(Zero::zero());
                        // Present sharing.
                        let p = node_present_sharing_cost
                            .get(node)
                            .copied()
                            .unwrap_or(Zero::zero())
                            .to_f64()
                            .unwrap();

                        let b = b.to_f64().unwrap();
                        let h = h.to_f64().unwrap();
                        let cost = (b * (1. + 0.1 * h));

                        let cost = (slack_ratio) * b + (1.0 - slack_ratio) * cost;

                        R::Cost::from_f64(cost).unwrap()
                    };

                    // Compute edge cost from the base cost and the history cost.
                    let new_edge_cost_fn = |n1: &R::GraphNode, n2: &R::GraphNode| -> R::Cost {
                        // Base cost.
                        let b = edge_cost_fn(n1, n2);
                        // History cost.
                        let edge = (*n1, *n2);
                        let h = edge_history_costs
                            .get(&edge)
                            .copied()
                            .unwrap_or(Zero::zero());

                        // Congestion penalty.
                        let p = {
                            let relative_usage = edge_relative_usage(n1, n2);
                            let p = 0.5 * logistic_function(4. * (relative_usage - 0.8));
                            p
                        };

                        let b = b.to_f64().unwrap();
                        let h = h.to_f64().unwrap();
                        // let cost = (1. + h) * p + b;
                        let cost = (b * (1. + 0.1 * h + 1. * p));

                        let cost = (slack_ratio) * b + (1.0 - slack_ratio) * cost;

                        R::Cost::from_f64(cost).unwrap()
                    };

                    let compute_route_cost = |route: &R::RoutingResult| -> R::Cost {
                        let edge_cost: R::Cost = route
                            .edges()
                            .map(|(n1, n2)| new_edge_cost_fn(&n1, &n2))
                            .fold(Zero::zero(), |a, b| a + b);
                        let node_cost: R::Cost = route
                            .nodes()
                            .map(|n| new_node_cost_fn(&n))
                            .fold(Zero::zero(), |a, b| a + b);
                        edge_cost + node_cost
                    };

                    // If there is already a route, check if the cost increased. Search a new
                    // route only if the cost of the old route did change. (Edge costs only increase over time).
                    let reuse_previous_solution =
                        if let Some(previous_solution) = &previous_solution {
                            let new_cost = compute_route_cost(previous_solution);

                            previous_cost
                                .map(|prev| {
                                    let prev = prev.to_f64().unwrap();
                                    let new_cost = new_cost.to_f64().unwrap();
                                    let rel_diff = (new_cost - prev) / prev;
                                    // log::info!("rel_diff = {}", rel_diff);
                                    rel_diff < 0.05 // Reuse the solution if the cost did not increase more than 5%.
                                })
                                .unwrap_or(false)
                        } else {
                            false
                        };
                    // let reuse_previous_solution = false;

                    let (route, cost) = if reuse_previous_solution {
                        // Reuse old solution.
                        (previous_solution, *previous_cost)
                    } else {
                        // Costs increased, need to find a new solution.

                        debug_assert!(pins
                            .iter()
                            .flat_map(|nodes| nodes.iter())
                            .all(|n| graph.contains_node(n)));

                        let new_route = self.single_signal_router.route(
                            graph,
                            &new_node_cost_fn,
                            &new_edge_cost_fn,
                            pins,
                            previous_solution.as_ref(),
                        );
                        if new_route.is_none() {
                            log::error!("Failed to find route.");
                            dbg!(&pins);
                            panic!("Failed to find route.");
                        }
                        let new_cost = new_route.as_ref().map(compute_route_cost);
                        (new_route, new_cost)
                    };

                    // Update previous cost.
                    *previous_cost = cost;

                    // Update present sharing costs.
                    if let Some(route) = &route {
                        for node in route.nodes() {
                            *node_present_sharing_cost.entry(node).or_insert(0) += 1;
                        }
                        for (n1, n2) in route.edges() {
                            *edge_present_sharing_cost.entry((n1, n2)).or_insert(0) += 1;
                            *edge_present_sharing_cost.entry((n2, n1)).or_insert(0) += 1;
                        }
                    }

                    debug_assert!(new_routes[i].is_none());
                    new_routes[i] = route;
                }

                // Compute new slack ratios.
                {
                    // Find the cost of the most expensive route.
                    let max_route_cost = route_costs
                        .iter()
                        .copied()
                        .filter_map(|c| c)
                        .max()
                        .unwrap_or(Zero::zero())
                        .to_f64()
                        .unwrap();

                    let slack_ratio_scaling = 0.1;

                    slack_ratios = route_costs
                        .iter()
                        .copied()
                        .map(|c| {
                            let s = c.unwrap_or(Zero::zero()).to_f64().unwrap() / max_route_cost;

                            (s - 0.5) * slack_ratio_scaling + 0.5
                        })
                        .collect();
                }

                new_routes
            };

            intermediary_routing_solutions = new_routing_solutions;

            debug_assert!(
                intermediary_routing_solutions
                    .iter()
                    .all(|sol| sol.is_some()),
                "No routing solution found for some signals."
            );

            // Count by how many signals a node is used.
            let node_usage = {
                let all_signal_nodes =
                    intermediary_routing_solutions
                        .iter()
                        .flat_map(|maybe_routed_signal| {
                            maybe_routed_signal
                                .iter()
                                .flat_map(|routed_signal| routed_signal.nodes())
                        });
                count_node_usage(all_signal_nodes)
            };

            // Count by how many signals an edge is used.
            edge_usage = {
                let all_signal_edges =
                    intermediary_routing_solutions
                        .iter()
                        .flat_map(|maybe_routed_signal| {
                            maybe_routed_signal
                                .iter()
                                .flat_map(|routed_signal| routed_signal.edges())
                        });
                count_undirected_edge_usage(all_signal_edges)
            };

            // Find nodes which are used more often than their capacity allows.
            let node_overflow = node_usage.iter().filter_map(|(node, &count)| {
                let capacity = node_capacity_fn(node);
                if count > capacity {
                    Some((*node, count - capacity))
                } else {
                    None
                }
            });

            // Find edges which are used more often than their capacity allows.
            let edge_overflow = edge_usage.iter().filter_map(|(edge, &count)| {
                let (n1, n2) = edge;
                let capacity = edge_capacity_fn(n1, n2);
                if count > capacity {
                    Some((*edge, count - capacity))
                } else {
                    None
                }
            });

            // 2) Update costs.
            // Costs are increased when there is congestion. They are never decreased.
            // This allows to efficiently reuse the previous routing result as a hint
            // for the updated routing result.

            // Count capacity overflows on nodes.
            let mut total_node_overflow = 0u32;
            for (node, overflow) in node_overflow {
                let c = node_history_costs.entry(node).or_insert(Zero::zero());
                *c = *c + node_history_cost_increment;

                total_node_overflow += overflow;
            }

            // Count capacity overflows on edges.
            let mut total_edge_overflow = 0u32;
            for (edge, overflow) in edge_overflow {
                let c = edge_history_costs.entry(edge).or_insert(Zero::zero());
                *c = *c + edge_history_cost_increment;

                total_edge_overflow += overflow;
            }

            let num_nodes = node_history_costs.len();
            let num_edges = edge_history_costs.len();
            // log::debug!("Total node / edge overflow: {} {}", total_node_overflow, total_edge_overflow);
            log::info!("Total edge overflow: {}", total_edge_overflow);

            let has_overflow = !total_edge_overflow.is_zero() || !total_node_overflow.is_zero();

            // 3) Reached capacity constraints?
            if !has_overflow {
                // Found a solution which meets all capacity constraints.
                log::debug!("Reached capacity constraints in iteration {}.", iteration);
                break Some(intermediary_routing_solutions);
            }
        };

        // Convert output format.
        if let Some(solution) = routing_solution {
            // Check that all signals are routed. I.e. that no signal route is `None`.
            let mut solution: Option<Vec<_>> = solution.into_iter().collect();
            if let Some(solution) = solution {
                // Associate with net ID.
                let solution = signal_pins
                    .iter()
                    .map(|(id, _)| id.clone())
                    .zip(solution)
                    .collect();
                Ok(solution)
            } else {
                Err(())
            }
        } else {
            Err(())
        }
    }

    fn enable_pin_feed_through(&mut self, enable_feed_through: bool) {
        todo!();
        // self.single_signal_router
        //     .enable_pin_feed_through(enable_feed_through)
    }

    fn is_pin_feed_through_enabled(&self) -> bool {
        self.single_signal_router.is_pin_feed_through_enabled()
    }
}

/// Count how many times a graph node has been used.
fn count_node_usage<I, N>(nodes: I) -> FnvHashMap<N, u32>
where
    N: Hash + Eq + Copy,
    I: Iterator<Item = N>,
{
    let mut count: FnvHashMap<N, u32> = Default::default();

    for n in nodes {
        *count.entry(n).or_insert(0) += 1;
    }

    count
}

/// Count how many times an undirected edge has been used.
/// Edges are tuples of two nodes. The nodes are not required to be ordered.
/// For each edge, two entries are created for each edge direction.
fn count_undirected_edge_usage<I, N>(edges: I) -> FnvHashMap<(N, N), u32>
where
    N: Hash + Eq + Copy,
    I: Iterator<Item = (N, N)>,
{
    let mut count: FnvHashMap<(N, N), u32> = Default::default();

    for (n1, n2) in edges {
        // Count both directions of the edge. Edges are undirected but their nodes are not sorted.
        *count.entry((n1, n2)).or_insert(0) += 1;
        *count.entry((n2, n1)).or_insert(0) += 1;
    }

    count
}

#[test]
fn test_pathfinder() {
    // Find route for three signals where the independent solutions conflict with eachother.

    use super::simple_signal_router::SimpleSignalRouter;
    use crate::graph::GridGraph;
    use iron_shapes::prelude::{Rect, Vector};

    let graph = GridGraph::new(
        Rect::new((0, 0), (100, 100)),
        2,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    let net1 = vec![vec![Node3D::new(1, 10, 0)], vec![Node3D::new(30, 10, 0)]];

    let net2 = vec![vec![Node3D::new(5, 10, 0)], vec![Node3D::new(25, 10, 0)]];

    let net3 = vec![vec![Node3D::new(15, 10, 0)], vec![Node3D::new(20, 10, 0)]];

    let signal_pins = vec![(1, net1), (2, net2), (3, net3)];

    type T = i32;

    // A-star heuristic.
    let heuristic_fn = |n1: &Node3D<_>, n2: &Node3D<T>| -> T { (n1.0 - n2.0).norm1() };

    let node_cost_fn = |n1: &Node3D<T>| -> T { 0 };

    let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
        if n1.1 != n2.1 {
            // Different layers.
            1 as T // Via cost
        } else {
            let (h_cost, v_cost) = if n1.1 % 2 == 0 {
                // Vertical layer. Lower cost for verticals.
                (40, 10)
            } else {
                (10, 40)
            };
            let mut diff = (n1.0 - n2.0);
            diff.x *= h_cost;
            diff.y *= v_cost;
            diff.norm1()
        }
    };

    let node_capacity_fn = |n1: &Node3D<T>| -> u32 { 1 };

    let edge_capacity_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> u32 { 1 };

    // Signal router which will be called from the Pathfinder algorithm.
    let signal_router = {
        let mut router = SimpleSignalRouter::new();
        router.min_spacing = 1;
        router.wire_half_width = 1;
        router
    };

    let path_finder = PathFinderSequential {
        max_iterations: 100,
        single_signal_router: &signal_router,
        _net_id_type: Default::default(),
    };

    // Find routes with pathfinder.
    let result = path_finder
        .route_multisignal(
            &graph,
            &node_cost_fn,
            &edge_cost_fn,
            &node_capacity_fn,
            &edge_capacity_fn,
            &|_, _| true,
            &signal_pins,
            &HashMap::new(),
        )
        .expect("Routing failed.");

    // Convert to Vec for plotting.
    let routes: Vec<_> = result
        .iter()
        .map(|(net_id, route)| route.routes.clone())
        .collect();

    // assert_eq!(path[0], Node3D::new(8, 0, 0));
    // assert_eq!(path[path.len() - 1], Node3D::new(8, 8, 0));

    // Plot the result.
    use crate::visualize::TermPlot;
    use iron_shapes::prelude::REdge;
    let mut plt = TermPlot::new();

    for paths in routes {
        for path in paths {
            path.iter().zip(path.iter().skip(1)).for_each(|(a, b)| {
                let Node3D(p1, l1) = a;
                let Node3D(p2, l2) = b;
                if l1 == l2 {
                    let edge = REdge::new(p1, p2);
                    plt.draw_edge(*l1 as i32, edge);
                }
            });
        }
    }

    // Plot pin locations.
    for (_, pins) in &signal_pins {
        for pin in pins {
            for &Node3D(p, layer) in pin {
                plt.set_char(3, p, 'x');
            }
        }
    }

    println!("Resulting routes:");
    plt.enable_colors(false);
    plt.print()
}
