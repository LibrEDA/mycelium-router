// Copyright (c) 2020-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Aerial monotonic routing.
//! Monotonic routing tries to find a path between two terminals without making detours.
//! If such path exists, the shortest will be found.
//! Aerial monotonic routing makes possible detours in z-direction. The projection of an aerial-monotonic route
//! on the xy-plane (seen from the air) looks like a monotonic route.
//!
//! # References
//! 1) "Multilayer Global Routing with Via and Wire Capacity Considerations" <http://eda.ee.ntu.edu.tw/~yellowfish/tcad10/tcad10.pdf>

use db::traits::TryIntoBoundingBox;
use fnv::FnvHashSet;
use libreda_pnr::db;
use num_traits::{PrimInt, Signed};
use std::hash::Hash;

use crate::graph::*;

pub fn multi_source_multi_target_aerial_monotonic_route<T>(
    graph: &GridGraph<T>,
    source: &Vec<Node3D<T>>,
    target: &Vec<Node3D<T>>,
) where
    T: db::CoordinateType + PrimInt,
{
    todo!()
    // let source_bounding_box = source.iter()
    //     .map(|(p, _layer)| *p)
    //     .try_into_bounding_box();
    //
    // let target_bounding_box = target.iter()
    //     .map(|(p, _layer)| *p)
    //     .try_into_bounding_box();
    //
    // // Compute bounding box of all source and sink nodes.
    // let bounding_box = {
    //     let points = source.iter()
    //         .chain(target.iter())
    //         .map(|(p, _layer)| *p);
    //     points.try_into_bounding_box()
    // };
}

/// Store predecessor of a node (for back tracking) and distance to the source.
#[derive(Copy, Clone, Debug)]
struct NodeProperty<T> {
    /// Next node in the trace towards the source node.
    predecessor: Option<Node3D<T>>,
    /// Distance to the source node.
    distance: T,
}

/// Find the lowest-cost aerial-monotonic path between the source nodes and target nodes.
pub fn full_aerial_monotonic_route_multi_terminal<T, FEdgeCost>(
    graph: &GridGraph<T>,
    sources: &Vec<Node3D<T>>,
    targets: &Vec<Node3D<T>>,
    edge_cost: &FEdgeCost,
) -> Option<Vec<Node3D<T>>>
where
    T: db::CoordinateType + PrimInt + Signed + Hash + std::fmt::Debug,
    FEdgeCost: Fn(&Node3D<T>, &Node3D<T>) -> T,
{
    debug_assert!(
        sources.iter().all(|n| graph.contains_node(n)),
        "All source nodes must be part of the graph."
    );
    debug_assert!(
        targets.iter().all(|n| graph.contains_node(n)),
        "All target nodes must be part of the graph."
    );

    if sources.is_empty() || targets.is_empty() {
        // Trivial case. Return empty route.
        return Some(vec![]);
    }

    // Compute bounding box of all source nodes.
    let source_bounding_box = {
        // 2D points.
        let points = sources.iter().map(|Node3D(p, _layer)| p);
        points
            .try_into_bounding_box()
            .expect("Bounding-box of source nodes must be defined.")
    };

    // Compute bounding box of all target nodes.
    let target_bounding_box = {
        // 2D points.
        let points = targets.iter().map(|Node3D(p, _layer)| p);
        points
            .try_into_bounding_box()
            .expect("Bounding-box of target nodes must be defined.")
    };

    let expansion_dir = |source: db::Point<T>, target: db::Point<T>| -> db::Vector<T> {
        let dir = target - source;
        db::Vector::new(dir.x.signum(), dir.y.signum())
    };

    // Compute necessary search directions.
    let search_directions =
        find_necessary_search_directions(source_bounding_box, target_bounding_box);

    // Do multiple monotonic searches with different directions.
    let candidate_paths = search_directions.into_iter().filter_map(|(start, end)| {
        let direction = expansion_dir(start, end);
        let bbox = db::Rect::new(start, end);

        // Consider only nodes which are inside the bounding box.
        let filtered_sources: Vec<_> = sources
            .iter()
            .filter(|Node3D(p, _)| bbox.contains_point(*p))
            .copied()
            .collect();

        let filtered_targets: Vec<_> = targets
            .iter()
            .filter(|Node3D(p, _)| bbox.contains_point(*p))
            .copied()
            .collect();

        partial_aerial_monotonic_route_multi_terminal(
            graph,
            &filtered_sources,
            &filtered_targets,
            direction,
            edge_cost,
        )
    });

    // Find lowest-cost path.
    let best_path = candidate_paths.min_by_key(|path| {
        // Compute path cost.
        let edges = path.iter().zip(path.iter().skip(1));
        let cost = edges
            .map(|(n1, n2)| edge_cost(n1, n2))
            .fold(T::zero(), |a, b| a + b);
        cost
    });

    best_path
}

#[test]
fn test_full_aerial_monotonic_route() {
    use std::collections::HashSet;

    let graph = GridGraph::new(
        db::Rect::new((0, 0), (10, 10)),
        1,
        db::Vector::new(1i32, 1),
        db::Vector::new(0, 0),
    );

    let sources = vec![Node3D((1, 0).into(), 0), Node3D((0, 2).into(), 0)];
    let targets = vec![Node3D((1, 1).into(), 0)];

    let result =
        full_aerial_monotonic_route_multi_terminal(&graph, &sources, &targets, &|_, _| 1i32);

    dbg!(&result);

    assert!(result.is_some());
    let route = result.unwrap();

    assert!(
        {
            let terminals = vec![sources, targets];
            let all_route_nodes: HashSet<_> = route.into_iter().collect();
            terminals
                .iter()
                .all(|ports| ports.iter().any(|port| all_route_nodes.contains(port)))
        },
        "Route does not contain all pins of the net."
    );
}

/// Compute necessary search directions such that all possible paths between the two
/// boxes can be found with monotonic searches.
///
/// Return value has the form `[(start point, end point), ...]`.
///
/// See Fig. 13 in [1](#references)
fn find_necessary_search_directions<T>(
    source_bounding_box: db::Rect<T>,
    target_bounding_box: db::Rect<T>,
) -> Vec<(db::Point<T>, db::Point<T>)>
where
    T: Copy + db::CoordinateType + Signed,
{
    // Necessary path searches to find the optimum path.
    // Of the form `[(start corner, end corner), ...]`.
    let mut searches: Vec<(db::Point<T>, db::Point<T>)> = Vec::new();

    let expansion_dir = |source: db::Point<T>, target: db::Point<T>| -> db::Vector<T> {
        let dir = target - source;
        db::Vector::new(dir.x.signum(), dir.y.signum())
    };

    for s in source_bounding_box.into_iter() {
        for t in target_bounding_box.into_iter() {
            let covered_area = db::Rect::new(s, t); // Covered rectangle region.
            let dir = expansion_dir(s, t);

            let is_redundant = searches.iter().any(|&(p1, p2)| {
                let r = db::Rect::new(p1, p2);
                let d = expansion_dir(p1, p2);
                d == dir && r.contains_rectangle(&covered_area)
            });

            if !is_redundant {
                // Remove searches which are made redundant by the current one.
                searches.retain(|&(p1, p2)| {
                    let r = db::Rect::new(p1, p2);
                    let d = expansion_dir(p1, p2);
                    let is_redundant = d == dir && covered_area.contains_rectangle(&r);
                    !is_redundant
                });

                searches.push((s, t))
            }
        }
    }

    searches
}

#[test]
fn test_find_necessary_search_directions_no_overlap() {
    let r1 = db::Rect::new((0, 0), (10, 10));
    let r2 = db::Rect::new((90, 90), (100, 100));

    let searches = find_necessary_search_directions(r1, r2);
    assert_eq!(searches, vec![(r1.lower_left(), r2.upper_right())])
}

#[test]
fn test_find_necessary_search_directions_overlap() {
    let r1 = db::Rect::new((0, 0), (10, 10));
    let r2 = db::Rect::new((5, 5), (15, 15));

    let searches = find_necessary_search_directions(r1, r2);
    assert_eq!(searches.len(), 4);
    assert!(searches.contains(&(r1.lower_left(), r2.upper_right())));
    assert!(searches.contains(&(r1.lower_right(), r2.upper_left())));
    assert!(searches.contains(&(r1.upper_left(), r2.lower_right())));
    assert!(searches.contains(&(r1.upper_right(), r2.lower_left())));
}

/// Aerial-monotonic routing for multiple source and target nodes.
/// Finds a cost-optimal areal-monotonic path from the `sources` to the `targets`.
/// The route is restricted to the specified search direction (finds only routes in certain direction).
/// `full_aerial_monotonic_route_multi_terminal()` searches all necessary directions.
///
/// The monotonic search is started from the minimum x and y coordinates of the
/// source terminals and directs towards the target nodes. Target nodes which
/// are located in lower x or y coordinates than the source nodes will not be found.
///
/// # Parameters
/// * `direction`: Propagation direction of the monotonic search. Only two values `1`, `0`, and `-1` are allowed for both
/// `x` and `y` component. A step size of `0` will be replaced by `1`.
fn partial_aerial_monotonic_route_multi_terminal<T, FEdgeCost>(
    graph: &GridGraph<T>,
    sources: &Vec<Node3D<T>>,
    targets: &Vec<Node3D<T>>,
    direction: db::Vector<T>,
    edge_cost: &FEdgeCost,
) -> Option<Vec<Node3D<T>>>
where
    T: db::CoordinateType + PrimInt + Signed + Hash + std::fmt::Debug,
    FEdgeCost: Fn(&Node3D<T>, &Node3D<T>) -> T,
{
    debug_assert!(
        sources.iter().all(|n| graph.contains_node(n)),
        "All source nodes must be part of the graph."
    );
    debug_assert!(
        targets.iter().all(|n| graph.contains_node(n)),
        "All target nodes must be part of the graph."
    );

    assert!(
        direction.x.abs() <= T::one(),
        "direction.x must be either -1, 0 or 1."
    );
    assert!(
        direction.y.abs() <= T::one(),
        "direction.y must be either -1, 0 or 1."
    );

    // Make sure that the step size is -1 or 1 for both axes.
    let direction = {
        let mut d = direction;
        if d.x.is_zero() {
            d.x = T::one();
        }
        if d.y.is_zero() {
            d.y = T::one();
        }
        d
    };

    if sources.is_empty() || targets.is_empty() {
        return None;
    }

    // Compute bounding box of all source and sink nodes.
    let bounding_box = {
        // 2D points.
        let points = sources
            .iter()
            .chain(targets.iter())
            .map(|Node3D(p, _layer)| p);
        points
            .try_into_bounding_box()
            .expect("Bounding-box of source and target nodes must be defined.")
    };

    let lower_left = graph.grid_floor(bounding_box.lower_left);
    let upper_right = graph.grid_floor(bounding_box.upper_right);

    // Set default distances to infinity (maximum value).
    let mut node_properties = graph.create_attributes(NodeProperty {
        predecessor: None,
        distance: T::max_value(),
    });

    // Set distance of source node to zero.
    for source in sources {
        node_properties.get_mut(source).distance = T::zero();
    }

    {
        let x_step = graph.grid_pitch.x.abs() * direction.x;
        let y_step = graph.grid_pitch.y.abs() * direction.y;

        let (x_start, x_end) = if direction.x == T::one() {
            // Search in positive x-direction.
            (lower_left.x, upper_right.x)
        } else {
            // Search in negative x-direction.
            (upper_right.x, lower_left.x)
        };

        let (y_start, y_end) = if direction.y == T::one() {
            // Search in positive y-direction.
            (lower_left.y, upper_right.y)
        } else {
            // Search in negative x-direction.
            (upper_right.y, lower_left.y)
        };

        let x_end = x_end + x_step; // Include the end.
        let y_end = y_end + y_step;

        // TODO: Terminate search when a target is found and the costs of the propagating wave front
        // are all higher than the target cost and all source nodes have been processed (otherwise
        // costs could drop again).
        //
        // Set of target nodes, used to determine when search can be terminated before
        // the the full bounding box is covered.
        let mut target_set: FnvHashSet<_> = targets.iter().copied().collect();
        let mut target_xy_set: FnvHashSet<_> = targets.iter().map(|Node3D(p, _layer)| *p).collect();

        let mut closest_target_distance = T::max_value();

        // Keep track of source nodes coordinates which still need to be processed.
        let mut remaining_source_nodes_xy: FnvHashSet<_> =
            sources.iter().map(|Node3D(p, _)| *p).collect();

        // Loop through x and y.
        let mut y = y_start;
        'outer_loop: while y != y_end {
            // Distance from the source nodes to the current wavefront nodes.
            let mut smallest_wavefront_distance = T::max_value();

            let mut x = x_start;
            while x != x_end {
                // Current coordinate.
                let p = db::Point::new(x, y);

                pile_update(graph, p, &mut node_properties, edge_cost);

                if x + x_step != x_end {
                    let p_next = db::Point::new(x + x_step, y);
                    pile_propagation(graph, p, p_next, &mut node_properties, edge_cost);
                }

                if y + y_step != y_end {
                    let p_next = db::Point::new(x, y + y_step);
                    pile_propagation(graph, p, p_next, &mut node_properties, edge_cost);
                }

                // Eventually remove the coordinate from the remaining source nodes coordinates.
                remaining_source_nodes_xy.remove(&p);

                // Keep track of closest target node. This helps to terminate the search early.
                if target_xy_set.contains(&p) {
                    for l in 0..graph.num_layers {
                        let n = Node3D(p, l);
                        if target_set.contains(&n) {
                            let node_property = node_properties.get(&n);
                            let distance = node_property.distance;
                            // Update the distance to the closest target.
                            closest_target_distance = closest_target_distance.min(distance);
                        }
                        // We won't pass at this node anymore.
                        target_set.remove(&n);
                    }

                    // No need to remember this target coordinate anymore. We won't pass here anymore.
                    target_xy_set.remove(&p);
                }

                // Update smallest distance to source nodes of the current wavefront.
                for l in 0..graph.num_layers {
                    let n = Node3D(p, l);
                    let distance = node_properties.get(&n).distance;
                    smallest_wavefront_distance = smallest_wavefront_distance.min(distance);
                }

                // Can terminate the search early?
                // Can be the case if all source nodes where processed and a terminal node is found.
                if remaining_source_nodes_xy.is_empty() {
                    // Can potentially terminate the search early. No new source node will come.
                    if closest_target_distance < smallest_wavefront_distance {
                        // We won't find any node which is closer than the current closest target.
                        break 'outer_loop;
                    }
                }

                x = x + x_step;
            }
            y = y + y_step;
        }
    }

    // Find shortest path by backtracking.
    // TODO: Backtracking based on distances. No need to store the predecessor node.
    {
        let closest_target = targets
            .iter()
            .min_by_key(|t| node_properties.get(t).distance)
            .copied();

        let source_set: FnvHashSet<_> = sources.iter().copied().collect();

        let path = if let Some(mut current_node) = closest_target {
            let mut path = vec![current_node];
            loop {
                let properties = node_properties.get(&current_node);

                if source_set.contains(&current_node) {
                    // Reached a source node.
                    path.reverse();
                    break Some(path);
                }

                if let Some(prev) = properties.predecessor {
                    current_node = prev;
                    path.push(current_node);
                } else {
                    // No path was found.
                    break None;
                }
            }
        } else {
            // No target was reached.
            None
        };

        path
    }
}

#[test]
fn test_multi_terminal_aerial_monotonic_route() {
    use db::{Point, Rect, Vector};
    let grid = GridGraph::new(
        Rect::new((-100, -100), (100, 100)),
        4,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    let sources = vec![
        Node3D(Point::new(-50, -50), 0),
        Node3D(Point::new(-5, -5), 0),
    ];
    let targets = vec![Node3D(Point::new(4, 4), 3), Node3D(Point::new(50, 50), 3)];

    {
        // Route from source to target.
        let route = partial_aerial_monotonic_route_multi_terminal(
            &grid,
            &sources,
            &targets,
            db::Vector::new(1, 1),
            &|_, _| 1,
        );

        assert!(route.is_some());
        let path = route.unwrap();
        assert!(sources.contains(&path[0]));
        assert!(targets.contains(&path[path.len() - 1]));
        assert_eq!(path[0], Node3D(Point::new(-5, -5), 0));
        assert_eq!(path[path.len() - 1], Node3D(Point::new(4, 4), 3));
    }

    {
        // Route in reversed direction. From targets to sources.
        let route = partial_aerial_monotonic_route_multi_terminal(
            &grid,
            &targets,
            &sources,
            db::Vector::new(-1, -1), // Reverse the search direction
            &|_, _| 1,
        );

        assert!(route.is_some());
        let path = route.unwrap();
        assert!(targets.contains(&path[0]));
        assert!(sources.contains(&path[path.len() - 1]));
        assert_eq!(path[0], Node3D(Point::new(4, 4), 3));
        assert_eq!(path[path.len() - 1], Node3D(Point::new(-5, -5), 0));
    }
}

/// Aerial-monotonic routing.
///
/// Find a cost-optimal areal-monotonic path from the `source` to the `target`.
pub fn aerial_monotonic_route_two_terminal<T, FEdgeCost>(
    graph: &GridGraph<T>,
    source: Node3D<T>,
    target: Node3D<T>,
    edge_cost: &FEdgeCost,
) -> Option<Vec<Node3D<T>>>
where
    T: db::CoordinateType + PrimInt + Signed + Hash + std::fmt::Debug,
    FEdgeCost: Fn(&Node3D<T>, &Node3D<T>) -> T,
{
    debug_assert!(graph.contains_node(&source));
    debug_assert!(graph.contains_node(&target));

    // 2D points.
    let p_source = source.0;
    let p_target = target.0;

    // Compute bounding box of all source and sink nodes.
    let bounding_box = db::Rect::new(p_source, p_target);

    // Set default distances to infinity (maximum value).
    let mut node_properties = graph.create_attributes(NodeProperty {
        predecessor: None,
        distance: T::max_value(),
    });
    // Set distance of source node to zero.
    node_properties.get_mut(&source).distance = T::zero();

    let x_step = {
        let signum = (p_target.x - p_source.x).signum();
        let signum = if signum.is_zero() { T::one() } else { signum };
        signum * graph.grid_pitch.x.abs()
    };
    let y_step = {
        let signum = (p_target.y - p_source.y).signum();
        let signum = if signum.is_zero() { T::one() } else { signum };
        signum * graph.grid_pitch.y.abs()
    };

    let x_start = p_source.x;
    let y_start = p_source.y;
    let x_end = p_target.x + x_step; // Include the end.
    let y_end = p_target.y + y_step;

    // Loop through x and y.
    let mut y = y_start;
    while y != y_end {
        let mut x = x_start;
        while x != x_end {
            let p = db::Point::new(x, y);

            pile_update(graph, p, &mut node_properties, edge_cost);

            if x + x_step != x_end {
                let p_next = db::Point::new(x + x_step, y);
                pile_propagation(graph, p, p_next, &mut node_properties, edge_cost);
            }

            if y + y_step != y_end {
                let p_next = db::Point::new(x, y + y_step);
                pile_propagation(graph, p, p_next, &mut node_properties, edge_cost);
            }

            x = x + x_step;
        }
        y = y + y_step;
    }

    // Find shortest path by backtracking.
    {
        let mut current_node = target;
        let mut path = vec![current_node];
        loop {
            let properties = node_properties.get(&current_node);

            if current_node == source {
                path.reverse();
                break Some(path);
            }

            if let Some(prev) = properties.predecessor {
                current_node = prev;
                path.push(current_node);
            } else {
                // No path was found.
                break None;
            }
        }
    }
}

#[test]
fn test_two_terminal_aerial_monotonic_route() {
    use db::{Point, Rect, Vector};
    let grid = GridGraph::new(
        Rect::new((-5i32, -5), (5, 5)),
        4,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    let source = Node3D(Point::new(-5, -5), 0);
    let target = Node3D(Point::new(4, 4), 3);

    {
        // Route from source to target.
        let route = aerial_monotonic_route_two_terminal(&grid, source, target, &|_, _| 1);

        assert!(route.is_some());
        let path = route.unwrap();
        assert_eq!(path[0], source);
        assert_eq!(path[path.len() - 1], target);
    }
    {
        // Route from target to source (reverse).
        let route = aerial_monotonic_route_two_terminal(&grid, target, source, &|_, _| 1);

        assert!(route.is_some());
        let path = route.unwrap();
        assert_eq!(path[0], target);
        assert_eq!(path[path.len() - 1], source);
    }
}

#[test]
fn test_two_terminal_aerial_monotonic_route_same_target_source() {
    use db::{Point, Rect, Vector};
    let grid = GridGraph::new(
        Rect::new((-5i32, -5), (5, 5)),
        4,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    let source = Node3D(Point::new(0, 0), 0);
    let target = Node3D(Point::new(0, 0), 0);

    {
        // Route from source to target.
        let route = aerial_monotonic_route_two_terminal(&grid, source, target, &|_, _| 1);
        assert_eq!(route, Some(vec![source]));
    }
}

/// Update costs and predecessors in the z direction at one xy location.
fn pile_update<T, FEdgeCost>(
    graph: &GridGraph<T>,
    point: db::Point<T>,
    node_properties: &mut GridMap<T, NodeProperty<T>>,
    edge_cost_fn: &FEdgeCost,
) where
    T: db::CoordinateType + PrimInt + Hash + std::fmt::Debug,
    FEdgeCost: Fn(&Node3D<T>, &Node3D<T>) -> T,
{
    let num_layers = graph.num_layers;

    if num_layers > 1 {
        // Pile update is not necessary if there is only one layer.
        let mut propagate_in_z = |layer: LayerNum, layer_next: LayerNum| {
            let n = Node3D(point, layer);
            let n_next = Node3D(point, layer_next);

            let dist_n = node_properties.get(&n).distance;
            if dist_n != T::max_value() {
                let cost = edge_cost_fn(&n, &n_next);

                let props_n_next = node_properties.get_mut(&n_next);

                if dist_n + cost < props_n_next.distance {
                    props_n_next.distance = dist_n + cost;
                    props_n_next.predecessor = Some(n);
                }
            }
        };

        // Propagate upwards.
        for l in 0..num_layers - 1 {
            propagate_in_z(l, l + 1);
        }
        // Propagate downwards.
        for l in 1..num_layers {
            let l_upper = num_layers - l;
            let l_lower = l_upper - 1;
            propagate_in_z(l_upper, l_lower);
        }
    }
}

/// Propagate costs and predecessors monotonically in x and y direction.
fn pile_propagation<T, FEdgeCost>(
    graph: &GridGraph<T>,
    point: db::Point<T>,
    point_next: db::Point<T>,
    node_properties: &mut GridMap<T, NodeProperty<T>>,
    edge_cost_fn: &FEdgeCost,
) where
    T: db::CoordinateType + PrimInt + Hash,
    FEdgeCost: Fn(&Node3D<T>, &Node3D<T>) -> T,
{
    let num_layers = graph.num_layers;

    let mut propagate_in_xy = |p: db::Point<T>, p_next: db::Point<T>, layer: LayerNum| {
        let n = Node3D(p, layer);
        let n_next = Node3D(p_next, layer);

        let dist_n = node_properties.get(&n).distance;

        if dist_n != T::max_value() {
            let props_n_next = node_properties.get_mut(&n_next);
            let cost = edge_cost_fn(&n, &n_next);

            if dist_n + cost < props_n_next.distance {
                props_n_next.distance = dist_n + cost;
                props_n_next.predecessor = Some(n);
            }
        }
    };

    for l in 0..num_layers {
        propagate_in_xy(point, point_next, l);
    }
}
