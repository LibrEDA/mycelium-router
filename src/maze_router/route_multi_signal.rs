// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Sequentially route multiple signals in a graph.

use super::simple_signal_router::route_single_signal;
use crate::graph::*;
use crate::visualize::TermPlot;
use iron_shapes::prelude::*;
use num_traits::PrimInt;
use std::collections::HashMap;
use std::hash::Hash;
use std::time;

/// Find non-conflicting routes for all signals using a greedy and naive algorithm.
///
/// Signals are routed sequentially in the most simple way. Once a signal is routed, it becomes
/// an obstacle for other signals.
/// No rip-up reroute is done.
///
/// # Parameters
///
/// * `graph`: The graph which represends the routing grid.
/// * `signal_pins`: Provide for each net a list of pins that should get connected by routing. A pin consists of a list of terminal nodes.
/// * `can_use_node`: A function that tells whether a node can be used for a certain net.
pub fn graph_route_multi_signal<FE, FH, FN, G, T>(
    graph: &G,
    signal_pins: &Vec<(usize, Vec<Vec<Node3D<T>>>)>,
    wire_half_width: T,
    min_spacing: T,
    edge_cost_fn: &FE,
    heuristic_fn: &FH,
    can_use_node: &FN, // Can a node be used for the signal?
) -> HashMap<usize, Vec<Vec<Node3D<T>>>>
where
    G: TraverseGraph<Node3D<T>> + CreateNodeAttributes<Node3D<T>, bool>,
    T: PrimInt + Hash,
    FE: Fn(&Node3D<T>, &Node3D<T>) -> T,
    FH: Fn(&Node3D<T>, &Node3D<T>) -> T,
    FN: Fn(usize, &Node3D<T>) -> bool,
{
    // Collection of found routes.
    let mut results = HashMap::new();

    // // Mark nodes that are used for pins with the net ID of the pin.
    // let terminal_net_id = {
    //     let mut net_ids = graph.create_attributes(None);
    //     signal_pins.iter()
    //         .for_each(|(net_id, pins)| {
    //             pins.iter()
    //                 .flatten()
    //                 .for_each(|n| net_ids.set(n, Some(*net_id)))
    //         });
    //     net_ids
    // };

    // Set of nodes that cannot be used anymore. They are blocked by already routed signals.
    let mut is_blocked = graph.create_attributes(false);

    let mut last_log_output_time = time::Instant::now();

    for (i, (net_id, signal)) in signal_pins.into_iter().enumerate() {
        // Print progress not more often than every 10s.
        if time::Instant::now() - last_log_output_time > time::Duration::from_secs(10) {
            log::info!(
                "Route signal {}/{}. Number of pins: {}.",
                i,
                signal_pins.len(),
                signal.len()
            );
            last_log_output_time = time::Instant::now();
        } else {
            log::debug!(
                "Route signal {}/{}. Number of pins: {}.",
                i,
                signal_pins.len(),
                signal.len()
            );
        }

        // Select a subset of nodes that can be used for routing.
        let filtered_nodes = FilterNodes::new(graph, |n| {
            can_use_node(*net_id, n) && !is_blocked.get(n)
            // // If n is a terminal then use it only if it is a terminal of this net.
            // && terminal_net_id.get(n)
            // .map(|term_net_id| net_id == &term_net_id)
            // .unwrap_or(true)
        });

        let routes = route_single_signal(
            &filtered_nodes,
            signal,
            wire_half_width,
            min_spacing,
            &edge_cost_fn,
            &heuristic_fn,
            false,
        );

        if let Some(routes) = routes {
            // Exclude used nodes from the graph.
            routes
                .iter()
                .flatten()
                .for_each(|n| is_blocked.set(n, true));

            // Store result.
            results.insert(*net_id, routes);
        }
    }

    results
}

#[test]
fn test_route_multi_signal() {
    type T = i32;

    // A-star heuristic.
    let heuristic_fn = |n1: &Node3D<_>, n2: &Node3D<T>| -> T { (n1.0 - n2.0).norm1() };

    let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
        if n1.1 != n2.1 {
            1 as T // Via cost
        } else {
            (n1.0 - n2.0).norm1()
        }
    };

    let graph = GridGraph::new(
        Rect::new((0, 0), (10, 10)),
        2,
        Vector::new(1, 1),
        Vector::new(0, 0),
    );

    let p = Point::new(1, 1);
    assert_eq!(p, graph.closest_grid_point(p));

    let net_pins = vec![
        (
            0,
            vec![vec![Node3D::new(2, 2, 0)], vec![Node3D::new(8, 8, 0)]],
        ),
        (
            1,
            vec![
                vec![Node3D::new(8, 4, 0)],
                vec![Node3D::new(5, 5, 0)],
                vec![Node3D::new(5, 9, 0)],
            ],
        ),
        (
            3,
            vec![vec![Node3D::new(1, 9, 0)], vec![Node3D::new(9, 1, 0)]],
        ),
    ];

    let pins = net_pins.into_iter().collect();

    let can_use_node = |_: usize, _: &Node3D<T>| true;

    let result = graph_route_multi_signal(
        &graph,
        &pins,
        1,
        1,
        &edge_cost_fn,
        &heuristic_fn,
        &can_use_node,
    );

    dbg!(&result);

    // assert_eq!(result.unwrap().3.len(), 8 * 2 + 1);

    // Plot the result.
    let mut plt = TermPlot::new();

    for (net_id, paths) in result {
        for path in paths {
            path.iter().zip(path.iter().skip(1)).for_each(|(a, b)| {
                let Node3D(p1, l1) = a;
                let Node3D(p2, l2) = b;
                if l1 == l2 {
                    let edge = REdge::new(p1, p2);
                    plt.draw_edge(*l1 as i32, edge);
                }
            });
        }
    }

    // Plot pin locations.
    for (_, pins) in &pins {
        for pin in pins {
            for &Node3D(p, layer) in pin {
                plt.set_char(3, p, 'x');
            }
        }
    }

    println!("Resulting routes:");
    plt.enable_colors(true);
    plt.print()
}

#[test]
fn test_route_multi_signal_offgrid() {
    type T = i32;

    // A-star heuristic.
    let heuristic_fn = |n1: &Node3D<_>, n2: &Node3D<T>| -> T { (n1.0 - n2.0).norm1() };

    let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
        if n1.1 != n2.1 {
            1 as T // Via cost
        } else {
            (n1.0 - n2.0).norm1()
        }
    };

    let graph = GridGraph::new(
        Rect::new((0, 0), (10, 10)),
        2,
        Vector::new(2, 2),
        Vector::new(0, 0),
    );

    // let p = Point::new(1, 1);
    // assert_eq!(p, graph.closest_grid_point(p));

    let net_pins = vec![
        (
            0,
            vec![vec![Node3D::new(3, 2, 0)], vec![Node3D::new(7, 7, 0)]],
        ),
        // (1, vec![
        //     vec![Node3D::new(8, 4, 0)],
        //     vec![Node3D::new(5, 5, 0)],
        //     vec![Node3D::new(5, 9, 0)],
        // ]),
        // (3, vec![
        //     vec![Node3D::new(9, 1, 0)],
        //     vec![Node3D::new(9, 1, 0)],
        // ]),
    ];

    let pins = net_pins.into_iter().collect();

    let can_use_node = |_: usize, _: &Node3D<T>| true;

    let off_grid_graph = OffGridGraph::new(graph);

    println!("Start multi signal routing.");
    let result = graph_route_multi_signal(
        &off_grid_graph,
        &pins,
        1,
        1,
        &edge_cost_fn,
        &heuristic_fn,
        &can_use_node,
    );

    dbg!(&result);

    assert_eq!(result.len(), pins.len());

    // assert_eq!(result.unwrap().3.len(), 8 * 2 + 1);

    // Plot the result.
    let mut plt = TermPlot::new();

    for (net_id, paths) in result {
        for path in paths {
            path.iter().zip(path.iter().skip(1)).for_each(|(a, b)| {
                let Node3D(p1, l1) = a;
                let Node3D(p2, l2) = b;
                if l1 == l2 {
                    let edge = REdge::new(p1, p2);
                    plt.draw_edge(*l1 as i32, edge);
                }
            });
        }
    }

    // Plot pin locations.
    for (_, pins) in &pins {
        for pin in pins {
            for &Node3D(p, layer) in pin {
                plt.set_char(3, p, 'x');
            }
        }
    }

    println!("Resulting routes:");
    plt.enable_colors(false);
    plt.print()
}
