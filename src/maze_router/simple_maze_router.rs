// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use iron_shapes_algorithms::rectangle_decomposition::decompose_rectangles;
use itertools::Itertools;
use libreda_pnr::db::*;
use libreda_pnr::route::simple_router::{SimpleRoutedNet, SimpleRouter};

use iron_shapes_algorithms::prelude::decompose_rectangles_vertical;
use log;
use num_traits::{FromPrimitive, NumCast, PrimInt, Signed, ToPrimitive};

use std::collections::{HashMap, HashSet};
use std::hash::Hash;

use crate::util::simplify_point_string;

use crate::graph::*;

use super::route_multi_signal::graph_route_multi_signal;
use super::router_traits::RoutedSignal;
use super::simple_signal_router;
use crate::global_router::GlobalRoutingGraph;
use crate::maze_router::pathfinder_parallel::PathFinder;
use crate::maze_router::pathfinder_sequential::PathFinderSequential;
use crate::maze_router::router_traits::MultiSignalRouter;
use crate::maze_router::simple_signal_router::SimpleSignalRouter;
use crate::multilevel_maze_router::MultilevelMazeRouter;
use libreda_pnr::route::prelude::*;

/// Example implementation of a simple detail router.
#[derive(Clone, Debug)]
pub struct SimpleMazeRouter<T, LayerId> {
    /// Half of the wire width.
    /// The same wire width is used on all layers.
    half_wire_width: T,
    /// Minimum spacing between metal shapes.
    /// The same spacing rule is used on all routing layers.
    min_spacing: T,
    /// Horizontal and vertical wire weights for each layer.
    /// Starting with the lowest routing layer (closest to the transistors).
    /// The cost of a wire segment is computed by the product of the length and the wire weight.
    wire_weights: Vec<(T, T)>,
    /// Cost of a via.
    via_cost: T,

    routing_layers: Vec<LayerId>,
    via_layers: Vec<LayerId>,
    /// Shape of a via. TODO: This is oversimplified. Need shapes for different via types.
    default_via_shape: Rect<T>,
}

//
// impl<LayerId> SimpleRouter for SimpleMazeRouter<SInt, LayerId> {
//     fn name(&self) -> &str {
//         "SimpleMazeRouter"
//     }
//
//     fn compute_routes_impl(&self,
//                            boundary: Rect<SInt>,
//                            net_terminals: &HashMap<usize, Vec<Vec<(SimpleRPolygon<SInt>, u8)>>>,
//                            obstacles: &Vec<(SimpleRPolygon<SInt>, u8)>,
//     ) -> HashMap<usize, SimpleRoutedNet> {
//
//         let results = self.route_multi_signal(
//             boundary,
//             &net_terminals,
//             &obstacles,
//         );
//
//         // Convert the rectangles into `Geometry`s.
//
//         let geometries = results.into_iter()
//             .map(|(net, (shapes, vias))| {
//                 let geometries = shapes.into_iter()
//                     .map(|(layer, s)| (layer, s.into()))
//                     .collect();
//
//                 let routed_net = SimpleRoutedNet {
//                     routes: geometries,
//                     vias: vias,
//                 };
//
//                 (net, routed_net)
//             })
//             .collect();
//
//         geometries
//     }
// }

/// Result of detail routing: Routes for each net.
pub struct SimpleRoutingResult<L: LayoutIds> {
    routing_layers: Vec<L::LayerId>,
    via_layers: Vec<L::LayerId>,
    routes: HashMap<usize, SimpleRoutedNet<L::Coord>>,
    /// Shape of a via.
    /// TODO: This is oversimplified. Include shapes for different via types.
    default_via_shape: Rect<L::Coord>,
}

impl<LN> DrawDetailRoutes<LN> for SimpleRoutingResult<LN>
where
    LN: L2NEdit,
{
    type Error = ();

    fn draw_detail_routes(&self, chip: &mut LN, top: &LN::CellId) -> Result<(), Self::Error> {
        for (_net, routed_net) in &self.routes {
            // Draw routing paths.

            // Draw wires.
            let route_geometries = routed_net.routes.iter().cloned();
            for (layer, rect) in route_geometries {
                if layer as usize > self.routing_layers.len() {
                    log::error!("Route uses more layers than specified in the layerstack.");
                }

                chip.insert_shape(&top, &self.routing_layers[layer as usize], rect.into());
            }

            // Draw vias.
            let via_shape0 = self.default_via_shape.clone();
            let vias = routed_net.vias.iter().cloned();
            for (layer1, layer2, location) in vias {
                assert_eq!(
                    layer1 + 1,
                    layer2,
                    "Invalid via between two non-neighbour layers."
                );
                let via_shape = via_shape0.translate(location.into());
                chip.insert_shape(&top, &self.via_layers[layer1 as usize], via_shape.into());
            }
        }

        Ok(())
    }
}

impl<LN> DetailRouter<LN> for SimpleMazeRouter<SInt, LN::LayerId>
where
    LN: L2NEdit<Coord = SInt>,
{
    type RoutingResult = SimpleRoutingResult<LN>;
    /// On error: return the successfully routed nets and a vector of unrouted nets.
    type Error = (SimpleRoutingResult<LN>, Vec<LN::NetId>);

    fn name(&self) -> String {
        "SimpleMazeRouter".into()
    }

    fn route<RP>(&self, routing_problem: RP) -> Result<Self::RoutingResult, Self::Error>
    where
        RP: DetailRoutingProblem<LN>,
    {
        let chip = routing_problem.fused_layout_netlist();
        let top_cell = routing_problem.top_cell();
        let routing_layers = &self.routing_layers;
        let via_layers = &self.via_layers;

        assert_eq!(
            routing_layers.len(),
            via_layers.len() + 1,
            "There must be exactly one more routing layer than via layers."
        );

        // ** PREPARE for ROUTING **
        //

        // Find geometries of terminals for each net.
        let net_terminals = extract_net_terminal_geometries(chip, &top_cell);

        // Take only nets that should be routed.
        let nets_set: HashSet<_> = routing_problem.nets().collect();
        let net_terminals: HashMap<_, _> = net_terminals
            .into_iter()
            .filter(|(n, _)| nets_set.contains(n))
            .collect();

        // Print some statistics on terminals.
        {
            let num_terminals: usize = net_terminals.values().map(|t| t.len()).sum();
            let num_one_terminal_nets = net_terminals.iter().filter(|(_, t)| t.len() <= 1).count();
            log::info!("Number of nets with terminals: {}", net_terminals.len());
            log::info!("Number of net terminals: {}", num_terminals);
            if num_one_terminal_nets > 0 {
                log::warn!(
                    "Number of nets with less than two terminals: {}",
                    num_one_terminal_nets
                );
            }
        }

        // Mapping from layer IDs to subsequent layer numbers.
        let layer_numbers: HashMap<_, _> = routing_layers
            .iter()
            .enumerate()
            .map(|(i, layer)| (layer.clone(), i as u8))
            .collect();

        // Convert terminals into rectilinear polygons.
        let net_terminals: HashMap<_, _> = net_terminals
            .into_iter()
            .map(|(net, terminals)| {
                let r_terminals: Vec<_> = terminals
                    .into_iter()
                    .map(|(t, layer)| {
                        assert_eq!(
                            t.interiors.len(),
                            0,
                            "Polygons with holes are not supported."
                        );
                        vec![(
                            SimpleRPolygon::try_new(t.exterior.points())
                                .expect("Only rectilinear polygons are supported"),
                            layer_numbers[&layer],
                        )] // Assign correct layer.
                    })
                    .collect();
                (net, r_terminals)
            })
            .collect();

        // Create a set of obstacles.
        // For testing everything is put on a single layer.
        let mut obstacles: Vec<_> = net_terminals
            .values()
            .flat_map(|terminals| terminals.iter().flatten().cloned())
            .collect();

        debug_assert!(
            obstacles
                .iter()
                .all(|(poly, _)| poly.orientation().is_counter_clock_wise()),
            "Obstacle polygons must have counter-clock-wise orientation."
        );

        // Get the bounding box of the top cell.
        let top_cell_bounding_box = chip.bounding_box(&top_cell).unwrap();

        assert!(routing_layers.len() <= u8::MAX as usize);
        let num_layers = routing_layers.len() as u8;

        // Register existing shapes such as the power grid as obstacles.
        for (layer_num, layer) in routing_layers.iter().enumerate() {
            let layer_info = chip.layer_info(layer);
            log::info!(
                "Find obstacles on layer {}/{} (name={:?})",
                layer_info.index,
                layer_info.datatype,
                layer_info.name
            );
            // let mut debug_shapes = Vec::new();
            chip.for_each_shape_recursive(&top_cell, layer, |tf, _id, shape| {
                let poly = shape.transformed(&tf).to_polygon();
                assert_eq!(
                    poly.interiors.len(),
                    0,
                    "Polygons with holes are not supported."
                );

                let rpoly = SimpleRPolygon::try_new(poly.exterior.points())
                    .expect("Only rectilinear polygons are supported");

                // debug_shapes.push((rpoly.clone(), layer.clone()));
                obstacles.push((rpoly, layer_num as u8));
            });
        }
        log::info!("Number of obstacle shapes: {}", obstacles.len());

        // Replace net IDs by identifiers of type usize.
        // Create lookup table for the net-to-ID mapping.
        let nets_to_be_routed: Vec<_> = net_terminals.keys().cloned().collect();
        let net_id_lookup_table: HashMap<_, _> = nets_to_be_routed
            .iter()
            .enumerate()
            .map(|(i, net)| (net.clone(), i))
            .collect();
        // Replace net IDs with integers.
        let terminals = net_terminals
            .into_iter()
            .map(|(net, terms)| (net_id_lookup_table[&net], terms))
            .collect();

        // TODO: This sizing is arbitrary.
        let boundary = top_cell_bounding_box.sized(4000, 4000);

        assert!(
            num_layers <= u8::MAX,
            "maximum number of routing layers is limited to 255 but found {}",
            num_layers
        );

        {
            // Warn about nets without routing guide.
            let num_nets_without_routing_guide = nets_to_be_routed
                .iter()
                .filter(|net| routing_problem.routing_guide(net).is_none())
                .count();

            if num_nets_without_routing_guide > 0 {
                log::warn!(
                    "Number of nets without routing guide: {}",
                    num_nets_without_routing_guide
                );
            }
        }

        // Compute the routes.
        let is_point_in_routing_guide =
            |net: &LN::NetId, layer: &LN::LayerId, p: Point<LN::Coord>| -> bool {
                let guide = routing_problem.routing_guide(net);
                guide
                    .map(|guide| guide.contains_point(layer, p))
                    .unwrap_or(false)
            };

        let can_use_node = |net: usize, node: &Node3D<LN::Coord>| -> bool {
            // Translate net IDs and layer IDs.
            let Node3D(p, layer) = node;
            let layer = &routing_layers[*layer as usize];
            let net = &nets_to_be_routed[net];

            is_point_in_routing_guide(net, layer, *p)
        };

        let routes = self.route_multi_signal(boundary, &terminals, &obstacles, &can_use_node);

        // Convert the rectangles into `Geometry`s.
        let routes: HashMap<_, _> = routes
            .into_iter()
            .map(|(net, (shapes, vias))| {
                let geometries = shapes
                    .into_iter()
                    .map(|(layer, s)| (layer, s.into()))
                    .collect();

                let routed_net = SimpleRoutedNet {
                    routes: geometries,
                    vias: vias,
                };

                (net, routed_net)
            })
            .collect();

        // Find nets that have not been routed.
        let unrouted_nets: Vec<_> = nets_to_be_routed
            .iter()
            .filter(|&n| !routes.contains_key(&net_id_lookup_table[n]))
            .cloned()
            .collect();

        let result = SimpleRoutingResult {
            routing_layers: self.routing_layers.clone(),
            via_layers: self.via_layers.clone(),
            routes,
            default_via_shape: self.default_via_shape,
        };

        if unrouted_nets.is_empty() {
            Ok(result)
        } else {
            Err((result, unrouted_nets))
        }
    }
}

impl<T, LayerId> SimpleMazeRouter<T, LayerId>
where
    T: CoordinateType
        + PrimInt
        + Signed
        + std::fmt::Display
        + std::fmt::Debug
        + Hash
        + ToPrimitive
        + FromPrimitive
        + Send
        + Sync
        + 'static,
{
    /// Create a new router instance.
    pub fn new(
        half_wire_width: T,
        min_spacing: T,
        wire_weights: Vec<(T, T)>,
        routing_layers: Vec<LayerId>,
        via_layers: Vec<LayerId>,
    ) -> Self {
        Self {
            half_wire_width,
            min_spacing,
            wire_weights,
            via_cost: T::from(100).unwrap(),
            routing_layers,
            via_layers,
            default_via_shape: Rect::new((-20, -20), (20, 20)).cast(),
        }
    }

    /// Connect terminals of multiple signals.
    ///
    /// Returns: `HashMap< net id, (Vec<(layer, rectangle)>, Vec<(layer1, layer2, via_location)>)>`
    fn route_multi_signal<Guide>(
        &self,
        boundary: Rect<T>,
        net_terminals: &HashMap<usize, Vec<Vec<(SimpleRPolygon<T>, u8)>>>,
        obstacles: &Vec<(SimpleRPolygon<T>, u8)>,
        routing_guide: &Guide,
    ) -> HashMap<usize, (Vec<(u8, Rect<T>)>, Vec<(u8, u8, Point<T>)>)>
    where
        Guide: Fn(usize, &Node3D<T>) -> bool,
    {
        let half_wire_width = self.half_wire_width;
        let wire_weights = &self.wire_weights;
        let via_cost = self.via_cost;
        let min_spacing = self.min_spacing;
        let num_layers = wire_weights.len() as u8;

        assert_eq!(wire_weights.len(), num_layers as usize);

        debug_assert!(
            (0..net_terminals.len()).all(|i| net_terminals.contains_key(&i)),
            "Net IDs must be consecutive integers."
        );

        // Compute minimal costs for both routing directions.
        // This is used to compute the lower bound on the cost. Used for the A-star heuristic.
        let min_weight_horizontal = wire_weights.iter().map(|&(h, _)| h).min().unwrap();
        let min_weight_vertical = wire_weights.iter().map(|&(_, v)| v).min().unwrap();

        // A-star heuristic.
        let heuristic_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
            let diff = n1.0 - n2.0;
            let weighted: Vector<_> =
                (diff.x * min_weight_horizontal, diff.y * min_weight_vertical).into();
            weighted.norm1()
        };

        // Cost estimate from node `n1` to node `n2`.
        let edge_cost_fn = |n1: &Node3D<T>, n2: &Node3D<T>| -> T {
            if n1.1 != n2.1 {
                // Layers are not the same, must be a via.
                via_cost // Via cost
            } else {
                // Edge on same layer.
                let layer = n1.1;
                // Get wire costs for horizontal and vertical directions.
                let (wh, wv) = wire_weights[layer as usize];
                let diff = n1.0 - n2.0;
                let weighted: Vector<_> = (diff.x * wh, diff.y * wv).into();
                weighted.norm1()
            }
        };

        let grid_pitch = half_wire_width + half_wire_width + min_spacing;

        // Fine routing grid.
        let fine_grid = grid_graph::GridGraph::new(
            boundary,
            num_layers,
            Vector::new(grid_pitch, grid_pitch),
            Vector::zero(),
        )
        .set_preferred_routing_directions(grid_graph::PreferredRoutingDirection::XY);

        log::info!("Fine grid: {}x{}", fine_grid.num_x(), fine_grid.num_y());
        log::info!(
            "Preferred routing direction on layer 0: {:?}",
            fine_grid.routing_directions
        );

        let mut off_grid_graph = OffGridGraph::new(fine_grid);

        // Translate the terminal shapes into nodes in the graph.
        log::info!("Find pin nodes.");
        let pin_nodes =
            self.convert_terminals_to_grid_nodes(net_terminals, &fine_grid, &mut off_grid_graph);

        // Mark nodes that are reserved for a specific net with the label (net_id + 2).
        // Nodes that can be used by any net have label 0.
        // Nodes that cannot used by any net have label 1.
        log::info!("Find occupied grid nodes.");
        let mut reserved_nodes = off_grid_graph.create_attributes(0);
        {
            // Mark all routing nodes that interact with `poly` within a certain margin.
            let mut mark = |net: usize, layer: u8, poly: &SimpleRPolygon<T>, margin: T| {
                let rects = decompose_rectangles(poly.edges());
                for r in rects {
                    let margin_x = margin;
                    let margin_y = margin;
                    fine_grid
                        .all_nodes_xy(&r.sized(margin_x, margin_y))
                        .map(move |xy| Node3D(xy, layer))
                        .for_each(|p| {
                            let reserved = *reserved_nodes.get(&p);
                            if reserved == 0 || reserved == 1 {
                                // Pin shapes overwrite obstacles.
                                reserved_nodes.set(&p, net);
                            } else if reserved != net {
                                // Reserved by more than one net => no net can use the node.
                                reserved_nodes.set(&p, 1);
                            }
                        })
                }
            };

            // Mark obstacles.
            for (shape, layer) in obstacles {
                mark(1, *layer, shape, min_spacing + half_wire_width);
            }

            // Mark terminals to be used for their nets only.
            for (net, pins) in net_terminals {
                for pin in pins {
                    for (terminal_shape, layer) in pin {
                        mark(
                            net + 2,
                            *layer,
                            terminal_shape,
                            min_spacing + half_wire_width,
                        );
                    }
                }
            }
        }

        // Check if a node can be used for routing: The node must be in the coarse routing
        // guide and it must not be an obstacle.
        let can_use_node = |net: usize, node: &Node3D<T>| {
            // Check if the global route allows this point to be used by the net.
            let is_in_coarse_route = routing_guide(net, node);

            // Check that the node is in the coarse route and not blocked by an obstacle.
            let net_reservation = *reserved_nodes.get(node);
            is_in_coarse_route && (net_reservation == 0 || net_reservation == net + 2)
        };

        // // Sanity check: all pin nodes must be included in the coarse routes.
        // debug_assert!(
        //     pin_nodes.iter()
        //         .all(|(net, pins)| {
        //             pins.iter().all(|terminals| {
        //                 terminals.iter().any(|n| {
        //                     can_use_node(*net, n)
        //                 })
        //             })
        //         }),
        //         "Some pin nodes are not contained in the global route."
        // );

        log::info!("Start fine graph router.");
        let results = graph_route_multi_signal(
            // &fine_grid,
            &off_grid_graph,
            &pin_nodes,
            half_wire_width,
            min_spacing,
            &edge_cost_fn,
            &heuristic_fn,
            &can_use_node,
        )
        .into_iter()
        .collect_vec();

        // Simplify routes.
        // Split routes into point strings that are on single layers only.
        let results_simplified: Vec<(usize, Vec<(_, PointString<T>)>)> = results
            .iter()
            .map(|(net_id, routes)| {
                let simplified: Vec<(_, PointString<_>)> = routes
                    .iter()
                    .flat_map(|path| {
                        // Split the new path onto layers.
                        path.iter()
                            // Create path segments that are on a single layer only.
                            .group_by(|Node3D(point, layer)| *layer)
                            .into_iter()
                            // Create (layer, path) pairs.
                            .map(|(layer, path_segment)| {
                                let points = path_segment.map(|n| n.point()).dedup().collect();
                                // Remove redundant points. Leave corners only.
                                let points = simplify_point_string(&points);
                                let path_segment = PointString::new(points);
                                (layer, path_segment)
                            })
                            .collect_vec()
                            .into_iter()
                    })
                    .collect_vec();

                (*net_id, simplified)
            })
            .collect();

        // Extract via locations.
        // Whenever a path jumps from one layer to another, a via must be inserted.
        let via_locations: Vec<(usize, _)> = results
            .iter()
            .map(|(net_id, routes)| {
                let via_locations: Vec<(u8, u8, Point<_>)> = routes
                    .iter()
                    .flat_map(|path| {
                        path.iter().zip(path.iter().skip(1)).filter_map(|(a, b)| {
                            if a.1 != b.1 {
                                let lower = a.1.min(b.1);
                                let upper = a.1.max(b.1);
                                Some((lower, upper, a.0))
                            } else {
                                None
                            }
                        })
                    })
                    .collect_vec();

                (*net_id, via_locations)
            })
            .collect();

        // Convert paths into rectangle shapes.
        let results = results_simplified
            .iter()
            .zip(via_locations)
            .map(|((net, paths), (via_net, net_via_locations))| {
                debug_assert_eq!(*net, via_net);

                // Convert point strings to geometric shapes of wires.
                let path_shapes: Vec<_> = paths
                    .iter()
                    .flat_map(|(layer, path)| {
                        path.iter().zip(path.iter().skip(1)).map(move |(p1, p2)| {
                            let edge = REdge::new(p1, p2);
                            let rect = edge_to_rect(edge, half_wire_width);
                            (*layer, rect)
                        })
                    })
                    .collect();

                (*net, (path_shapes, net_via_locations))
            })
            .collect();

        results
    }

    /// Find grid nodes for the terminal shapes.
    /// If necessary, create off-grid nodes in the `off_grid_graph`..
    fn convert_terminals_to_grid_nodes(
        &self,
        net_terminals: &HashMap<usize, Vec<Vec<(SimpleRPolygon<T>, u8)>>>,
        fine_grid: &grid_graph::GridGraph<T>,
        off_grid_graph: &mut OffGridGraph<T>,
    ) -> Vec<(usize, Vec<Vec<Node3D<T>>>)> {
        let mut pin_nodes: Vec<_> = net_terminals
            .iter()
            .map(|(net, terminals)| {
                log::debug!("Find terminal nodes for net {}", net);
                let nodes: Vec<_> = terminals
                    .iter()
                    .enumerate()
                    .map(|(i, shapes)| {
                        let pin_nodes = shapes
                            .iter()
                            .flat_map(|(shape, layer)| {
                                let bbox = shape.try_bounding_box().unwrap();
                                // Find all nodes that interact with the terminal shape.
                                // Candidate nodes are nodes that interact with the bounding box.
                                log::debug!("Find on-grid nodes.");
                                let on_grid_nodes = fine_grid
                                    .all_nodes_xy(&bbox)
                                    .filter(move |p| shape.contains_point(*p))
                                    .map(move |xy| Node3D(xy, *layer))
                                    .collect_vec();

                                if on_grid_nodes.is_empty() {
                                    log::debug!("Find off-grid nodes.");
                                    // There's no on-grid nodes. Find off-grid nodes instead.
                                    let mut off_grid_nodes = vec![];

                                    // Decompose the pin shape into rectangles. Take the center
                                    // of the biggest rectangle as terminal node.
                                    log::debug!("Decompose rectangles.");
                                    let rects = decompose_rectangles(shape.edges());
                                    let rects90 = decompose_rectangles_vertical(shape.edges());

                                    log::debug!("Find largest rectangle.");
                                    let biggest_rect = rects
                                        .into_iter()
                                        .chain(rects90)
                                        .max_by_key(|r| r.area_doubled_oriented())
                                        .expect("Pin has no area.");

                                    // Create a terminal node in the center.
                                    let center = biggest_rect.center();
                                    let n = Node3D(center, *layer);
                                    // Remember the off-grid node.
                                    off_grid_graph.insert_node(n);
                                    // Store node as terminal.
                                    off_grid_nodes.push(n);

                                    // let _2 = T::one() + T::one();
                                    // // Use the middle points of the polygon edges as terminals.
                                    // shape.edges()
                                    //     .for_each(|edge| {
                                    //         let center = (edge.start() + edge.end()) / _2;
                                    //         let n = (center, *layer);
                                    //         // Remember the off-grid node.
                                    //         off_grid_graph.insert_node(n);
                                    //         // Store node as terminal.
                                    //         off_grid_nodes.push(n)
                                    //     });

                                    off_grid_nodes.into_iter()
                                } else {
                                    // No need for off-grid nodes, take on-grid nodes.
                                    on_grid_nodes.into_iter()
                                }
                            })
                            .unique()
                            .collect_vec();
                        if pin_nodes.is_empty() {
                            // TODO
                            log::warn!("Pin without routing nodes.");
                        }
                        pin_nodes
                    })
                    .collect();

                (*net, nodes)
            })
            .collect();

        // Sort nets by the perimeter of their bounding boxes such that small nets are routed first.
        self.sort_pin_nodes_by_bounding_box(&mut pin_nodes);

        pin_nodes
    }

    /// Sort nets by the perimeter of their bounding boxes.
    fn sort_pin_nodes_by_bounding_box(&self, pin_nodes: &mut Vec<(usize, Vec<Vec<Node3D<T>>>)>) {
        pin_nodes.sort_by_cached_key(|(net, node)| {
            if let Some(Node3D(p, _layer)) = node.iter().flatten().nth(0) {
                let mut x_min = p.x;
                let mut x_max = p.x;
                let mut y_min = p.y;
                let mut y_max = p.y;

                for Node3D(p, _layer) in node.iter().flatten() {
                    x_min = x_min.min(p.x);
                    x_max = x_max.max(p.x);
                    y_min = y_min.min(p.y);
                    y_max = y_max.max(p.y);
                }

                let dx = x_max - x_min;
                let dy = y_max - y_min;

                dx + dy
            } else {
                T::zero()
            }
        });
    }

    /// Create the coarse global routing grid and estimate its routing capacities
    /// based on the obstacle shapes.
    fn create_global_routing_grid(
        &self,
        fine_grid: &grid_graph::GridGraph<T>,
        coarsening_factor: T,
        routing_pitch: &[T],
        via_pitch: &[T],
        obstacles: &[(SimpleRPolygon<T>, u8)],
    ) -> GlobalRoutingGraph<T> {
        log::info!("Fine grid: {}x{}", fine_grid.num_x(), fine_grid.num_y());
        {
            let num_layers = fine_grid.num_layers;
            assert_eq!(routing_pitch.len(), num_layers as usize);
            assert_eq!(via_pitch.len(), num_layers as usize);
        }

        // Find routing guides on a coarser grid.
        // Create a coarser grid.
        let coarse_grid = grid_graph::GridGraph {
            grid_pitch: fine_grid.grid_pitch * coarsening_factor,
            ..*fine_grid
        };

        log::info!(
            "Coarse grid: {}x{}",
            coarse_grid.num_x(),
            coarse_grid.num_y()
        );

        let mut coarse_graph = GlobalRoutingGraph::new(coarse_grid);

        log::info!("Estimate routing capacities of the global routing grid.");
        crate::global_router::capacity_estimation::estimate_gcell_capacities(
            &mut coarse_graph,
            obstacles,
            routing_pitch,
            via_pitch,
        );

        coarse_graph
    }

    /// Find coarse routes based on the Pathfinder negotiation algorithm.
    fn find_coarse_routes<FE, FH>(
        &self,
        coarse_graph: &GlobalRoutingGraph<T>,
        coarsening_factor: T,
        pin_nodes: &Vec<(usize, Vec<Vec<Node3D<T>>>)>,
        edge_cost_fn: &FE,
        heuristic_fn: &FH,
    ) -> (grid_graph::GridGraph<T>, Vec<(usize, GridMap<T, bool>)>)
    where
        FE: Fn(&Node3D<T>, &Node3D<T>) -> T + Sync,
        FH: Fn(&Node3D<T>, &Node3D<T>) -> T + Sync,
    {
        let coarse_grid = *coarse_graph.grid_graph();

        // Find coarse routing terminals first.
        let coarse_pin_nodes: Vec<(usize, Vec<Vec<Node3D<T>>>)> = pin_nodes
            .iter()
            .map(|(net, pins)| {
                let coarse_pins: Vec<_> = pins
                    .iter()
                    .map(|terminals| {
                        // Map the terminal nodes to the closest nodes on the coarse grid.
                        terminals
                            .iter()
                            .map(|&Node3D(p, layer)| {
                                Node3D(coarse_grid.closest_grid_point(p), layer)
                            })
                            .unique()
                            .collect_vec()
                    })
                    .collect();

                (*net, coarse_pins)
            })
            .collect();

        // Setup pathfinder router.
        let single_signal_router = SimpleSignalRouter::new();
        // let pathfinder = PathFinder::new(&single_signal_router);
        let pathfinder = PathFinderSequential::new(&single_signal_router);
        let mut multi_signal_router = MultilevelMazeRouter::new(&pathfinder);
        // multi_signal_router.set_global_congestion_output_directory("/tmp/".into());
        // let multi_signal_router = pathfinder;

        log::info!("Start coarse routing to find routing guides.");

        let node_cost_fn = |n: &Node3D<T>| -> T { Zero::zero() };

        let edge_cost_float_fn = |a: &Node3D<T>, b: &Node3D<T>| -> T { edge_cost_fn(a, b) };

        let node_capacity_fn =
            |n: &Node3D<T>| -> u32 { (coarsening_factor * coarsening_factor).to_u32().unwrap() };

        let edge_capacity_fn = |a: &Node3D<T>, b: &Node3D<T>| -> u32 {
            let is_same_layer = a.1 == b.1;
            if is_same_layer {
                coarsening_factor.to_u32().unwrap() //* 3 / 4
            } else {
                // Via
                (coarsening_factor * coarsening_factor).to_u32().unwrap() // / 4
            }
        };

        // Run pathfinder.
        let routing_result = multi_signal_router.route_multisignal(
            // coarse_graph.grid_graph(),
            &coarse_graph,
            &node_cost_fn,
            &edge_cost_float_fn,
            &node_capacity_fn,
            &edge_capacity_fn,
            &|_, _| true,
            &coarse_pin_nodes,
            &HashMap::new(),
        );

        if routing_result.is_err() {
            log::error!("Failed to find global routes.");
        }
        let routing_result = routing_result.expect("Failed to find global routes.");

        // Sanity check: All pin nodes must be included in the found route.
        for ((net_id, route), (net_id_orig, pins)) in routing_result.iter().zip(coarse_pin_nodes) {
            assert_eq!(
                *net_id, net_id_orig,
                "Ordering of routing results is not matching the ordering of the input nets."
            );
            let all_route_nodes: HashSet<_> = route.nodes().collect();
            debug_assert!(
                pins.iter()
                    .all(|sub_pins| { sub_pins.iter().any(|pin| all_route_nodes.contains(pin)) }),
                "Route does not contain all pins of the net."
            );
        }

        let mut coarse_guide_nodes: Vec<_> = routing_result
            .iter()
            .map(|(net_id, route)| {
                // Mark all nodes used by the coarse route as 'allowed'.
                let mut allowed_nodes = coarse_grid.create_attributes(false);
                route
                    .nodes()
                    // .for_each(|(p, _layer)| allowed_nodes.set(&Node3D(p, 0), true)); // Ignore metal layer of coarse route and allow all other metal layers for fine routes.
                    .for_each(|Node3D(p, layer)| allowed_nodes.set(&Node3D(p, layer), true));

                (*net_id, allowed_nodes)
            })
            .collect();

        (coarse_grid, coarse_guide_nodes)
    }

    /// Find coarse routes without any congestion avoidance.
    fn find_coarse_routes_naive<FE, FH>(
        &self,
        fine_grid: &grid_graph::GridGraph<T>,
        pin_nodes: &Vec<(usize, Vec<Vec<Node3D<T>>>)>,
        edge_cost_fn: &FE,
        heuristic_fn: &FH,
    ) -> (grid_graph::GridGraph<T>, Vec<(usize, GridMap<T, bool>)>)
    where
        FE: Fn(&Node3D<T>, &Node3D<T>) -> T,
        FH: Fn(&Node3D<T>, &Node3D<T>) -> T,
    {
        // Create global routing tiles that have a width of `coarsening_factor` routing tracks.
        let coarsening_factor = T::from(32).unwrap();

        // Find routing guides on a coarser grid.
        // Create a coarser grid.
        let coarse_grid = grid_graph::GridGraph {
            grid_pitch: fine_grid.grid_pitch * coarsening_factor,
            ..*fine_grid
        };

        log::info!(
            "Coarse grid: {}x{}",
            coarse_grid.num_x(),
            coarse_grid.num_y()
        );

        // Find coarse routing terminals first.
        let mut coarse_pin_nodes: Vec<(usize, Vec<Vec<_>>)> = pin_nodes
            .iter()
            .map(|(net, pins)| {
                let coarse_pins: Vec<_> = pins
                    .iter()
                    .map(|terminals| {
                        // Map the terminal nodes to the closest nodes on the coarse grid.
                        terminals
                            .iter()
                            .map(|&Node3D(p, layer)| {
                                Node3D(coarse_grid.closest_grid_point(p), layer)
                            })
                            .unique()
                            .collect_vec()
                    })
                    .collect();

                (*net, coarse_pins)
            })
            .collect();

        // For each net find a coarse route independent of the other nets.
        // The route in the coarse grid graph is then used as a guide for this net in the finer routing step.
        log::info!("Start coarse routing to find routing guides.");
        let num_nets = coarse_pin_nodes.len();
        let coarse_guide_nodes: Vec<_> = coarse_pin_nodes
            .iter()
            .enumerate()
            .map(|(i, (net_id, pins))| {
                log::info!(
                    "Find coarse route {}/{}. Number of pins: {}",
                    i + 1,
                    num_nets,
                    pins.len()
                );

                let routes = simple_signal_router::route_single_signal(
                    // let routes = route_single_signal::route_single_signal_from_source(
                    &coarse_grid,
                    pins,
                    self.half_wire_width,
                    self.min_spacing,
                    &edge_cost_fn,
                    &heuristic_fn,
                    false,
                );

                // Mark all nodes used by the coarse route as 'allowed'.
                let mut allowed_nodes = coarse_grid.create_attributes(false);
                routes
                    .into_iter()
                    .flatten()
                    .flatten()
                    .for_each(|Node3D(p, _layer)| allowed_nodes.set(&Node3D(p, 0), true));

                (*net_id, allowed_nodes)
            })
            .collect();

        (coarse_grid, coarse_guide_nodes)
    }
}

/// Convert an edge into a rectangle which has the edge as center line
/// and width `2*half_wire_width`.
fn edge_to_rect<T: CoordinateType>(e: REdge<T>, half_wire_width: T) -> Rect<T> {
    // Create rectangle shape from this edge.
    let start = e.start();
    let end = e.end();
    let (lower_left, upper_right) = if e.is_horizontal() {
        let w = Vector::new(half_wire_width, half_wire_width);
        if start.x < end.x {
            (start - w, end + w)
        } else {
            (end - w, start + w)
        }
    } else {
        debug_assert!(e.is_vertical() || e.is_degenerate());
        let w = Vector::new(half_wire_width, half_wire_width);
        if start.y < end.y {
            (start - w, end + w)
        } else {
            (end - w, start + w)
        }
    };

    Rect::new(lower_left, upper_right)
}
