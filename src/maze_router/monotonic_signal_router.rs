// Copyright (c) 2020-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Single-net router which uses monotonic routing for subnets.

use itertools::Itertools;

use crate::graph::{GridGraph, Node3D, TraverseGraph};
use crate::maze_router::monotonic_route;
use num_traits::{Num, PrimInt, Signed};
use std::hash::Hash;

use crate::maze_router::router_traits::RoutedSignal;
use crate::maze_router::router_traits::SignalRouter;
use crate::maze_router::simple_signal_router::SimpleSignalRouterResult;
use iron_shapes::prelude::TryIntoBoundingBox;
use std::marker::PhantomData;

use libreda_pnr::db;
use std::collections::HashSet;

/// Route a single net by decomposing it into 2-terminal nets which are then
/// connected with monotonic routes.
pub struct MonotonicSignalRouter<Coord> {
    _coordinate_type: PhantomData<Coord>,
}

impl<T> MonotonicSignalRouter<T> {
    /// Create a new router instance.
    pub fn new() -> Self {
        Self {
            _coordinate_type: Default::default(),
        }
    }
}

impl<Coord> SignalRouter for MonotonicSignalRouter<Coord>
where
    Coord: db::CoordinateType
        + Copy
        + PartialOrd
        + Num
        + Eq
        + Hash
        + PrimInt
        + Signed
        + std::fmt::Debug,
{
    type Graph = GridGraph<Coord>;
    type GraphNode = Node3D<Coord>;
    type RoutingResult = SimpleSignalRouterResult<Self::GraphNode, Coord>;
    type Cost = Coord;

    fn route<NodeCostFn, EdgeCostFn>(
        &self,
        graph: &Self::Graph,
        _node_cost_fn: &NodeCostFn,
        edge_cost_fn: &EdgeCostFn,
        terminals: &Vec<Vec<Self::GraphNode>>,
        _previous_solution: Option<&Self::RoutingResult>,
    ) -> Option<Self::RoutingResult>
    where
        NodeCostFn: Fn(&Self::GraphNode) -> Self::Cost,
        EdgeCostFn: Fn(&Self::GraphNode, &Self::GraphNode) -> Self::Cost,
    {
        let routes =
            self.route_single_signal_with_net_decomposition(graph, terminals, edge_cost_fn);

        // Wrap into a struct.
        let routing_result = routes.map(|routes| SimpleSignalRouterResult {
            routes,
            total_cost: Coord::zero(),
        });

        if let Some(route) = &routing_result {
            // Sanity check: All pin nodes must be included in the found route.
            debug_assert!(
                {
                    let all_route_nodes: HashSet<_> = route.nodes().collect();
                    terminals
                        .iter()
                        .all(|ports| ports.iter().any(|port| all_route_nodes.contains(port)))
                },
                "Route does not contain all pins of the net."
            );
        }

        routing_result
    }

    fn enable_pin_feed_through(&mut self, enable_feed_through: bool) {
        todo!()
    }

    fn is_pin_feed_through_enabled(&self) -> bool {
        todo!()
    }
}

impl<T> MonotonicSignalRouter<T>
where
    T: PrimInt + Signed + Hash + std::fmt::Debug,
{
    /// Connect all pins of a single signal.
    /// Return a list of paths that create the routing tree.
    fn route_single_signal_with_net_decomposition<FE>(
        &self,
        graph: &GridGraph<T>,
        pins: &Vec<Vec<Node3D<T>>>,
        edge_cost_fn: &FE,
    ) -> Option<Vec<Vec<Node3D<T>>>>
    where
        T: PrimInt + Signed + Hash + std::fmt::Debug,
        FE: Fn(&Node3D<T>, &Node3D<T>) -> T,
    {
        // Create local mutable copy of the pins.
        // Copy only pins which have terminals.
        let mut pins = pins
            .iter()
            .filter(|terms| !terms.is_empty())
            .cloned()
            .collect_vec();

        // Create buffer to store found paths.
        let mut paths = Vec::new();

        // As long as there is more than one pin left, connect the two closest pins and merge them.
        while pins.len() > 1 {
            // Find the two subnets which are closest (by manhattan distance of their bounding boxes).
            let (pin_id_1, pin_id_2) = {
                // Find bounding boxes of the pins.
                let pin_bounding_boxes: Vec<_> = pins
                    .iter()
                    .map(
                        |pin_locations| {
                            pin_locations
                                .iter()
                                .map(|Node3D(point, _layer)| point)
                                .try_into_bounding_box()
                                .unwrap()
                        }, // Pins without shapes have been removed already, to there should be no case without bounding box.
                    )
                    .collect();

                crate::multi_pin_decomposition::find_closest_rectangles(&pin_bounding_boxes)
                    .unwrap()
            };

            // Sanity check.
            assert_ne!(pin_id_1, pin_id_2, "Need two different pins.");

            let closest_subnets = vec![pins[pin_id_1].clone(), pins[pin_id_2].clone()];

            let sources = &pins[pin_id_1];
            let targets = &pins[pin_id_2];

            // Try to find a route between the closest pins.
            let path = monotonic_route::full_aerial_monotonic_route_multi_terminal(
                graph,
                sources,
                targets,
                edge_cost_fn,
            );

            if let Some(path) = path {
                // Found a path between closest pins.

                // Merge the nodes of the connected pins. They form now a new pin.
                let pins2 = std::mem::replace(&mut pins[pin_id_2], vec![]);
                pins[pin_id_1].extend(pins2);

                // Construct new pin nodes.
                // Nodes of the two connected pins are now merged.
                pins.remove(pin_id_2);

                // Remember this route.
                paths.push(path);
            } else {
                // Routing failed.
                return None;
            }
        }

        Some(paths)
    }
}

#[test]
fn test_monotonic_signal_router() {
    let graph = GridGraph::new(
        db::Rect::new((0, 0), (10, 10)),
        1,
        db::Vector::new(1i32, 1),
        db::Vector::new(0, 0),
    );

    let pins = vec![
        // vec![((1, 0).into(), 0)],
        // vec![((1, 1).into(), 0)],
        // vec![((0, 2).into(), 0)],
        vec![Node3D((1, 0).into(), 0), Node3D((0, 2).into(), 0)],
        vec![Node3D((1, 1).into(), 0)],
    ];

    let router = MonotonicSignalRouter::new();

    let result = router.route(&graph, &|_| 1, &|_, _| 1i32, &pins, None);

    dbg!(&result);

    assert!(result.is_some());
}
