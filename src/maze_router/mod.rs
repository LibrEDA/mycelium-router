// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Simple-stupid maze router.

mod monotonic_route;
pub mod monotonic_signal_router;
pub mod router_traits;
pub mod simple_signal_router;

mod simple_global_router;
mod simple_global_router_v2;
mod simple_maze_router;

mod route_multi_signal;

pub mod pathfinder_parallel;
pub mod pathfinder_sequential;
mod rmst_signal_router;

pub use simple_global_router::{SimpleGlobalRoute, SimpleGlobalRouter};
pub use simple_maze_router::SimpleMazeRouter;
