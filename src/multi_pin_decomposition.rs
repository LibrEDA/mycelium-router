// Copyright (c) 2022-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Decompose multi-pin nets into a tree which consists of two-pin nets.

use iron_shapes::prelude::Rect;
use libreda_pnr::db;
use libreda_pnr::db::BoundingBox;

#[derive(Debug, PartialEq)]
pub struct TwoPinTree<C> {
    /// Bounding box around all the pins of this subnet.
    bounding_box: db::Rect<C>,
    /// Two subnets or pins.
    content: TwoPinTreeContent<C>,
}

impl<C: Copy + PartialOrd> TwoPinTree<C> {
    fn new(r: db::Rect<C>, index: usize) -> Self {
        Self {
            bounding_box: r,
            content: TwoPinTreeContent::Pin(index),
        }
    }

    fn merged(a: Self, b: Self) -> Self {
        let bbox = a.bounding_box.add_rect(&b.bounding_box);
        Self {
            bounding_box: bbox,
            content: TwoPinTreeContent::SubTree(Box::new(a), Box::new(b)),
        }
    }
}

impl<C: Copy> BoundingBox<C> for TwoPinTree<C> {
    fn bounding_box(&self) -> Rect<C> {
        self.bounding_box
    }
}

#[derive(Debug, PartialEq)]
pub enum TwoPinTreeContent<C> {
    /// Original index of the pin.
    Pin(usize),
    SubTree(Box<TwoPinTree<C>>, Box<TwoPinTree<C>>),
}

/// Group multiple pins of a net into a tree structure.
/// Each tree node (subnet) has two children which represent subnets or pins.
pub fn decompose_multi_pin_net<C>(pin_locations: &Vec<db::Rect<C>>) -> Option<TwoPinTree<C>>
where
    C: db::CoordinateType,
{
    let mut trees: Vec<_> = pin_locations
        .iter()
        .enumerate()
        .map(|(index, pin)| TwoPinTree::new(*pin, index))
        .collect();

    while trees.len() > 1 {
        // Find the two trees with smallest distance.
        let closest_trees = find_closest_rectangles(&trees)
            .expect("There must be two closest trees because there is more than one tree.");

        // Sort indices to be able to safely remove trees from the list.
        let (a, b) = if closest_trees.0 < closest_trees.1 {
            closest_trees
        } else {
            (closest_trees.1, closest_trees.0)
        };

        // Merge closest trees into one tree.
        // Remove both trees in O(n). Can do this because indices are sorted.
        debug_assert!(a < b);
        debug_assert!(a + 1 != trees.len());
        let tree_b = trees.swap_remove(b);
        let tree_a = trees.swap_remove(a);

        // Merge the two subtrees.
        let merged_tree = TwoPinTree::merged(tree_a, tree_b);
        trees.push(merged_tree)
    }

    assert!(trees.len() <= 1);

    trees.into_iter().next()
}

#[test]
fn test_decompose_multi_pin_net_empty() {
    let pins: Vec<db::Rect<i32>> = vec![];
    assert_eq!(decompose_multi_pin_net(&pins), None);
}

#[test]
fn test_decompose_multi_pin_net_single() {
    let pins = vec![db::Rect::new((0, 0), (1, 1))];
    let tree = decompose_multi_pin_net(&pins).unwrap();
    assert_eq!(tree.bounding_box, pins[0]);
    assert_eq!(tree.content, TwoPinTreeContent::Pin(0));
}

#[test]
fn test_decompose_multi_pin_net_three() {
    let pins = vec![
        db::Rect::new((0, 0), (1, 1)),
        db::Rect::new((10, 10), (11, 11)),
        db::Rect::new((15, 10), (15, 11)),
    ];
    let tree = decompose_multi_pin_net(&pins).unwrap();
    assert_eq!(tree.bounding_box, pins[0].add_rect(&pins[2]));
}

/// Find the indices of the two trees whose bounding boxes are closest to each other.
pub fn find_closest_rectangles<C, BB>(subnet_locations: &Vec<BB>) -> Option<(usize, usize)>
where
    C: db::CoordinateType,
    BB: BoundingBox<C>,
{
    // Will hold the indices of the two closest rectangles.
    let mut closest_pair = None;
    let mut closest_distance = None;

    // Compute all combinations of distances between pin locations.
    // O(n^2), could be done faster by using nearest-neighbour search of an r-tree.
    for i in 0..subnet_locations.len() {
        for j in i + 1..subnet_locations.len() {
            let a = &subnet_locations[i].bounding_box();
            let b = &subnet_locations[j].bounding_box();
            // Compute distance between rectangle boundaries.
            let boundary_dist = manhattan_distance_between_rectangles(&a, &b);
            // Compute distance between rectangle centers.
            let center_dist = (a.center() - b.center()).norm1();

            // Find closest rectangles by boundary distance, break ties by distance of centers.
            let dist = (boundary_dist, center_dist);

            // Remember the pair if it is closer than the previously found optimum.
            let is_improvement = closest_distance.map(|d| d < dist).unwrap_or(true);

            if is_improvement {
                closest_distance = Some(dist);
                closest_pair = Some((i, j));
            }
        }
    }
    if let Some(closest_pair) = closest_pair {
        debug_assert_ne!(closest_pair.0, closest_pair.1);
    }
    closest_pair
}

/// Compute the manhattan distance of two rectilinear rectangles.
fn manhattan_distance_between_rectangles<C>(a: &db::Rect<C>, b: &db::Rect<C>) -> C
where
    C: db::CoordinateType,
{
    // Find relative positions in x direction.
    let a_is_left = a.upper_right.x < b.lower_left.x;
    let a_is_right = b.upper_right.x < a.lower_left.x;

    // Find relative positions in y direction.
    let a_is_lower = a.upper_right.y < b.lower_left.y;
    let a_is_upper = b.upper_right.y < a.lower_left.y;

    let dx = match (a_is_left, a_is_right) {
        (false, false) => C::zero(), // x overlap.
        (true, false) => b.lower_left.x - a.upper_right.x,
        (false, true) => a.lower_left.x - b.upper_right.x,
        (true, true) => unreachable!(),
    };

    let dy = match (a_is_lower, a_is_upper) {
        (false, false) => C::zero(), // y overlap.
        (true, false) => b.lower_left.y - a.upper_right.y,
        (false, true) => a.lower_left.y - b.upper_right.y,
        (true, true) => unreachable!(),
    };

    debug_assert!(dx >= C::zero());
    debug_assert!(dy >= C::zero());

    dx + dy
}

#[test]
fn test_manhattan_distance_between_rectangles_no_overlap() {
    let a = db::Rect::new((0, 0), (1, 1));
    let b = db::Rect::new((2, 2), (3, 3));
    assert_eq!(manhattan_distance_between_rectangles(&a, &b), 2);
}

#[test]
fn test_manhattan_distance_between_rectangles_x_overlap() {
    let a = db::Rect::new((0, 0), (1, 1));
    let b = db::Rect::new((-1, 2), (3, 3));
    assert_eq!(manhattan_distance_between_rectangles(&a, &b), 1);
}

#[test]
fn test_manhattan_distance_between_rectangles_y_overlap() {
    let a = db::Rect::new((0, 0), (1, 1));
    let b = db::Rect::new((2, -1), (3, 3));
    assert_eq!(manhattan_distance_between_rectangles(&a, &b), 1);
}
