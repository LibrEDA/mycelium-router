// Copyright (c) 2020-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Representation of multi-pin nets with implicit branching points.
//!
//! # References
//! * "Completing High-quality Global Routes", DOI:10.1145/1735023.1735035,
//! [PDF](https://www.researchgate.net/profile/Igor-Markov/publication/220915792_Completing_high-quality_global_routes/links/02e7e51637fd76529a000000/Completing-high-quality-global-routes.pdf)

use crate::graph::Node3D;

/// Branch-free route (BFR).
/// Representation of multi-pin nets with implicit branching points.
///
/// A net is split into sub-nets. Each subnet connects two pins. Together they form a spanning tree of
/// the net.
///
/// The route is represented as a list of routing edges together with the number of subnets using them.
pub struct BranchFreeRoute<P> {
    pins: Vec<P>,
    edges: Vec<RoutingEdge<P>>,
}

pub struct RoutingEdge<P> {
    start: P,
    end: P,
    count: u32,
}

impl<T> BranchFreeRoute<Node3D<T>> {
    // TODO
}
