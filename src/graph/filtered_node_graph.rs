// Copyright (c) 2020-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Construct a sub-graph by filtering the nodes.

use super::graph::TraverseGraph;
use std::hash::Hash;

/// Wrapper around a graph which takes a filtered set of nodes from the wrapped graph.
#[derive(Debug)]
pub struct FilterNodes<'a, F, G> {
    graph: &'a G,
    filter_fn: F,
}

impl<'a, F, G> FilterNodes<'a, F, G> {
    /// Create a new graph that includes a set of nodes from the wrapped graph based on the
    /// filtering function `filter_fn`.
    /// This is useful for expressing obstacles (like already routed signals) in maze-routing
    /// algorithms.
    pub fn new<N>(graph: &'a G, filter_fn: F) -> Self
    where
        G: TraverseGraph<N>,
        N: Eq + Hash,
        F: Fn(&N) -> bool,
    {
        Self { graph, filter_fn }
    }
}

impl<'a, F, G, N> TraverseGraph<N> for FilterNodes<'a, F, G>
where
    G: TraverseGraph<N>,
    N: Eq + Hash,
    F: Fn(&N) -> bool,
{
    fn neighbours(&self, buffer: &mut Vec<N>, n: &N) {
        debug_assert!(buffer.is_empty(), "Buffer needs to be cleared.");

        // Get all neighbours except the ones that have been excluded.
        self.graph.neighbours(buffer, n);
        buffer.retain(|n| (self.filter_fn)(n)); // In-place filter.
    }

    fn contains_node(&self, n: &N) -> bool {
        self.graph.contains_node(n) && (self.filter_fn)(n)
    }
}
