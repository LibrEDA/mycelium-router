// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Data structure for routing grid graphs.

use ndarray::Array2;

use iron_shapes::prelude::*;
use num_traits::{FromPrimitive, Num, NumCast, PrimInt};

use petgraph::visit::{GraphBase, GraphRef, IntoNeighbors, VisitMap, Visitable};

use std::collections::HashSet;
use std::fmt::Display;
use std::hash::Hash;
use std::io::Write;

use super::graph::TraverseGraph;
use super::graph_attributes::CreateNodeAttributes;
use super::grid_map::GridMap;

/// Data type for the routing layer number.
pub type LayerNum = u8;

/// 3-dimensional graph node.
#[derive(Copy, Clone, Debug, Hash, Ord, PartialOrd, Eq, PartialEq)]
pub struct Node3D<T>(pub Point<T>, pub LayerNum);

impl<T: Copy> Node3D<T> {
    /// Create a new 3D node at point `(x, y)` and layer `z`.
    pub fn new(x: T, y: T, z: LayerNum) -> Self {
        Self(Point::new(x, y), z)
    }

    /// Get the nodes location in the xy plane.
    pub fn point(&self) -> Point<T> {
        self.0
    }

    /// Get the layer of the node.
    pub fn layer(&self) -> LayerNum {
        self.1
    }
}

/// Unique identifier for an edge.
#[derive(Copy, Clone, Hash, Debug, PartialEq, Eq)]
pub struct GridGraphEdgeId<T> {
    a: Node3D<T>,
    b: Node3D<T>,
}

impl<T: Ord> From<(Node3D<T>, Node3D<T>)> for GridGraphEdgeId<T> {
    fn from((n1, n2): (Node3D<T>, Node3D<T>)) -> Self {
        Self::new_undirected(n1, n2)
    }
}

impl<T> Into<(Node3D<T>, Node3D<T>)> for GridGraphEdgeId<T> {
    fn into(self) -> (Node3D<T>, Node3D<T>) {
        (self.a, self.b)
    }
}

impl<T: Ord> GridGraphEdgeId<T> {
    /// Create a new unique identifier for the undirected edge between a and b.
    pub fn new_undirected(a: Node3D<T>, b: Node3D<T>) -> Self {
        if a < b {
            Self { a, b }
        } else {
            Self { a: b, b: a }
        }
    }
}

/// Graph with implicit nodes and edges on a 3D grid.
/// The grid spans in x and y direction and has a relatively small
/// number of layers in z direction.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct GridGraph<T> {
    /// Boundary of the graph projected to the xy plane.
    pub boundary: Rect<T>,
    /// Number of layers.
    pub num_layers: LayerNum,
    /// Pitch of the routing tracks in x and y direction.
    pub grid_pitch: Vector<T>,
    /// Shift of the grid.
    pub grid_offset: Vector<T>,
    /// Routing direction on layer 0. The direction is alternated on the above layers.
    pub routing_directions: PreferredRoutingDirection,
}

/// Preferred routing direction on a layer.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum PreferredRoutingDirection {
    /// Horizontal only.
    X,
    /// Vertical only.
    Y,
    /// Horizontal and vertical.
    XY,
}

impl PreferredRoutingDirection {
    /// Prefers routing in X direction?
    fn allow_x(self) -> bool {
        match self {
            PreferredRoutingDirection::Y => false,
            _ => true,
        }
    }
    /// Prefers routing in Y direction?
    fn allow_y(self) -> bool {
        match self {
            PreferredRoutingDirection::X => false,
            _ => true,
        }
    }
    /// Get the opposite routing direction.
    fn alternated(self) -> Self {
        match self {
            PreferredRoutingDirection::X => PreferredRoutingDirection::Y,
            PreferredRoutingDirection::Y => PreferredRoutingDirection::X,
            PreferredRoutingDirection::XY => PreferredRoutingDirection::XY,
        }
    }
}

impl<T: CoordinateType + PrimInt + NumCast> GridGraph<T> {
    /// Create a new grid.
    pub fn new(
        boundary: Rect<T>,
        num_layers: LayerNum,
        grid_pitch: Vector<T>,
        grid_offset: Vector<T>,
    ) -> Self {
        assert!(num_layers < LayerNum::MAX, "Can use at most 255 layers because the maximum layer value is used for a special purpose."); // When computing neighbours.
        GridGraph {
            boundary,
            num_layers,
            grid_pitch,
            grid_offset: Vector::new(grid_offset.x % grid_pitch.x, grid_offset.y % grid_pitch.y),
            routing_directions: PreferredRoutingDirection::XY,
        }
    }

    /// Set the preferred routing direction on the layer 0. Routing directions on the layers above
    /// alternate.
    pub fn set_preferred_routing_directions(mut self, dir: PreferredRoutingDirection) -> Self {
        self.routing_directions = dir;
        self
    }

    /// Number of grid points in x direction.
    pub fn num_x(&self) -> usize {
        let (x, _y) = self.dim();
        x
    }

    /// Number of grid points in y direction.
    pub fn num_y(&self) -> usize {
        let (_x, y) = self.dim();
        y
    }

    /// Number of grid points in x and y direction.
    fn dim(&self) -> (usize, usize) {
        let ll = self.lower_left_node_coord();
        let ur = self.upper_right_node_coord();
        let w = ur.x - ll.x;
        let h = ur.y - ll.y;

        let w = w / self.grid_pitch.x;
        let h = h / self.grid_pitch.y;

        (w.to_usize().unwrap() + 1, h.to_usize().unwrap() + 1)
    }

    /// Round down to the next grid node which is left and below of `p`.
    pub fn grid_floor(&self, p: Point<T>) -> Point<T> {
        let dx = ((p.x - self.grid_offset.x) % self.grid_pitch.x + self.grid_pitch.x)
            % self.grid_pitch.x;
        let dy = ((p.y - self.grid_offset.y) % self.grid_pitch.y + self.grid_pitch.y)
            % self.grid_pitch.y;

        debug_assert!(dx >= T::zero());
        debug_assert!(dy >= T::zero());

        let r = Point::new(p.x - dx, p.y - dy);

        r
    }

    /// Round up in x and y directions to the next gridpoint.
    pub(crate) fn grid_ceil(&self, ll: Point<T>) -> Point<T> {
        self.grid_floor(ll + self.grid_pitch - Vector::new(T::one(), T::one()))
    }

    /// Find the index of a grid point given it's coordinates.
    pub(crate) fn node_coord_to_index(&self, p: Point<T>) -> (usize, usize) {
        assert!(self.contains_point(p));

        let start = self.lower_left_node_coord();

        let i = (p.x - start.x) / self.grid_pitch.x;
        let j = (p.y - start.y) / self.grid_pitch.y;

        debug_assert!(i.to_usize().unwrap() < self.num_x());
        debug_assert!(j.to_usize().unwrap() < self.num_y());

        (i.to_usize().unwrap(), j.to_usize().unwrap())
    }

    /// Find the closest point to `p` that is on the grid and within the boundary.
    pub fn closest_grid_point(&self, p: Point<T>) -> Point<T> {
        // Clip to boundary.
        let b = self.boundary.translate(Vector::zero() - self.grid_offset);
        let p = p - self.grid_offset;
        let p = Point::new(
            p.x.max(b.lower_left().x).min(b.upper_right().x),
            p.y.max(b.lower_left().y).min(b.upper_right().y),
        );

        // Round to nearest grid point.
        let dx = p.x % self.grid_pitch.x;
        let dy = p.y % self.grid_pitch.y;
        let _2 = T::one() + T::one();
        let x_rounded = if dx * _2 < self.grid_pitch.x {
            p.x - dx
        } else {
            p.x - dx + self.grid_pitch.x
        };
        let y_rounded = if dy * _2 < self.grid_pitch.y {
            p.y - dy
        } else {
            p.y - dy + self.grid_pitch.y
        };

        // Make sure the rounded point is inside the boundary.
        debug_assert!(x_rounded >= b.lower_left.x);
        debug_assert!(x_rounded <= b.upper_right.x);
        debug_assert!(y_rounded >= b.lower_left.y);
        debug_assert!(y_rounded <= b.upper_right.y);

        let p_rounded = Point::new(x_rounded, y_rounded);

        // Shift back.
        let result = p_rounded + self.grid_offset;
        debug_assert!(self.contains_node(&Node3D(result, 0)));
        result
    }

    /// Iterate over all nodes in this 3D grid.
    pub fn all_nodes(&self) -> impl Iterator<Item = Node3D<T>> + '_ {
        (0..self.num_layers).flat_map(move |layer| {
            self.all_nodes_xy(&self.boundary)
                .map(move |p| Node3D(p, layer))
        })
    }

    /// Iterate over all xy coordinates of the points in the graph.
    pub fn all_nodes_xy(&self, boundary: &Rect<T>) -> impl Iterator<Item = Point<T>> + '_ {
        self.boundary
            .intersection(boundary)
            .into_iter()
            .flat_map(move |boundary| {
                let upper_right_corner = boundary.upper_right();
                let start = self.grid_ceil(boundary.lower_left());
                let pitch = self.grid_pitch;

                (0..)
                    .scan(start.x, move |x, _| {
                        let x_prev = *x;
                        *x = x_prev + pitch.x;
                        Some(x_prev)
                    })
                    .take_while(move |x| *x < upper_right_corner.x)
                    .flat_map(move |x| {
                        (0..)
                            .scan(start.y, move |y, _| {
                                let y_prev = *y;
                                *y = y_prev + pitch.y;
                                Some(y_prev)
                            })
                            .take_while(move |y| *y < upper_right_corner.y)
                            .map(move |y| Point::new(x, y))
                    })
            })
    }

    /// Get the lowest and leftmost corner coordinate of the grid which is still within the boundary.
    pub fn lower_left_node_coord(&self) -> Point<T> {
        self.grid_ceil(self.boundary.lower_left())
    }

    /// Get the upper right corner coordinate of the grid which is still within the boundary.
    pub fn upper_right_node_coord(&self) -> Point<T> {
        self.grid_floor(self.boundary.upper_right() - Vector::new(T::one(), T::one()))
    }
}

/// Coordinate axes.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
#[allow(missing_docs)]
pub enum Axis3D {
    X,
    Y,
    Z,
}

#[test]
fn test_grid_dimension() {
    let offset = Vector::new(0, 0);
    let g = GridGraph::new(Rect::new((-5, -5), (5, 5)), 1, Vector::new(2, 2), offset);
    assert_eq!(g.num_x(), 5);
    assert_eq!(g.num_y(), 5);
    assert_eq!(g.all_nodes_xy(&g.boundary).count(), 25);
}

#[test]
fn test_grid_dimension2() {
    let offset = Vector::new(0, 0);
    let g = GridGraph::new(
        Rect::new((-15000, -15000), (55000, 55000)),
        1,
        Vector::new(1600, 1600),
        offset,
    );
    assert_eq!(g.lower_left_node_coord(), Point::new(-14400, -14400));
    assert_eq!(g.upper_right_node_coord(), Point::new(54400, 54400));
    assert_eq!(g.num_x(), 44);
    assert_eq!(g.num_y(), 44);

    assert!(g.all_nodes_xy(&g.boundary).all(|p| g.contains_point(p)));
    assert_eq!(g.all_nodes_xy(&g.boundary).count(), 44 * 44);
}

#[test]
fn test_grid_floor_no_offset() {
    let offset = Vector::zero();
    let g = GridGraph::new(
        Rect::new((-100, -100), (100, 100)),
        1,
        Vector::new(10, 10),
        offset,
    );

    assert_eq!(g.grid_floor(Point::new(-100, -100)), Point::new(-100, -100));
    assert_eq!(g.grid_floor(Point::new(-91, -99)), Point::new(-100, -100));
}

impl<T: CoordinateType + PrimInt + FromPrimitive + PartialOrd> GridGraph<T> {
    /// Get the coordinates of the node `(i, j)`. `(0, 0)` denotes the node with lowest x and y coordinates.
    /// Returns `None` if the graph does not contain such node.
    pub fn node_coord_by_index(&self, i: usize, j: usize) -> Option<Point<T>> {
        let lower_left = self.lower_left_node_coord();
        let dx = self.grid_pitch.x * T::from_usize(i).unwrap();
        let dy = self.grid_pitch.y * T::from_usize(j).unwrap();
        let d = Vector::new(dx, dy);
        let coord = lower_left + d;

        if self.contains_node(&Node3D(coord, 0)) {
            Some(coord)
        } else {
            None
        }
    }

    /// Iterate over all edges in this 3D grid.
    pub fn all_edges(&self) -> impl Iterator<Item = GridGraphEdgeId<T>> + '_ {
        // For each node create the edges to the neighbours in increasing coordinate directions.
        self.all_nodes().flat_map(move |n| {
            let axes = [Axis3D::X, Axis3D::Y, Axis3D::Z].iter().copied();
            axes.filter_map(move |ax| self.monotonic_neighbour(ax, n))
                .map(move |n2| GridGraphEdgeId::new_undirected(n, n2))
        })
    }

    /// Get all neighbours of `n` in increasing coordinate directions.
    /// There are at most 3 such neighbours. Nodes in corners have less or none at all.
    pub fn monotonic_neighbours(&self, n: Node3D<T>) -> Vec<Node3D<T>> {
        assert!(self.contains_node(&n));

        let mut coarse_neighbours = vec![];
        coarse_neighbours.extend(self.monotonic_neighbour_x(n));
        coarse_neighbours.extend(self.monotonic_neighbour_y(n));
        coarse_neighbours.extend(self.monotonic_neighbour_z(n));

        coarse_neighbours
    }

    /// Get the neighbour of `n` in increasing x, y or z coordinate direction.
    pub fn monotonic_neighbour(&self, axis: Axis3D, n: Node3D<T>) -> Option<Node3D<T>> {
        match axis {
            Axis3D::X => self.monotonic_neighbour_x(n),
            Axis3D::Y => self.monotonic_neighbour_y(n),
            Axis3D::Z => self.monotonic_neighbour_z(n),
        }
    }

    /// Get the preferred routing direction on a layer.
    fn routing_direction_on_layer(&self, layer: LayerNum) -> PreferredRoutingDirection {
        debug_assert!(layer < self.num_layers, "Layer does not exist in graph.");
        if layer % 2 == 0 {
            self.routing_directions
        } else {
            self.routing_directions.alternated()
        }
    }

    /// Get the neighbour of `n` in increasing x coordinate direction.
    pub fn monotonic_neighbour_x(&self, n: Node3D<T>) -> Option<Node3D<T>> {
        debug_assert!(self.contains_node(&n));
        let Node3D(p, layer) = n;
        if self.routing_direction_on_layer(layer).allow_x() {
            // Routing in X is allowed on this layer.
            let (i, j) = self.node_coord_to_index(p);
            let neighbour = self.node_coord_by_index(i + 1, j).map(|p| Node3D(p, layer));
            debug_assert!(neighbour.iter().all(|n| self.contains_node(&n)));
            neighbour
        } else {
            // Routing in X direction is not allowed on this layer.
            None
        }
    }

    /// Get the neighbour of `n` in increasing y coordinate direction.
    pub fn monotonic_neighbour_y(&self, n: Node3D<T>) -> Option<Node3D<T>> {
        debug_assert!(self.contains_node(&n));
        let Node3D(p, layer) = n;
        if self.routing_direction_on_layer(layer).allow_y() {
            // Routing in X is allowed on this layer.
            let (i, j) = self.node_coord_to_index(p);
            let neighbour = self.node_coord_by_index(i, j + 1).map(|p| Node3D(p, layer));

            debug_assert!(neighbour.iter().all(|n| self.contains_node(&n)));
            neighbour
        } else {
            // Routing in Y direction is not allowed on this layer.
            None
        }
    }

    /// Get the neighbour of `n` in increasing z coordinate direction.
    pub fn monotonic_neighbour_z(&self, n: Node3D<T>) -> Option<Node3D<T>> {
        debug_assert!(self.contains_node(&n));
        let Node3D(p, layer) = n;
        let neighbour = if layer + 1 < self.num_layers {
            Some(Node3D(p, layer + 1))
        } else {
            None
        };

        debug_assert!(neighbour.iter().all(|n| self.contains_node(&n)));
        neighbour
    }
}

impl<T: Num + PartialOrd + PrimInt> GridGraph<T> {
    /// Iterate over all neighbours of `n`.
    pub fn neighbours_iter(&self, n: Node3D<T>) -> GridNeighbours<T> {
        GridNeighbours {
            grid_graph: self,
            node: n,
            iter_index: 0,
        }
    }

    /// Check if the node is in the graph.
    #[allow(unused_comparisons)]
    pub fn contains_node(&self, n: &Node3D<T>) -> bool {
        let Node3D(p, layer) = *n;
        (layer >= 0 && layer < self.num_layers) && self.contains_point(p)
    }

    /// Check if the point lies on the grid.
    pub fn contains_point(&self, p: Point<T>) -> bool {
        (p.x >= self.boundary.lower_left().x && p.x < self.boundary.upper_right().x)
            && (p.y >= self.boundary.lower_left().y && p.y < self.boundary.upper_right().y)
            && ((p.x - self.grid_offset.x) % self.grid_pitch.x == T::zero())
            && ((p.y - self.grid_offset.y) % self.grid_pitch.y == T::zero())
    }

    /// Check if the edge exists in the grid graph.
    pub fn contains_edge(&self, edge: &GridGraphEdgeId<T>) -> bool {
        debug_assert!(edge.a <= edge.b, "Edge nodes must be sorted.");

        let contains_nodes = self.contains_node(&edge.a) && self.contains_node(&edge.b);

        if contains_nodes {
            let (ia, ja) = self.node_coord_to_index(edge.a.0);
            let (ib, jb) = self.node_coord_to_index(edge.b.0);
            let layer_a = edge.a.1;
            let layer_b = edge.b.1;

            let is_x_neighbours = ia + 1 == ib && ja == jb && layer_a == layer_b;
            let is_y_neighbours = ia == ib && ja + 1 == jb && layer_a == layer_b;
            let is_z_neighbours = ia == ib && ja == jb && layer_a + 1 == layer_b;

            is_x_neighbours || is_y_neighbours || is_z_neighbours
        } else {
            false
        }
    }
}

impl<C, T> CreateNodeAttributes<Node3D<C>, T> for GridGraph<C>
where
    C: CoordinateType + PrimInt + Hash + Eq,
    T: Clone,
{
    type NodeAttrs = GridMap<C, T>;

    fn create_attributes(&self, default_value: T) -> GridMap<C, T> {
        GridMap::new(self.clone(), default_value)
    }
}

impl<T: PrimInt> TraverseGraph<Node3D<T>> for GridGraph<T> {
    fn neighbours(&self, buffer: &mut Vec<Node3D<T>>, n: &Node3D<T>) {
        debug_assert!(buffer.is_empty(), "Buffer must be cleared.");
        buffer.extend(self.neighbors(*n))
    }

    fn contains_node(&self, n: &Node3D<T>) -> bool {
        self.contains_node(n)
    }
}

#[test]
fn test_neighbours() {
    let offset = Vector::new(0, 1);
    let g = GridGraph::new(Rect::new((0, 0), (10, 10)), 3, Vector::new(2, 3), offset);

    let n = Node3D(Point::new(0, 0) + offset, 0);
    assert!(g.contains_node(&n));
    assert_eq!(g.get_neighbours(&n).len(), 3);
    assert_eq!(g.neighbors(n).count(), 3);

    let n = Node3D(Point::new(2, 3) + offset, 1);
    assert!(g.contains_node(&n));
    assert_eq!(g.get_neighbours(&n).len(), 6);
    assert_eq!(g.neighbors(n).count(), 6);
}

#[test]
fn test_node_coord_to_index_conversion() {
    let offset = Vector::new(0i32, 0);
    let g = GridGraph::new(
        Rect::new((-11, -11), (10, 10)),
        1,
        Vector::new(2, 2),
        offset,
    );
    assert_eq!(g.node_coord_to_index(Point::new(-10, -10)), (0, 0));
    assert_eq!(g.node_coord_to_index(Point::new(0, 0)), (5, 5));
    assert_eq!(g.node_coord_by_index(5, 5), Some(Point::new(0, 0)));
}

impl<T> GraphBase for &GridGraph<T>
where
    T: Copy + PartialEq,
{
    type EdgeId = GridGraphEdgeId<T>;
    type NodeId = Node3D<T>;
}

impl<T> GraphRef for &GridGraph<T> where T: Copy + PartialEq {}

/// Iterator over neighbours of a node.
pub struct GridNeighbours<'a, T> {
    grid_graph: &'a GridGraph<T>,
    node: Node3D<T>,
    iter_index: u8,
}

impl<'a, T> Iterator for GridNeighbours<'a, T>
where
    T: Copy + PrimInt + PartialEq,
{
    type Item = Node3D<T>;

    fn next(&mut self) -> Option<Self::Item> {
        let contains_node = self.grid_graph.contains_node(&self.node);
        let Node3D(p, layer) = self.node;
        let pitch = self.grid_graph.grid_pitch;

        let candidate = |i: u8| -> Option<Node3D<T>> {
            match i {
                0 => Some(Node3D(Point::new(p.x - pitch.x, p.y), layer)),
                1 => Some(Node3D(Point::new(p.x + pitch.x, p.y), layer)),
                2 => Some(Node3D(Point::new(p.x, p.y - pitch.y), layer)),
                3 => Some(Node3D(Point::new(p.x, p.y + pitch.y), layer)),
                4 => Some(Node3D(Point::new(p.x, p.y), layer.wrapping_sub(1))),
                5 => Some(Node3D(Point::new(p.x, p.y), layer + 1)),
                _ => None,
            }
        };

        while contains_node && self.iter_index < 6 {
            let c = candidate(self.iter_index);
            self.iter_index += 1;
            if let Some(c) = c {
                if self.grid_graph.contains_node(&c) {
                    return Some(c);
                }
            }
        }
        return None;
    }
}

impl<'a, T> IntoNeighbors for &'a GridGraph<T>
where
    T: Copy + PrimInt + PartialEq,
{
    type Neighbors = GridNeighbours<'a, T>;

    fn neighbors(self, a: Self::NodeId) -> Self::Neighbors {
        self.neighbours_iter(a)
    }
}

/// Track visited nodes of a `GridGraph`.
pub struct GridGraphVisitMap<Coord> {
    visit_map: GridMap<Coord, bool>,
}

impl<Coord> VisitMap<Node3D<Coord>> for GridGraphVisitMap<Coord>
where
    Coord: CoordinateType + PrimInt + NumCast + Hash,
{
    fn visit(&mut self, a: Node3D<Coord>) -> bool {
        let is_first_visit = self.is_visited(&a);
        if !is_first_visit {
            self.visit_map.set(&a, true);
        }
        is_first_visit
    }

    fn is_visited(&self, a: &Node3D<Coord>) -> bool {
        *self.visit_map.get(a)
    }
}

impl<Coord> Visitable for &GridGraph<Coord>
where
    Coord: Copy + PrimInt + NumCast + Hash,
{
    type Map = GridGraphVisitMap<Coord>;

    fn visit_map(self: &Self) -> Self::Map {
        GridGraphVisitMap {
            visit_map: GridMap::new(**self, false),
        }
    }

    fn reset_map(self: &Self, map: &mut Self::Map) {
        *map = self.visit_map();
    }
}

/// A 3D grid-graph with edge data and node data.
#[derive(Debug, Clone)]
pub struct GridGraphWithData<Coord, NodeData, EdgeData> {
    /// Definition of the 3D grid.
    grid_graph: GridGraph<Coord>,
    /// Store node data for all the layers.
    node_data: Vec<Array2<NodeData>>,
    /// Store data for all edges in x direction.
    edge_data_x: Vec<Array2<EdgeData>>,
    /// Store data for all edges in y direction.
    edge_data_y: Vec<Array2<EdgeData>>,
    /// Store data for all edges in z direction.
    edge_data_z: Vec<Array2<EdgeData>>,
}

impl<C, ND, ED> GridGraphWithData<C, ND, ED> {
    /// Format the 2D array with edge data.
    /// Use the `display_fn` function to convert the actual edge data into something displayable, e.g.
    /// extract a numerical value from the edge data struct.
    pub fn dump_edge_data<W: Write, D: Display>(
        &self,
        writer: &mut W,
        display_fn: &dyn Fn(&ED) -> D,
        layer: usize,
        edge_direction: Axis3D,
    ) -> std::io::Result<()> {
        let array = match edge_direction {
            Axis3D::X => &self.edge_data_x,
            Axis3D::Y => &self.edge_data_y,
            Axis3D::Z => &self.edge_data_z,
        };
        let array = &array[layer];
        let display_array = array.map(|d| display_fn(d));

        // Print CSV-matrix.
        let (w, h) = (display_array.shape()[0], display_array.shape()[1]);
        for j in 0..h {
            for i in 0..w {
                write!(writer, "{}", display_array[[i, j]])?;
                if i + 1 < w {
                    write!(writer, ",");
                }
            }
            if j + 1 < h {
                writeln!(writer, "");
            }
        }
        Ok(())
    }
}

fn grow_array<T: Clone + Default>(
    arr: &Array2<T>,
    num_add_cols: usize,
    num_add_rows: usize,
) -> Array2<T> {
    let (w, h) = (arr.shape()[0], arr.shape()[1]);
    let new_shape = (w + num_add_cols, h + num_add_rows);
    let mut new_arr = Array2::default(new_shape);
    new_arr.slice_mut(ndarray::s![..w, ..h]).assign(arr);
    new_arr
}

impl<C, ND, ED> GridGraphWithData<C, ND, ED>
where
    C: PrimInt + FromPrimitive,
    ND: Clone + Default,
    ED: Clone + Default,
{
    /// Create an enlarged copy of this graph inclusive it's data.
    /// The data of newly created nodes and edges will be set to the default values.
    pub fn enlarged(&self, grow_x: usize, grow_y: usize) -> Self {
        let mut grid_graph = self.grid_graph.clone();
        // Extend the boundary.
        grid_graph.boundary.upper_right.x = grid_graph.boundary.upper_right.x
            + grid_graph.grid_pitch.x * C::from_usize(grow_x).unwrap();
        grid_graph.boundary.upper_right.y = grid_graph.boundary.upper_right.y
            + grid_graph.grid_pitch.y * C::from_usize(grow_y).unwrap();

        assert_eq!(grid_graph.num_x(), self.grid_graph.num_x() + grow_x);
        assert_eq!(grid_graph.num_y(), self.grid_graph.num_y() + grow_y);

        Self {
            grid_graph,
            node_data: self
                .node_data
                .iter()
                .map(|arr| grow_array(arr, grow_x, grow_y))
                .collect(),
            edge_data_x: self
                .edge_data_x
                .iter()
                .map(|arr| grow_array(arr, grow_x, grow_y))
                .collect(),
            edge_data_y: self
                .edge_data_y
                .iter()
                .map(|arr| grow_array(arr, grow_x, grow_y))
                .collect(),
            edge_data_z: self
                .edge_data_z
                .iter()
                .map(|arr| grow_array(arr, grow_x, grow_y))
                .collect(),
        }
    }
}

impl<C, ND, ED> GridGraphWithData<C, ND, ED> {
    /// Access the underlying 3D grid definition.
    pub fn grid_graph(&self) -> &GridGraph<C> {
        &self.grid_graph
    }
}

impl<C, ND, ED> GridGraphWithData<C, ND, ED>
where
    C: PrimInt + CoordinateType,
    ND: Clone + Default,
    ED: Clone + Default,
{
    /// Create a new grid graph with node and edge data.
    /// Node and edge data is set to their default values as defined by their implementation of the `Default` trait.
    pub fn new(grid_graph: GridGraph<C>) -> Self {
        let num_layers = grid_graph.num_layers as usize;
        let num_x = grid_graph.num_x();
        let num_y = grid_graph.num_y();

        Self {
            grid_graph,
            node_data: vec![Array2::default((num_x, num_y)); num_layers],
            edge_data_x: vec![Array2::default((num_x - 1, num_y)); num_layers],
            edge_data_y: vec![Array2::default((num_x, num_y - 1)); num_layers],
            edge_data_z: vec![Array2::default((num_x, num_y)); num_layers - 1],
        }
    }

    /// Get a immutable reference to the node data.
    /// Panics if the node does not exist in the graph.
    pub fn get_node_data(&self, node: &Node3D<C>) -> &ND {
        assert!(
            self.grid_graph.contains_node(node),
            "Node not contained in graph."
        );

        let Node3D(p, layer) = *node;
        let (i, j) = self.grid_graph.node_coord_to_index(p);

        &self.node_data[layer as usize][[i, j]]
    }

    /// Get a mutable reference to the node data.
    /// Panics if the node does not exist in the graph.
    pub fn get_node_data_mut(&mut self, node: &Node3D<C>) -> &mut ND {
        assert!(
            self.grid_graph.contains_node(node),
            "Node not contained in graph."
        );

        let Node3D(p, layer) = *node;
        let (i, j) = self.grid_graph.node_coord_to_index(p);

        &mut self.node_data[layer as usize][[i, j]]
    }

    /// Get a immutable reference to the edge data.
    /// Panics if the edge does not exist in the graph.
    pub fn get_edge_data(&self, edge: &GridGraphEdgeId<C>) -> &ED {
        assert!(
            self.grid_graph.contains_edge(&edge),
            "Edge is not contained in graph."
        );

        let (ia, ja) = self.grid_graph.node_coord_to_index(edge.a.0);
        let (ib, jb) = self.grid_graph.node_coord_to_index(edge.b.0);
        let layer_a = edge.a.1;
        let layer_b = edge.b.1;

        let is_x_neighbours = ia + 1 == ib && ja == jb && layer_a == layer_b;
        let is_y_neighbours = ia == ib && ja + 1 == jb && layer_a == layer_b;
        let is_z_neighbours = ia == ib && ja == jb && layer_a + 1 == layer_b;

        debug_assert!(is_x_neighbours || is_y_neighbours || is_z_neighbours);

        let edge_data = if is_x_neighbours {
            &self.edge_data_x
        } else if is_y_neighbours {
            &self.edge_data_y
        } else {
            debug_assert!(is_z_neighbours);
            &self.edge_data_z
        };

        &edge_data[layer_a as usize][[ia, ja]]
    }

    /// Get a mutable reference to the edge data.
    /// Panics if the edge does not exist in the graph.
    pub fn get_edge_data_mut(&mut self, edge: &GridGraphEdgeId<C>) -> &mut ED {
        assert!(
            self.grid_graph.contains_edge(&edge),
            "Edge is not contained in graph."
        );

        let (ia, ja) = self.grid_graph.node_coord_to_index(edge.a.0);
        let (ib, jb) = self.grid_graph.node_coord_to_index(edge.b.0);
        let layer_a = edge.a.1;
        let layer_b = edge.b.1;

        let is_x_neighbours = ia + 1 == ib && ja == jb && layer_a == layer_b;
        let is_y_neighbours = ia == ib && ja + 1 == jb && layer_a == layer_b;
        let is_z_neighbours = ia == ib && ja == jb && layer_a + 1 == layer_b;

        debug_assert!(is_x_neighbours || is_y_neighbours || is_z_neighbours);

        let edge_data = if is_x_neighbours {
            &mut self.edge_data_x
        } else if is_y_neighbours {
            &mut self.edge_data_y
        } else {
            debug_assert!(is_z_neighbours);
            &mut self.edge_data_z
        };

        &mut edge_data[layer_a as usize][[ia, ja]]
    }
}
