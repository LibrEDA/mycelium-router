// Copyright (c) 2022-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Traits for graph traversal.

use libreda_pnr::db;

/// Minimal trait that allows traversing undirected graphs.
pub trait TraverseGraph<N> {
    // type Neighbours;

    /// Get all neighbours of a node.
    /// Writes the nodes into a buffer instead of returning a vector to avoid allocation.
    fn neighbours(&self, buffer: &mut Vec<N>, n: &N);

    // fn iter_neighbours(&self, n: &N) -> Self::Neighbours;

    /// Get all neighbours of a node.
    /// Allocates a `Vec` in each call.
    fn get_neighbours(&self, n: &N) -> Vec<N> {
        let mut ns = Vec::new();
        self.neighbours(&mut ns, n);
        ns
    }

    /// Check if the node is in the graph.
    fn contains_node(&self, n: &N) -> bool;
}

/// Associate graph nodes with a location in the Euclidean plane.
pub trait NodeLocation2D<N, Coord> {
    /// Get the euclidean coordinates of the node.
    fn node_location(&self, n: &N) -> db::Point<Coord>;
}
