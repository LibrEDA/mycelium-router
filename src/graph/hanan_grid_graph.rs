// Copyright (c) 2020-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/// A Hanan grid graph in the euclidean plane.
/// Given a set of points P the Hanan grid is defined by the set of vertical and horizontal
/// lines which go through the points in P. Each intersection of this lines forms a node
/// in the Hanan grid.
#[derive(Clone)]
pub struct HananGridGraph<C> {
    x_coords: Vec<C>,
    y_coords: Vec<C>,
}
