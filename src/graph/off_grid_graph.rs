// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Extension of a grid-graph to support off-grid nodes.

use super::graph::TraverseGraph;
use super::graph_attributes::CreateNodeAttributes;
use super::grid_graph::{GridGraph, Node3D};
use super::grid_map::GridMap;

use iron_shapes::point::Point;
use iron_shapes::rect::Rect;
use iron_shapes::transform::SimpleTransform;
use iron_shapes::vector::Vector;
use iron_shapes::CoordinateType;
use num_traits::PrimInt;
use std::hash::Hash;

/// Graph with implicit nodes and edges on a 2.5D grid. Additional to the nodes on the grid
/// also off-grid nodes are supported. They can be off-grid on the x-y plane but must be still
/// on one of the layers.
///
/// Off-grid nodes are connected to the graph with up to four additional nodes and edges.
///
/// # Example
/// The grid-nodes are marked with `+`, the off grid node with `o`.
/// Additional nodes `x` are inserted to connect `o` to the graph.
/// The edges from the additional nodes are directed. So paths can be found from off-grid nodes
/// to on-grid nodes but not the other way around. This is fine as long as path searches expand from
/// source and target points at the same time.
///
/// ```txt
/// +<-x--->+
/// ^  ^    ^
/// |  |    |
/// x<-o--->x
/// |  |    |
/// v  v    v
/// +--x----+
/// ```
#[derive(Clone)]
pub struct OffGridGraph<T: CoordinateType + PrimInt> {
    grid_graph: GridGraph<T>,
    off_grid_neighbors: GridMap<T, Option<Point<T>>>,
}

impl<T: CoordinateType + PrimInt + Hash> OffGridGraph<T> {
    pub fn new(grid_graph: GridGraph<T>) -> Self {
        Self {
            grid_graph,
            off_grid_neighbors: GridMap::new(grid_graph, None),
        }
    }

    /// Insert an off-grid node into the graph.
    /// It is automatically connected to the four surrounding on-grid nodes.
    pub fn insert_node(&mut self, n: Node3D<T>) {
        let Node3D(p, layer) = n;
        let floor = self.grid_graph.grid_floor(p);
        debug_assert!(self.grid_graph.contains_node(&Node3D(floor, layer)));
        let pitch = self.grid_graph.grid_pitch;
        if floor != p {
            // This is an off-grid node.
            // Make the surrounding on-grid nodes aware of their off-grid neighbour.
            self.off_grid_neighbors.set(&Node3D(floor, layer), Some(p));
        }
    }
}

impl<T: PrimInt + Hash> TraverseGraph<Node3D<T>> for OffGridGraph<T> {
    fn neighbours(&self, buffer: &mut Vec<Node3D<T>>, n: &Node3D<T>) {
        let Node3D(p, layer) = *n;
        let pitch = self.grid_graph.grid_pitch;
        let pitch_x = Vector::new(pitch.x, T::zero());
        let pitch_y = Vector::new(T::zero(), pitch.y);

        if self.grid_graph.contains_node(n) {
            // Node is on the grid.

            // // Find closest neighbours in all four directions in the plane.
            // let n1 = self.off_grid_neighbors.get(&(p, layer))
            //     .unwrap_or(p + pitch);
            // let n2 = self.off_grid_neighbors.get(&(p - pitch_x, layer))
            //     .unwrap_or(p + pitch_y - pitch_x);
            // let n3 = self.off_grid_neighbors.get(&(p - pitch, layer))
            //     .unwrap_or(p - pitch);
            // let n4 = self.off_grid_neighbors.get(&(p - pitch_y, layer))
            //     .unwrap_or(p - pitch_y + pitch_x);
            //
            // //
            // let r = Rect::new(n1, n2).add_point(n3).add_point(n4);

            // Get default neighbours.
            self.grid_graph.neighbours(buffer, n);

            // for new in buffer.iter_mut() {
            //     let (p, layer) = *new;
            //
            //     // Limit to the rectangle.
            //     let p = Point::new(p.x.max(r.lower_left.x).min(r.upper_right.x),
            //     p.y.max(r.lower_left.y).min(r.upper_right.y));
            //
            //     *new = (p, layer);
            // }
            //
            // // if let Some(neighbor) = neighbor {} else {
            // //     self.grid_graph.neighbours(buffer, n);
            // // }
        } else {
            // Node is off the grid.

            let Node3D(p, layer) = *n;
            let floor = self.grid_graph.grid_floor(p);
            let ceil = floor + self.grid_graph.grid_pitch;

            if p.y != floor.y {
                let p_new = Point::new(p.x, floor.y);
                if p_new != p {
                    buffer.push(Node3D(p_new, layer));
                }
                let p_new = Point::new(p.x, ceil.y);
                if p_new != p {
                    buffer.push(Node3D(p_new, layer));
                }
            }

            if p.x != floor.x {
                let p_new = Point::new(floor.x, p.y);
                if p_new != p {
                    buffer.push(Node3D(p_new, layer));
                }
                let p_new = Point::new(ceil.x, p.y);
                if p_new != p {
                    buffer.push(Node3D(p_new, layer));
                }
            }

            // if layer > 0 {
            //     buffer.push((p, layer - 1));
            // }
            // if layer < self.grid_graph.num_layers+ 1  {
            //     buffer.push((p, layer + 1));
            // }
        }
    }

    fn contains_node(&self, n: &Node3D<T>) -> bool {
        self.grid_graph.contains_node(n) || {
            let Node3D(p, layer) = *n;
            let floor = self.grid_graph.grid_floor(p);
            let neighbor = self.off_grid_neighbors.get(&Node3D(floor, layer));

            neighbor == &Some(p)
        }
    }
}

impl<C, T> CreateNodeAttributes<Node3D<C>, T> for OffGridGraph<C>
where
    C: CoordinateType + PrimInt + Hash + Eq,
    T: Clone,
{
    type NodeAttrs = GridMap<C, T>;

    fn create_attributes(&self, default_value: T) -> GridMap<C, T> {
        self.grid_graph.create_attributes(default_value)
    }
}

#[test]
fn test_off_grid_graph() {
    let offset = Vector::new(0, 0);
    let grid = GridGraph::new(
        Rect::new((0, 0), (100, 100)),
        3,
        Vector::new(10, 10),
        offset,
    );
    let mut og = OffGridGraph::new(grid);

    assert!(og.contains_node(&Node3D::new(10, 10, 0)));
    assert!(!og.contains_node(&Node3D::new(11, 12, 0)));

    og.insert_node(Node3D::new(11, 12, 0));
    assert!(og.contains_node(&Node3D::new(11, 12, 0)));
}

#[test]
fn test_off_grid_graph_neighbours_of_offgrid_node() {
    let offset = Vector::new(0, 0);
    let grid = GridGraph::new(
        Rect::new((0, 0), (100, 100)),
        3,
        Vector::new(10, 10),
        offset,
    );
    let mut og = OffGridGraph::new(grid);

    og.insert_node(Node3D::new(11, 12, 0));

    let ns = og.get_neighbours(&Node3D::new(11, 12, 0));
    assert_eq!(ns.len(), 4);
    assert_eq!(
        ns,
        [
            Node3D::new(11, 10, 0),
            Node3D::new(11, 20, 0),
            Node3D::new(10, 12, 0),
            Node3D::new(20, 12, 0)
        ]
    );

    let ns = og.get_neighbours(&Node3D::new(10, 12, 0));
    assert_eq!(ns.len(), 2);
    assert_eq!(ns, [Node3D::new(10, 10, 0), Node3D::new(10, 20, 0)]);

    let ns = og.get_neighbours(&Node3D::new(11, 10, 0));
    assert_eq!(ns.len(), 2);
    assert_eq!(ns, [Node3D::new(10, 10, 0), Node3D::new(20, 10, 0)]);
}
