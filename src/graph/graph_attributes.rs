// Copyright (c) 2020-2022 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Traits for creating custom attributes for graph nodes.

use super::grid_map::GridMap;
use crate::graph::Node3D;
use libreda_pnr::db::CoordinateType;
use num_traits::PrimInt;
use petgraph::graph::Node;

/// Create storage objects for node attributes.
pub trait CreateNodeAttributes<NodeId, AttrValue: Clone> {
    type NodeAttrs: NodeAttributes<NodeId, AttrValue>;

    /// Create storage objects for node attributes.
    fn create_attributes(&self, default_value: AttrValue) -> Self::NodeAttrs;
}

pub trait NodeAttributes<K, V> {
    /// Get a reference to the attribute value of a node.
    fn get(&self, node: &K) -> &V;

    /// Get a mutable reference to the attribute value of a node.
    fn get_mut(&mut self, node: &K) -> &mut V;

    /// Set the attribute value of a node.
    fn set(&mut self, node: &K, value: V);
}
