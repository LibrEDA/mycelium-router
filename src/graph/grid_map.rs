// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Storage for grid point properties.

use super::grid_graph::{GridGraph, Node3D};
use crate::graph::NodeAttributes;
use crate::sparse_array2d::SparseArray2D;
use iron_shapes::CoordinateType;
use num_traits::{NumCast, PrimInt};
use petgraph::data::Element::Node;
use std::collections::HashMap;
use std::hash::Hash;

/// Map from nodes on the grid to values of type `T`.
/// Also off-grid nodes are supported but access to them is slower.
#[derive(Clone)]
pub struct GridMap<Coord, T> {
    grid: GridGraph<Coord>,
    default: T,
    /// Values for each layer.
    values: Vec<SparseArray2D<T>>,
    offgrid_values: HashMap<Node3D<Coord>, T>,
}

impl<Coord, T> GridMap<Coord, T>
where
    Coord: PrimInt + NumCast + Hash,
    T: Clone,
{
    /// Create a new map that efficiently stores values for nodes of the `grid`.
    pub fn new_with_tile_size(
        grid: GridGraph<Coord>,
        default: T,
        tile_size: (usize, usize),
    ) -> Self {
        // Size of tiles.
        let (tile_width, tile_height) = tile_size;

        // Get number of grid points in each direction.
        let num_x = grid.num_x();
        let num_y = grid.num_y();

        // Get number of tiles.
        let num_tiles_x = (num_x + tile_width - 1) / tile_width;
        let num_tiles_y = (num_y + tile_height - 1) / tile_height;

        // Create arrays that hold values for each layer.
        let values = (0..grid.num_layers)
            .map(|_| {
                SparseArray2D::new(
                    num_tiles_x,
                    num_tiles_y,
                    tile_width,
                    tile_height,
                    default.clone(),
                )
            })
            .collect();

        Self {
            grid,
            default,
            values,
            offgrid_values: Default::default(),
        }
    }

    /// Create a new map that efficiently stores values for nodes of the `grid`.
    /// Tile size defaults to 64x64.
    pub fn new(grid: GridGraph<Coord>, default: T) -> Self {
        Self::new_with_tile_size(grid, default, (64, 64))
    }

    /// Get value at `n`.
    pub fn get(&self, n: &Node3D<Coord>) -> &T {
        if self.grid.contains_node(n) {
            let Node3D(p, layer) = *n;
            debug_assert!(p.x >= self.grid.boundary.lower_left.x);
            debug_assert!(p.x >= self.grid.boundary.lower_left.y);
            let ll = self.grid.closest_grid_point(self.grid.boundary.lower_left);
            debug_assert!(self.grid.boundary.contains_point(p));
            debug_assert!(self.grid.closest_grid_point(p) == p);
            debug_assert!(ll.x <= p.x);
            debug_assert!(ll.y <= p.y);
            let i = ((p.x - ll.x) / self.grid.grid_pitch.x).to_usize().unwrap();
            let j = ((p.y - ll.y) / self.grid.grid_pitch.y).to_usize().unwrap();

            self.values[layer as usize].get(i, j)
        } else {
            // Off-grid node.
            self.offgrid_values.get(n).unwrap_or(&self.default)
        }
    }

    /// Get mutable value at `n`.
    pub fn get_mut(&mut self, n: &Node3D<Coord>) -> &mut T {
        if self.grid.contains_node(n) {
            let Node3D(p, layer) = *n;
            let ll = self.grid.closest_grid_point(self.grid.boundary.lower_left);
            debug_assert!(ll.x <= p.x);
            debug_assert!(ll.y <= p.y);
            let i = ((p.x - ll.x) / self.grid.grid_pitch.x).to_usize().unwrap();
            let j = ((p.y - ll.y) / self.grid.grid_pitch.y).to_usize().unwrap();
            self.values[layer as usize].get_mut(i, j)
        } else {
            // Off-grid node.
            self.offgrid_values
                .entry(*n)
                .or_insert(self.default.clone())
        }
    }

    /// Set value at `n`.
    pub fn set(&mut self, n: &Node3D<Coord>, value: T) {
        *self.get_mut(n) = value;
    }
}

impl<Coord, V> NodeAttributes<Node3D<Coord>, V> for GridMap<Coord, V>
where
    Coord: PrimInt + Hash,
    V: Clone,
{
    fn get(&self, node: &Node3D<Coord>) -> &V {
        GridMap::get(self, node)
    }

    fn get_mut(&mut self, node: &Node3D<Coord>) -> &mut V {
        GridMap::get_mut(self, node)
    }

    fn set(&mut self, node: &Node3D<Coord>, value: V) {
        GridMap::set(self, node, value)
    }
}

#[test]
pub fn test_grid_map() {
    use iron_shapes::prelude::{Point, Rect};

    let grid = GridGraph::new(
        Rect::new((0, 0), (100, 100)),
        2,
        (10, 10).into(),
        (0, 0).into(),
    );

    // On-grid values.
    let mut gm = GridMap::new(grid, false);
    assert_eq!(gm.get(&Node3D::new(0, 0, 1)), &false);
    gm.set(&Node3D::new(0, 0, 1), true);
    assert_eq!(gm.get(&Node3D::new(0, 0, 1)), &true);
    gm.set(&Node3D::new(10, 0, 1), true);

    assert_eq!(gm.offgrid_values.len(), 0);

    // Off-grid values.
    gm.set(&Node3D::new(5, 0, 1), true);
    assert_eq!(gm.offgrid_values.len(), 1);
    assert_eq!(gm.get(&Node3D::new(5, 0, 1)), &true);
}
