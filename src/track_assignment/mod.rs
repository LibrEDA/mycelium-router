// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! (work-in-progress) Track-assignment allocates pre-defined routing tracks to routes in order to minimize congestion.
//! The result of track assignment can be used for detail routing or congestion estimation.
//!
//! # Literature
//! * "Negotiation-Based Track Assignment Considering Local Nets" (doi:10.1109/aspdac.2016.7428041)

use rayon::prelude::*;

use fnv::FnvHashMap;
use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::marker::PhantomData;

use libreda_pnr::db;
use num_traits::{FromPrimitive, NumCast, PrimInt};

mod cost_functions;
mod initial_assignment;
mod interval_map;
mod overlap_reduction;
mod types;

use crate::track_assignment::initial_assignment::initial_assignment;
use crate::track_assignment::overlap_reduction::overlap_reduction;
use types::*;

use super::pin_access_analysis::Tracks;

/// Track assignment engine which reduces the overlap of `iroutes`.
/// Initial `iroutes` are typically the center-lines of global routes. They are used as a rough
/// resource allocation of the detail route.
pub struct TrackAssignmentRouter<LN> {
    /// Number of routing tracks per panel.
    ///
    /// A panel is a slice of a routing layer. It is directed into the preferred routing direction and
    /// spans the full widths of the routing tracks.
    panel_size: usize,
    _ln: PhantomData<LN>,
}

impl<LN: db::L2NBase> TrackAssignmentRouter<LN>
where
    LN::Coord: PrimInt + FromPrimitive,
{
    pub fn new(panel_size: usize) -> Self {
        assert!(panel_size > 0);
        Self {
            panel_size,
            _ln: Default::default(),
        }
    }

    /// Assign iroutes to tracks and try to minimize their overlap.
    ///
    /// # Parameters
    /// * `tracks`: The routing tracks which can be used.
    /// * `iroutes`: IRoutes defined as line segments associated with their net and a layer.
    /// * `pin_locations`: Locations of net terminals.
    /// Used to estimate the wirelength during track assignment.
    pub fn do_track_assignment<IRoutes, Pins>(
        &self,
        chip: &LN, // Needed to extract blockages and get layer names for debugging output.
        _top: &LN::CellId,
        tracks: &HashMap<LN::LayerId, Tracks<LN::Coord>>,
        iroutes: IRoutes,
        pin_locations: Pins,
    ) -> HashMap<LN::NetId, Vec<(LN::LayerId, db::REdge<LN::Coord>)>>
    where
        IRoutes: Iterator<Item = (LN::NetId, LN::LayerId, db::REdge<LN::Coord>)>,
        Pins: Iterator<Item = (LN::NetId, db::Point<LN::Coord>)>,
        LN::LayerId: Sync + Send,
        LN::Coord: Sync + Send,
    {
        // Initialize iroutes (convert to internal data structures).
        let (
            all_iroutes,
            layer_ids,         // mapping to internal IDs
            layer_ids_reverse, // mapping to external IDs
            net_ids,           // mapping to internal IDs
            net_ids_reverse,   // mapping to external IDs
        ) = self.init_iroutes(iroutes);

        // Sanity check.
        assert_eq!(layer_ids.len(), layer_ids_reverse.len());
        assert_eq!(net_ids.len(), net_ids_reverse.len());

        // Check that for all used layers there's routing tracks defined.
        {
            let missing_layers: Vec<_> = layer_ids_reverse
                .iter()
                .filter(|layer| !tracks.contains_key(layer))
                .collect();

            // Write a warning about missing layers.
            if !missing_layers.is_empty() {
                let layer_names = missing_layers
                    .iter()
                    .map(|l| format!("'{:?}'", chip.layer_info(l).name))
                    .join(", ");

                log::warn!(
                    "No tracks defined on {} layer{}: {}",
                    missing_layers.len(),
                    if missing_layers.len() == 1 { "" } else { "s" }, // 's' for plural
                    layer_names,
                )
            }
        }

        let num_nets = net_ids_reverse.len();

        let used_layers = &layer_ids_reverse;
        let panels = self.create_routing_panels(tracks, used_layers);

        // Translate layer IDs.
        let mut panels: HashMap<_, _> = panels
            .into_iter()
            .map(|(layer, panels)| (layer_ids[&layer], panels))
            .collect();

        // Collect initial iroutes and pins for all nets.
        // They are used for wirelength estimation during the track assignment and overlap reduction.
        let initial_net_components = {
            let mut components = vec![vec![]; num_nets];

            let mut nets_without_iroutes: HashSet<_> = Default::default();
            // Populate components with pins.
            for (net, pin_location) in pin_locations {
                if let Some(net_index) = net_ids.get(&net) {
                    let net_index = net_index.0;
                    components[net_index].push(NetComponent::Pin(pin_location));
                } else {
                    // This net has no iroutes.
                    nets_without_iroutes.insert(net);
                }
            }

            if !nets_without_iroutes.is_empty() {
                log::warn!(
                    "Number of nets without iroutes: {}",
                    nets_without_iroutes.len()
                );
            }

            // Populate components with initial iroutes.
            for iroute in &all_iroutes {
                let net_index = iroute.net.0;
                components[net_index].push(NetComponent::IRoute(*iroute));
            }

            components
        };

        // Assign iroutes to panels.
        let mut ignored_iroutes = vec![];
        for iroute in &all_iroutes {
            // Find panel where this iroute belongs to.
            let layer_panels = {
                let maybe_layer_panel = panels.get_mut(&iroute.layer);
                if maybe_layer_panel.is_none() {
                    let l = &layer_ids_reverse[iroute.layer.0 as usize];
                    log::error!("No panels found for layer {:?}", chip.layer_info(l).name);
                }
                maybe_layer_panel.expect("no panels found for this layer")
            };

            // if layer_panels.orientation.is_horizontal() != iroute.edge.orientation.is_horizontal() {
            //     log::warn!("orientation mismatch");
            //     continue;
            // }

            if let Some(panel_index) = layer_panels.find_panel_by_track_offset(iroute.edge.offset) {
                layer_panels.panels[panel_index].iroutes.push(*iroute);
            } else {
                // Iroute lies outside of the specified tracks.
                ignored_iroutes.push(iroute);
            }
        }

        // Print a warning if there where any iroutes outside of the specified track region.
        if !ignored_iroutes.is_empty() {
            log::warn!("ignored {} out-of-bounds iroutes (see debug log for details). Check if the routing tracks cover the whole circuit!", ignored_iroutes.len());

            for iroute in ignored_iroutes {
                log::debug!("ignore out-of-bounds iroute: {:?}", iroute);
            }
        }

        // Do track-assignment per panel.
        let assigned_panels = {
            // Flatten the hash map with the panels.
            let mut all_panels: Vec<_> = panels
                .into_iter()
                .flat_map(|(_layer, panels)| panels.panels.into_iter().map(|panel| panel))
                .collect();

            // Do assignment for each panel independently.
            log::info!("Do initial track-assignment per panel.");
            all_panels.par_iter_mut().for_each(|panel| {
                initial_assignment(panel, &initial_net_components);
            });

            log::info!("Do overlap-reduction per panel.");
            all_panels.par_iter_mut().for_each(|panel| {
                overlap_reduction(panel, &initial_net_components);
            });

            all_panels
        };

        // Collect all assigned iroutes.
        log::info!("Collect all assigned iroutes.");
        {
            let assigned_iroutes = assigned_panels
                .iter()
                .flat_map(|panel| panel.iroutes.iter());

            // Map layer and net IDs back to the external IDs.
            let iroutes_with_remapped_ids = assigned_iroutes.map(|iroute| {
                let net_id = net_ids_reverse[iroute.net.0].clone();
                let layer_id = layer_ids_reverse[iroute.layer.0 as usize].clone();

                (net_id, layer_id, iroute.edge)
            });

            let mut iroutes_per_net = HashMap::new();

            for (net_id, layer_id, edge) in iroutes_with_remapped_ids {
                iroutes_per_net
                    .entry(net_id)
                    .or_insert(vec![])
                    .push((layer_id, edge));
            }

            iroutes_per_net
        }
    }

    /// Create `IRoute` structs and create internal IDs for nets and layers.
    fn init_iroutes<IRoutes>(
        &self,
        iroutes: IRoutes,
    ) -> (
        Vec<IRoute<LN::Coord>>,
        FnvHashMap<LN::LayerId, LayerId>,
        Vec<LN::LayerId>,
        FnvHashMap<LN::NetId, NetId>,
        Vec<LN::NetId>,
    )
    where
        IRoutes: Iterator<Item = (LN::NetId, LN::LayerId, db::REdge<LN::Coord>)>,
    {
        // Mapping between IDs used in the data base and IDs used internally for track assignment.
        let mut layer_ids: FnvHashMap<LN::LayerId, LayerId> = Default::default();
        let mut layer_ids_reverse: Vec<LN::LayerId> = vec![];
        let mut net_ids: FnvHashMap<LN::NetId, NetId> = Default::default();
        let mut net_ids_reverse: Vec<LN::NetId> = vec![];

        let mut get_or_create_net_id = |net: &LN::NetId| -> NetId {
            if let Some(id) = net_ids.get(net) {
                *id
            } else {
                let id = NetId(net_ids_reverse.len());
                net_ids_reverse.push(net.clone());
                net_ids.insert(net.clone(), id);
                id
            }
        };

        let mut get_or_create_layer_id = |layer: &LN::LayerId| -> LayerId {
            if let Some(id) = layer_ids.get(layer) {
                *id
            } else {
                assert!(
                    layer_ids_reverse.len() <= 255,
                    "cannot handle more than 255 routing layers"
                );
                let id = LayerId(layer_ids_reverse.len() as u8);
                layer_ids_reverse.push(layer.clone());
                layer_ids.insert(layer.clone(), id);
                id
            }
        };

        // Create iroutes
        let iroutes: Vec<_> = iroutes
            .enumerate()
            .map(|(i, (net, layer, redge))| IRoute {
                edge: redge,
                net: get_or_create_net_id(&net),
                id: IRouteId(i),
                layer: get_or_create_layer_id(&layer),
            })
            .collect();

        (
            iroutes,
            layer_ids,
            layer_ids_reverse,
            net_ids,
            net_ids_reverse,
        )
    }
    /// Split the tracks into panels.
    ///
    /// # Parameters
    /// * `tracks`: Routing track definitions.
    /// * `used_layers`: Layers used by the routes. Create panels only for the used layers.
    fn create_routing_panels(
        &self,
        tracks: &HashMap<LN::LayerId, Tracks<LN::Coord>>,
        used_layers: &[LN::LayerId],
    ) -> HashMap<LN::LayerId, Panels<LN::Coord>> {
        // Split tracks on each layer into routing panels.
        tracks
            .iter()
            .filter(|(layer, _)| used_layers.contains(layer))
            .map(|(layer, ts)| (layer.clone(), self.create_routing_panel(ts)))
            .collect()
    }

    fn create_routing_panel(&self, tracks: &Tracks<LN::Coord>) -> Panels<LN::Coord> {
        let total_num_tracks = tracks.all_offsets_in_region(&tracks.region).count();
        let tracks_per_panel = self.panel_size;

        let first_track = tracks.first_track_offset();

        Panels::new(
            tracks.orientation,
            tracks.pitch,
            //tracks.offset,
            first_track,
            total_num_tracks,
            tracks_per_panel,
        )
    }
}

#[test]
fn test_track_assignment() {
    use steiner_tree;

    let steiner_lut = steiner_tree::gen_lut::gen_full_lut(7);

    let points = vec![(0, 0), (100, 50), (50, 70), (10, 90)];

    let (tree, cost) =
        steiner_lut.rsmt_low_degree(points.iter().copied().map(|p| p.into()).collect());

    /// Convert a tree from the `steiner_tree` crate into a set of IRoutes.
    fn tree_to_iroutes<Crd: PrimInt>(
        tree: &[(steiner_tree::Point<Crd>, steiner_tree::Point<Crd>)],
    ) -> Vec<IRoute<Crd>> {
        tree.into_iter()
            .map(|(p1, p2)| {
                let edge = db::REdge::try_from_points((p1.x, p1.y), (p2.x, p2.y))
                    .expect("tree segments must be rectilinear");

                let layer_id = if edge.is_horizontal() {
                    LayerId(0)
                } else {
                    LayerId(1)
                };

                IRoute {
                    edge,
                    net: NetId(0),
                    id: IRouteId(0),
                    layer: layer_id,
                }
            })
            .collect()
    }

    let iroutes = tree_to_iroutes(&tree);
}
