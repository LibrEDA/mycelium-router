// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Store a set of bounded intervals with associated values.

use itertools::Itertools;
use num_traits::PrimInt;
use smallvec::SmallVec;

/// A bounded interval with range `[start, end]` and an associated value.
#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct IntervalEntry<K, V> {
    pub start: K,
    pub end: K,
    pub value: V,
}

/// Intervals get splitted while they are put into `IntervalMap::interval_fragments`. Each part
/// is represented by a 'fragment'.
struct IntervalFragment<K> {
    start: K,
    end: K,
    /// Position+1 of the interval in `IntervalMap::entries`.
    /// A value of 0 means that the entry is not present. This is used to represent an empty region.
    id: usize,
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
struct IntervalStart<K> {
    start: K,
    id: usize,
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
struct IntervalEnd<K> {
    end: K,
    id: usize,
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq, PartialOrd, Ord)]
struct Event<K> {
    position: K,
    is_end: bool,
    interval_id: usize,
    /// Number of intervals on the right of this event.
    count: usize,
}

/// A set of intervals.
pub type IntervalSet<K> = IntervalMap<K, ()>;

/// Store a set of bounded intervals with associated values.
///
/// Naive implementation.
///
#[derive(Debug, Clone, Default)]
pub struct IntervalMap<K, V> {
    /// Unordered intervals. Used to store their associated value and unsplitted bounds.
    entries: Vec<Option<IntervalEntry<K, V>>>,
    /// Sorted list of events. The intervals are stored as sorted 'start' and 'end' events.
    events: Vec<Event<K>>,
}

impl<K, V> IntervalMap<K, V>
where
    K: Ord + Eq + Copy,
    V: Eq,
{
    /// Remove at most one interval which maches the given associated value.
    /// TODO: more efficient implementation
    pub fn remove_by_value(&mut self, value: &V) -> Option<V> {
        // Find entry with the given value.
        let idx = self
            .entries
            .iter()
            .find_position(|e| e.as_ref().map(|e| &e.value) == Some(value));

        if let Some((idx, _)) = idx {
            // Remove events.
            self.events.retain(|e| e.interval_id == idx);

            // Remove entry.
            self.entries[idx].take().map(|e| e.value)
        } else {
            None
        }
    }
}

impl<K, V> IntervalMap<K, V> {
    /// Create an empty set of intervals.
    pub fn new() -> Self {
        Self {
            entries: vec![],
            events: vec![],
        }
    }
}

impl<K, V> IntervalMap<K, V>
where
    K: Ord + Eq + Copy,
{
    /// Find a free storage location for an entry.
    /// Return None if the new entry has to be appended to the vector.
    fn find_free_id(&self) -> Option<usize> {
        self.entries
            .iter()
            .find_position(|e| e.is_none())
            .map(|(pos, _)| pos)
    }

    pub fn insert(&mut self, key: (K, K), value: V) {
        let (start, end) = key;
        assert!(start <= end, "bounds of interval must be ordered");
        let entry = IntervalEntry { start, end, value };

        // Insert the entry.
        let id = self.find_free_id();
        let id = if let Some(id) = id {
            // Reuse free space.
            debug_assert!(self.entries[id].is_none());
            self.entries[id] = Some(entry);
            id
        } else {
            // Append.
            self.entries.push(Some(entry));
            self.entries.len()
        };

        let mut start_event = Event {
            interval_id: id,
            position: start,
            is_end: false,
            count: 1,
        };

        let mut end_event = Event {
            interval_id: id,
            position: end,
            is_end: true,
            count: 0,
        };

        let start_index = self.find_start_event_position(start);
        let end_index = self.find_end_event_position(end);

        debug_assert!(end_index >= start_index);

        // Set overlap counts.
        if start_index > 0 {
            start_event.count = self.events[start_index - 1].count + 1;
        }

        if end_index > 0 {
            end_event.count = self.events[end_index - 1].count;
        }

        // Update overlap counts of events between the new start and end events.
        self.events[start_index..end_index]
            .iter_mut()
            .for_each(|e| e.count += 1);

        self.events.insert(start_index, start_event);
        self.events.insert(end_index + 1, end_event);
    }

    /// Get the number of intervals which contain `position`.
    pub fn num_intervals_at(&self, position: K) -> usize {
        // Naive search. TODO: Use bisection.
        self.events
            .iter()
            .take_while(|e| e.position <= position)
            .last()
            .map(|e| e.count)
            .unwrap_or(0)
    }

    /// Get the overlap counts on the given interval.
    pub fn overlap_count(
        &self,
        (start, end): (K, K),
    ) -> impl Iterator<Item = ((K, K), usize)> + '_ {
        assert!(start <= end);

        let bisect_result = self.events.binary_search_by_key(&start, |e| e.position);

        let pos = match bisect_result {
            Ok(i) => i,
            Err(i) => i,
        };
        let pos = pos.max(1) - 1;

        self.events[pos..self.events.len()]
            .windows(2)
            .take_while(move |w| w[0].position <= end)
            .map(move |w| {
                let actual_start = w[0].position.max(start);
                let actual_end = w[1].position.min(end);
                let count = w[0].count;
                ((actual_start, actual_end), count)
            })
            .filter(|((a, b), count)| a != b)
    }

    pub fn intervals_at(&self, position: K) -> impl Iterator<Item = &IntervalEntry<K, V>> {
        // TODO: Efficient implementation.
        self.entries.iter().filter_map(move |e| {
            if let Some(e) = e {
                if e.start <= position && e.end <= position {
                    Some(e)
                } else {
                    None
                }
            } else {
                None
            }
        })
    }

    /// Find the index of the right-most fragment which starts on the left from `start`.
    fn find_start_event_position(&self, start: K) -> usize {
        let bisect_result = self
            .events
            .binary_search_by_key(&(start, false), |e| (e.position, e.is_end));

        match bisect_result {
            Ok(i) => i,
            Err(i) => i,
        }
    }

    fn find_end_event_position(&self, end: K) -> usize {
        let bisect_result = self
            .events
            .binary_search_by_key(&(end, true), |e| (e.position, e.is_end));

        match bisect_result {
            Ok(i) => i,
            Err(i) => i,
        }
    }

    /// Get the number of intervals.
    pub fn len(&self) -> usize {
        self.entries.len()
    }
}

#[test]
fn test_interval_insert() {
    let mut intervals = IntervalMap::new();
    assert_eq!(intervals.num_intervals_at(0), 0);

    intervals.insert((0, 10), ());
    dbg!(&intervals.events);

    assert_eq!(intervals.num_intervals_at(0), 1);
    assert_eq!(intervals.num_intervals_at(5), 1);
    assert_eq!(intervals.num_intervals_at(9), 1);

    intervals.insert((20, 30), ());
    dbg!(&intervals.events);

    assert_eq!(intervals.num_intervals_at(20), 1);
    assert_eq!(intervals.num_intervals_at(25), 1);

    intervals.insert((0, 100), ());
    assert_eq!(intervals.num_intervals_at(0), 2);
    assert_eq!(intervals.num_intervals_at(15), 1);
    assert_eq!(intervals.num_intervals_at(25), 2);
    assert_eq!(intervals.num_intervals_at(50), 1);
    assert_eq!(intervals.num_intervals_at(99), 1);
}

#[test]
fn test_interval_insert_overlapping() {
    let mut intervals = IntervalMap::new();

    intervals.insert((10, 30), ());
    intervals.insert((0, 20), ());

    assert_eq!(intervals.num_intervals_at(0), 1);
    assert_eq!(intervals.num_intervals_at(10), 2);
    assert_eq!(intervals.num_intervals_at(21), 1);
    assert_eq!(intervals.num_intervals_at(31), 0);
}

#[test]
fn test_interval_overlap_counts() {
    let mut intervals = IntervalMap::new();

    intervals.insert((10, 30), ());
    intervals.insert((0, 20), ());

    let overlap_counts: Vec<_> = intervals.overlap_count((0, 0)).collect();
    assert_eq!(overlap_counts, vec![]);

    let overlap_counts: Vec<_> = intervals.overlap_count((-2, -1)).collect();
    assert_eq!(overlap_counts, vec![]);

    let overlap_counts: Vec<_> = intervals.overlap_count((-1, 1)).collect();
    assert_eq!(overlap_counts, vec![((0, 1), 1)]);

    let overlap_counts: Vec<_> = intervals.overlap_count((1, 2)).collect();
    assert_eq!(overlap_counts, vec![((1, 2), 1)]);

    let overlap_counts: Vec<_> = intervals.overlap_count((1, 40)).collect();
    assert_eq!(
        overlap_counts,
        vec![((1, 10), 1), ((10, 20), 2), ((20, 30), 1)]
    );
}
