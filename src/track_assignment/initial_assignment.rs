// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Greedy track assignment used for the initial assignment.

use super::*;
use cost_functions::{blockage_cost, overlap_cost, wirelength_cost};
use iron_shapes::isotropy::Orientation2D;
use types::*;

/// Assign iroutes to tracks in the panel.
/// The tracks of the panel get populated accordingly.
///
/// Updates the tracks in the panel with the initial assignment.
/// Returns the iroutes with updated positions. Their ordering will be sorted by the lengths.
///
/// # Parameters
/// * `panel`: The set of tracks which can be used for assignment.
/// * `initial_net_components`: Net components indexed by net ID.
/// * `iroutes`: The set of iroutes which should be assigned. They must all be oriented along the tracks.
pub(super) fn initial_assignment<Crd: PrimInt + FromPrimitive>(
    panel: &mut Panel<Crd>,
    initial_net_components: &Vec<Vec<NetComponent<Crd>>>,
) {
    // Take the set of iroutes from the panel. Will put it back at the end.
    let mut iroutes = panel.take_iroutes();

    debug_assert!(
        iroutes
            .iter()
            .all(|i| i.edge.orientation.is_horizontal() == panel.orientation.is_horizontal()),
        "iroutes must have the same orientation as the tracks in the panel"
    );

    assert!(panel.tracks.len() > 0, "need more than zero tracks");

    // Weights for computing the assignment cost as a weighted sum.
    let wirelength_weight = Crd::from_usize(10).unwrap(); // The paper says '1'. The factor 10 effectively creates the same ratios as in the paper but allows integer arithmetic.
    let overlap_weight = Crd::from_usize(1).unwrap(); // The paper says 0.1.
    let blockage_weight = Crd::from_usize(1000).unwrap(); // The paper just says 'very large number'.

    // Sort iroutes by decreasing length.
    iroutes.sort_by_key(|iroute| Crd::zero() - iroute.length());

    // Hold temporary net components.
    let mut net_component_buffer = vec![];

    // Sequentially find optimal track for iroutes.
    for iroute in &mut iroutes {
        // Start with an empty temporary buffer.
        net_component_buffer.clear();

        // Get all other net components of the same net as the 'iroute'.
        // They are used to compute the wirelength cost.
        {
            let net = iroute.net;
            let initial_components = &initial_net_components[net.0];

            let other_iroutes = initial_components
                .iter()
                .copied()
                // Take only net components different from the current iroute.
                .filter(|component| match component {
                    NetComponent::IRoute(r) => r.id != iroute.id,
                    _ => true,
                });

            // Store the components in the temporary buffer.
            net_component_buffer.extend(other_iroutes);
        }

        // Find minimal-cost track for this iroute.
        let (best_track_idx, track_offset, _cost) = panel
            .tracks
            .iter()
            .enumerate()
            .map(|(track_index, track)| {
                let track_offset =
                    panel.offset + panel.pitch * Crd::from_usize(track_index).unwrap();

                // Tentatively shift the iroute to the current track.
                let mut modified_iroute = *iroute;
                modified_iroute.edge.offset = track_offset;

                net_component_buffer.push(NetComponent::IRoute(modified_iroute));
                let wl_cost = wirelength_cost(&net_component_buffer);
                net_component_buffer.pop();

                let overlap_cost = overlap_cost(iroute.interval(), &track.routes);

                let blockage_cost = Crd::zero(); // TODO

                // Compute weighted sum of costs.
                let cost: Crd = wirelength_weight * wl_cost
                    + overlap_weight * overlap_cost
                    + blockage_weight * blockage_cost;

                (track_index, track_offset, cost)
            })
            .min_by_key(|(_, _, cost)| *cost)
            .expect("no tracks in panel");

        // Insert the iroute on this track.
        {
            let best_track = &mut panel.tracks[best_track_idx];
            best_track.routes.insert(iroute.interval(), iroute.id);
        }

        // Update the iroute.
        {
            iroute.edge.offset = track_offset;
        }
    }

    // Store iroutes in the panel (where they were taken from before).
    panel.set_iroutes(iroutes);
}

#[test]
fn test_initial_assignment_single_iroute() {
    let tracks = (0..10).map(|_| Track::default()).collect();

    let iroute1 = IRoute {
        edge: db::REdge::new((100, 1500), (800, 1500)),
        net: NetId(0),
        id: IRouteId(0),
        layer: LayerId(0),
    };

    let initial_components = vec![
        // Net 0
        vec![
            NetComponent::Pin((0, 1200).into()),
            NetComponent::IRoute(iroute1),
        ],
    ];

    let iroutes = vec![iroute1];

    let mut panel = Panel {
        orientation: db::Orientation2D::Horizontal,
        offset: 1000i32,
        tracks,
        pitch: 100,
        iroutes,
    };

    initial_assignment(&mut panel, &initial_components);
    let assigned_iroutes = &panel.iroutes;

    assert_eq!(assigned_iroutes.len(), 1);

    // Iroute should be placed close to the pin at (0, 1200).
    assert_eq!(
        assigned_iroutes[0].edge,
        db::REdge::new((100, 1200), (800, 1200))
    );

    assert_eq!(panel.tracks[2].routes.len(), 1);
}

#[test]
fn test_initial_assignment_conflicting_iroutes() {
    // Do initial assignment of two iroutes which would want to use the same track.
    // The longer route gets routed first and should get the track it wants.
    // The shorter route should get another track.

    let tracks = (0..10).map(|_| Track::default()).collect();

    let iroute0 = IRoute {
        edge: db::REdge::new((100, 1500), (2000, 1500)),
        net: NetId(0),
        id: IRouteId(0),
        layer: LayerId(0),
    };

    // Iroute1 is shorter than iroute0 and hence will be assigned after iroute0.
    let iroute1 = IRoute {
        edge: db::REdge::new((120, 1500), (2000, 1500)),
        net: NetId(1),
        id: IRouteId(1),
        layer: LayerId(0),
    };

    let initial_components = vec![
        // Net 0
        vec![
            NetComponent::Pin((0, 1200).into()),
            NetComponent::IRoute(iroute0),
        ],
        // Net 1
        vec![
            NetComponent::Pin((100, 1200 - 1).into()),
            NetComponent::IRoute(iroute1),
        ],
    ];

    let iroutes = vec![iroute0, iroute1];

    let mut panel = Panel {
        orientation: db::Orientation2D::Horizontal,
        offset: 1000i32,
        tracks,
        pitch: 100,
        iroutes,
    };

    initial_assignment(&mut panel, &initial_components);

    let assigned_iroutes = &panel.iroutes;

    assert_eq!(assigned_iroutes.len(), 2);

    // Iroute should be placed close to the pin at (0, 1200).
    assert_eq!(
        assigned_iroutes[0].edge,
        db::REdge::new((100, 1200), (2000, 1200))
    );
    // Iroute should be placed close to the pin at (0, 1200).
    assert_eq!(
        assigned_iroutes[1].edge,
        db::REdge::new((120, 1100), (2000, 1100))
    );

    assert_eq!(panel.tracks[0].routes.len(), 0);
    assert_eq!(panel.tracks[1].routes.len(), 1);
    assert_eq!(panel.tracks[2].routes.len(), 1);
    assert_eq!(panel.tracks[3].routes.len(), 0);
}
