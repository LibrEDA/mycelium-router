// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Cost functions used for track assignment.

use iron_shapes::concepts::PointConcept;
use petgraph::data::Element;

use super::*;
use interval_map::IntervalMap;
use types::*;

/// Compute the wirelength cost of a set of iroutes and pins.
pub(super) fn wirelength_cost<Crd: PrimInt>(components: &[NetComponent<Crd>]) -> Crd {
    use petgraph as pg;

    // Create a full-mesh distance-graph of the components.
    // TODO: Store the graph in a 'net' data structure such that it does not need to be built on each evaluation of the wirelength cost.
    let graph = {
        let graph_edges = (0..components.len()).into_iter().tuple_combinations();

        let mut graph = pg::Graph::new();
        {
            // Reserve required memory for the full mesh.
            let l = components.len();
            graph.reserve_nodes(l);
            graph.reserve_edges(l * (l.max(1) - 1) / 2);
        }

        // Create graph nodes.
        let nodes: Vec<_> = (0..components.len()).map(|i| graph.add_node(i)).collect();

        // Create a full mesh between net components.
        graph_edges.for_each(|(a_idx, b_idx)| {
            let dist = distance_between_components(&components[a_idx], &components[b_idx]);

            graph.add_edge(nodes[a_idx], nodes[b_idx], dist);
        });

        graph
    };

    let minimum_spanning_tree = pg::algo::min_spanning_tree(&graph);

    // Compute sum of edge weights.
    minimum_spanning_tree
        .into_iter()
        .map(|element| match element {
            Element::Edge { weight, .. } => weight,
            _ => Crd::zero(),
        })
        .fold(Crd::zero(), |w, acc| acc + w)
}

#[test]
fn test_wirelength_cost_empty() {
    let components = vec![];
    assert_eq!(wirelength_cost::<i32>(&components), 0)
}

#[test]
fn test_wirelength_cost_already_connected() {
    // If the net components are already touching each other, there should be no additional wirelength cost.

    let components = vec![
        NetComponent::IRoute(IRoute {
            edge: db::REdge::new((0i32, 0), (10, 0)),
            net: NetId(0),
            id: IRouteId(0),
            layer: LayerId(0),
        }),
        NetComponent::IRoute(IRoute {
            edge: db::REdge::new((0, 0), (0, 10)),
            net: NetId(0),
            id: IRouteId(0),
            layer: LayerId(0),
        }),
    ];

    assert_eq!(wirelength_cost(&components), 0)
}

#[test]
fn test_wirelength_cost_cshape() {
    // Create net components which should be connected in a C shape by a minimum-spanning tree:
    //
    //   ---------------
    //
    //   x
    //   x
    //   ---------------

    let components = vec![
        NetComponent::IRoute(IRoute {
            edge: db::REdge::new((0i32, 0), (10, 0)),
            net: NetId(0),
            id: IRouteId(0),
            layer: LayerId(0),
        }),
        NetComponent::IRoute(IRoute {
            edge: db::REdge::new((0, 10), (10, 10)),
            net: NetId(0),
            id: IRouteId(0),
            layer: LayerId(0),
        }),
        NetComponent::Pin(db::Point::new(0, 5)),
        NetComponent::Pin(db::Point::new(0, 2)),
    ];

    assert_eq!(wirelength_cost(&components), 10)
}

/// Compute the manhattan distance between two net components.
/// A net component is either a point or a rectilinear line segment.
fn distance_between_components<Crd: PrimInt>(a: &NetComponent<Crd>, b: &NetComponent<Crd>) -> Crd {
    match (a, b) {
        // Two points.
        (NetComponent::Pin(a), NetComponent::Pin(b)) => {
            (*a - *b).norm1() // Manhattan distance.
        }

        // A point and a line-segment.
        (NetComponent::Pin(p), NetComponent::IRoute(edge))
        | (NetComponent::IRoute(edge), NetComponent::Pin(p)) => {
            edge.edge.manhattan_distance_to_point(*p)
        }

        // Two line-segments.
        (NetComponent::IRoute(a), NetComponent::IRoute(b)) => {
            if a.edge.edges_intersect(&b.edge).inclusive_bounds() {
                Crd::zero()
            } else {
                let (a1, a2) = (a.edge.start(), a.edge.end());
                let (b1, b2) = (b.edge.start(), b.edge.end());

                a.edge
                    .manhattan_distance_to_point(b1)
                    .min(a.edge.manhattan_distance_to_point(b2))
                    .min(b.edge.manhattan_distance_to_point(a1))
                    .min(b.edge.manhattan_distance_to_point(a2))
            }
        }
    }
}

/// Compute the overlap-cost when inserting a route segment of given span into the track.
pub(super) fn overlap_cost<Crd: PrimInt + FromPrimitive, E>(
    iroute_span: (Crd, Crd),
    track: &IntervalMap<Crd, E>,
) -> Crd {
    debug_assert!(iroute_span.0 <= iroute_span.1);

    track
        .overlap_count(iroute_span)
        .map(|((start, end), count)| {
            let width = end - start;
            Crd::from_usize(count).unwrap() * width
        })
        .fold(Crd::zero(), |a, b| a + b)
}

#[test]
fn test_overlap_cost_empty() {
    let intervals: IntervalMap<_, ()> = IntervalMap::new();
    assert_eq!(overlap_cost((-100, 100), &intervals), 0);
}

#[test]
fn test_overlap_cost_one_overlap() {
    let mut intervals: IntervalMap<_, ()> = IntervalMap::new();
    intervals.insert((0, 7), ());
    assert_eq!(overlap_cost((-100, 100), &intervals), 7);
}

pub(super) fn blockage_cost() {
    todo!()
}
