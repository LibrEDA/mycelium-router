// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Data structures used for track assignment algorithm.

use super::interval_map::IntervalMap;
use itertools::assert_equal;
use libreda_pnr::db;
use num_traits::{FromPrimitive, NumCast, PrimInt, ToPrimitive};

/// Identifier for a net.
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub(super) struct NetId(pub(super) usize);

/// Unique ID of an IRoute.
#[derive(Debug, Default, Copy, Clone, Hash, Eq, PartialEq)]
pub(super) struct IRouteId(pub(super) usize);

/// Identifier for a metal layer. The lowest layer has ID 0.
/// Used to keep track of IRoutes even when their location is shifted.
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub(super) struct LayerId(pub(super) u8);

/// A segment of a global route.
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub(super) struct IRoute<Crd> {
    pub(super) edge: db::REdge<Crd>,
    pub(super) net: NetId,
    pub(super) id: IRouteId,
    pub(super) layer: LayerId,
}

/// Rectangular routing blockage.
/// Used to pass blockage information to the track-assignment algorithm.
pub(super) struct Blockage<Crd> {
    /// Shape of the blockage.
    pub(super) shape: db::Rect<Crd>,
    /// Layer of the blockage.
    pub(super) layer: LayerId,
}

impl<Crd: PrimInt> IRoute<Crd> {
    /// Compute the length of the segment.
    pub fn length(&self) -> Crd {
        self.edge.length()
    }

    /// Get start and end coordinate when projecting on the parallel axis.
    /// Return the spanned interval along the track direction.
    pub fn interval(&self) -> (Crd, Crd) {
        let (a, b) = (self.edge.start, self.edge.end);
        debug_assert!(a <= b);
        (a, b)
    }
}

/// Part of the geometrical representation of net.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub(super) enum NetComponent<Crd> {
    /// Segment of a routing tree. Derived from global routes.
    IRoute(IRoute<Crd>),
    /// End-point of a net.
    Pin(db::Point<Crd>),
}

// Not used. TODO: Delete
// /// Compressed representation of net components for many nets.
// /// Allocates only two memory blocks instead of `n` for `n` nets.
// #[derive(Default, Clone)]
// pub(super) struct Nets<Crd> {
//     /// Contains start indices of blocks of net components for all the nets.
//     net_components: Vec<NetComponent<Crd>>,
//     /// Contains components of all nets. They are grouped together. `net_components` contains
//     /// the indices to the begin of each group.
//     net_components_limits: Vec<usize>,
// }
//
// impl<Crd> Nets<Crd> {
//     /// Reserve memory for a given number of nets.
//     pub fn reserve_nets(&mut self, num_nets: usize) {
//         self.net_components_limits.reserve(num_nets + 1);
//     }
//
//     /// Reserve memory for a given number of net components.
//     pub fn reserve_components(&mut self, num_components: usize) {
//         self.net_components.reserve(num_components);
//     }
//
//     /// Add a net with its components (iroutes, pins).
//     pub fn add_net<I: Iterator<Item=NetComponent<Crd>>>(&mut self, components: I) -> NetId {
//         if self.net_components_limits.is_empty() {
//             self.net_components_limits.push(0)
//         }
//         let net_id = NetId(self.net_components_limits.len() - 1);
//         self.net_components.extend(components);
//         self.net_components_limits.push(self.net_components.len());
//
//         net_id
//     }
//
//     /// Get the components (iroutes, pins) of a net.
//     pub fn get_net_components(&self, net_id: NetId) -> &[NetComponent<Crd>] {
//         let start = self.net_components_limits[net_id.0];
//         let end = self.net_components_limits[net_id.0 + 1];
//
//         &self.net_components[start..end]
//     }
// }

/// A horizontal or vertical line through the chip which may contain routes.
#[derive(Debug, Default, Clone)]
pub(super) struct Track<Crd> {
    pub(super) routes: IntervalMap<Crd, IRouteId>,
}

impl<Crd> Track<Crd> {
    /// Create an empty track.
    pub fn new() -> Self {
        Self {
            routes: IntervalMap::new(),
        }
    }
}

/// A bundle of routing tracks.
pub(super) struct Panel<Crd> {
    /// Orientation of the routing tracks in this panel.
    pub(super) orientation: db::Orientation2D,
    /// Distance from `(0, 0)` to the line along the first track.
    pub(super) offset: Crd,
    pub(super) tracks: Vec<Track<Crd>>,
    /// Spacing between tracks.
    pub(super) pitch: Crd,

    /// Iroutes associated with this panel.
    pub(super) iroutes: Vec<IRoute<Crd>>,
}

impl<Crd> Panel<Crd>
where
    Crd: PrimInt + FromPrimitive,
{
    /// Create a new panel.
    pub fn new(
        first_track_offset: Crd,
        pitch: Crd,
        num_tracks: usize,
        orientation: db::Orientation2D,
    ) -> Self {
        Self {
            orientation,
            offset: first_track_offset,
            tracks: (0..num_tracks).map(|_| Track::new()).collect(),
            pitch,
            iroutes: vec![],
        }
    }

    /// Set the iroutes associated with this panel.
    pub fn set_iroutes(&mut self, iroutes: Vec<IRoute<Crd>>) {
        debug_assert!(
            {
                let (start, end) = self.covered_interval();
                iroutes
                    .iter()
                    .all(|r| r.edge.offset >= start && r.edge.offset < end)
            },
            "iroute does not belong to this panel"
        );
        self.iroutes = iroutes;
    }

    /// Take the vector of associated iroutes away from this panel.
    pub fn take_iroutes(&mut self) -> Vec<IRoute<Crd>> {
        std::mem::replace(&mut self.iroutes, vec![])
    }

    /// Get the interval of covered track offsets.
    /// Return a tuple `(offset of first track, offset of last_track+1)`.
    pub fn covered_interval(&self) -> (Crd, Crd) {
        let num_tracks = self.tracks.len();
        let end_exclusive = self.offset + self.pitch * Crd::from_usize(num_tracks).unwrap();

        (self.offset, end_exclusive)
    }
}

/// Panels of routing tracks covering the full routing region.
pub(super) struct Panels<Crd> {
    /// The size of each of the panels.
    pub(super) panel_height: Crd,
    /// Orientation of the routing tracks in this panels.
    pub(super) orientation: db::Orientation2D,
    pub(super) panels: Vec<Panel<Crd>>,
}

impl<Crd> Panels<Crd>
where
    Crd: PrimInt + FromPrimitive + ToPrimitive,
{
    pub fn new(
        orientation: db::Orientation2D,
        track_pitch: Crd,
        track_start: Crd,
        total_num_tracks: usize,
        tracks_per_panel: usize,
    ) -> Self {
        assert!(track_pitch > Crd::zero());
        assert!(tracks_per_panel > 0);

        let num_panels = (total_num_tracks + tracks_per_panel - 1) / tracks_per_panel;

        // Height of each panel except the last.
        let default_panel_height = track_pitch * Crd::from_usize(tracks_per_panel).unwrap();

        let panels = (0..num_panels)
            .map(|i| {
                let num_tracks_below = i * tracks_per_panel;
                assert!(num_tracks_below <= total_num_tracks);

                let num_tracks = (total_num_tracks - num_tracks_below).min(tracks_per_panel);
                assert!(num_tracks > 0, "panel should have at least one track");

                if i + 1 != num_panels {
                    assert_eq!(
                        num_tracks, tracks_per_panel,
                        "only the last panel can have less tracks than the others"
                    );
                }
                let panel_height = track_pitch * Crd::from_usize(num_tracks).unwrap();
                let start = Crd::from_usize(num_tracks_below).unwrap() * track_pitch + track_start;

                Panel::new(start, track_pitch, num_tracks, orientation)
            })
            .collect();

        Self {
            panel_height: default_panel_height,
            orientation,
            panels,
        }
    }

    /// Get the index of a panel which contains the given track offset.
    /// Returns `None` if there's no panel containing the given track.
    /// Used to assign iroutes to the correct panel.
    pub fn find_panel_by_track_offset(&self, track_offset: Crd) -> Option<usize> {
        if self.panels.is_empty() {
            return None;
        }

        let start = self.panels[0].offset;
        let (_, end) = self.panels[self.panels.len() - 1].covered_interval();

        if track_offset < start || track_offset >= end {
            // Offset is out of bounds.
            return None;
        }

        let panel_index = ((track_offset - start) / self.panel_height)
            .to_usize()
            .unwrap();

        assert!(panel_index < self.panels.len());

        Some(panel_index)
    }
}

/// Stack of routing tracks.
pub(super) struct RoutingStack<Crd> {
    pub(super) layers: Vec<Panels<Crd>>,
}
