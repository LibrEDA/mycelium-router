// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Iterative overlap reduction algorithm.

use itertools::Itertools;

use super::*;
use crate::track_assignment::interval_map::IntervalMap;
use cost_functions::{blockage_cost, overlap_cost, wirelength_cost};
use num_traits::ToPrimitive;
use types::*;

/// Iteratively reduce the overlaps of iroutes by re-assigning them to other tracks.
///
/// # Parameters
/// * `panel`: Panel with the routing tracks which can be used for assignment. The tracks will be updated to hold the moved iroutes.
/// * `initial_net_components`: Net components indexed by net ID.
/// * `iroutes`: The set of iroutes which should be assigned. They must all be oriented along the tracks.
///
/// Returns the iroutes with updated locations.
///
/// # Algorithm
///
/// 0) Compute iroute costs
/// 1) Loop through
///     2) find maximum-cost iroute `r_max`
///     3) rip-up `r_max`
///     4) find mininum-cost track `t_min` to put `r_max`
///     5) assign `r_max` to `t_min`
///     6) update the costs (history cost) for t_min
///     7) terminate if some condition is met (either a number of iterations is exceeded or
///         the relative improvement of the total assignment cost is less than 5%)
///
pub(super) fn overlap_reduction<Crd: PrimInt + FromPrimitive + ToPrimitive>(
    panel: &mut Panel<Crd>,
    initial_net_components: &Vec<Vec<NetComponent<Crd>>>, // Other net components. Used to estimate the wire lengths of a net.
) {
    // Weights for computing the assignment cost as a weighted sum.
    let wirelength_weight = Crd::from_usize(1).unwrap(); // The paper says '0.1'.
    let mut overlap_weight = Crd::from_usize(1).unwrap(); // The paper says 0.1 in the beginning, then increases by 0.1 after every `num_iroutes/k` iteration.
    let blockage_weight = Crd::from_usize(1000).unwrap(); // The paper just says 'very large number'.
    let history_cost_weight = Crd::from_usize(10).unwrap(); // The paper says 1.

    // After re-assigning an iroute, don't touch it for this amount of iterations.
    let freeze_for_num_iterations = 20;

    // Get mutable access of the set of iroutes. (Will be put back at the end of the function).
    let mut iroutes = panel.take_iroutes();

    let num_iroutes = iroutes.len();
    let max_iterations = num_iroutes * 2;

    let overlap_weight_update_frequency = (num_iroutes / 10).max(1);

    // Sanity check.
    debug_assert!(
        iroutes
            .iter()
            .all(|i| i.edge.orientation.is_horizontal() == panel.orientation.is_horizontal()),
        "iroutes must have the same orientation as the tracks in the panel"
    );

    if iroutes.is_empty() {
        // Nothing to do.
        panel.set_iroutes(iroutes);
        return;
    }

    let num_tracks = panel.tracks.len();
    assert!(num_tracks > 0, "need more than zero tracks");

    // Find the track index based on the location of an iroute.
    let get_track_index = {
        let panel_offset = panel.offset;
        let panel_pitch = panel.pitch;

        move |ir: &IRoute<Crd>| -> usize {
            let iroute_offset = ir.edge.offset;
            ((iroute_offset - panel_offset) / panel_pitch)
                .to_usize()
                .unwrap() // TODO: Store the track index in the iroute data.
        }
    };

    // Sanity check.
    debug_assert!(
        iroutes.iter().all(|i| {
            let track_idx = get_track_index(i);
            track_idx < panel.tracks.len()
        }),
        "iroutes must already be assigned to tracks in this panel"
    );

    // Storage for overlap history.
    // Used to compute history costs.
    let mut overlap_history: Vec<OverlapHistory<Crd>> = Vec::new();
    overlap_history.resize_with(num_tracks, || OverlapHistory::new());

    // 0) Compute initial costs.
    let mut iroute_costs: Vec<_> = iroutes
        .iter()
        .enumerate()
        .map(|(iroute_idx, iroute)| {
            // Compute the index of the track based on the iroute position.
            let track_idx = get_track_index(iroute);
            let track = &panel.tracks[track_idx];

            overlap_cost(iroute.interval(), &track.routes)
                + overlap_history[track_idx].history_cost(iroute.interval())
        })
        .collect();

    // Counter values associated with iroutes.
    // If this value is larger than zero, the iroute is not touched in the iteration.
    let mut iroute_frozen = vec![0u8; iroutes.len()];

    // 1)
    assert!(!iroutes.is_empty());

    // Hold temporary net components.
    let mut net_component_buffer = vec![];

    let mut iteration_count = 0;
    loop {
        // 7) Termination condition 1
        if iteration_count > max_iterations {
            break;
        }

        // 2) Find maximum-cost iroute.
        let (r_max_idx, _max_cost) = iroute_costs
            .iter()
            .enumerate()
            .filter(|(i, _cost)| iroute_frozen[*i] == 0)
            .max_by_key(|(_i, cost)| *cost)
            .expect("no non-frozen iroute found");

        // 3) Rip-up r_max.
        {
            // Find the current track of the iroute.
            let r_max = &iroutes[r_max_idx];
            let track = &mut panel.tracks[get_track_index(r_max)];

            // Remove the iroute from the track.
            track.routes.remove_by_value(&r_max.id);
        }

        // Prepare the net-component buffer with the other components of this net. This is needed for wirelength computation.
        {
            // Start with an empty temporary buffer.
            net_component_buffer.clear();

            // Get all other net components of the same net as the 'iroute'.
            // They are used to compute the wirelength cost.
            {
                let iroute = &iroutes[r_max_idx];
                let net = iroute.net;
                let initial_components = &initial_net_components[net.0];

                let other_iroutes = initial_components
                    .iter()
                    .copied()
                    // Take only net components different from the current iroute.
                    .filter(|component| match component {
                        NetComponent::IRoute(r) => r.id != iroute.id,
                        _ => true,
                    });

                // Store the components in the temporary buffer.
                net_component_buffer.extend(other_iroutes);
            }
        }

        // 4) Find minimum-cost track for iroute r_max.
        let (t_min_idx, t_min_offset): (usize, Crd) = {
            let iroute = &iroutes[r_max_idx];

            // Find minimal-cost track for this iroute.
            let (best_track_idx, track_offset, _cost) = panel
                .tracks
                .iter()
                .enumerate()
                .map(|(track_index, track)| {
                    // Compute track cost.
                    let track_offset =
                        panel.offset + panel.pitch * Crd::from_usize(track_index).unwrap();

                    // Tentatively shift the iroute to the current track.
                    let mut modified_iroute = *iroute;
                    modified_iroute.edge.offset = track_offset;

                    net_component_buffer.push(NetComponent::IRoute(modified_iroute));
                    let wl_cost = wirelength_cost(&net_component_buffer);
                    net_component_buffer.pop();

                    let overlap_cost = overlap_cost(iroute.interval(), &track.routes);

                    let blockage_cost = Crd::zero(); // TODO

                    let history_cost = overlap_history[track_index].history_cost(iroute.interval());

                    // Compute weighted sum of costs.
                    let cost: Crd = wirelength_weight * wl_cost
                        + overlap_weight * overlap_cost
                        + blockage_weight * blockage_cost
                        + history_cost_weight * history_cost;

                    (track_index, track_offset, cost)
                })
                .min_by_key(|(_, _, cost)| *cost)
                .expect("no tracks in panel"); // Should not happen because of assertion at the beginning of this function.

            (best_track_idx, track_offset)
        };

        // 5) Assign the iroute 'r_max' to the track 't_min'.
        {
            let iroute = &mut iroutes[r_max_idx];
            iroute.edge.offset = t_min_offset;
            panel.tracks[t_min_idx]
                .routes
                .insert(iroute.interval(), iroute.id);

            // Don't touch this iroute in the next couple iterations.
            iroute_frozen[r_max_idx] = freeze_for_num_iterations + 1;
        }

        let total_assignment_cost_prev = iroute_costs.iter().fold(Crd::zero(), |a, b| a + *b);

        // 5.1) Update the cost of 'r_max'.
        // Compute the cost of the iroute when it is moved to the minimum-cost track `t_min`.
        {
            let iroute = &iroutes[r_max_idx];

            let new_cost = {
                let track = &panel.tracks[t_min_idx];

                overlap_cost(iroute.interval(), &track.routes)
                    + overlap_history[t_min_idx].history_cost(iroute.interval())
            };

            iroute_costs[r_max_idx] = new_cost;
        }

        let relative_cost_improvement = {
            let total_assignment_cost_next = iroute_costs.iter().fold(Crd::zero(), |a, b| a + *b);
            Crd::from_i32(100).unwrap() * (total_assignment_cost_next - total_assignment_cost_prev)
                / total_assignment_cost_prev
        };

        // 7) Termination condition 2
        if relative_cost_improvement < Crd::from_i32(5).unwrap() {
            // Stop if cost improvement is lower than 5%.
            log::debug!("terminate because of low improvement");
            break;
        }

        // 6) Update history cost of track 't_min'.
        {
            let iroute = &iroutes[r_max_idx];
            // Find all sub-intervals which contain more than one route.
            let overlaps = panel.tracks[t_min_idx]
                .routes
                .overlap_count(iroute.interval())
                .filter(|(_, count)| *count > 1);

            for ((a, b), _) in overlaps {
                overlap_history[t_min_idx].mark_overlap((a, b));
            }
        }

        // Update the freeze-counter.
        {
            // Decrement freeze-counter.
            iroute_frozen
                .iter_mut()
                .for_each(|ctr| *ctr = ctr.saturating_sub(1));

            // Make sure at least one iroute is not frozen.
            let min_value = *iroute_frozen.iter().min().unwrap();
            if min_value > 0 {
                iroute_frozen
                    .iter_mut()
                    .for_each(|ctr| *ctr = ctr.saturating_sub(min_value));
            }
        }

        iteration_count += 1;

        if iteration_count % overlap_weight_update_frequency == 0 {
            // Increase the weight of overlaps.
            overlap_weight = overlap_weight + Crd::from_usize(1).unwrap();
        }
    }

    panel.set_iroutes(iroutes); // Put back the set of iroutes to the panel.
}

/// Store the overlap history of a track.
#[derive(Default, Clone)]
struct OverlapHistory<Crd> {
    history_data: IntervalMap<Crd, ()>,
}

impl<Crd: PrimInt + FromPrimitive> OverlapHistory<Crd> {
    /// Create an empty overlap history.
    fn new() -> Self {
        Self {
            history_data: IntervalMap::new(),
        }
    }

    /// Remember an overlap.
    fn mark_overlap(&mut self, (start, end): (Crd, Crd)) {
        debug_assert!(start <= end);
        self.history_data.insert((start, end), ());
    }

    /// Integrate the overlap history from `start` to `end`.
    fn history_cost(&self, (start, end): (Crd, Crd)) -> Crd {
        debug_assert!(start <= end);
        self.history_data
            .overlap_count((start, end))
            // Compute cost of single intervals with overlaps: 'overlap count' * 'length of interval'
            .map(|((start, end), count)| {
                debug_assert!(start <= end);
                let len = end - start;
                let cost = Crd::from_usize(count).expect("failed to cast from usize") * len; // count should in practice never exceed the range of an i32, likely not even the range of i16.
                cost
            })
            // Compute sum.
            .fold(Crd::zero(), |a, b| a + b)
    }
}

#[test]
fn test_overlap_reduction() {
    let tracks = (0..10).map(|_| Track::default()).collect();

    let iroute0 = IRoute {
        edge: db::REdge::new((100, 1500), (1000, 1500)),
        net: NetId(0),
        id: IRouteId(0),
        layer: LayerId(0),
    };

    // Iroute1 is shorter than iroute0 and hence will be assigned after iroute0.
    let iroute1 = IRoute {
        edge: db::REdge::new((120, 1500), (1000, 1500)),
        net: NetId(1),
        id: IRouteId(1),
        layer: LayerId(0),
    };

    let initial_components = vec![
        // Net 0
        vec![
            NetComponent::Pin((0, 1200).into()),
            NetComponent::IRoute(iroute0),
        ],
        // Net 1
        vec![
            NetComponent::Pin((100, 1200 - 1).into()),
            NetComponent::IRoute(iroute1),
        ],
    ];

    let iroutes = vec![iroute0, iroute1];

    let mut panel = Panel {
        orientation: db::Orientation2D::Horizontal,
        offset: 1000i32,
        tracks,
        pitch: 100,
        iroutes,
    };

    super::initial_assignment::initial_assignment(&mut panel, &initial_components);

    overlap_reduction(&mut panel, &initial_components);

    let optimized_iroutes = panel.take_iroutes();

    // TODO: Better test.
}
