// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Utility functions.

use iron_shapes::prelude::*;
use num_traits::PrimInt;

/// Strip away redundant vertices from a rectilinear point string.
/// Leaves only the corner vertices.
pub fn simplify_point_string<T: PrimInt>(path: &Vec<Point<T>>) -> Vec<Point<T>> {
    // Simplify by removing redundant vertices along straight lines.
    let corners = (1..path.len() - 1).filter_map(|i| {
        let a = path[i - 1];
        let b = path[i];
        let c = path[i + 1];
        let same_x = a.x == b.x && a.x == c.x;
        let same_y = a.y == b.y && a.y == c.y;
        if same_x || same_y {
            // Not a corner.
            None
        } else {
            Some(path[i])
        }
    });

    let mut path_simplified = vec![path[0]];
    path_simplified.extend(corners);
    path_simplified.push(path[path.len() - 1]); // Push the last.

    path_simplified
}
