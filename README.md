<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# mycelium-router

Example router implementation for the LibrEDA framework.

This crate implements signal routers for the LibrEDA framework.
The name comes from [fungi](https://en.wikipedia.org/wiki/Mycelium) and their capability to find routes.
At this time there are two detail router implementations. One is a grid-based maze router in a school-book version.
The other is a grid-less router based on a line-search algorithm which operates directly on the geometry and requires no grid. However, the latter
is even more experimental and non-functional work-in-progress.


## Doc

The main documentation of the crate is inlined with the code and can be opened with `cargo doc --open`.

Use `cargo doc cargo doc --document-private-items --open` to also display documentation of the non-public code.

## Acknowledgements

* From December 2020 to 2021 this project is part of libreda.org and hence funded by [NLnet](https://nlnet.nl) and [NGI0](https://nlnet.nl/NGI0).
* During 2022 the development of the new detail router was funded by [NGI Pointer](https://pointer.ngi.eu/).

