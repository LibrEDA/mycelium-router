# SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import numpy as np
import matplotlib.pyplot as plt
import sys

input_file = sys.argv[1]
output_file = sys.argv[2]

array = np.loadtxt(input_file, dtype=int, delimiter=',')

plt.imshow(array)
plt.savefig(output_file)
