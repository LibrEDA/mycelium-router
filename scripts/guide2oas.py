#!/bin/env python3

# SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Convert global routes in 'guide' files into OASIS layout format.
"""

from klayout import db
import sys

guide_file_path = sys.argv[1]
output_path = sys.argv[2]

print(f"Read guide file: {guide_file_path}")


with open(guide_file_path) as f:
    lines = list(f)

lines = (l.strip() for l in lines)
lines = [l for l in lines if l]

lines = (l for l in lines)

guides = dict()

current_net_name = None
current_guide = list()

for l in lines:
    if current_net_name is None:
        current_net_name = l
    elif l == '(':
        pass
    elif l == ')':
        guides[current_net_name] = current_guide
        current_net_name = None
        current_guide = list()
    else:
        x1, y1, x2, y2, _layer = l.split(" ")
        x1, y1, x2, y2 = (int(x) for x in [x1, y1, x2, y2])
        rect = db.Box(db.Point(x1, y1), db.Point(x2, y2))
        # Ignore layer.
        current_guide.append(rect)

# Draw guides.
layout = db.Layout()
layer = layout.layer(db.LayerInfo(100, 0))
top = layout.create_cell("guides")
for net, guide in guides.items():
    print(f"Draw guide of net '{net}'")
    
    cell = layout.create_cell(f"guides_{net}")

    r = db.Region()
    for rect in guide:
        r.insert(guide)
    r.merge()

    cell.shapes(layer).insert(r)
    top.insert(db.CellInstArray(cell.cell_index(), db.Trans()))


print(f"Save guide layout to '{output_path}'")
layout.write(output_path)

